package pl.adroads.api.model.driver;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DriverCampaignDetailsResource {

    private String campaignId;
    private String campaignName;

    public DriverCampaignDetailsResource() {
        this(null, null);
    }
}
