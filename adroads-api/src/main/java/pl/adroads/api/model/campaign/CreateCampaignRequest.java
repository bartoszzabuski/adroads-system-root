package pl.adroads.api.model.campaign;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreateCampaignRequest {

  private String name;
  private String description;
  private String startDate;
  private String endDate;

  public CreateCampaignRequest() {
    this(null, null, null, null);
  }

}
