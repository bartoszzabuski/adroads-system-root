package pl.adroads.api.model.driver;


import pl.adroads.api.model.shared.PageMetadataResource;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PagedDriversResource {

  private PageMetadataResource metadata;
  private List<DriverResource> drivers;

  public PagedDriversResource(){
    this(null, null);
  }

}
