package pl.adroads.api.model.campaign;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CampaignIndexResource {

    private String id;
    private String name;

    public CampaignIndexResource() {
        this(null, null);
    }

}
