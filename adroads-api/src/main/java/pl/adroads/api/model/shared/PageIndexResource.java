package pl.adroads.api.model.shared;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PageIndexResource {

  private String uuid;
  private Integer pageNumber;
}
