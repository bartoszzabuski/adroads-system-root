package pl.adroads.api.model.shared;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class ExceptionResponse {

  private final String type;
  private final String msg;

  @Deprecated
  public ExceptionResponse(String msg){
    this.msg = msg;
    this.type = null;
  }

}
