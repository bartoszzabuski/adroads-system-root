package pl.adroads.api.model.driver;

import lombok.AllArgsConstructor;
import lombok.Data;
import pl.adroads.api.model.shared.AddressResource;
import pl.adroads.api.model.shared.CarDetailsResource;
import pl.adroads.api.model.shared.DriverDetailsResource;

@AllArgsConstructor
@Data
public class DriverResource {

    private String uuid;
    private String name;
    private String surname;
    private String dateOfBirth;
    private String email;
    private String status;
    private AddressResource address;
    private CarDetailsResource carDetails;
    private DriverDetailsResource driverDetails;
    private DriverCampaignDetailsResource driverCampaignDetailsResource;

    public DriverResource() {
        this(null, null, null, null, null, null, null, null, null, null);
    }

}
