package pl.adroads.api.model.driver;

import lombok.AllArgsConstructor;
import lombok.Data;
import pl.adroads.api.model.shared.AddressResource;
import pl.adroads.api.model.shared.CarDetailsResource;
import pl.adroads.api.model.shared.DriverDetailsResource;

@Data
@AllArgsConstructor
public class CreateDriverRequest {

    protected String name;
    protected String surname;
    protected String dateOfBirth;
    protected String email;
    protected AddressResource address;
    protected CarDetailsResource carDetails;
    protected DriverDetailsResource driverDetails;

    public CreateDriverRequest() {
        this(null, null, null, null, null, null, null);
    }

}
