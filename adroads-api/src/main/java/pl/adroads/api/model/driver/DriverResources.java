package pl.adroads.api.model.driver;

import java.util.List;

public final class DriverResources {

  private final List<DriverResource> driverResources;

  public DriverResources(List<DriverResource> driverResourceList) {
    this.driverResources = driverResourceList;
  }

  public DriverResources() {
    this(null);
  }

}