package pl.adroads.api.model.shared;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PageMetadataResource {

  private Integer itemCount;
  private List<PageIndexResource> pageMetadataList;

}
