package pl.adroads.api.model.driver;

import lombok.AllArgsConstructor;
import lombok.Data;
import pl.adroads.api.model.shared.AddressResource;
import pl.adroads.api.model.shared.CarDetailsResource;
import pl.adroads.api.model.shared.DriverDetailsResource;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Data
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateDriverRequest {

    private String uuid;
    private String name;
    private String surname;
    private String dateOfBirth;
    private String email;
    private AddressResource address;
    private CarDetailsResource carDetails;
    private DriverDetailsResource driverDetails;

    public UpdateDriverRequest() {
        this(null, null, null, null, null, null, null, null);
    }

}
