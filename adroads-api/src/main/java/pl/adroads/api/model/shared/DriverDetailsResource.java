package pl.adroads.api.model.shared;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class DriverDetailsResource {

  private Integer type;
  private Integer monthlyDistanceIndex;

  public DriverDetailsResource(){
    this(null, null);
  }
}
