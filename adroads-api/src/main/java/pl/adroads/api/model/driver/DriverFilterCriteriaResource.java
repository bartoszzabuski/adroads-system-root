package pl.adroads.api.model.driver;


import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.WebApplicationException;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
@ToString
@AllArgsConstructor
public final class DriverFilterCriteriaResource {

  private final String sortDir;
  private final String sortedBy;
  private final String email;
  private final String name;
  private final String surname;
  private final String status;
  private final String street;
  private final String postcode;
  private final String city;
  private final String province;
  private final String carMake;
  private final String carModel;
  private final Integer yearFrom;
  private final Integer yearTo;
  private final List<String> carBody;
  private final String carColour;
  private final Integer mileageFrom;
  private final Integer mileageTo;

  private DriverFilterCriteriaResource() {
    this(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
	 null, null, null);
  }

  //  TODO should this exception be some kind of infrastructure exception?
  public static DriverFilterCriteriaResource fromString(final String jsonString)
      throws WebApplicationException {
    final DriverFilterCriteriaResource resource;
    try {
      resource = new ObjectMapper().readValue(jsonString, DriverFilterCriteriaResource.class);
    } catch (IOException e) {
      log.error("Failed to parse filter criteria: " + e.getMessage());
      throw new WebApplicationException("Failed to parse filtering request.");
    }
    return resource;
  }
}
