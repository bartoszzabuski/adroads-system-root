package pl.adroads.api.model.campaign;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CampaignDriverResource {

    private String driverId;
    private String email;
    private String status;

    private CampaignDriverResource() {
        this(null, null, null);
    }
}
