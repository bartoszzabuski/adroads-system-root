package pl.adroads.api.model.shared;


import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class CarDetailsResource {

  private String make;
  private String model;
  private String year;
  private Integer mileage;
  private String body;
  private String colour;
  private WindscreenDimensionsResource windscreenDimensions;

  public CarDetailsResource(){}
}