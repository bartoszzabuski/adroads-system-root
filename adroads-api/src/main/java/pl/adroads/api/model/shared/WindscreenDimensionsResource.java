package pl.adroads.api.model.shared;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class WindscreenDimensionsResource {

  private Integer height;
  private Integer width;

  public WindscreenDimensionsResource(){
    this(null, null);
  }

}
