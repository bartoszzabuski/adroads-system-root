package pl.adroads.api.model.campaign;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CampaignResource {

  private String uuid;
  private String name;
  private String description;
  private String startDate;
  private String endDate;
  private List<CampaignDriverResource> campaignDriverResources;

  public CampaignResource() {
    this(null, null, null, null, null, null);
  }

}
