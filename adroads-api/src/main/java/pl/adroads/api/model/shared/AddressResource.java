package pl.adroads.api.model.shared;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AddressResource {

  private String postcode;
  private String city;
  private String street;
  private String province;

  public AddressResource(){
    this(null, null, null, null);
  }
}
