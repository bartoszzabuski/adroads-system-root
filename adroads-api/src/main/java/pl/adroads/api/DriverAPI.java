package pl.adroads.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import pl.adroads.api.model.driver.CreateDriverRequest;
import pl.adroads.api.model.driver.UpdateDriverRequest;

@Path("/drivers")
public interface DriverAPI {

    String PATH = "/drivers/";

    @POST
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Produces(MediaType.APPLICATION_JSON)
    Response createNewDriver(CreateDriverRequest createDriverRequest);

    @PUT
    @Path("{driverId}")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Produces(MediaType.APPLICATION_JSON)
    Response updateDriver(@PathParam("driverId") String driverId, UpdateDriverRequest updateDriverRequest);

    // @PUT
    // @Consumes(MediaType.APPLICATION_JSON)
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response approveCampaign(@PathParam("driverId") String driverId,
    // @PathParam("campaignId") String campaignId);
    //
    // @PUT
    // @Path("/{driverId}/potentialCampaign/{campaignId}")
    // @Consumes(MediaType.APPLICATION_JSON)
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response assignPotentialCampaign(@PathParam("driverId") String driverId,
    // @PathParam("campaignId") String campaignId);
    //
    // @DELETE
    // @Path("/{driverId}/potentialCampaign/{campaignId}")
    // @Consumes(MediaType.APPLICATION_JSON)
    // @Produces(MediaType.APPLICATION_JSON)
    // public Response rejectPotentialCampaign(@PathParam("driverId") String driverId,
    // @PathParam("campaignId") String campaignId);
    //
    // @GET
    //// @Path("/")
    // @Produces(MediaType.APPLICATION_JSON)
    // public List<DriverResource> getDriver(@QueryParam("name") String name,
    // @QueryParam("surname") String surname, @QueryParam("email") String email,
    // @QueryParam("direction") Sort.Direction direction,
    // @QueryParam("sortField") List<String> sortField, @QueryParam("page") Integer page,
    // @QueryParam("pageSize") Integer pageSize);

}
