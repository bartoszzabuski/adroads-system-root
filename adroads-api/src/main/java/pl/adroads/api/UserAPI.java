package pl.adroads.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/users")
public interface UserAPI {

    String PATH = "/users/";

    @POST
    @Path("/activate/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response verifyNewlyRegisteredUser(@QueryParam("email") String email,
            @QueryParam("token") String token);

    @POST
    @Path("/login/")
    @Produces("application/json")
    @Consumes("application/x-www-form-urlencoded")
    Response authenticateUser(@FormParam("login") String login,
            @FormParam("password") String password);

}
