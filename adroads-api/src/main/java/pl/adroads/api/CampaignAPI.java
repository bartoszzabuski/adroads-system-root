package pl.adroads.api;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import pl.adroads.api.model.campaign.CreateCampaignRequest;

@Path("/campaigns")
public interface CampaignAPI {

    String PATH = "/campaigns/";

    @POST
    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    Response createCampaign(CreateCampaignRequest createCampaignRequest);

    @PUT
    @Path("{campaignId}/drivers/{driverId}")
    @Produces(APPLICATION_JSON)
    Response addCampaignDriver(@PathParam("campaignId") String campaignId, @PathParam("driverId") String driverId);

    @POST
    @Path("{campaignId}/driverApproval")
    @Produces(APPLICATION_JSON)
    Response driverApprovesCampaign(@PathParam("campaignId") String campaignId, @QueryParam("email") String email,
            @QueryParam("token") String token);

    @POST
    @Path("{campaignId}/driverRejection")
    @Produces(APPLICATION_JSON)
    Response driverRejectsCampaign(@PathParam("campaignId") String campaignId, @QueryParam("email") String email,
            @QueryParam("token") String token);

}
