package pl.adroads.api;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;

import pl.adroads.api.model.campaign.CampaignIndexResource;
import pl.adroads.api.model.campaign.CampaignResource;
import pl.adroads.api.model.driver.DriverFilterCriteriaResource;
import pl.adroads.api.model.driver.DriverResource;
import pl.adroads.api.model.driver.PagedDriversResource;

@Path("/query/")
public interface QueryAPI {

    String QUERY_PATH = "/query/";

    // @GET
    // @Path(QUERY_PATH)
    // @Consumes(MediaType.APPLICATION_JSON)
    // @Produces(MediaType.APPLICATION_JSON)
    // PagedDriversResource getFirstDriversPage(Integer page, Integer size);

    // NO PAGINATION
    // @GET
    // @Path(QUERY_PATH)
    // @Consumes(APPLICATION_JSON)
    // @Produces(APPLICATION_JSON)
    // PagedDriversResource getDrivers(DriverFilterCriteriaResource filterCriteria);

    @GET
    @Path("drivers/{driverId}")
    @Produces(APPLICATION_JSON)
    DriverResource getDriver(@PathParam("driverId") String driverId);

    @GET
    @Path("drivers")
    @Produces(APPLICATION_JSON)
    PagedDriversResource getFirstPage(
            @QueryParam("filterCriteriaResource") DriverFilterCriteriaResource filterCriteria,
            @QueryParam("pageSize") Integer pageSize);

    @GET
    @Path("drivers/page")
    @Produces(APPLICATION_JSON)
    PagedDriversResource getSubsequentPage(
            @QueryParam("filterCriteriaResource") DriverFilterCriteriaResource filterCriteria,
            @QueryParam("pageSize") Integer pageSize, @QueryParam("uuid") String id);

    @GET
    @Path("campaigns")
    @Produces(APPLICATION_JSON)
    List<CampaignResource> getCampaigns();

    @GET
    @Path("campaigns/{campaignId}")
    @Produces(APPLICATION_JSON)
    CampaignResource getCampaign(@PathParam("campaignId") String campaignId);

    @GET
    @Path("campaign-indexes")
    @Produces(APPLICATION_JSON)
    List<CampaignIndexResource> getCampaignNames(@Context SecurityContext securityContext);

}
