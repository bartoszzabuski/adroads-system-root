package pl.adroads.api.driver;


import nl.jqno.equalsverifier.EqualsVerifier;

import org.junit.Test;

import pl.adroads.api.model.driver.DriverFilterCriteriaResource;

import java.util.function.Supplier;

import static jersey.repackaged.com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class DriverFilterCriteriaResourceUTest {

  private final static String VALID_DRIVER_FILTER_CRITERIA_RESOURCE_JSON
      = "{\"sortDir\":\"ASC\", \"sortedBy\":\"sortedBy\", \"email\":\"email\",\"name\":\"name\",\"surname\":\"surname\",\"status\":\"REGISTERED\",\"postcode\":\"postcode\",\"city\":\"city\",\"street\":\"street\",\"province\":\"dolnośląskie\",\"carMake\":\"carMake\",\"carModel\":\"carModel\",\"carColour\":\"carColour\",\"mileageFrom\":\"5000\",\"mileageTo\":\"15000\",\"yearFrom\":\"2005\",\"yearTo\":\"2010\",\"carBody\":[\"coupe\",\"sedan\"]}";
  private final static DriverFilterCriteriaResource DRIVER_FILTER_CRITERIA_RESOURCE
      = new DriverFilterCriteriaResource("ASC", "sortedBy", "email", "name", "surname",
					 "REGISTERED", "street", "postcode", "city", "dolnośląskie",
					 "carMake", "carModel", 2005, 2010,
					 newArrayList("coupe", "sedan"), "carColour", 5000, 15000);

  @Test
  public void test_equals() {
    EqualsVerifier.forClass(DriverFilterCriteriaResource.class).verify();
  }

  @Test
  public void test_fromString_creates_valid_object() {
    final Supplier<DriverFilterCriteriaResource> supplier = () -> DriverFilterCriteriaResource
	.fromString(VALID_DRIVER_FILTER_CRITERIA_RESOURCE_JSON);

    assertThat(supplier.get(), is(DRIVER_FILTER_CRITERIA_RESOURCE));
  }

}
