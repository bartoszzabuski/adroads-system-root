package pl.adroads.system.infrastructure.adapters.shared.file.csv;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static java.lang.Boolean.TRUE;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static pl.adroads.system.CommonTestFixtures.DATE_OF_BIRTH_STRING;
import static pl.adroads.system.CommonTestFixtures.EMAIL_STRING;
import static pl.adroads.system.CommonTestFixtures.NAME;
import static pl.adroads.system.CommonTestFixtures.PERSISTENCE_ADDRESS;
import static pl.adroads.system.CommonTestFixtures.PERSISTENCE_CAR_DETAILS;
import static pl.adroads.system.CommonTestFixtures.PERSISTENCE_DRIVER;
import static pl.adroads.system.CommonTestFixtures.PERSISTENCE_DRIVER_DETAILS;
import static pl.adroads.system.CommonTestFixtures.STATUS_CREATED;
import static pl.adroads.system.CommonTestFixtures.SURNAME;
import static pl.adroads.system.CommonTestFixtures.VALID_UUID;

@RunWith(MockitoJUnitRunner.class)
public class PersistenceDriverToCSVConverterUTest {

  public static final String COMMA = ",";
  public static final String END_OF_LINE = "\r\n";
  private PersistenceDriverToCSVConverter persistenceDriverToCSVConverter
      = new PersistenceDriverToCSVConverter();

  @Test
  public void verifyFromPersistenceDriverMethodConvertsPersistenceDriverToString() {

    Optional<String> csvRow = persistenceDriverToCSVConverter
	.fromPersistenceDriver(PERSISTENCE_DRIVER);

    assertThat(csvRow.isPresent(), is(TRUE));
    final String csvRowString = csvRow.get();
    List<String> csvRowColumnValues = asList(csvRowString.split(COMMA));
    assertCSVRowContainsValidDriverInformation(csvRowColumnValues);
  }

  @Test
  public void verifyFromPersistenceDriverMethodConvertsPersistenceDriverListToString() {
    Optional<String> csvRow = persistenceDriverToCSVConverter
        .fromPersistenceDriver(newArrayList(PERSISTENCE_DRIVER));

    assertThat(csvRow.isPresent(), is(TRUE));
    final String csvRowString = csvRow.get();
    List<String> csvRowColumnValues = asList(csvRowString.split(COMMA));
    assertCSVRowContainsValidDriverInformation(csvRowColumnValues);
  }

  private void assertCSVRowContainsValidDriverInformation(List<String> csvRowColumnValues) {
    assertThat(csvRowColumnValues,
	       containsInAnyOrder(VALID_UUID.toString(), NAME, SURNAME, DATE_OF_BIRTH_STRING,
				  EMAIL_STRING, PERSISTENCE_ADDRESS.getPostcode(),
				  PERSISTENCE_ADDRESS.getStreet(), PERSISTENCE_ADDRESS.getCity(),
				  PERSISTENCE_ADDRESS.getProvince(), STATUS_CREATED,
				  PERSISTENCE_CAR_DETAILS.getMake(),
				  PERSISTENCE_CAR_DETAILS.getModel(),
				  PERSISTENCE_CAR_DETAILS.getYear(),
				  PERSISTENCE_CAR_DETAILS.getMileage().toString(),
				  PERSISTENCE_CAR_DETAILS.getBody(),
				  PERSISTENCE_CAR_DETAILS.getColour(),
				  PERSISTENCE_CAR_DETAILS.getWindscreenDimensions().getHeight()
				      .toString(),
				  PERSISTENCE_CAR_DETAILS.getWindscreenDimensions().getWidth()
				      .toString(), PERSISTENCE_DRIVER_DETAILS.getType().toString(),
				  PERSISTENCE_DRIVER_DETAILS.getMonthlyDistanceIndex()
				  + END_OF_LINE));
  }

}
