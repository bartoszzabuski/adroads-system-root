package pl.adroads.system.infrastructure.locking;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

@RunWith(MockitoJUnitRunner.class)
public class LockingTemplateUTest {

    private static final String KEY = "testKey";

    private static final int LESS_THAN_MAX_RETRIES = 10;
    private static final int MORE_THAN_MAX_RETRIES = 1000;

    @Mock
    private Lockable<String> mockLockable;

    @Mock
    private LockedOperation<String, Object> mockWriteLocking;

    @Mock
    private Lock mockLock;

    @InjectMocks
    private LockingTemplate lockTemplate;

    @Test
    public void verifyPerformWithLockTriesToLockWithRetries() throws Exception {
        when(mockLockable.getLockFor(KEY)).thenReturn(mockLock);
        when(mockLock.tryLock(anyLong(), any(TimeUnit.class))).thenAnswer(answerTrueAfter(LESS_THAN_MAX_RETRIES));

        lockTemplate.performWithLock(mockLockable, KEY, mockWriteLocking);

        verify(mockLock, times(LESS_THAN_MAX_RETRIES)).tryLock(anyLong(), any(TimeUnit.class));
        verify(mockWriteLocking).perform(KEY);
        verify(mockLock).unlock();
    }

    @Test
    public void verifyPerformWithLockThrowsExceptionAfterExceedingRetries() throws Exception {
        when(mockLockable.getLockFor(KEY)).thenReturn(mockLock);
        when(mockLock.tryLock(anyLong(), any(TimeUnit.class))).thenAnswer(answerTrueAfter(MORE_THAN_MAX_RETRIES));

        catchException(lockTemplate).performWithLock(mockLockable, KEY, mockWriteLocking);

        verify(mockLock).unlock();
        assertThat(caughtException(), instanceOf(RuntimeException.class));
    }

    @Test
    public void verifyPerformWithLockTriesToLockWithRetriesAndIgnoresInterruptExceptions() throws Exception {
        when(mockLockable.getLockFor(KEY)).thenReturn(mockLock);
        when(mockLock.tryLock(anyLong(), any(TimeUnit.class))).thenThrow(InterruptedException.class).thenReturn(true);

        lockTemplate.performWithLock(mockLockable, KEY, mockWriteLocking);

        verify(mockLock, times(2)).tryLock(anyLong(), any(TimeUnit.class));
        verify(mockWriteLocking).perform(KEY);
        verify(mockLock).unlock();
    }

    private static AnswerLockIterations answerTrueAfter(final int iterations) {
        return new AnswerLockIterations(iterations);
    }

    public static class AnswerLockIterations implements Answer<Boolean> {

        private int iterations;
        private int count = 0;

        private AnswerLockIterations(int iterations) {
            this.iterations = iterations;
        }

        @Override
        public Boolean answer(InvocationOnMock invocation) throws Throwable {
            count++;
            if (count < iterations) {
                return false;
            } else {
                return true;
            }
        }
    }

}
