package pl.adroads.system.infrastructure.adapters.driving.query.persistence.repository;

import static com.google.common.collect.Lists.newArrayList;
import static java.lang.String.format;
import static java.util.Optional.of;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static pl.adroads.system.CommonTestFixtures.CAMPAIGN_ID;
import static pl.adroads.system.CommonTestFixtures.CAMPAIGN_NAME;
import static pl.adroads.system.CommonTestFixtures.DRIVER_ID_1;
import static pl.adroads.system.CommonTestFixtures.DRIVER_UUID_1;
import static pl.adroads.system.CommonTestFixtures.EMAIL_STRING;
import static pl.adroads.system.infrastructure.adapters.driving.query.persistance.repository.CampaignQueryRepositoryImpl.TBL_CAMPAIGNS_TABLE_NAME;

import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.domain.query.model.DriverFilterCriteria;
import pl.adroads.system.domain.query.model.Page;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.assemblers.PersistenceDriverAssembler;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceDriver;
import pl.adroads.system.infrastructure.adapters.driving.query.persistance.model.PersistenceCampaignDetails;
import pl.adroads.system.infrastructure.adapters.driving.query.persistance.model.PersistenceCampaignDriverDetails;
import pl.adroads.system.infrastructure.adapters.driving.query.persistance.model.PersistenceQueryDriver;
import pl.adroads.system.infrastructure.adapters.driving.query.persistance.repository.DriverQueryRepositoryImpl;
import pl.adroads.system.infrastructure.adapters.shared.postgre.CampaignDriversQueryDao;
import pl.adroads.system.infrastructure.adapters.shared.postgre.DriversQueryDao;
import pl.adroads.system.infrastructure.adapters.shared.postgre.PageMetadata;

@RunWith(MockitoJUnitRunner.class)
public class DriverQueryRepositoryImplUTest {

    private static final String TBL_DRIVERS_TABLE_NAME = "tbl_drivers";
    public static final int PAGE_SIZE = 10;
    public static final String EXPECTED_CONDITION = "data ->>  'email' ILIKE '%mockEmail@mock.com%'";
    public static final String DRIVERSIDS_JSON_ARRAYS = format("'[{\"uuid\":\"%s\"}]'", DRIVER_UUID_1.toString());
    public static final String DRIVER_IDS_ARRAY = format("'%s'", DRIVER_UUID_1.toString());
    @Mock
    private PersistenceDriverAssembler driverAssembler;

    @Mock
    private CampaignDriversQueryDao mockCampaignDriversQueryDao;

    //    @formatter:off
  //    @formatter:on

    private static final DriverFilterCriteria EMPTY_DRIVER_FILTER_CRITERIA = new DriverFilterCriteria(
            Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(),
            Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(),
            Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(),
            Optional.empty(), Optional.empty(), Optional.empty());

    @InjectMocks
    private DriverQueryRepositoryImpl driverQueryRepository;

    private DriverFilterCriteria mockDriverFilterCriteria = new DriverFilterCriteria(Optional.empty(),
            Optional.empty(),
            of(EMAIL_STRING),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            Optional.empty());

    @Captor
    private ArgumentCaptor<Optional<String>> conditionsCaptor;
    @Mock
    private List<PersistenceDriver> mockDrivers;
    @Mock
    private DriversQueryDao mockDriversQueryDao;
    @Mock
    private PersistenceQueryDriver mockPersistenceDriver;

    private List<PersistenceQueryDriver> persistenceQueryDrivers;
    @Mock
    private PersistenceCampaignDriverDetails mockPersistenceCampaignDriverDetails;
    @Mock
    private List<PageMetadata> mockMetadata;

    @Before
    public void setup() {
        persistenceQueryDrivers = newArrayList(mockPersistenceDriver);
        when(mockPersistenceCampaignDriverDetails.getDriverId()).thenReturn(DRIVER_ID_1.getValue().toString());
        when(mockPersistenceCampaignDriverDetails.getCampaignId()).thenReturn(CAMPAIGN_ID.getValue().toString());
        when(mockPersistenceCampaignDriverDetails.getCampaignName()).thenReturn(CAMPAIGN_NAME);
    }

    @Test
    public void verifyGetFirstPageDelegatesToDriverQueryDaoToFetchPageMetadataAndFirstPageOfDrivers() {
        driverQueryRepository.getFirstPage(mockDriverFilterCriteria,
                PAGE_SIZE);

        verify(mockDriversQueryDao)
                .getPagesMetadata(eq(TBL_DRIVERS_TABLE_NAME), conditionsCaptor.capture(), eq(PAGE_SIZE));
        assertThat(conditionsCaptor.getValue().get(), is(EXPECTED_CONDITION));
        verify(mockDriversQueryDao).getFirstPage(eq(TBL_DRIVERS_TABLE_NAME), conditionsCaptor.capture(), eq(PAGE_SIZE));
        assertThat(conditionsCaptor.getValue().get(), is(EXPECTED_CONDITION));
    }

    @Test
    public void verifyGetFirstPageDelegatesToCampaignStoreToFetchCampaignInformation() {
        when(mockPersistenceDriver.getUuid()).thenReturn(DRIVER_UUID_1.toString());
        when(mockDriversQueryDao.getFirstPage(eq(TBL_DRIVERS_TABLE_NAME), conditionsCaptor.capture(), eq(PAGE_SIZE)))
                .thenReturn(persistenceQueryDrivers);

        driverQueryRepository.getFirstPage(mockDriverFilterCriteria, PAGE_SIZE);

        verify(mockCampaignDriversQueryDao).getCampaignDetailsForDrivers(TBL_CAMPAIGNS_TABLE_NAME,
                DRIVERSIDS_JSON_ARRAYS, DRIVER_IDS_ARRAY);
    }

    @Test
    public void verifyGetFirstPageFetchesCampaignDetailsForEachMatchingDriver() {

        when(mockDriversQueryDao.getPagesMetadata(eq(TBL_DRIVERS_TABLE_NAME), conditionsCaptor.capture(),
                eq(PAGE_SIZE))).thenReturn(mockMetadata);
        when(mockPersistenceDriver.getUuid()).thenReturn(DRIVER_UUID_1.toString());
        when(mockDriversQueryDao.getFirstPage(eq(TBL_DRIVERS_TABLE_NAME), conditionsCaptor.capture(), eq(PAGE_SIZE)))
                .thenReturn(persistenceQueryDrivers);
        when(mockCampaignDriversQueryDao.getCampaignDetailsForDrivers(TBL_CAMPAIGNS_TABLE_NAME,
                DRIVERSIDS_JSON_ARRAYS, DRIVER_IDS_ARRAY))
                        .thenReturn(newArrayList(mockPersistenceCampaignDriverDetails));

        Page<PersistenceQueryDriver> persistenceQueryDriversPage = driverQueryRepository
                .getFirstPage(mockDriverFilterCriteria, PAGE_SIZE);

        verify(mockPersistenceDriver)
                .setCampaignDetails(new PersistenceCampaignDetails(CAMPAIGN_ID.getValue().toString(), CAMPAIGN_NAME));

        assertThat(persistenceQueryDriversPage.getMetadata(), is(Optional.of(mockMetadata)));
        assertThat(persistenceQueryDriversPage.getDrivers(), hasSize(1));
        assertThat(persistenceQueryDriversPage.getDrivers().get(0), is(mockPersistenceDriver));
    }

    @Test
    public void verifyGetFirstPageFetchesNoCampaignDetailsIfNoMatchingCampaignsFound() {

        when(mockDriversQueryDao.getPagesMetadata(eq(TBL_DRIVERS_TABLE_NAME), conditionsCaptor.capture(),
                eq(PAGE_SIZE))).thenReturn(mockMetadata);
        when(mockPersistenceDriver.getUuid()).thenReturn(DRIVER_UUID_1.toString());
        when(mockDriversQueryDao.getFirstPage(eq(TBL_DRIVERS_TABLE_NAME), conditionsCaptor.capture(), eq(PAGE_SIZE)))
                .thenReturn(persistenceQueryDrivers);
        when(mockCampaignDriversQueryDao.getCampaignDetailsForDrivers(TBL_CAMPAIGNS_TABLE_NAME,
                DRIVERSIDS_JSON_ARRAYS, DRIVER_IDS_ARRAY))
                        .thenReturn(newArrayList());

        Page<PersistenceQueryDriver> persistenceQueryDriversPage = driverQueryRepository
                .getFirstPage(mockDriverFilterCriteria, PAGE_SIZE);

        verify(mockPersistenceDriver, never())
                .setCampaignDetails(new PersistenceCampaignDetails(CAMPAIGN_ID.getValue().toString(), CAMPAIGN_NAME));

        assertThat(persistenceQueryDriversPage.getMetadata(), is(Optional.of(mockMetadata)));
        assertThat(persistenceQueryDriversPage.getDrivers(), hasSize(1));
        assertThat(persistenceQueryDriversPage.getDrivers().get(0), is(mockPersistenceDriver));
    }

    @Test
    public void verifyGetSubsequentPageDelegatesToDriverQueryDaoToFetchDrivers() {
        driverQueryRepository.getSubsequentPage(mockDriverFilterCriteria, DRIVER_UUID_1.toString(),
                PAGE_SIZE);

        verify(mockDriversQueryDao).getSubsequentPage(eq(TBL_DRIVERS_TABLE_NAME), conditionsCaptor.capture(),
                eq(DRIVER_UUID_1.toString()), eq(PAGE_SIZE));
        assertThat(conditionsCaptor.getValue().get(), is(EXPECTED_CONDITION));
        verifyNoMoreInteractions(mockDriversQueryDao);

    }

    @Test
    public void verifyGetSubsequentPageDelegatesToCampaignStoreToFetchCampaignInformation() {
        when(mockPersistenceDriver.getUuid()).thenReturn(DRIVER_UUID_1.toString());
        when(mockDriversQueryDao.getSubsequentPage(eq(TBL_DRIVERS_TABLE_NAME), conditionsCaptor.capture(),
                eq(DRIVER_ID_1.getValue().toString()), eq(PAGE_SIZE)))
                        .thenReturn(persistenceQueryDrivers);

        driverQueryRepository.getSubsequentPage(mockDriverFilterCriteria, DRIVER_UUID_1.toString(), PAGE_SIZE);

        verify(mockCampaignDriversQueryDao).getCampaignDetailsForDrivers(TBL_CAMPAIGNS_TABLE_NAME,
                DRIVERSIDS_JSON_ARRAYS, DRIVER_IDS_ARRAY);
    }

    @Test
    public void verifyGetSubsequentPageFetchesCampaignDetailsForEachMatchingDriver() {

        when(mockPersistenceDriver.getUuid()).thenReturn(DRIVER_UUID_1.toString());
        when(mockDriversQueryDao.getSubsequentPage(eq(TBL_DRIVERS_TABLE_NAME), conditionsCaptor.capture(),
                eq(DRIVER_UUID_1.toString()), eq(PAGE_SIZE))).thenReturn(persistenceQueryDrivers);
        when(mockCampaignDriversQueryDao.getCampaignDetailsForDrivers(TBL_CAMPAIGNS_TABLE_NAME,
                DRIVERSIDS_JSON_ARRAYS, DRIVER_IDS_ARRAY))
                        .thenReturn(newArrayList(mockPersistenceCampaignDriverDetails));

        Page<PersistenceQueryDriver> persistenceQueryDriversPage = driverQueryRepository
                .getSubsequentPage(mockDriverFilterCriteria, DRIVER_UUID_1.toString(), PAGE_SIZE);

        verify(mockPersistenceDriver)
                .setCampaignDetails(new PersistenceCampaignDetails(CAMPAIGN_ID.getValue().toString(), CAMPAIGN_NAME));

        assertThat(persistenceQueryDriversPage.getMetadata(), is(Optional.empty()));
        assertThat(persistenceQueryDriversPage.getDrivers(), hasSize(1));
        assertThat(persistenceQueryDriversPage.getDrivers().get(0), is(mockPersistenceDriver));
    }

    @Test
    public void verifyGetSubsequentPageFetchesNoCampaignDetailsIfNoMatchingCampaignsFound() {

        when(mockPersistenceDriver.getUuid()).thenReturn(DRIVER_UUID_1.toString());
        when(mockDriversQueryDao.getSubsequentPage(eq(TBL_DRIVERS_TABLE_NAME), conditionsCaptor.capture(),
                eq(DRIVER_UUID_1.toString()), eq(PAGE_SIZE)))
                        .thenReturn(persistenceQueryDrivers);
        when(mockCampaignDriversQueryDao.getCampaignDetailsForDrivers(TBL_CAMPAIGNS_TABLE_NAME,
                DRIVERSIDS_JSON_ARRAYS, DRIVER_IDS_ARRAY))
                        .thenReturn(newArrayList());

        Page<PersistenceQueryDriver> persistenceQueryDriversPage = driverQueryRepository
                .getSubsequentPage(mockDriverFilterCriteria, DRIVER_UUID_1.toString(), PAGE_SIZE);

        verify(mockPersistenceDriver, never())
                .setCampaignDetails(new PersistenceCampaignDetails(CAMPAIGN_ID.getValue().toString(), CAMPAIGN_NAME));

        assertThat(persistenceQueryDriversPage.getMetadata(), is(Optional.empty()));
        assertThat(persistenceQueryDriversPage.getDrivers(), hasSize(1));
        assertThat(persistenceQueryDriversPage.getDrivers().get(0), is(mockPersistenceDriver));
    }

}
