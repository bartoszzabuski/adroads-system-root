package pl.adroads.system.infrastructure.adapters.driven.user.integration.listeners;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.application.driver.DriverManagementAppService;
import pl.adroads.system.domain.driver.model.DriverId;
import pl.adroads.system.domain.user.events.UserActivatedDomainEvent;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.adroads.system.CommonTestFixtures.USER_ID;


@RunWith(MockitoJUnitRunner.class)
public class UsersEventsListenerUTest {

  @Mock
  private DriverManagementAppService driverManagementAppService;

  @InjectMocks
  private UsersEventsListener usersEventsListener;

  @Mock
  private UserActivatedDomainEvent mockUserActivatedDomainEvent;

  @Test
  public void test_userActivated_delegates_to_userManagementService() {
    when(mockUserActivatedDomainEvent.getUserId()).thenReturn(USER_ID);

    usersEventsListener.userActivated(mockUserActivatedDomainEvent);

    verify(driverManagementAppService).activateDriver(DriverId.of(USER_ID.getValue()));
  }

}
