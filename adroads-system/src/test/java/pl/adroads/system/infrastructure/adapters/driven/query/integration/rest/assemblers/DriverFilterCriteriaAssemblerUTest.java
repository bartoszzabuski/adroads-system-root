package pl.adroads.system.infrastructure.adapters.driven.query.integration.rest.assemblers;


import org.junit.Test;

import pl.adroads.api.model.driver.DriverFilterCriteriaResource;
import pl.adroads.system.domain.driver.model.Status;
import pl.adroads.system.domain.query.model.DriverFilterCriteria;

import java.util.Optional;

import static jersey.repackaged.com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static pl.adroads.system.domain.driver.model.PolishProvince.DOLNOŚLĄSKIE;
import static pl.adroads.system.domain.query.model.SortDirection.ASC;

public class DriverFilterCriteriaAssemblerUTest {

  public static final String ASC_DIR_VALUE = "ASC";
  public static final String SORTED_BY = "sortedBy";
  public static final String EMAIL = "email";
  public static final String NAME = "name";
  public static final String SURNAME = "surname";
  public static final String REGISTERED = "REGISTERED";
  public static final String STREET = "street";
  public static final String POSTCODE = "postcode";
  public static final String CITY = "city";
  public static final String PROVINCE = "dolnośląskie";
  public static final String CAR_MAKE = "carMake";
  public static final String CAR_MODEL = "carModel";
  public static final int YEAR_FROM = 2005;
  public static final int YEAR_TO = 2010;
  public static final String COUPE = "coupe";
  public static final String SEDAN = "sedan";
  public static final String CAR_COLOUR = "carColour";
  public static final int MILEAGE_FROM = 5000;
  public static final int MILEAGE_TO = 15000;
  private final static DriverFilterCriteriaResource DRIVER_FILTER_CRITERIA_RESOURCE
      = new DriverFilterCriteriaResource(ASC_DIR_VALUE, SORTED_BY, EMAIL, NAME, SURNAME, REGISTERED,
					 STREET, POSTCODE, CITY, PROVINCE, CAR_MAKE, CAR_MODEL,
					 YEAR_FROM, YEAR_TO, newArrayList(COUPE, SEDAN), CAR_COLOUR,
					 MILEAGE_FROM, MILEAGE_TO);
  private static final DriverFilterCriteriaResource NULL_DRIVER_FILTER_CRITERIA_RESOURCE
      = new DriverFilterCriteriaResource(null, null, null, null, null, null, null, null, null, null,
					 null, null, null, null, null, null, null, null);


  private DriverFilterCriteriaAssembler driverFilterCriteriaAssembler
      = new DriverFilterCriteriaAssembler();

  @Test
  public void verify_assembleFilterCriteria_assembles_a_valid_object() {
    DriverFilterCriteria driverFilterCriteria = driverFilterCriteriaAssembler
	.assembleFilterCriteria(DRIVER_FILTER_CRITERIA_RESOURCE);

    assertThat(driverFilterCriteria, is(notNullValue()));
    assertThat(driverFilterCriteria.getSortDir(), is(Optional.of(ASC)));
    assertThat(driverFilterCriteria.getSortedBy(), is(Optional.of(SORTED_BY)));
    assertThat(driverFilterCriteria.getEmail(), is(Optional.of(EMAIL)));
    assertThat(driverFilterCriteria.getName(), is(Optional.of(NAME)));
    assertThat(driverFilterCriteria.getSurname(), is(Optional.of(SURNAME)));
    assertThat(driverFilterCriteria.getStatus(), is(Optional.of(Status.REGISTERED)));
    assertThat(driverFilterCriteria.getStreet(), is(Optional.of(STREET)));
    assertThat(driverFilterCriteria.getPostcode(), is(Optional.of(POSTCODE)));
    assertThat(driverFilterCriteria.getCity(), is(Optional.of(CITY)));
    assertThat(driverFilterCriteria.getProvince(), is(Optional.of(DOLNOŚLĄSKIE)));
  }

  @Test
  public void when_resource_has_null_fields_then_assembleFilterCriteria_assembles_a_valid_object() {
    DriverFilterCriteria driverFilterCriteria = driverFilterCriteriaAssembler
	.assembleFilterCriteria(NULL_DRIVER_FILTER_CRITERIA_RESOURCE);

    assertThat(driverFilterCriteria, is(notNullValue()));
    assertThat(driverFilterCriteria.getSortDir(), is(Optional.empty()));
    assertThat(driverFilterCriteria.getSortedBy(), is(Optional.empty()));
    assertThat(driverFilterCriteria.getEmail(), is(Optional.empty()));
    assertThat(driverFilterCriteria.getName(), is(Optional.empty()));
    assertThat(driverFilterCriteria.getSurname(), is(Optional.empty()));
    assertThat(driverFilterCriteria.getStatus(), is(Optional.empty()));
    assertThat(driverFilterCriteria.getStreet(), is(Optional.empty()));
    assertThat(driverFilterCriteria.getPostcode(), is(Optional.empty()));
    assertThat(driverFilterCriteria.getCity(), is(Optional.empty()));
    assertThat(driverFilterCriteria.getProvince(), is(Optional.empty()));
  }


}
