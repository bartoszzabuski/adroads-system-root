package pl.adroads.system.infrastructure.adapters.shared.postgre.annotations;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.skife.jdbi.v2.SQLStatement;

import java.util.Optional;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.lang.String.format;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static pl.adroads.system.infrastructure.adapters.shared.postgre.annotations.BindOptional.BindOptionalFactory.OptionalBinder;
import static pl.adroads.system.infrastructure.adapters.shared.postgre.annotations.BindOptional.BindOptionalFactory.OptionalBinder.IS_PRESENT_POSTFIX;

@RunWith(MockitoJUnitRunner.class)
public class BindOptionalUTest {

  private static final String FIELD_NAME = "fieldName";
  public static final String VALUE = "test";

  @Mock
  private BindOptional mockBindOptional;

  private OptionalBinder binder = new OptionalBinder(FIELD_NAME);

  @Mock
  private SQLStatement<?> mockStatement;
  @Mock
  private Object mockNotOptionalArg;
  @Mock
  private Object mockValue;

  private Optional optionalArg = Optional.ofNullable(mockValue);

  @Test
  public void when_passed_argument_is_null_binder_throws_exception() {
    catchException(binder).bind(mockStatement, mockBindOptional, null);
    assertThat(caughtException(), instanceOf(NullPointerException.class));
  }

  @Test
  public void when_passed_argument_is_not_an_optional_binder_throws_exception() {
    catchException(binder).bind(mockStatement, mockBindOptional, mockNotOptionalArg);
    assertThat(caughtException(), instanceOf(RuntimeException.class));
  }

  @Test
  public void when_optional_is_empty_bind_method_defines_XXX_isPresent_variable_to_false() {
    binder.bind(mockStatement, mockBindOptional, Optional.empty());
    verify(mockStatement).define(getExpectedIsPresentFieldName(), FALSE);
    verifyNoMoreInteractions(mockStatement);
  }

  private String getExpectedIsPresentFieldName() {
    return format("%s%s", FIELD_NAME, IS_PRESENT_POSTFIX);
  }

  @Test
  public void when_optional_is_not_empty_bind_method_defines_two_variables() {
    binder.bind(mockStatement, mockBindOptional, Optional.of(VALUE));
    verify(mockStatement).define(getExpectedIsPresentFieldName(), TRUE);
    verify(mockStatement).define(FIELD_NAME, VALUE);
    verifyNoMoreInteractions(mockStatement);
  }

}
