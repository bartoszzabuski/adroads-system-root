package pl.adroads.system.infrastructure.adapters.shared.exceptions;

import pl.adroads.api.model.shared.ExceptionResponse;
import pl.adroads.system.common.util.SystemException;
import pl.adroads.system.infrastructure.adapters.shared.ObjectSerializer;
import pl.adroads.system.infrastructure.adapters.shared.exceptions.localization.ErrorLocalizationMapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Response;

import static pl.adroads.system.CommonTestFixtures.EMAIL_STRING;
import static pl.adroads.system.common.util.GlobalConstants.CONTENT_TYPE_APPLICATION_JSON;
import static pl.adroads.system.domain.driver.exception.DriverDomainError.DRIVER_ALREADY_EXISTS;
import static org.apache.http.HttpStatus.SC_CONFLICT;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SystemExceptionMapperUTest {

  private static final String MOCK_MESSAGE = "temp";
  private static final String SERIALIZED_EXCEPTION_RESPONSE = "serializedExceptionResponse";

  @Mock
  private ErrorLocalizationMapper errorLocalizationMapper;

  @Mock
  private ObjectSerializer objectSerializer;

  @InjectMocks
  private SystemExceptionMapper systemExceptionMapper;

  private SystemException driverAlreadyExists = SystemException.builder(DRIVER_ALREADY_EXISTS)
      .withParams(EMAIL_STRING).build();

  @Test
  public void when_valid_exception_provided_then_toResponse_build_valid_errorResponse() {
    when(errorLocalizationMapper.getMessageFor(driverAlreadyExists.getSystemError()))
        .thenReturn(MOCK_MESSAGE);
    when(objectSerializer
             .serialize(new ExceptionResponse(DRIVER_ALREADY_EXISTS.getType(), MOCK_MESSAGE)))
        .thenReturn(SERIALIZED_EXCEPTION_RESPONSE);

    Response response = systemExceptionMapper.toResponse(driverAlreadyExists);

    assertThat(response.getStatus(), is(SC_CONFLICT));
    assertThat(response.getEntity(), is(SERIALIZED_EXCEPTION_RESPONSE));
//    assertThat(response.getMediaType().toString(), is(CONTENT_TYPE_APPLICATION_JSON));
  }

}
