package pl.adroads.system.infrastructure.adapters.shared.postgre;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.util.ReflectionTestUtils;

import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceDriver;
import pl.adroads.system.infrastructure.adapters.shared.ObjectSerializer;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.adroads.system.CommonTestFixtures.EMAIL_STRING;


@RunWith(MockitoJUnitRunner.class)
public class PostgreDriverStorageUTest {

  private String tableName;
  @Mock
  private PostgreSQLJSONStorageFacade<PersistenceDriver> storageFacade;
  @Mock
  private JdbcTemplate jdbcTemplate;
  @Mock
  private ObjectSerializer serializer;
  @Mock
  private PersistenceDriver mockPersistenceDriver;

  @InjectMocks
  private PostgreDriverStorage postgreDriverStorage;

  @Before
  public void setup() {
    tableName = (String) ReflectionTestUtils.getField(postgreDriverStorage, "DRIVER_TABLE_NAME");
  }

  //  TODO fix tests for this class and consider how to test AbstractPostgreSQLJSONStorage
  @Test
  public void test_saveNewDriver_correctly_delegates_to_PostgreSQLJSONStorageFacade() {

    postgreDriverStorage.saveNewDriver(mockPersistenceDriver);
    verify(storageFacade).insertAsJSON(tableName, mockPersistenceDriver);
  }

  @Test
  public void test_findByEmail_correctly_delegates_to_PostgreSQLJSONStorageFacade() {
    when(storageFacade.findExact(eq(tableName), eq(PersistenceDriver.class), anyString(), eq(""),
                                 eq(EMAIL_STRING))).thenReturn(mockPersistenceDriver);

    Optional<PersistenceDriver> driverFound = postgreDriverStorage.findByEmail(EMAIL_STRING);
    verify(storageFacade).findExact(eq(tableName), eq(PersistenceDriver.class), anyString(), eq(""),
                                    eq(EMAIL_STRING));
    assertThat(driverFound.get(), equalTo(mockPersistenceDriver));
  }

}
