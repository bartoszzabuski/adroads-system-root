package pl.adroads.system.infrastructure.locking;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

import java.util.concurrent.TimeUnit;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import com.hazelcast.core.IMap;

public class HazelcastMapEntryLockUTest {

    private static final String KEY = "testKey";
    private static final long TIMEOUT = 1L;
    private static final TimeUnit TIMEOUT_UNIT = TimeUnit.MILLISECONDS;

    private IMap<String, String> mockStore = mock(IMap.class);

    private HazelcastMapEntryLock mapEntryLock;

    @Before
    public void setup() {
        mapEntryLock = new HazelcastMapEntryLock(KEY, mockStore);
    }

    @Test
    public void verifyLockDelegatesToStore() {
        mapEntryLock.lock();

        verify(mockStore).lock(KEY);
    }

    @Test
    public void verifyTryLockDelegatesToStore() {
        mapEntryLock.tryLock();

        verify(mockStore).tryLock(KEY);
    }

    @Test
    public void verifyTryLockWithTimeoutDelegatesToStore() throws Exception {
        mapEntryLock.tryLock(TIMEOUT, TIMEOUT_UNIT);

        verify(mockStore).tryLock(KEY, TIMEOUT, TIMEOUT_UNIT);
    }

    @Test
    public void verifyTryLockWithTimeoutDelegatesInterruptedException() throws Exception {
        InterruptedException interruptedException = mock(InterruptedException.class);
        when(mockStore.tryLock(KEY, TIMEOUT, TIMEOUT_UNIT)).thenThrow(interruptedException);

        catchException(mapEntryLock).tryLock(TIMEOUT, TIMEOUT_UNIT);

        assertThat(caughtException(), Matchers.<Exception> equalTo(interruptedException));
    }

    @Test
    public void verifyUnlockDelegatesToStore() {
        mapEntryLock.unlock();

        verify(mockStore).unlock(KEY);
    }
}
