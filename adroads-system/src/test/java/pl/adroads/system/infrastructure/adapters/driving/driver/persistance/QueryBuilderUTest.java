package pl.adroads.system.infrastructure.adapters.driving.driver.persistance;


import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static java.lang.Boolean.TRUE;
import static java.lang.String.format;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static pl.adroads.system.CommonTestFixtures.BODY;
import static pl.adroads.system.CommonTestFixtures.PROVINCE;
import static pl.adroads.system.domain.driver.model.Status.CREATED;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.CAR_BODY_KEY_AS_TEXT;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.CAR_COLOUR_AS_TEXT;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.CAR_MAKE_KEY_AS_TEXT;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.CAR_MILEAGE_KEY_AS_TEXT;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.CAR_MODEL_KEY_AS_TEXT;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.CAR_YEAR_KEY_AS_TEXT;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.CITY_KEY_AS_TEXT;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.EMAIL_KEY;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.NAME_KEY;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.POSTCODE_KEY_AS_TEXT;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.PROVINCE_KEY_AS_TEXT;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.STATUS_KEY;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.STREET_KEY_AS_TEXT;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.SURNAME_KEY;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.PostgresSqlConditionsHelper.anyInArray;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.PostgresSqlConditionsHelper.equalTo;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.PostgresSqlConditionsHelper.greaterThanOrEqual;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.PostgresSqlConditionsHelper.iLike;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.PostgresSqlConditionsHelper.lessThanOrEqualTo;

public class QueryBuilderUTest {

  private static final String TEST_STRING = "testString";
  private static final Integer TEST_INTEGER = 123;
  private QueryBuilder queryBuilder;

  @Before
  public void setup() {
    queryBuilder = new QueryBuilder();
  }

  @Test
  public void when_no_conditions_specified_then_build_returns_empty() {
    assertThat(queryBuilder.build(), is(empty()));
  }

  @Test
  public void when_name_to_be_queried_is_empty_then_build_returns_no_condition() {
    assertThat(queryBuilder.searchName(empty()).build(), is(empty()));
  }

  @Test
  public void when_name_to_be_queried_is_specified_then_build_condtion() {
    Optional<String> conditionOptional = queryBuilder.searchName(of(TEST_STRING)).build();
    assertThat(conditionOptional.isPresent(), is(TRUE));
    assertThat(conditionOptional.get(), is(iLike(NAME_KEY, "%" + TEST_STRING + "%")));
  }

  @Test
  public void when_carMake_to_be_queried_is_empty_then_build_returns_no_condition() {
    assertThat(queryBuilder.searchCarMake(empty()).build(), is(empty()));
  }

  @Test
  public void when_carMake_to_be_queried_is_specified_then_build_condtion() {
    Optional<String> conditionOptional = queryBuilder.searchCarMake(of(TEST_STRING)).build();
    assertThat(conditionOptional.isPresent(), is(TRUE));
    assertThat(conditionOptional.get(), is(iLike(CAR_MAKE_KEY_AS_TEXT, TEST_STRING + "%")));
  }

  @Test
  public void when_CarModel_to_be_queried_is_empty_then_build_returns_no_condition() {
    assertThat(queryBuilder.searchCarModel(empty()).build(), is(empty()));
  }

  @Test
  public void when_CarModel_to_be_queried_is_specified_then_build_condtion() {
    Optional<String> conditionOptional = queryBuilder.searchCarModel(of(TEST_STRING)).build();
    assertThat(conditionOptional.isPresent(), is(TRUE));
    assertThat(conditionOptional.get(), is(iLike(CAR_MODEL_KEY_AS_TEXT, "%" + TEST_STRING + "%")));
  }

  @Test
  public void when_City_to_be_queried_is_empty_then_build_returns_no_condition() {
    assertThat(queryBuilder.searchCity(empty()).build(), is(empty()));
  }

  @Test
  public void when_City_to_be_queried_is_specified_then_build_condtion() {
    Optional<String> conditionOptional = queryBuilder.searchCity(of(TEST_STRING)).build();
    assertThat(conditionOptional.isPresent(), is(TRUE));
    assertThat(conditionOptional.get(), is(iLike(CITY_KEY_AS_TEXT, "%" + TEST_STRING + "%")));
  }

  @Test
  public void when_Colour_to_be_queried_is_empty_then_build_returns_no_condition() {
    assertThat(queryBuilder.searchColour(empty()).build(), is(empty()));
  }

  @Test
  public void when_Colour_to_be_queried_is_specified_then_build_condtion() {
    Optional<String> conditionOptional = queryBuilder.searchColour(of(TEST_STRING)).build();
    assertThat(conditionOptional.isPresent(), is(TRUE));
    assertThat(conditionOptional.get(), is(iLike(CAR_COLOUR_AS_TEXT, TEST_STRING + "%")));
  }

  @Test
  public void when_Email_to_be_queried_is_empty_then_build_returns_no_condition() {
    assertThat(queryBuilder.searchEmail(empty()).build(), is(empty()));
  }

  @Test
  public void when_Email_to_be_queried_is_specified_then_build_condtion() {
    Optional<String> conditionOptional = queryBuilder.searchEmail(of(TEST_STRING)).build();
    assertThat(conditionOptional.isPresent(), is(TRUE));
    assertThat(conditionOptional.get(), is(iLike(EMAIL_KEY, "%" + TEST_STRING + "%")));
  }

  @Test
  public void when_Postcode_to_be_queried_is_empty_then_build_returns_no_condition() {
    assertThat(queryBuilder.searchPostcode(empty()).build(), is(empty()));
  }

  @Test
  public void when_Postcode_to_be_queried_is_specified_then_build_condtion() {
    Optional<String> conditionOptional = queryBuilder.searchPostcode(of(TEST_STRING)).build();
    assertThat(conditionOptional.isPresent(), is(TRUE));
    assertThat(conditionOptional.get(), is(iLike(POSTCODE_KEY_AS_TEXT, TEST_STRING + "%")));
  }

  @Test
  public void when_Street_to_be_queried_is_empty_then_build_returns_no_condition() {
    assertThat(queryBuilder.searchStreet(empty()).build(), is(empty()));
  }

  @Test
  public void when_Street_to_be_queried_is_specified_then_build_condtion() {
    Optional<String> conditionOptional = queryBuilder.searchStreet(of(TEST_STRING)).build();
    assertThat(conditionOptional.isPresent(), is(TRUE));
    assertThat(conditionOptional.get(), is(iLike(STREET_KEY_AS_TEXT, "%" + TEST_STRING + "%")));
  }

  @Test
  public void when_Surname_to_be_queried_is_empty_then_build_returns_no_condition() {
    assertThat(queryBuilder.searchSurname(empty()).build(), is(empty()));
  }

  @Test
  public void when_Surname_to_be_queried_is_specified_then_build_condtion() {
    Optional<String> conditionOptional = queryBuilder.searchSurname(of(TEST_STRING)).build();
    assertThat(conditionOptional.isPresent(), is(TRUE));
    assertThat(conditionOptional.get(), is(iLike(SURNAME_KEY, "%" + TEST_STRING + "%")));
  }

  @Test
  public void when_mileage_mileage_be_queried_is_empty_then_build_returns_no_condition() {
    assertThat(queryBuilder.mileageFrom(empty()).build(), is(empty()));
  }

  @Test
  public void when_mileage_from_be_queried_is_specified_then_build_condtion() {
    Optional<String> conditionOptional = queryBuilder.mileageFrom(of(TEST_INTEGER)).build();
    assertThat(conditionOptional.isPresent(), is(TRUE));
    assertThat(conditionOptional.get(),
	       is(greaterThanOrEqual(CAR_MILEAGE_KEY_AS_TEXT, TEST_INTEGER)));
  }

  @Test
  public void when_mileage_to_be_queried_is_empty_then_build_returns_no_condition() {
    assertThat(queryBuilder.mileageTo(empty()).build(), is(empty()));
  }

  @Test
  public void when_mileage_to_be_queried_is_specified_then_build_condtion() {
    Optional<String> conditionOptional = queryBuilder.mileageTo(of(TEST_INTEGER)).build();
    assertThat(conditionOptional.isPresent(), is(TRUE));
    assertThat(conditionOptional.get(),
	       is(lessThanOrEqualTo(CAR_MILEAGE_KEY_AS_TEXT, TEST_INTEGER)));
  }

  @Test
  public void when_CarBody_to_be_queried_is_empty_then_build_returns_no_condition() {
    assertThat(queryBuilder.withCarBody(empty()).build(), is(empty()));
  }

  @Test
  public void when_CarBody_to_be_queried_is_specified_then_build_condtion() {
    Optional<String> conditionOptional = queryBuilder.withCarBody(of(newArrayList(BODY, BODY)))
	.build();
    assertThat(conditionOptional.isPresent(), is(TRUE));
    assertThat(conditionOptional.get(), is(anyInArray(CAR_BODY_KEY_AS_TEXT,
						      newArrayList(withQuotes(BODY.toString()),
								   withQuotes(BODY.toString())))));
  }

  private String withQuotes(final String body) {
    return format("'%s'", body);
  }


  @Test
  public void when_Province_to_be_queried_is_empty_then_build_returns_no_condition() {
    assertThat(queryBuilder.withProvince(empty()).build(), is(empty()));
  }

  @Test
  public void when_Province_to_be_queried_is_specified_then_build_condtion() {
    Optional<String> conditionOptional = queryBuilder.withProvince(of(PROVINCE)).build();

    assertThat(conditionOptional.isPresent(), is(TRUE));
    assertThat(conditionOptional.get(), is(equalTo(PROVINCE_KEY_AS_TEXT, PROVINCE.toString())));
  }

  @Test
  public void when_Status_to_be_queried_is_empty_then_build_returns_no_condition() {
    assertThat(queryBuilder.withStatus(empty()).build(), is(empty()));
  }

  @Test
  public void when_Status_to_be_queried_is_specified_then_build_condtion() {
    Optional<String> conditionOptional = queryBuilder.withStatus(of(CREATED)).build();

    assertThat(conditionOptional.isPresent(), is(TRUE));
    assertThat(conditionOptional.get(), is(equalTo(STATUS_KEY, CREATED.toString())));
  }

  @Test
  public void when_yearFrom_to_be_queried_is_empty_then_build_returns_no_condition() {
    assertThat(queryBuilder.yearFrom(empty()).build(), is(empty()));
  }

  @Test
  public void when_yearFrom_to_be_queried_is_specified_then_build_condtion() {
    Optional<String> conditionOptional = queryBuilder.yearFrom(of(TEST_INTEGER)).build();

    assertThat(conditionOptional.isPresent(), is(TRUE));
    assertThat(conditionOptional.get(), is(greaterThanOrEqual(CAR_YEAR_KEY_AS_TEXT, TEST_INTEGER)));
  }

  @Test
  public void when_yearTo_to_be_queried_is_empty_then_build_returns_no_condition() {
    assertThat(queryBuilder.yearFrom(empty()).build(), is(empty()));
  }

  @Test
  public void when_yearTo_to_be_queried_is_specified_then_build_condtion() {
    Optional<String> conditionOptional = queryBuilder.yearTo(of(TEST_INTEGER)).build();

    assertThat(conditionOptional.isPresent(), is(TRUE));
    assertThat(conditionOptional.get(), is(lessThanOrEqualTo(CAR_YEAR_KEY_AS_TEXT, TEST_INTEGER)));
  }

}
