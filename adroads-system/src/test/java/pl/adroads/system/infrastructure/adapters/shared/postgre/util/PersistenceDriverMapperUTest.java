package pl.adroads.system.infrastructure.adapters.shared.postgre.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.skife.jdbi.v2.StatementContext;

import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceDriver;
import pl.adroads.system.infrastructure.adapters.shared.ObjectSerializer;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PersistenceDriverMapperUTest {

  private static final int INTEGER = 1;
  private static final String DATA_COLUMN = "data";
  private static final Class<PersistenceDriver> PERSISTENCE_DRIVER_CLASS = PersistenceDriver.class;
  @Mock
  private ObjectSerializer serializer;

  @InjectMocks
  private PersistenceDriverMapper persistenceDriverMapper;
  @Mock
  private ResultSet mockResultSet;
  @Mock
  private StatementContext mockStatementContext;
  private final String JSON_STRING = "temp";
  @Mock
  private PersistenceDriver mockPersistenceDriver;

  @Test
  public void verifyMapMethodFetchesJSONFromResultSetAndDelegatesToObjectSerializer()
      throws SQLException {
    when(mockResultSet.getString(DATA_COLUMN)).thenReturn(JSON_STRING);
    when(serializer.deserialize(JSON_STRING, PERSISTENCE_DRIVER_CLASS))
	.thenReturn(mockPersistenceDriver);
    PersistenceDriver persistenceDriver = persistenceDriverMapper
	.map(INTEGER, mockResultSet, mockStatementContext);

    assertThat(persistenceDriver, is(mockPersistenceDriver));
  }


}
