package pl.adroads.system.infrastructure.adapters.shared.exceptions.localization;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static pl.adroads.system.domain.driver.exception.DriverDomainError.DRIVER_ALREADY_EXISTS;

@RunWith(MockitoJUnitRunner.class)
public class ErrorLocalizationMapperUTest {

  private static final String DRIVER_ALREADY_EXISTS_MESSAGE = "%s istnieje juz w systemie";
  private ErrorLocalizationMapper errorLocalizationMapper = new ErrorLocalizationMapper();

  @Test
  public void when_existing_type_is_specified_then_return_message() {
    String message = errorLocalizationMapper.getMessageFor(DRIVER_ALREADY_EXISTS);

    assertThat(message, is(DRIVER_ALREADY_EXISTS_MESSAGE));
  }

}
