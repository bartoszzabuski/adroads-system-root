package pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.assemblers;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.CommonTestFixtures;
import pl.adroads.system.domain.campaign.model.Campaign;
import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model.PersistenceCampaign;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static pl.adroads.system.CommonTestFixtures.*;

@RunWith(MockitoJUnitRunner.class)
public class PersistenceCampaignAssemblerUTest {

  private PersistenceCampaignAssembler persistenceCampaignAssembler
      = new PersistenceCampaignAssembler();

  @Mock
  private Campaign mockCampaign;

  @Test
  public void test_assemble_works_correctly() {
    when(mockCampaign.getUuid()).thenReturn(CAMPAIGN_ID);
    when(mockCampaign.getName()).thenReturn(CAMPAIGN_NAME);
    when(mockCampaign.getDescription()).thenReturn(CAMPAIGN_DESCRIPTION);
    when(mockCampaign.getStartDateTime()).thenReturn(START_TIME);
    when(mockCampaign.getEndDateTime()).thenReturn(END_TIME);
    when(mockCampaign.getCampaignStatus()).thenReturn(CAMPAIGN_STATUS);
//    when(mockCampaign.getClient()).thenReturn(CLIENT_UUID);
//    when(mockCampaign.getDrivers()).thenReturn(CAMPAIGN_DRIVERS);

    PersistenceCampaign persistenceCampaign = persistenceCampaignAssembler.assemble(mockCampaign);

    assertThat(persistenceCampaign.getUuid(), equalTo(CAMPAIGN_ID.getValue().toString()));
    assertThat(persistenceCampaign.getName(), equalTo(CAMPAIGN_NAME));
    assertThat(persistenceCampaign.getDescription(), equalTo(CAMPAIGN_DESCRIPTION));
    assertThat(persistenceCampaign.getCampaignStatus(), equalTo(CAMPAIGN_STATUS.name()));
    assertThat(persistenceCampaign.getStartDate(), equalTo(START_TIME.toString()));
    assertThat(persistenceCampaign.getEndDate(), equalTo(END_TIME.toString()));
//    assertThat(persistenceCampaign.getClient(), equalTo(CLIENT_UUID.toString()));
//    assertDriversList(persistenceCampaign.getDrivers(), mockCampaign.getDrivers());

  }

//  private void assertDriversList(final Set<String> persistenceDrivers, final Set<CampaignDriver> drivers) {
//    assertThat(persistenceDrivers.size(), equalTo(drivers.size()));
//    for (CampaignDriver driver : drivers) {
//      assertThat(persistenceDrivers.contains(driver), equalTo(true));
//    }
//  }


}
