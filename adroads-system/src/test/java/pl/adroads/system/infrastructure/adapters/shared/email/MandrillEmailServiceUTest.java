package pl.adroads.system.infrastructure.adapters.shared.email;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.application.user.email.MandrillEmailService;
import pl.adroads.system.domain.user.model.User;
import pl.adroads.system.infrastructure.adapters.shared.email.MandrillFacade;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.adroads.system.CommonTestFixtures.EMAIL;
import static pl.adroads.system.CommonTestFixtures.EMAIL_STRING;
import static pl.adroads.system.CommonTestFixtures.URL;
import static pl.adroads.system.CommonTestFixtures.URL_STRING;


@RunWith(MockitoJUnitRunner.class)
public class MandrillEmailServiceUTest {

  @Mock
  private MandrillFacade mandrillFacade;
  @InjectMocks
  private MandrillEmailService mandrillEmailService;

  @Mock
  private User mockUser;

  @Test
  public void verifySendNewUserActivationEmailDelegatesToMandrillFacade() {
    when(mockUser.getEmail()).thenReturn(EMAIL);

    mandrillEmailService.sendEmailConfirmationTo(mockUser, URL);

    verify(mandrillFacade).sendNewUserActivationEmail(EMAIL_STRING, URL_STRING);
  }
}

