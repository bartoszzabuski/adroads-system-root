package pl.adroads.system.infrastructure.locking;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.junit.Assert.*;

import javax.inject.Inject;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import pl.adroads.system.config.RootApplicationConfig;

import com.hazelcast.core.IMap;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RootApplicationConfig.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ActiveProfiles("dev")
public class HazelcastMapEntryLockITest {

    private final static Long KEY_FIVE = 5L;
    private final static Long KEY_TEN = 10L;
    private final static Long KEY_TWENTY = 20L;

    private static boolean LOCK_1;
    private static boolean LOCK_2;

    private static Exception CAUGHT_EXCEPTION_1;
    private static Exception CAUGHT_EXCEPTION_2;

    @Inject
    private HazelcastLockStore campaignRepositoryLockStore;

    private IMap<String, String> store = null;

    @BeforeClass
    public static void init() {
        System.setProperty("jms.broker.url", "noActiveMqForITest");
    }

    @AfterClass
    public static void destroy() {
        System.clearProperty("jms.broker.url");
    }

    @Before
    public void before() {
        store = campaignRepositoryLockStore.getMap();
    }

    @Test
    public void testTryLockThread1AcquiresLocksThread2FailToAcquireLock() throws InterruptedException {

        final HazelcastMapEntryLock testObj1 = new HazelcastMapEntryLock(KEY_FIVE.toString(), store);
        final HazelcastMapEntryLock testObj2 = new HazelcastMapEntryLock(KEY_FIVE.toString(), store);

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                LOCK_1 = testObj1.tryLock();
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                LOCK_2 = testObj2.tryLock();
            }
        });
        t1.setPriority(Thread.MAX_PRIORITY);
        t2.setPriority(Thread.MIN_PRIORITY);
        t1.start();
        t1.join();
        t2.start();
        t2.join();
        assertTrue("try lock should be successful", LOCK_1);
        assertFalse("try lock should not be successful", LOCK_2);
    }

    @Test
    public void testLockThread1AcquiresLockThread2AcquiresLockWithDifferentKey() throws InterruptedException {
        final HazelcastMapEntryLock testObj1 = new HazelcastMapEntryLock(KEY_FIVE.toString(), store);
        final HazelcastMapEntryLock testObj2 = new HazelcastMapEntryLock(KEY_TEN.toString(), store);

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                catchException(testObj1).lock();
                CAUGHT_EXCEPTION_1 = caughtException();
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                catchException(testObj2).lock();
                CAUGHT_EXCEPTION_2 = caughtException();
            }
        });
        t1.start();
        t1.join();
        t2.start();
        t2.join();
        assertNull(CAUGHT_EXCEPTION_1);
        assertNull(CAUGHT_EXCEPTION_2);
    }

    @Test
    public void testLockThread1AcquiresLocksSleepsAndReleasesTheLock() throws InterruptedException {
        final HazelcastMapEntryLock testObj1 = new HazelcastMapEntryLock(KEY_TWENTY.toString(), store);
        final HazelcastMapEntryLock testObj2 = new HazelcastMapEntryLock(KEY_TWENTY.toString(), store);

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                catchException(testObj1).lock();
                CAUGHT_EXCEPTION_1 = caughtException();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                testObj1.unlock();
            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                catchException(testObj2).lock();
                CAUGHT_EXCEPTION_2 = caughtException();
            }
        });
        t1.setPriority(Thread.MAX_PRIORITY);
        t2.setPriority(Thread.MIN_PRIORITY);
        t1.start();
        t1.join();
        t2.start();
        t2.join();
        assertNull(CAUGHT_EXCEPTION_1);
        assertNull(CAUGHT_EXCEPTION_2);
    }
}
