package pl.adroads.system.infrastructure.adapters.shared.postgre;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.postgresql.util.PGobject;
import org.springframework.jdbc.core.JdbcTemplate;

import pl.adroads.system.infrastructure.adapters.shared.postgre.util.PGObjectAssembler;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.adroads.system.CommonTestFixtures.FILTER;
import static pl.adroads.system.CommonTestFixtures.ORDER;
import static pl.adroads.system.CommonTestFixtures.TABLE_NAME;
import static pl.adroads.system.CommonTestFixtures.VALID_UUID;

@RunWith(MockitoJUnitRunner.class)
public class PostgreSQLHelperUTest {

  @Mock
  private PGobject mockPGObject;
  @Mock
  private Object mockArg;
  @Mock
  private JdbcTemplate jdbcTemplate;
  @Mock
  private PGObjectAssembler pgObjectAssembler;
  @InjectMocks
  private PostgreSQLHelper postgreSQLHelper;

  private List<PGobject> mockPGObjectList = Arrays.asList(mockPGObject);

  //  TODO SQL string is not test. question is should it be tested? maybe capture SQL and compare against some constant?
  @Test
  public void test_insertAsJSON_assembles_SQL_and_calls_jdbctemplate() {
    postgreSQLHelper.insert(TABLE_NAME, VALID_UUID, mockPGObject);
    verify(jdbcTemplate).update(anyString(), eq(VALID_UUID), eq(mockPGObject));
  }

  //  TODO SQL string is not test. question is should it be tested? maybe capture SQL and compare against some constant?
  @Test
  public void test_findAllPGObject_assembles_SQL_and_calls_jdbctemplate() throws SQLException {
    when(jdbcTemplate.queryForList(anyString(), eq(PGobject.class), eq(mockArg)))
        .thenReturn(mockPGObjectList);
    List<PGobject> foundDrivers = postgreSQLHelper
        .findAllPGObject(TABLE_NAME, FILTER, ORDER, mockArg);

    verify(jdbcTemplate).queryForList(anyString(), eq(PGobject.class), eq(mockArg));
    assertThat(foundDrivers, equalTo(mockPGObjectList));
  }
//
//  public List<PGobject> findAllPGObject(final String tableName, final String aFilterExpression,
//                                        final String anOrderBy,
//                                        final Object... anArguments) throws SQLException {
//    String query =
//        "select data from "
//        + tableName
//        + " where "
//        + pgObjectAssembler.toPGobject(aFilterExpression)
//        + " "
//        + anOrderBy;
//
//    List<PGobject>
//        pGobjectList =
//        jdbcTemplate.queryForList(query, PGobject.class, anArguments);
//    return pGobjectList;
//
//  }

}
