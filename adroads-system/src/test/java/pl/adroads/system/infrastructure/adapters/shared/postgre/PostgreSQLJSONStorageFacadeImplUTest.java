package pl.adroads.system.infrastructure.adapters.shared.postgre;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.postgresql.util.PGobject;

import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceDriver;
import pl.adroads.system.infrastructure.adapters.shared.postgre.util.PGObjectAssembler;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PostgreSQLJSONStorageFacadeImplUTest {

  private static final String DB_TABLE_NAME = "test";
  private static final String JSON_STRING = "json_string";
  private static final UUID MOCK_UUID = UUID.randomUUID();
  private static final String PERSISTENCE_DRIVER_STRING = "persitenceDriverString";
  private static final String EMPTY_STRING = "";
  private static final String FILTER_EXPRESSION = "filterExpression";
  private static final String ORDER_BY = "orderBy";
  private static final Object ARG = new Object();

  @Mock
  private PersistenceDriver persistenceDriver;
  @Mock
  private PostgreSQLHelper postgreSQLHelper;
  @Mock
  private PGObjectAssembler pgObjectAssembler;
  @Mock
  private PGobject mockPGobject;
  @Mock
  private List<PGobject> mockPGobjectList;

  @InjectMocks
  private PostgreSQLJSONStorageFacade<PersistenceDriver> storageCommon
      = new PostgreSQLJSONStorageFacadeImpl();

  @Test
  public void test_insertAsJSON_works_correctly() throws SQLException {
    when(pgObjectAssembler.toPGobject(persistenceDriver)).thenReturn(mockPGobject);
    when(persistenceDriver.getUuid()).thenReturn(String.valueOf(MOCK_UUID));

    storageCommon.insertAsJSON(DB_TABLE_NAME, persistenceDriver);

    verify(pgObjectAssembler).toPGobject(persistenceDriver);
    verify(postgreSQLHelper).insert(DB_TABLE_NAME, MOCK_UUID, mockPGobject);
  }

  @Test
  public void test_insertAsJSON_throws_exception_when_db_has_problems() throws SQLException {
    when(pgObjectAssembler.toPGobject(persistenceDriver)).thenReturn(mockPGobject);
    when(persistenceDriver.getUuid()).thenReturn(String.valueOf(MOCK_UUID));
    doThrow(new RuntimeException()).when(postgreSQLHelper)
        .insert(DB_TABLE_NAME, MOCK_UUID, mockPGobject);
    catchException(storageCommon).insertAsJSON(DB_TABLE_NAME, persistenceDriver);

    verify(pgObjectAssembler).toPGobject(persistenceDriver);
    verify(postgreSQLHelper).insert(DB_TABLE_NAME, MOCK_UUID, mockPGobject);
    assertThat(caughtException().getClass(), equalTo(RuntimeException.class));
  }

  @Test
  public void test_findExact_works_correctly_when_results_found() throws SQLException {
    when(postgreSQLHelper.findAllPGObject(DB_TABLE_NAME, FILTER_EXPRESSION, ORDER_BY, ARG))
        .thenReturn(mockPGobjectList);
    when(mockPGobjectList.size()).thenReturn(1);
    when(mockPGobjectList.get(0)).thenReturn(mockPGobject);
    storageCommon
        .findExact(DB_TABLE_NAME, PersistenceDriver.class, FILTER_EXPRESSION, ORDER_BY, ARG);
    verify(postgreSQLHelper).findAllPGObject(DB_TABLE_NAME, FILTER_EXPRESSION, ORDER_BY, ARG);
    verify(pgObjectAssembler).fromPGobject(PersistenceDriver.class, mockPGobject);
  }

  @Test
  public void test_findExact_works_correctly_when_no_results_found() throws SQLException {
    when(postgreSQLHelper.findAllPGObject(DB_TABLE_NAME, FILTER_EXPRESSION, ORDER_BY, ARG))
        .thenReturn(mockPGobjectList);
    when(mockPGobjectList.size()).thenReturn(0);
    storageCommon
        .findExact(DB_TABLE_NAME, PersistenceDriver.class, FILTER_EXPRESSION, ORDER_BY, ARG);
    verify(postgreSQLHelper).findAllPGObject(DB_TABLE_NAME, FILTER_EXPRESSION, ORDER_BY, ARG);
    verify(pgObjectAssembler).fromPGobject(eq(PersistenceDriver.class), isNull(PGobject.class));
  }

  @Test
  public void test_findExact_throws_exception_when_db_has_problems() throws SQLException {
    doThrow(new RuntimeException()).when(postgreSQLHelper)
        .findAllPGObject(DB_TABLE_NAME, FILTER_EXPRESSION, ORDER_BY, ARG);
    catchException(storageCommon)
        .findExact(DB_TABLE_NAME, PersistenceDriver.class, FILTER_EXPRESSION, ORDER_BY, ARG);
    verify(postgreSQLHelper).findAllPGObject(DB_TABLE_NAME, FILTER_EXPRESSION, ORDER_BY, ARG);
    assertThat(caughtException().getClass(), equalTo(RuntimeException.class));
  }

  @Test
  public void test_findAll_works_correctly_when_results_found() throws SQLException {
    when(postgreSQLHelper.findAllPGObject(DB_TABLE_NAME, FILTER_EXPRESSION, ORDER_BY, ARG))
        .thenReturn(mockPGobjectList);

    storageCommon.findAll(DB_TABLE_NAME, PersistenceDriver.class, FILTER_EXPRESSION, ORDER_BY, ARG);
    verify(postgreSQLHelper).findAllPGObject(DB_TABLE_NAME, FILTER_EXPRESSION, ORDER_BY, ARG);
    verify(pgObjectAssembler).fromPGobjectList(PersistenceDriver.class, mockPGobjectList);
  }

//  @Test
//  public void test_findAll_works_correctly_when_no_results_found() throws SQLException {
//    doThrow(new RuntimeException()).when(postgreSQLHelper).findAllPGObject(DB_TABLE_NAME,
//                                                                           FILTER_EXPRESSION,
//                                                                           ORDER_BY, ARG);
//    catchException(storageCommon).findExact(DB_TABLE_NAME, PersistenceDriver.class,
//                                            FILTER_EXPRESSION,
//                                            ORDER_BY, ARG);
//    verify(postgreSQLHelper).findAllPGObject(DB_TABLE_NAME, FILTER_EXPRESSION, ORDER_BY, ARG);
//    assertThat(caughtException().getClass(), equalTo(RuntimeException.class));
//  }


  @Test
  public void test_findAll_throws_exception_when_db_has_problems() throws SQLException {
    doThrow(new RuntimeException()).when(postgreSQLHelper)
        .findAllPGObject(DB_TABLE_NAME, FILTER_EXPRESSION, ORDER_BY, ARG);
    catchException(storageCommon)
        .findAll(DB_TABLE_NAME, PersistenceDriver.class, FILTER_EXPRESSION, ORDER_BY, ARG);
    verify(postgreSQLHelper).findAllPGObject(DB_TABLE_NAME, FILTER_EXPRESSION, ORDER_BY, ARG);
    assertThat(caughtException().getClass(), equalTo(RuntimeException.class));
  }

//  @Override
//  public E findExact(final String tableName, final Class<E> aType, final String aFilterExpression,
//                     final String anOrderBy, final Object... anArguments) {
//    try {
//      List<PGobject> pgObjectResults = postgreSQLHelper.findAllPGObject(tableName,
//                                                                        aFilterExpression,
//                                                                        anOrderBy, anArguments);
//      PGobject pgObjectResult = (pgObjectResults.size() > 0) ? pgObjectResults.get(0) : null;
//      return postgreSQLHelper.fromPGobject(aType, pgObjectResult);
//    } catch (Exception e) {
//      throw new RuntimeException(
//          "Cannot find exact for " + aType + "(" + aFilterExpression + ") because: " + e
//              .getMessage());
//    }
//  }
}
