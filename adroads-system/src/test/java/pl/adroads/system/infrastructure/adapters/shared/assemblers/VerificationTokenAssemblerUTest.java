package pl.adroads.system.infrastructure.adapters.shared.assemblers;

import static java.lang.Boolean.FALSE;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static pl.adroads.system.CommonTestFixtures.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.domain.user.model.VerificationToken;
import pl.adroads.system.infrastructure.adapters.driving.user.persistence.model.PersistenceVerificationToken;

@RunWith(MockitoJUnitRunner.class)
public class VerificationTokenAssemblerUTest {

    private VerificationTokenAssembler verificationTokenAssembler = new VerificationTokenAssembler();
    @Mock
    private VerificationToken mockVerificationToken;
    @Mock
    private PersistenceVerificationToken mockPersistenceVerificationToken;

    @Test
    public void when_verificationDate_is_null_then_verificationTokenAssembler_constructs_valid_object() {
        when(mockVerificationToken.getToken()).thenReturn(TOKEN_STRING);
        when(mockVerificationToken.getExpiryDate()).thenReturn(EXPIRY_DATE);
        when(mockVerificationToken.getTokenType()).thenReturn(REGISTRATION_CONFIRMATION);
        when(mockVerificationToken.getVerificationDate()).thenReturn(null);
        when(mockVerificationToken.isVerified()).thenReturn(false);
        when(mockVerificationToken.getTimestamp()).thenReturn(TIMESTAMP);

        PersistenceVerificationToken persistenceVerificationToken = verificationTokenAssembler
                .assemble(mockVerificationToken);

        assertThat(persistenceVerificationToken.getToken(), is(TOKEN_STRING));
        assertThat(persistenceVerificationToken.getExpiryDate(), is(EXPIRY_DATE.toString()));
        assertThat(persistenceVerificationToken.getTokenType(), is(REGISTRATION_CONFIRMATION.name()));
        assertThat(persistenceVerificationToken.isVerified(), is(NOT_VERIFIED));
        assertThat(persistenceVerificationToken.getVerificationDate(), is(nullValue()));
        assertThat(persistenceVerificationToken.getTimestamp(), is(TIMESTAMP.toString()));
    }

    @Test
    public void when_verificationDate_is_not_null_then_verificationTokenAssembler_constructs_valid_object() {

        PersistenceVerificationToken persistenceVerificationToken = verificationTokenAssembler
                .assemble(VERIFIED_VERIFICATION_TOKEN);

        assertThat(persistenceVerificationToken.getToken(), is(TOKEN_STRING));
        assertThat(persistenceVerificationToken.getExpiryDate(), is(EXPIRY_DATE.toString()));
        assertThat(persistenceVerificationToken.getTokenType(), is(REGISTRATION_CONFIRMATION.name()));
        assertThat(persistenceVerificationToken.isVerified(), is(NOT_VERIFIED));
        assertThat(persistenceVerificationToken.getVerificationDate(),
                is(VERIFICATION_DATE.toString()));
    }

    @Test
    public void verificationTokenAssembler_disassembles_a_valid_domain_object() {
        when(mockPersistenceVerificationToken.getToken()).thenReturn(TOKEN_STRING);
        when(mockPersistenceVerificationToken.getExpiryDate()).thenReturn(EXPIRY_DATE.toString());
        when(mockPersistenceVerificationToken.getTokenType())
                .thenReturn(REGISTRATION_CONFIRMATION.toString());
        when(mockPersistenceVerificationToken.isVerified()).thenReturn(NOT_VERIFIED);
        when(mockPersistenceVerificationToken.getVerificationDate())
                .thenReturn(VERIFICATION_DATE.toString());
        when(mockPersistenceVerificationToken.getTimestamp()).thenReturn(TIMESTAMP.toString());

        VerificationToken verificationToken = verificationTokenAssembler.disassemble(mockPersistenceVerificationToken);

        assertThat(verificationToken.getToken(), is(TOKEN_STRING));
        assertThat(verificationToken.getExpiryDate(), is(EXPIRY_DATE));
        assertThat(verificationToken.getTokenType(), is(REGISTRATION_CONFIRMATION));
        assertThat(verificationToken.isVerified(), is(NOT_VERIFIED));
        assertThat(verificationToken.getVerificationDate(), is(VERIFICATION_DATE));
    }

    @Test
    public void when_token_is_not_verified_then_verificationTokenAssembler_disassembles_a_valid_domain_object() {

        VerificationToken verificationToken = verificationTokenAssembler
                .disassemble(NOT_VERIFIED_PERSISTENCE_VERIFICATION_TOKEN);

        assertThat(verificationToken.getToken(), is(TOKEN_STRING));
        assertThat(verificationToken.getExpiryDate(), is(EXPIRY_DATE));
        assertThat(verificationToken.getTokenType(), is(REGISTRATION_CONFIRMATION));
        assertThat(verificationToken.isVerified(), is(FALSE));
        assertThat(verificationToken.getVerificationDate(), is(nullValue()));
    }

}
