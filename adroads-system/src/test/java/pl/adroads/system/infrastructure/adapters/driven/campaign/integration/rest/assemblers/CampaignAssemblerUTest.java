package pl.adroads.system.infrastructure.adapters.driven.campaign.integration.rest.assemblers;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static pl.adroads.system.CommonTestFixtures.CAMPAIGN_DESCRIPTION;
import static pl.adroads.system.CommonTestFixtures.CAMPAIGN_NAME;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.api.model.campaign.CreateCampaignRequest;
import pl.adroads.system.domain.campaign.model.Campaign;
import pl.adroads.system.domain.campaign.model.CampaignStatus;

@RunWith(MockitoJUnitRunner.class)
public class CampaignAssemblerUTest {

    private static final ZonedDateTime END_DATE_TIME = ZonedDateTime.now().plusDays(2);
    private static final ZonedDateTime START_DATE_TIME = ZonedDateTime.now().plusDays(1);
    private CampaignAssembler campaignAssembler = new CampaignAssembler();

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ISO_INSTANT.withZone(ZoneId.systemDefault());
    private static final String END_DATE_STRING = END_DATE_TIME.format(FORMATTER);
    private static final String START_DATE_STRING = START_DATE_TIME.format(FORMATTER);

    @Test
    public void test_assemble_works_correctly() {

        Campaign campaign = campaignAssembler.assemble(
                new CreateCampaignRequest(CAMPAIGN_NAME, CAMPAIGN_DESCRIPTION, START_DATE_STRING, END_DATE_STRING));
        assertThat(campaign.getName(), equalTo(CAMPAIGN_NAME));
        assertThat(campaign.getDescription(), equalTo(CAMPAIGN_DESCRIPTION));
        assertThat(campaign.getStartDateTime(), equalTo(START_DATE_TIME.toLocalDateTime()));
        assertThat(campaign.getEndDateTime(), equalTo(END_DATE_TIME.toLocalDateTime()));
        assertThat(campaign.getCampaignStatus(), equalTo(CampaignStatus.CREATED));

    }

}
