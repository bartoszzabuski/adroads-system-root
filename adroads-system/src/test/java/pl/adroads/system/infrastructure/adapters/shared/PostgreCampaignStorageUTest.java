package pl.adroads.system.infrastructure.adapters.shared;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.util.ReflectionTestUtils;

import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model.PersistenceCampaign;
import pl.adroads.system.infrastructure.adapters.shared.postgre.PostgreCampaignStorage;
import pl.adroads.system.infrastructure.adapters.shared.postgre.PostgreSQLJSONStorageFacade;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class PostgreCampaignStorageUTest {

  private String tableName;
  @Mock
  private PostgreSQLJSONStorageFacade<PersistenceCampaign> storageFacade;
  @Mock
  private JdbcTemplate jdbcTemplate;
  @Mock
  private ObjectSerializer serializer;
  @Mock
  private PersistenceCampaign mockPersistenceCampaign;

  @InjectMocks
  private PostgreCampaignStorage postgreCampaignStorage;

  @Before
  public void setup() {
    tableName =
	(String) ReflectionTestUtils.getField(postgreCampaignStorage, "CAMPAIGN_TABLE_NAME");
  }

  @Test
  public void test_saveNewDriver_correctly_delegates_to_PostgreSQLJSONStorageFacade() {

    postgreCampaignStorage.save(mockPersistenceCampaign);
    verify(storageFacade).insertAsJSON(tableName, mockPersistenceCampaign);
  }

}
