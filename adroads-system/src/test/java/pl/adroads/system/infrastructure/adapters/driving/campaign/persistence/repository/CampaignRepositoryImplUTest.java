package pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.domain.campaign.model.Campaign;
import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.assemblers.PersistenceCampaignAssembler;
import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model.PersistenceCampaign;
import pl.adroads.system.infrastructure.adapters.shared.CampaignStorage;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CampaignRepositoryImplUTest {

  @InjectMocks
  private CampaignRepositoryImpl campaignRepository;
  @Mock
  private Campaign mockCampaign;
  @Mock
  private PersistenceCampaignAssembler mockPersistenceCampaignAssembler;
  @Mock
  private CampaignStorage mockCampaignStorage;
  @Mock
  private PersistenceCampaign mockPersistenceCampaign;

  @Test
  public void test_saveNewCampaign_assembles_persistence_object_and_delegates_to_storage() {
    when(mockPersistenceCampaignAssembler.assemble(mockCampaign))
	.thenReturn(mockPersistenceCampaign);

    campaignRepository.store(mockCampaign);

    verify(mockPersistenceCampaignAssembler).assemble(mockCampaign);
    verify(mockCampaignStorage).save(mockPersistenceCampaign);
  }

}
