package pl.adroads.system.infrastructure.adapters.driven.driver.integration.listeners;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.application.user.UserManagementService;
import pl.adroads.system.domain.driver.events.DriverCreatedEvent;
import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.domain.driver.model.DriverId;
import pl.adroads.system.domain.driver.model.Status;
import pl.adroads.system.domain.user.model.User;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.adroads.system.CommonTestFixtures.ADDRESS;
import static pl.adroads.system.CommonTestFixtures.CAR_DETAILS;
import static pl.adroads.system.CommonTestFixtures.DATE_OF_BIRTH;
import static pl.adroads.system.CommonTestFixtures.DRIVER_DETAILS;
import static pl.adroads.system.CommonTestFixtures.EMAIL;
import static pl.adroads.system.CommonTestFixtures.NAME;
import static pl.adroads.system.CommonTestFixtures.SURNAME;
import static pl.adroads.system.CommonTestFixtures.USER_ID;
import static pl.adroads.system.CommonTestFixtures.VALID_UUID;


@RunWith(MockitoJUnitRunner.class)
public class DriversEventsListenerUTest {

  @Mock
  private UserManagementService mockUserManagementService;
  @InjectMocks
  private DriversEventsListener driversEventsListener;
  @Mock
  private DriverCreatedEvent mockDriverCreatedEvent;
  @Mock
  private Driver mockDriver;
  @Captor
  private ArgumentCaptor<User> userCaptor;

  private static final Driver DRIVER = new Driver.DriverBuilder().withID(DriverId.of(VALID_UUID))
      .withName(NAME).withSurname(SURNAME).withDateOfBirth(DATE_OF_BIRTH).withEmail(EMAIL)
      .withAddress(ADDRESS).withStatus(Status.CREATED).withCarDetails(CAR_DETAILS)
      .withDriverDetails(DRIVER_DETAILS).build();

  @Test
  public void test_driverCreated_assemblesUser_and_delegates_to_service() {
    when(mockDriverCreatedEvent.getDriver()).thenReturn(DRIVER);

    driversEventsListener.driverCreated(mockDriverCreatedEvent);

    verify(mockUserManagementService).register(userCaptor.capture());
    User user = userCaptor.getValue();
    assertThat(user.getUuid(), is(USER_ID));
    assertThat(user.getEmail(), is(EMAIL));
    assertThat(user.getVerificationToken(), is(nullValue()));

  }
}
