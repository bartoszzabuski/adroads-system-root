package pl.adroads.system.infrastructure.adapters.shared.email;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.LoggerFactory;

import pl.adroads.system.application.user.email.LogEmailService;
import pl.adroads.system.domain.user.model.User;
import pl.adroads.system.infrastructure.adapters.shared.ObjectSerializer;

import java.net.URI;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;

import static java.lang.Boolean.TRUE;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LogEmailServiceUTest {

  @Mock
  private ObjectSerializer mockObjectSerializer;
//  @Mock
//  private Logger mockLogger;

  @InjectMocks
  private LogEmailService logEmailService;

  @Mock
  private Appender mockAppender;
  //Captor is genericised with ch.qos.logback.classic.spi.LoggingEvent
  @Captor
  private ArgumentCaptor<LoggingEvent> captorLoggingEvent;

  private static final URI VERIFICATION_URI = URI.create("url");

  //I've cheated a little here and added the mockAppender to the root logger
  //It's not quite necessary but it also shows you how it can be done
  @Before
  public void setup() {
    final Logger logger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
    logger.addAppender(mockAppender);
  }

  //Always have this teardown otherwise we can stuff up our expectations. Besides, it's
  //good coding practise
  @After
  public void teardown() {
    final Logger logger = (Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
    logger.detachAppender(mockAppender);
  }

  @Mock
  private User mockUser;
  private static final String serializedDriver = "driverString";

  @Test
  public void test_send_email_delegates_to_log() {
    when(mockObjectSerializer.serialize(mockUser)).thenReturn(serializedDriver);

    logEmailService.sendEmailConfirmationTo(mockUser, VERIFICATION_URI);

    //Now verify our logging interactions
    verify(mockAppender).doAppend(captorLoggingEvent.capture());
    //Having a genricised captor means we don't need to cast
    final LoggingEvent loggingEvent = captorLoggingEvent.getValue();
    //Check log level is correct
    assertThat(loggingEvent.getLevel(), is(Level.INFO));
    //Check the message being logged is correct
    assertThat(loggingEvent.getFormattedMessage().contains(serializedDriver), is(TRUE));
  }

}
