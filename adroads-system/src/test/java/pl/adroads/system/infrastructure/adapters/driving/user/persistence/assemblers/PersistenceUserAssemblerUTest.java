package pl.adroads.system.infrastructure.adapters.driving.user.persistence.assemblers;

import static java.lang.Boolean.FALSE;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.mockito.Mockito.when;
import static org.springframework.test.util.MatcherAssertionErrors.assertThat;
import static pl.adroads.system.CommonTestFixtures.EMAIL;
import static pl.adroads.system.CommonTestFixtures.EMAIL_STRING;
import static pl.adroads.system.CommonTestFixtures.PERSISTENCE_SESSION_TOKEN;
import static pl.adroads.system.CommonTestFixtures.SESSION_TOKEN;
import static pl.adroads.system.CommonTestFixtures.TEST_PASSWORD;
import static pl.adroads.system.CommonTestFixtures.USER_ID;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.domain.user.model.User;
import pl.adroads.system.domain.user.model.UserType;
import pl.adroads.system.domain.user.model.VerificationToken;
import pl.adroads.system.infrastructure.adapters.driving.user.persistence.model.PersistenceUser;
import pl.adroads.system.infrastructure.adapters.driving.user.persistence.model.PersistenceVerificationToken;
import pl.adroads.system.infrastructure.adapters.shared.assemblers.VerificationTokenAssembler;

@RunWith(MockitoJUnitRunner.class)
public class PersistenceUserAssemblerUTest {

    @Mock
    private User mockUser;
    @Mock
    private VerificationToken mockVerificationToken;
    @Mock
    private PersistenceUser mockPersistence;
    @Mock
    private PersistenceVerificationToken mockPersistenceVerificationToken;
    @Mock
    private VerificationTokenAssembler mockVerificationTokenAssembler;
    @InjectMocks
    private PersistenceUserAssembler persistenceUserAssembler;

    @Test
    public void when_assembles_a_valid_domain_object() {
        when(mockUser.getUuid()).thenReturn(USER_ID);
        when(mockUser.getEmail()).thenReturn(EMAIL);
        when(mockUser.getVerificationToken()).thenReturn(mockVerificationToken);
        when(mockVerificationTokenAssembler.assemble(mockVerificationToken))
                .thenReturn(mockPersistenceVerificationToken);
        when(mockUser.getUserType()).thenReturn(UserType.DRIVER);
        when(mockUser.getPassword()).thenReturn(Optional.of(TEST_PASSWORD));
        when(mockUser.getSessionToken()).thenReturn(Optional.of(SESSION_TOKEN));

        PersistenceUser persistenceUser = persistenceUserAssembler.assemble(mockUser);

        assertThat(persistenceUser.getUuid(), is(USER_ID.getValue().toString()));
        assertThat(persistenceUser.getEmail(), is(EMAIL_STRING));
        assertThat(persistenceUser.getPersistenceVerificationToken(), is(mockPersistenceVerificationToken));
        assertThat(persistenceUser.getUserType(), is(UserType.DRIVER.name()));
        assertThat(persistenceUser.getPassword(), is(TEST_PASSWORD));
        assertThat(persistenceUser.getPersistenceSessionToken().getToken(), is(SESSION_TOKEN.getToken()));
        assertThat(persistenceUser.getPersistenceSessionToken().getTimestamp(),
                is(SESSION_TOKEN.getTimestamp().toString()));
        assertThat(persistenceUser.getPersistenceSessionToken().getExpiryDate(),
                is(SESSION_TOKEN.getExpiryDate().toString()));

    }

    @Test
    public void when_password_or_sessionToken_are_absent_then_assembler_consturct_persistence_object_with_nulls() {
        when(mockUser.getUuid()).thenReturn(USER_ID);
        when(mockUser.getEmail()).thenReturn(EMAIL);
        when(mockUser.getVerificationToken()).thenReturn(mockVerificationToken);
        when(mockVerificationTokenAssembler.assemble(mockVerificationToken))
                .thenReturn(mockPersistenceVerificationToken);
        when(mockUser.getUserType()).thenReturn(UserType.DRIVER);
        when(mockUser.getPassword()).thenReturn(Optional.empty());
        when(mockUser.getSessionToken()).thenReturn(Optional.empty());

        PersistenceUser persistenceUser = persistenceUserAssembler.assemble(mockUser);

        assertThat(persistenceUser.getUuid(), is(USER_ID.getValue().toString()));
        assertThat(persistenceUser.getEmail(), is(EMAIL_STRING));
        assertThat(persistenceUser.getPersistenceVerificationToken(), is(mockPersistenceVerificationToken));
        assertThat(persistenceUser.getUserType(), is(UserType.DRIVER.name()));
        assertThat(persistenceUser.getPassword(), is(nullValue()));
        assertThat(persistenceUser.getPersistenceSessionToken(), is(nullValue()));

    }

    @Test
    public void persistenceUserAssembler_disassembles_a_valid_domain_object() {
        when(mockPersistence.getUuid()).thenReturn(USER_ID.getValue().toString());
        when(mockPersistence.getEmail()).thenReturn(EMAIL_STRING);
        when(mockPersistence.getPersistenceVerificationToken())
                .thenReturn(mockPersistenceVerificationToken);
        when(mockVerificationTokenAssembler.disassemble(mockPersistenceVerificationToken))
                .thenReturn(mockVerificationToken);
        when(mockPersistence.getUserType()).thenReturn(UserType.DRIVER.name());
        when(mockPersistence.getPassword()).thenReturn(TEST_PASSWORD);
        when(mockPersistence.getPersistenceSessionToken()).thenReturn(PERSISTENCE_SESSION_TOKEN);

        User user = persistenceUserAssembler.disassemble(mockPersistence);

        assertThat(user.getUuid(), is(USER_ID));
        assertThat(user.getEmail(), is(EMAIL));
        assertThat(user.getVerificationToken(), is(mockVerificationToken));
        assertThat(user.getUserType(), is(UserType.DRIVER));
        assertThat(user.getPassword().get(), is(TEST_PASSWORD));
        assertThat(user.getSessionToken().get(), is(SESSION_TOKEN));

    }

    @Test
    public void when_persistenceUserAssembler_has_null_password_and_token_then_assembler_disassembles_an_object_with_empty_optionals() {
        when(mockPersistence.getUuid()).thenReturn(USER_ID.getValue().toString());
        when(mockPersistence.getEmail()).thenReturn(EMAIL_STRING);
        when(mockPersistence.getPersistenceVerificationToken())
                .thenReturn(mockPersistenceVerificationToken);
        when(mockVerificationTokenAssembler.disassemble(mockPersistenceVerificationToken))
                .thenReturn(mockVerificationToken);
        when(mockPersistence.getUserType()).thenReturn(UserType.DRIVER.name());
        when(mockPersistence.getPassword()).thenReturn(null);
        when(mockPersistence.getPersistenceSessionToken()).thenReturn(null);

        User user = persistenceUserAssembler.disassemble(mockPersistence);

        assertThat(user.getUuid(), is(USER_ID));
        assertThat(user.getEmail(), is(EMAIL));
        assertThat(user.getVerificationToken(), is(mockVerificationToken));
        assertThat(user.getUserType(), is(UserType.DRIVER));
        assertThat(user.getPassword().isPresent(), is(FALSE));
        assertThat(user.getSessionToken().isPresent(), is(FALSE));

    }

}
