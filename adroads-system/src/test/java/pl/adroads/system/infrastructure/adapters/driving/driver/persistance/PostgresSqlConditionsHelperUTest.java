package pl.adroads.system.infrastructure.adapters.driving.driver.persistance;

import org.junit.Test;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.PostgresSqlConditionsHelper.anyInArray;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.PostgresSqlConditionsHelper.equalTo;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.PostgresSqlConditionsHelper.greaterThanOrEqual;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.PostgresSqlConditionsHelper.iLike;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.PostgresSqlConditionsHelper.lessThanOrEqualTo;

public class PostgresSqlConditionsHelperUTest {

  private static final String KEY = "key";
  private static final String STRING_VALUE = "value";
  private static final Integer INTEGER_VALUE = 123;

  @Test
  public void test_iLike_returns_expected_string() {
    assertThat(iLike(KEY, STRING_VALUE), is("key ILIKE 'value'"));
  }

  @Test
  public void test_equalTo_returns_expected_string() {
    assertThat(equalTo(KEY, STRING_VALUE), is("key = 'value'"));
  }

  @Test
  public void test_greaterThanOrEqual_returns_expected_string() {
    assertThat(greaterThanOrEqual(KEY, INTEGER_VALUE), is("CAST(key AS int) >= 123"));
  }

  @Test
  public void test_lessThanOrEqual_returns_expected_string() {
    assertThat(lessThanOrEqualTo(KEY, INTEGER_VALUE), is("CAST(key AS int) <= 123"));
  }

  @Test
  public void test_anyInArray_returns_expected_string() {
    assertThat( anyInArray(KEY, newArrayList(STRING_VALUE, STRING_VALUE)),
	       is("key ??| ARRAY[value, value]"));
  }


}
