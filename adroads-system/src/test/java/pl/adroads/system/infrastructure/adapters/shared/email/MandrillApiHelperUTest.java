package pl.adroads.system.infrastructure.adapters.shared.email;

import com.microtripit.mandrillapp.lutung.view.MandrillMessage;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.infrastructure.adapters.shared.email.MandrillApiHelper;

import java.util.List;

import static com.microtripit.mandrillapp.lutung.view.MandrillMessage.MergeVar;
import static com.microtripit.mandrillapp.lutung.view.MandrillMessage.MergeVarBucket;
import static com.microtripit.mandrillapp.lutung.view.MandrillMessage.Recipient;
import static java.lang.Boolean.TRUE;
import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static pl.adroads.system.CommonTestFixtures.EMAIL_STRING;

@RunWith(MockitoJUnitRunner.class)
public class MandrillApiHelperUTest {

  private static final Recipient RECIPIENT_1 = new Recipient();
  private static final Recipient RECIPIENT_2 = new Recipient();
  private static final String MERGE_VAR_CONTENT = "CONTENT";
  private static final String MERGE_VAR_NAME = "NAME";
  private static final MergeVar[] MERGE_VARS_ARRAY = new MergeVar[0];

  static {
    RECIPIENT_1.setEmail(EMAIL_STRING);
  }

  private MandrillApiHelper mandrillApiHelper = new MandrillApiHelper();
  @Mock
  private MergeVarBucket mockMergeVarBucket1;
  @Mock
  private MergeVarBucket mockMergeVarBucket2;
  @Mock
  private MergeVar mockMergeVar1;
  @Mock
  private MergeVar mockMergeVar2;
  @Mock
  private List<MergeVarBucket> mockMergeVarBuckets;
  @Mock
  private List<Recipient> mockRecipients;

  @Test
  public void verify_getMandrillMessageFor_constructs_a_valid_object() {
    MandrillMessage message = mandrillApiHelper
	.getMandrillMessageFor(mockRecipients, mockMergeVarBuckets);

    assertThat(message.getTo(), is(mockRecipients));
    assertThat(message.getMergeVars(), is(mockMergeVarBuckets));
    assertThat(message.getPreserveRecipients(), is(TRUE));
  }

  @Test
  public void verify_getRecipientFor_constructs_valid_recipents() {
    Recipient recipient = mandrillApiHelper.getRecipientFor(EMAIL_STRING);

    assertThat(recipient.getEmail(), is(EMAIL_STRING));
  }

  @Test
  public void verify_getListOfRecipients_assembles_list_of_recipients() {
    List<Recipient> recipientsList = mandrillApiHelper
	.getListOfRecipients(RECIPIENT_1, RECIPIENT_2);

    assertThat(recipientsList, containsInAnyOrder(RECIPIENT_1, RECIPIENT_2));
  }

  @Test
  public void verify_getMergeVarFor_constructs_a_valid_object() {
    MergeVar mergeVar = mandrillApiHelper.getMergeVarFor(MERGE_VAR_NAME, MERGE_VAR_CONTENT);

    assertThat(mergeVar.getName(), is(MERGE_VAR_NAME));
    assertThat(mergeVar.getContent(), is(MERGE_VAR_CONTENT));
  }

  @Test
  public void verify_getMergeVarsArray_constructs_a_valid_list_of_objects() {
    MergeVar[] mergeVarArray = mandrillApiHelper.getMergeVarsArray(mockMergeVar1, mockMergeVar2);

    assertThat(asList(mergeVarArray), containsInAnyOrder(mockMergeVar1, mockMergeVar2));
  }

  @Test
  public void verify_getMergeVarBucketFor_constructs_a_valid_object() {
    MergeVarBucket mergeVarBucket = mandrillApiHelper
	.getMergeVarBucketFor(RECIPIENT_1, MERGE_VARS_ARRAY);

    assertThat(mergeVarBucket.getRcpt(), is(RECIPIENT_1.getEmail()));
    assertThat(mergeVarBucket.getVars(), is(MERGE_VARS_ARRAY));
  }

  @Test
  public void verify_getListOfMergeVarBuckets_constructs_a_list_of_objects() {
    List<MergeVarBucket> mergeVarBucketList = mandrillApiHelper
	.getListOfMergeVarBuckets(mockMergeVarBucket1, mockMergeVarBucket2);

    assertThat(mergeVarBucketList, containsInAnyOrder(mockMergeVarBucket1, mockMergeVarBucket2));
  }

}
