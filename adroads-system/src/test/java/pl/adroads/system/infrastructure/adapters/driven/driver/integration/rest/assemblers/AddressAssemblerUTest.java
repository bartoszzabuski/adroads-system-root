package pl.adroads.system.infrastructure.adapters.driven.driver.integration.rest.assemblers;

import org.junit.Test;

import pl.adroads.api.model.shared.AddressResource;
import pl.adroads.system.domain.driver.model.Address;
import pl.adroads.system.domain.driver.model.Street;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static pl.adroads.system.CommonTestFixtures.ADDRESS_RESOURCE;
import static pl.adroads.system.CommonTestFixtures.CITY;
import static pl.adroads.system.CommonTestFixtures.POSTCODE;
import static pl.adroads.system.CommonTestFixtures.PROVINCE;
import static pl.adroads.system.CommonTestFixtures.STREET;

public class AddressAssemblerUTest {

  private static final AddressResource NULL_ADDRESS_RESOURCE = null;

  private AddressAssembler addressAssembler = new AddressAssembler();

  @Test
  public void when_addressResource_is_null_then_assembler_returns_null() {
    Address address = addressAssembler.assemble(NULL_ADDRESS_RESOURCE);
    assertThat(address, is(nullValue()));
  }

  @Test
  public void when_addressResource_is_not_null_then_assembler_returns_valid_address() {
    Address address = addressAssembler.assemble(ADDRESS_RESOURCE);
    assertThat(address, is(notNullValue()));
    assertThat(address.getCity(), is(CITY));
    assertThat(address.getStreet(), is(STREET));
    assertThat(address.getPostcode(), is(POSTCODE));
    assertThat(address.getProvince(), is(PROVINCE));
  }

}
