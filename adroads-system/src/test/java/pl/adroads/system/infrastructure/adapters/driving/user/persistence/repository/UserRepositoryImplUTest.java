package pl.adroads.system.infrastructure.adapters.driving.user.persistence.repository;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.adroads.system.CommonTestFixtures.EMAIL;
import static pl.adroads.system.CommonTestFixtures.EMAIL_STRING;
import static pl.adroads.system.CommonTestFixtures.PERSISTENCE_USER;
import static pl.adroads.system.CommonTestFixtures.SESSION_TOKEN_VALUE;
import static pl.adroads.system.CommonTestFixtures.USER;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.domain.user.model.User;
import pl.adroads.system.infrastructure.adapters.driving.user.persistence.UserStore;
import pl.adroads.system.infrastructure.adapters.driving.user.persistence.assemblers.PersistenceUserAssembler;
import pl.adroads.system.infrastructure.adapters.driving.user.persistence.model.PersistenceUser;

@RunWith(MockitoJUnitRunner.class)
public class UserRepositoryImplUTest {

    @Mock
    private UserStore mockUserStore;
    @Mock
    private PersistenceUserAssembler mockPersistenceUserAssembler;

    @InjectMocks
    private UserRepositoryImpl userRepositoryImpl;
    @Mock
    private PersistenceUser mockPersistenceUser;
    @Mock
    private User mockUser;

    @Test
    public void test_store_method_assembles_persistence_object_and_delegates_to_store() {
        when(mockPersistenceUserAssembler.assemble(USER)).thenReturn(PERSISTENCE_USER);

        userRepositoryImpl.store(USER);

        verify(mockUserStore).store(PERSISTENCE_USER);
    }

    @Test
    public void test_findByEmail_delegates_to_assembler_and_repository() {

        when(mockUserStore.findByEmail(EMAIL_STRING)).thenReturn(PERSISTENCE_USER);
        when(mockPersistenceUserAssembler.disassemble(PERSISTENCE_USER)).thenReturn(USER);

        Optional<User> foundUser = userRepositoryImpl.findByEmail(EMAIL);

        assertThat(foundUser, is(Optional.of(USER)));
    }

    @Test
    public void test_update_delegates_to_assembler_and_repository() {

        when(mockPersistenceUserAssembler.assemble(USER)).thenReturn(PERSISTENCE_USER);

        userRepositoryImpl.update(USER);

        verify(mockUserStore).update(PERSISTENCE_USER);
    }

    @Test
    public void when_user_with_sessionToken_not_found_in_db_then_findBySessionToken_returns_empty() {
        when(mockUserStore.findBySessionToken(SESSION_TOKEN_VALUE)).thenReturn(null);

        Optional<User> user = userRepositoryImpl.findBySessionToken(SESSION_TOKEN_VALUE);

        assertThat(user.isPresent(), is(FALSE));
    }

    @Test
    public void when_user_with_sessionToken_found_in_db_then_findBySessionToken_returns_empty() {
        when(mockUserStore.findBySessionToken(SESSION_TOKEN_VALUE)).thenReturn(mockPersistenceUser);
        when(mockPersistenceUserAssembler.disassemble(mockPersistenceUser)).thenReturn(mockUser);

        Optional<User> user = userRepositoryImpl.findBySessionToken(SESSION_TOKEN_VALUE);

        assertThat(user.isPresent(), is(TRUE));
        assertThat(user.get(), is(mockUser));
    }

}
