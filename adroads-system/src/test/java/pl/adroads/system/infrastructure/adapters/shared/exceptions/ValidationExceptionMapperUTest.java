package pl.adroads.system.infrastructure.adapters.shared.exceptions;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.api.model.shared.ExceptionResponse;
import pl.adroads.system.common.util.ValidationException;
import pl.adroads.system.infrastructure.adapters.shared.ObjectSerializer;
import pl.adroads.system.infrastructure.adapters.shared.exceptions.localization.ValidationLocalizationMapper;

import javax.ws.rs.core.Response;

import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static pl.adroads.system.domain.driver.exception.DriverValidationFields.ADDRESS;
import static pl.adroads.system.common.util.errors.ValidationError.VALUE_REQUIRED;

@RunWith(MockitoJUnitRunner.class)
public class ValidationExceptionMapperUTest {

  private static final String MOCK_MESSAGE = "mockMessage";
  private static final String SERIALIZED_EXCEPTION_RESPONSE = "mockResponse";
  @Mock
  private ValidationLocalizationMapper mockValidationLocalizationMapper;

  @Mock
  private ObjectSerializer mockObjectSerializer;

  @InjectMocks
  private ValidationExceptionMapper validationExceptionMapper;

  private ValidationException validationException = ValidationException.builder(VALUE_REQUIRED)
      .withField(ADDRESS).build();

  @Test
  public void when_valid_exception_provided_then_toResponse_constructs_valid_exceptionResponse() {

    when(mockValidationLocalizationMapper.getMappedMessageFor(ADDRESS, VALUE_REQUIRED))
        .thenReturn(MOCK_MESSAGE);
    when(mockObjectSerializer
             .serialize(new ExceptionResponse(VALUE_REQUIRED.getType(), MOCK_MESSAGE)))
        .thenReturn(SERIALIZED_EXCEPTION_RESPONSE);

    Response response = validationExceptionMapper.toResponse(validationException);

    assertThat(response.getStatus(), is(SC_BAD_REQUEST));
    assertThat(response.getEntity(), is(SERIALIZED_EXCEPTION_RESPONSE));
//    assertThat(response.getMediaType().toString(), is(CONTENT_TYPE_APPLICATION_JSON));
  }

}
