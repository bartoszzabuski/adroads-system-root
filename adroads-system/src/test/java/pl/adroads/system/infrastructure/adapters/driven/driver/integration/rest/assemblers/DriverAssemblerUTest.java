package pl.adroads.system.infrastructure.adapters.driven.driver.integration.rest.assemblers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import pl.adroads.api.model.driver.CreateDriverRequest;
import pl.adroads.api.model.shared.CarDetailsResource;
import pl.adroads.api.model.shared.DriverDetailsResource;
import pl.adroads.api.model.shared.WindscreenDimensionsResource;
import pl.adroads.system.domain.driver.model.CarDetails;
import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.domain.driver.model.DriverDetails;
import pl.adroads.system.domain.driver.model.Status;
import pl.adroads.system.domain.driver.model.WindscreenDimensions;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static pl.adroads.system.CommonTestFixtures.ADDRESS;
import static pl.adroads.system.CommonTestFixtures.ADDRESS_RESOURCE;
import static pl.adroads.system.CommonTestFixtures.BODY;
import static pl.adroads.system.CommonTestFixtures.CAR_DETAILS;
import static pl.adroads.system.CommonTestFixtures.CAR_DETAILS_RESOURCE;
import static pl.adroads.system.CommonTestFixtures.COLOUR;
import static pl.adroads.system.CommonTestFixtures.COLOUR_VALUE;
import static pl.adroads.system.CommonTestFixtures.DATE_OF_BIRTH;
import static pl.adroads.system.CommonTestFixtures.DATE_OF_BIRTH_STRING;
import static pl.adroads.system.CommonTestFixtures.DRIVER_DETAILS;
import static pl.adroads.system.CommonTestFixtures.DRIVER_DETAILS_RESOURCE;
import static pl.adroads.system.CommonTestFixtures.DRIVER_TYPE;
import static pl.adroads.system.CommonTestFixtures.EMAIL;
import static pl.adroads.system.CommonTestFixtures.EMAIL_STRING;
import static pl.adroads.system.CommonTestFixtures.MAKE;
import static pl.adroads.system.CommonTestFixtures.MILEAGE_VALUE;
import static pl.adroads.system.CommonTestFixtures.MODEL;
import static pl.adroads.system.CommonTestFixtures.MONTHLY_DISTANCE_INDEX;
import static pl.adroads.system.CommonTestFixtures.NAME;
import static pl.adroads.system.CommonTestFixtures.SURNAME;
import static pl.adroads.system.CommonTestFixtures.WINDSCREEN_DIMENSIONS_RESOURCE;
import static pl.adroads.system.CommonTestFixtures.WINDSCREEN_HEIGHT;
import static pl.adroads.system.CommonTestFixtures.WINDSCREEN_HEIGHT_VALUE;
import static pl.adroads.system.CommonTestFixtures.WINDSCREEN_WIDTH;
import static pl.adroads.system.CommonTestFixtures.WINDSCREEN_WIDTH_VALUE;
import static pl.adroads.system.CommonTestFixtures.YEAR;

@RunWith(MockitoJUnitRunner.class)
public class DriverAssemblerUTest {

  @Mock
  private AddressAssembler mockAddressAssembler;
  @Mock
  private CreateDriverRequest mockCreateDriverRequest;

  @InjectMocks
  private DriverAssembler driverAssembler = new DriverAssembler();

  @Mock
  private WindscreenDimensionsResource mockWindscreenDimensionsResources;
  @Mock
  private CarDetailsResource mockCarDetailsResource;
  @Mock
  private DriverDetailsResource mockDriverDetailsResources;

  @Test
  public void test_driverAssembler_correctly_converts_CreateDriverRequest_to_Driver() {

    when(mockCreateDriverRequest.getName()).thenReturn(NAME);
    when(mockCreateDriverRequest.getSurname()).thenReturn(SURNAME);
    when(mockCreateDriverRequest.getDateOfBirth()).thenReturn(DATE_OF_BIRTH_STRING);
    when(mockCreateDriverRequest.getEmail()).thenReturn(EMAIL_STRING);
    when(mockCreateDriverRequest.getAddress()).thenReturn(ADDRESS_RESOURCE);
    when(mockAddressAssembler.assemble(ADDRESS_RESOURCE)).thenReturn(ADDRESS);
    when(mockCreateDriverRequest.getCarDetails()).thenReturn(CAR_DETAILS_RESOURCE);
    when(mockCreateDriverRequest.getDriverDetails()).thenReturn(DRIVER_DETAILS_RESOURCE);

    Driver driver = driverAssembler.assemble(mockCreateDriverRequest);

    assertThat(driver.getUuid(), is(nullValue()));
    assertThat(driver.getName(), is(NAME));
    assertThat(driver.getSurname(), is(SURNAME));
    assertThat(driver.getDateOfBirth(), is(DATE_OF_BIRTH));
    assertThat(driver.getEmail(), is(EMAIL));
    assertThat(driver.getStatus(), is(Status.CREATED));
    assertThat(driver.getAddress(), is(ADDRESS));
    assertThat(driver.getDriverDetails(), is(DRIVER_DETAILS));
    assertThat(driver.getCarDetails(), is(CAR_DETAILS));

  }

  @Test
  public void test_assembleDriverDetails_correctly_converts_from_driverDetailsResource() {
    when(mockDriverDetailsResources.getType()).thenReturn(DRIVER_TYPE.getValue());
    when(mockDriverDetailsResources.getMonthlyDistanceIndex()).thenReturn(MONTHLY_DISTANCE_INDEX);

    DriverDetails driverDetails = ReflectionTestUtils
	.invokeMethod(driverAssembler, "assembleDriverDetails", mockDriverDetailsResources);

    assertThat(driverDetails.getType(), is(DRIVER_TYPE));
    assertThat(driverDetails.getMonthlyDistanceIndex(), is(MONTHLY_DISTANCE_INDEX));

  }

  @Test
  public void test_assembleWindscreenDimensions_correctly_converts_from_windscreenDimensionsResource() {
    when(mockWindscreenDimensionsResources.getHeight()).thenReturn(WINDSCREEN_HEIGHT_VALUE);
    when(mockWindscreenDimensionsResources.getWidth()).thenReturn(WINDSCREEN_WIDTH_VALUE);

    WindscreenDimensions windscreenDimensions = ReflectionTestUtils
	.invokeMethod(driverAssembler, "assembleWindscreenDimensions",
		      mockWindscreenDimensionsResources);

    assertThat(windscreenDimensions.getHeight(), is(WINDSCREEN_HEIGHT));
    assertThat(windscreenDimensions.getWidth(), is(WINDSCREEN_WIDTH));

  }

  @Test
  public void test_assembleCarDetails_correctly_converts_from_assembleCarDetailsResource() {
    when(mockCarDetailsResource.getMake()).thenReturn(MAKE);
    when(mockCarDetailsResource.getModel()).thenReturn(MODEL);
    when(mockCarDetailsResource.getYear()).thenReturn(YEAR.toString());
    when(mockCarDetailsResource.getMileage()).thenReturn(MILEAGE_VALUE);
    when(mockCarDetailsResource.getBody()).thenReturn(BODY.name());
    when(mockCarDetailsResource.getColour()).thenReturn(COLOUR_VALUE);
    when(mockCarDetailsResource.getWindscreenDimensions())
	.thenReturn(WINDSCREEN_DIMENSIONS_RESOURCE);

    CarDetails carDetails = ReflectionTestUtils
	.invokeMethod(driverAssembler, "assembleCarDetails", mockCarDetailsResource);

    assertThat(carDetails.getMake(), is(MAKE));
    assertThat(carDetails.getModel(), is(MODEL));
    assertThat(carDetails.getYear(), is(YEAR));
    assertThat(carDetails.getMileage().getValue(), is(MILEAGE_VALUE));
    assertThat(carDetails.getBody(), is(BODY));
    assertThat(carDetails.getColour(), is(COLOUR));
    assertThat(carDetails.getWindscreenDimensions().getHeight(), is(WINDSCREEN_HEIGHT));
    assertThat(carDetails.getWindscreenDimensions().getWidth(), is(WINDSCREEN_WIDTH));
  }

}
