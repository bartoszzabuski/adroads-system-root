package pl.adroads.system.infrastructure.adapters.driving.user.persistence.model;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static java.lang.Boolean.FALSE;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.util.MatcherAssertionErrors.assertThat;
import static pl.adroads.system.CommonTestFixtures.EXPIRY_DATE;
import static pl.adroads.system.CommonTestFixtures.REGISTRATION_CONFIRMATION;
import static pl.adroads.system.CommonTestFixtures.TIMESTAMP;
import static pl.adroads.system.CommonTestFixtures.TOKEN_STRING;
import static pl.adroads.system.CommonTestFixtures.VERIFICATION_DATE;
import static pl.adroads.system.CommonTestFixtures.NOT_VERIFIED;

@RunWith(MockitoJUnitRunner.class)
public class PersistenceVerificationTokenUTest {

  @Test
  public void test_constructor_assembles_valid_object() {
    PersistenceVerificationToken persistenceVerificationToken = new PersistenceVerificationToken(
        TOKEN_STRING, EXPIRY_DATE.toString(), REGISTRATION_CONFIRMATION.name(), TIMESTAMP.toString(),
        NOT_VERIFIED, VERIFICATION_DATE.toString());

    assertThat(persistenceVerificationToken.getToken(), is(TOKEN_STRING));
    assertThat(persistenceVerificationToken.getExpiryDate(), is(EXPIRY_DATE.toString()));
    assertThat(persistenceVerificationToken.getTokenType(), is(REGISTRATION_CONFIRMATION.name()));
    assertThat(persistenceVerificationToken.isVerified(), is(FALSE));
    assertThat(persistenceVerificationToken.getVerificationDate(),
	       is(VERIFICATION_DATE.toString()));
  }

  @Test
  public void test_hashcode_and_equals() {
    EqualsVerifier.forClass(PersistenceVerificationToken.class).suppress(Warning.STRICT_INHERITANCE)
	.verify();
  }

}
