package pl.adroads.system.infrastructure.adapters.shared.postgre;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.postgresql.util.PGobject;

import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceDriver;
import pl.adroads.system.infrastructure.adapters.shared.ObjectSerializer;
import pl.adroads.system.infrastructure.adapters.shared.postgre.util.PGObjectAssembler;

import java.sql.SQLException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static pl.adroads.system.CommonTestFixtures.EMAIL_STRING;
import static pl.adroads.system.CommonTestFixtures.POSTCODE_STRING;
import static pl.adroads.system.CommonTestFixtures.STATUS_CREATED;
import static pl.adroads.system.CommonTestFixtures.VALID_UUID;
import static pl.adroads.system.common.util.GlobalConstants.JSON_TYPE;

@RunWith(MockitoJUnitRunner.class)
public class PGObjectAssemblerUTest {

  private final String driverJSONString = "{ \"email\" : \"" + EMAIL_STRING + "\", " +
                                          "\"postcode\" : \" " + POSTCODE_STRING + "\", " +
                                          "\"status\" : \" " + STATUS_CREATED + "\", " +
                                          "\"uuid\" : \" " + VALID_UUID.toString() + "\" }";

  public static final String MOCK_STRING = "mockString";
  @Mock
  private PGobject mockPGObject;
  @Mock
  private ObjectSerializer mockSerializer;
  @Mock
  private PersistenceDriver mockPersitenceDriver;
  @InjectMocks
  private PGObjectAssembler pgObjectAssembler;

//  TODO that delegates to other method in the class how to verify as that is not a mock?
//  @Test
//  public void test_toPGObject_works_correctly() throws SQLException {
//    when(mockSerializer.serialize(mockPersitenceDriver)).thenReturn(MOCK_STRING);
//    pgObjectAssembler.toPGobject(mockPersitenceDriver);
//    verify(pgObjectAssembler.toPGobject(MOCK_STRING));
//  }

  @Test
  public void test_toPGObjectJSON_works_correctly() throws SQLException {
    PGobject result = pgObjectAssembler.toPGobject(MOCK_STRING);
    assertThat(result.getType(), equalTo(JSON_TYPE));
    assertThat(result.getValue(), equalTo(MOCK_STRING));
  }

  @Test
  public void test_fromPGObject_works_correctly() throws SQLException {

    when(mockPGObject.getValue()).thenReturn(driverJSONString);
    when(mockSerializer.deserialize(driverJSONString, PersistenceDriver.class))
        .thenReturn(mockPersitenceDriver);
    PersistenceDriver persistenceDriver = pgObjectAssembler
        .fromPGobject(PersistenceDriver.class, mockPGObject);
    assertThat(persistenceDriver, equalTo(mockPersitenceDriver));

  }

  //  TODO that delegates to other method in the class how to verify as that is not a mock?
//  public <E extends PersistenceAggregate<String>> List<E> fromPGobjectList(final Class<E> aType,
//                                                                           final List<PGobject> pgObjectList) {
//    List<E> persistenceAggregates = newArrayList();
//    for (PGobject pGobject : pgObjectList) {
//      persistenceAggregates.add(fromPGobject(aType, pGobject));
//    }
//    return persistenceAggregates;
//  }

}