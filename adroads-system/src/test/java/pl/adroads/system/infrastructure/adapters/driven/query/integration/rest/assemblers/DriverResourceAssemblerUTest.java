package pl.adroads.system.infrastructure.adapters.driven.query.integration.rest.assemblers;

import static com.google.common.collect.Iterables.getFirst;
import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static pl.adroads.system.CommonTestFixtures.DRIVER;
import static pl.adroads.system.CommonTestFixtures.DRIVER_RESOURCE;
import static pl.adroads.system.CommonTestFixtures.DRIVER_RESOURCE_WITHOUT_CAMPAIGN;
import static pl.adroads.system.CommonTestFixtures.PERSISTENCE_QUERY_DRIVER;

import java.util.List;

import org.junit.Test;

import pl.adroads.api.model.driver.DriverResource;

public class DriverResourceAssemblerUTest {

    private DriverResourceAssembler driverResourceAssembler = new DriverResourceAssembler();

    @Test
    public void test_assemble_method_constructs_valid_driverResource() {

        DriverResource driverResource = driverResourceAssembler.assemble(DRIVER);

        assertThat(driverResource, is(DRIVER_RESOURCE_WITHOUT_CAMPAIGN));
    }

    @Test
    public void test_assemble_method_constructs_valid_driverResource_list() {

        List<DriverResource> driverResources = driverResourceAssembler.assemble(newArrayList(PERSISTENCE_QUERY_DRIVER));

        assertThat(driverResources, hasSize(1));
        assertThat(getFirst(driverResources, null), is(DRIVER_RESOURCE));
    }

}
