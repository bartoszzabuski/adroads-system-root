package pl.adroads.system.infrastructure.adapters.driving.driver.persistance.assemblers;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import pl.adroads.system.domain.driver.model.Address;
import pl.adroads.system.domain.driver.model.CarDetails;
import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.domain.driver.model.DriverDetails;
import pl.adroads.system.domain.driver.model.Status;
import pl.adroads.system.domain.driver.model.WindscreenDimensions;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceAddress;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceCarDetails;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceDriver;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceDriverDetails;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceWindscreenDimensions;

import java.util.List;

import static com.google.common.collect.Iterables.getFirst;
import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static pl.adroads.system.CommonTestFixtures.ADDRESS;
import static pl.adroads.system.CommonTestFixtures.BODY;
import static pl.adroads.system.CommonTestFixtures.CAR_DETAILS;
import static pl.adroads.system.CommonTestFixtures.CITY;
import static pl.adroads.system.CommonTestFixtures.CITY_VALUE;
import static pl.adroads.system.CommonTestFixtures.COLOUR;
import static pl.adroads.system.CommonTestFixtures.COLOUR_VALUE;
import static pl.adroads.system.CommonTestFixtures.DATE_OF_BIRTH;
import static pl.adroads.system.CommonTestFixtures.DRIVER_DETAILS;
import static pl.adroads.system.CommonTestFixtures.DRIVER_ID_1;
import static pl.adroads.system.CommonTestFixtures.DRIVER_TYPE;
import static pl.adroads.system.CommonTestFixtures.DRIVER_UUID_1;
import static pl.adroads.system.CommonTestFixtures.EMAIL;
import static pl.adroads.system.CommonTestFixtures.EMAIL_STRING;
import static pl.adroads.system.CommonTestFixtures.MAKE;
import static pl.adroads.system.CommonTestFixtures.MILEAGE;
import static pl.adroads.system.CommonTestFixtures.MILEAGE_VALUE;
import static pl.adroads.system.CommonTestFixtures.MODEL;
import static pl.adroads.system.CommonTestFixtures.MONTHLY_DISTANCE_INDEX;
import static pl.adroads.system.CommonTestFixtures.NAME;
import static pl.adroads.system.CommonTestFixtures.PERSISTENCE_ADDRESS;
import static pl.adroads.system.CommonTestFixtures.PERSISTENCE_CAR_DETAILS;
import static pl.adroads.system.CommonTestFixtures.PERSISTENCE_DRIVER_DETAILS;
import static pl.adroads.system.CommonTestFixtures.PERSISTENCE_WINDSCREEN_DIMENSIONS;
import static pl.adroads.system.CommonTestFixtures.POSTCODE;
import static pl.adroads.system.CommonTestFixtures.POSTCODE_STRING;
import static pl.adroads.system.CommonTestFixtures.PROVINCE;
import static pl.adroads.system.CommonTestFixtures.STATUS_CREATED;
import static pl.adroads.system.CommonTestFixtures.STREET;
import static pl.adroads.system.CommonTestFixtures.STREET_VALUE;
import static pl.adroads.system.CommonTestFixtures.SURNAME;
import static pl.adroads.system.CommonTestFixtures.WINDSCREEN_DIMENSIONS;
import static pl.adroads.system.CommonTestFixtures.WINDSCREEN_HEIGHT_VALUE;
import static pl.adroads.system.CommonTestFixtures.WINDSCREEN_WIDTH_VALUE;
import static pl.adroads.system.CommonTestFixtures.YEAR;

@RunWith(MockitoJUnitRunner.class)
public class PersistenceDriverAssemblerUTest {

  @Mock
  private Driver mockDriver;
  @Mock
  private PersistenceDriver mockPersistenceDriver;
  @Mock
  private DriverDetails mockDriverDetails;
  @Mock
  private CarDetails mockCarDetails;
  @Mock
  private PersistenceAddress mockPersistenceAddress;

  private static final PersistenceDriverAssembler persistenceDriverAssembler
      = new PersistenceDriverAssembler();

  @Test
  public void test_assemblePersistenceDriverDetails_constructs_valid_PersistenceDriverDetails() {
    when(mockDriverDetails.getType()).thenReturn(DRIVER_TYPE);
    when(mockDriverDetails.getMonthlyDistanceIndex()).thenReturn(MONTHLY_DISTANCE_INDEX);

    PersistenceDriverDetails persistenceDriverDetails = ReflectionTestUtils
	.invokeMethod(persistenceDriverAssembler, "assemblePersistenceDriverDetails",
		      mockDriverDetails);

    assertThat(persistenceDriverDetails.getType(), is(DRIVER_TYPE.getValue()));
    assertThat(persistenceDriverDetails.getMonthlyDistanceIndex(), is(MONTHLY_DISTANCE_INDEX));
  }

  @Test
  public void test_assemblePersistenceWindscreenDimensions_works_correctly() {

    PersistenceWindscreenDimensions persistenceWindscreenDimensions = ReflectionTestUtils
	.invokeMethod(persistenceDriverAssembler, "assemblePersistenceWindscreenDimensions",
		      WINDSCREEN_DIMENSIONS);

    assertThat(persistenceWindscreenDimensions.getHeight(), is(WINDSCREEN_HEIGHT_VALUE));
    assertThat(persistenceWindscreenDimensions.getWidth(), is(WINDSCREEN_WIDTH_VALUE));
  }

  @Test
  public void test_assemblerPersistenceCarDetails_works_correctly() {
    when(mockCarDetails.getBody()).thenReturn(BODY);
    when(mockCarDetails.getColour()).thenReturn(COLOUR);
    when(mockCarDetails.getMake()).thenReturn(MAKE);
    when(mockCarDetails.getModel()).thenReturn(MODEL);
    when(mockCarDetails.getMileage()).thenReturn(MILEAGE);
    when(mockCarDetails.getYear()).thenReturn(YEAR);
    when(mockCarDetails.getWindscreenDimensions()).thenReturn(WINDSCREEN_DIMENSIONS);

    PersistenceCarDetails persistenceCarDetails = ReflectionTestUtils
	.invokeMethod(persistenceDriverAssembler, "assemblerPersistenceCarDetails", mockCarDetails);

    assertThat(persistenceCarDetails.getMake(), is(MAKE));
    assertThat(persistenceCarDetails.getModel(), is(MODEL));
    assertThat(persistenceCarDetails.getBody(), is(BODY.toString()));
    assertThat(persistenceCarDetails.getColour(), is(COLOUR_VALUE));
    assertThat(persistenceCarDetails.getMileage(), is(MILEAGE_VALUE));
    assertThat(persistenceCarDetails.getWindscreenDimensions(),
	       is(PERSISTENCE_WINDSCREEN_DIMENSIONS));
    assertThat(persistenceCarDetails.getYear(), is(YEAR.toString()));
  }

  @Test
  public void test_assemblePersistenceAddress_works_correctly() {

    PersistenceAddress persistenceAddress = ReflectionTestUtils
	.invokeMethod(persistenceDriverAssembler, "assemblePersistenceAddress", ADDRESS);

    assertThat(persistenceAddress.getCity(), is(CITY_VALUE));
    assertThat(persistenceAddress.getStreet(), is(STREET_VALUE));
    assertThat(persistenceAddress.getProvince(), is(PROVINCE.toString()));
    assertThat(persistenceAddress.getPostcode(), is(POSTCODE_STRING));
  }

  @Test
  public void test_assemble_works_correctly() {
    when(mockDriver.getUuid()).thenReturn(DRIVER_ID_1);
    when(mockDriver.getName()).thenReturn(NAME);
    when(mockDriver.getSurname()).thenReturn(SURNAME);
    when(mockDriver.getStatus()).thenReturn(Status.CREATED);
    when(mockDriver.getEmail()).thenReturn(EMAIL);
    when(mockDriver.getDateOfBirth()).thenReturn(DATE_OF_BIRTH);
    when(mockDriver.getDriverDetails()).thenReturn(DRIVER_DETAILS);
    when(mockDriver.getCarDetails()).thenReturn(CAR_DETAILS);
    when(mockDriver.getAddress()).thenReturn(ADDRESS);

    PersistenceDriver persistenceDriver = ReflectionTestUtils
	.invokeMethod(persistenceDriverAssembler, "assemble", mockDriver);

    assertThat(persistenceDriver.getUuid(), is(DRIVER_UUID_1.toString()));
    assertThat(persistenceDriver.getEmail(), is(EMAIL_STRING));
    assertThat(persistenceDriver.getStatus(), is(STATUS_CREATED));
    assertThat(persistenceDriver.getDateOfBirth(), is(DATE_OF_BIRTH.toString()));
    assertThat(persistenceDriver.getName(), is(NAME));
    assertThat(persistenceDriver.getSurname(), is(SURNAME));
    assertThat(persistenceDriver.getAddress(), is(PERSISTENCE_ADDRESS));
    assertThat(persistenceDriver.getCarDetails(), is(PERSISTENCE_CAR_DETAILS));
    assertThat(persistenceDriver.getDriverDetails(), is(PERSISTENCE_DRIVER_DETAILS));
  }

  @Test
  public void test_disassembleAddress_works_correctly() {

    Address address = ReflectionTestUtils
	.invokeMethod(persistenceDriverAssembler, "disassembleAddress", PERSISTENCE_ADDRESS);

    assertThat(address.getPostcode(), is(POSTCODE));
    assertThat(address.getStreet(), is(STREET));
    assertThat(address.getCity(), is(CITY));
    assertThat(address.getProvince(), is(PROVINCE));
  }

  @Test
  public void test_disassembleWindscreenDimensions_works_correctly() {
    WindscreenDimensions windscreenDimensions = ReflectionTestUtils
	.invokeMethod(persistenceDriverAssembler, "disassembleWindscreenDimensions",
		      PERSISTENCE_WINDSCREEN_DIMENSIONS);

    assertThat(windscreenDimensions.getHeight().getValue(), is(WINDSCREEN_HEIGHT_VALUE));
    assertThat(windscreenDimensions.getWidth().getValue(), is(WINDSCREEN_WIDTH_VALUE));
  }

  @Test
  public void test_disassembleCarDetails_works_correctly() {
    CarDetails carDetails = ReflectionTestUtils
	.invokeMethod(persistenceDriverAssembler, "disassembleCarDetails", PERSISTENCE_CAR_DETAILS);

    assertThat(carDetails.getMake(), is(MAKE));
    assertThat(carDetails.getModel(), is(MODEL));
    assertThat(carDetails.getBody(), is(BODY));
    assertThat(carDetails.getColour(), is(COLOUR));
    assertThat(carDetails.getMileage().getValue(), is(MILEAGE_VALUE));
    assertThat(carDetails.getWindscreenDimensions(), is(WINDSCREEN_DIMENSIONS));
    assertThat(carDetails.getYear(), is(YEAR));
  }

  @Test
  public void test_disassembleDriverDetails_works_correctly() {
    DriverDetails driverDetails = ReflectionTestUtils
	.invokeMethod(persistenceDriverAssembler, "disassembleDriverDetails",
		      PERSISTENCE_DRIVER_DETAILS);

    assertThat(driverDetails.getMonthlyDistanceIndex(), is(MONTHLY_DISTANCE_INDEX));
    assertThat(driverDetails.getType(), is(DRIVER_TYPE));
  }

  @Test
  public void test_disassemble_works_correctly() {
    when(mockPersistenceDriver.getUuid()).thenReturn(DRIVER_UUID_1.toString());
    when(mockPersistenceDriver.getName()).thenReturn(NAME);
    when(mockPersistenceDriver.getSurname()).thenReturn(SURNAME);
    when(mockPersistenceDriver.getStatus()).thenReturn(Status.CREATED.toString());
    when(mockPersistenceDriver.getEmail()).thenReturn(EMAIL_STRING);
    when(mockPersistenceDriver.getDateOfBirth()).thenReturn(DATE_OF_BIRTH.toString());
    when(mockPersistenceDriver.getDriverDetails()).thenReturn(PERSISTENCE_DRIVER_DETAILS);
    when(mockPersistenceDriver.getCarDetails()).thenReturn(PERSISTENCE_CAR_DETAILS);
    when(mockPersistenceDriver.getAddress()).thenReturn(PERSISTENCE_ADDRESS);

    Driver driver = ReflectionTestUtils
	.invokeMethod(persistenceDriverAssembler, "disassemble", mockPersistenceDriver);

    assertThat(driver.getUuid(), is(DRIVER_ID_1));
    assertThat(driver.getEmail(), is(EMAIL));
    assertThat(driver.getStatus(), is(Status.CREATED));
    assertThat(driver.getDateOfBirth(), is(DATE_OF_BIRTH));
    assertThat(driver.getName(), is(NAME));
    assertThat(driver.getSurname(), is(SURNAME));
    assertThat(driver.getAddress(), is(ADDRESS));
    assertThat(driver.getCarDetails(), is(CAR_DETAILS));
    assertThat(driver.getDriverDetails(), is(DRIVER_DETAILS));
  }

  @Test
  public void test_disassemble_assembles_a_valid_list() {
    when(mockPersistenceDriver.getUuid()).thenReturn(DRIVER_UUID_1.toString());
    when(mockPersistenceDriver.getName()).thenReturn(NAME);
    when(mockPersistenceDriver.getSurname()).thenReturn(SURNAME);
    when(mockPersistenceDriver.getStatus()).thenReturn(Status.CREATED.toString());
    when(mockPersistenceDriver.getEmail()).thenReturn(EMAIL_STRING);
    when(mockPersistenceDriver.getDateOfBirth()).thenReturn(DATE_OF_BIRTH.toString());
    when(mockPersistenceDriver.getDriverDetails()).thenReturn(PERSISTENCE_DRIVER_DETAILS);
    when(mockPersistenceDriver.getCarDetails()).thenReturn(PERSISTENCE_CAR_DETAILS);
    when(mockPersistenceDriver.getAddress()).thenReturn(PERSISTENCE_ADDRESS);

    List<Driver> drivers = ReflectionTestUtils
	.invokeMethod(persistenceDriverAssembler, "disassemble",
		      newArrayList(mockPersistenceDriver));

    assertThat(drivers, hasSize(1));
    Driver driver = getFirst(drivers, null);
    assertThat(driver.getUuid(), is(DRIVER_ID_1));
    assertThat(driver.getEmail(), is(EMAIL));
    assertThat(driver.getStatus(), is(Status.CREATED));
    assertThat(driver.getDateOfBirth(), is(DATE_OF_BIRTH));
    assertThat(driver.getName(), is(NAME));
    assertThat(driver.getSurname(), is(SURNAME));
    assertThat(driver.getAddress(), is(ADDRESS));
    assertThat(driver.getCarDetails(), is(CAR_DETAILS));
    assertThat(driver.getDriverDetails(), is(DRIVER_DETAILS));
  }
}
