package pl.adroads.system.infrastructure.adapters.driving.user.persistence;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.infrastructure.adapters.driving.user.persistence.model.PersistenceUser;
import pl.adroads.system.infrastructure.adapters.shared.postgre.PostgreSQLJSONStorageFacade;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static pl.adroads.system.CommonTestFixtures.EMAIL_STRING;
import static pl.adroads.system.CommonTestFixtures.PERSISTENCE_USER;

@RunWith(MockitoJUnitRunner.class)
public class UserStoreImplUTest {

  private static final String USER_TABLE_NAME = "tbl_users";
  public static final String EMPTY_STRING = "";
  @Mock
  private PostgreSQLJSONStorageFacade<PersistenceUser> mockStorageFacade;

  @InjectMocks
  private UserStoreImpl userStore;

  @Test
  public void test_store_method_delegates_to_storageFacade() {

    userStore.store(PERSISTENCE_USER);

    verify(mockStorageFacade).insertAsJSON(USER_TABLE_NAME, PERSISTENCE_USER);
  }

  @Test
  public void test_update_delegates_to_user_storage() {
    userStore.update(PERSISTENCE_USER);

    verify(mockStorageFacade).updateAsJSON(USER_TABLE_NAME, PERSISTENCE_USER);
  }

  @Test
  public void test_findByEmail_delegates_to_user_storage() {
    String filter = "data->>'email' = ?";

    userStore.findByEmail(EMAIL_STRING);

    verify(mockStorageFacade)
	.findExact(eq(USER_TABLE_NAME), eq(PersistenceUser.class), eq(filter), eq(EMPTY_STRING),
		   eq(EMAIL_STRING));
  }

  @Test
  public void test_findBySessionToken_delegates_to_user_storage() {
    String filter = "data->'persistenceSessionToken' ->> 'token' = ?";

    userStore.findBySessionToken(EMAIL_STRING);

    verify(mockStorageFacade)
	.findExact(eq(USER_TABLE_NAME), eq(PersistenceUser.class), eq(filter), eq(EMPTY_STRING),
		   eq(EMAIL_STRING));
  }

}
