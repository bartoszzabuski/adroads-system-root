package pl.adroads.system.infrastructure.adapters.shared.exceptions.localization;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.common.util.ValidationFields;
import pl.adroads.system.common.util.errors.ValidationError;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static pl.adroads.system.domain.driver.exception.DriverValidationFields.ADDRESS;
import static pl.adroads.system.domain.driver.exception.DriverValidationFields.DEFAULT;
import static pl.adroads.system.common.util.errors.ValidationError.VALUE_EMPTY;
import static pl.adroads.system.common.util.errors.ValidationError.VALUE_NOT_VALID;
import static pl.adroads.system.common.util.errors.ValidationError.VALUE_REQUIRED;

@RunWith(MockitoJUnitRunner.class)
public class ValidationLocalizationMapperUTest {

  private static final String INVALID_ADDRESS = "Nieprawidlowy adres";
  private static final String ADDRESS_EMPTY = "Adres musi by podany";
  private static final String DEFAULT_MESSAGE = "Uppsss.. niespodziewany blad..";

  private ValidationLocalizationMapper validationLocalizationMapper
      = new ValidationLocalizationMapper();

  private static final ValidationFields VALIDATION_FIELDS_ADDRESS = ADDRESS;

  @Test
  public void when_default_ValidationError_provided_then_getMappedMessageFor_returns_correct_message() {

    String message = validationLocalizationMapper
        .getMappedMessageFor(VALIDATION_FIELDS_ADDRESS, ValidationError.DEFAULT);

    assertThat(message, is(INVALID_ADDRESS));
  }

  @Test
  public void when_VALUE_EMTPY_validationError_provided_then_getMappedMessageFor_returns_correct_message() {

    String message = validationLocalizationMapper
        .getMappedMessageFor(VALIDATION_FIELDS_ADDRESS, VALUE_EMPTY);

    assertThat(message, is(ADDRESS_EMPTY));
  }

  @Test
  public void when_VALUE_REQUIRED_validationError_provided_then_getMappedMessageFor_returns_correct_message() {

    String message = validationLocalizationMapper
        .getMappedMessageFor(VALIDATION_FIELDS_ADDRESS, VALUE_REQUIRED);

    assertThat(message, is(ADDRESS_EMPTY));
  }

  @Test
  public void when_VALUE_NOT_VALID_validationError_provided_then_getMappedMessageFor_returns_correct_message() {

    String message = validationLocalizationMapper
        .getMappedMessageFor(VALIDATION_FIELDS_ADDRESS, VALUE_NOT_VALID);

    assertThat(message, is(INVALID_ADDRESS));
  }

  @Test
  public void when_VALUE_EMTPY_validationError_and_DEFAULT_ValidationField_then_getMappedMessageFor_returns_DEFAULT_message() {

    String message = validationLocalizationMapper.getMappedMessageFor(DEFAULT, VALUE_EMPTY);

    assertThat(message, is(DEFAULT_MESSAGE));
  }

}
