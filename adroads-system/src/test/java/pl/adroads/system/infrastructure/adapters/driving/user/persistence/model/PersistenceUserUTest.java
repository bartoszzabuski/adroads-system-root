package pl.adroads.system.infrastructure.adapters.driving.user.persistence.model;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.util.MatcherAssertionErrors.assertThat;
import static pl.adroads.system.CommonTestFixtures.*;
import static pl.adroads.system.CommonTestFixtures.EMAIL_STRING;
import static pl.adroads.system.CommonTestFixtures.PERSISTENCE_VERIFICATION_TOKEN;
import static pl.adroads.system.CommonTestFixtures.TEST_PASSWORD;
import static pl.adroads.system.CommonTestFixtures.VALID_UUID;
import static pl.adroads.system.domain.user.model.UserType.DRIVER;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.CommonTestFixtures;

@RunWith(MockitoJUnitRunner.class)
public class PersistenceUserUTest {

    @Test
    public void test_constructor_assembles_valid_object() {
        PersistenceUser persistenceUser = new PersistenceUser(VALID_UUID.toString(), EMAIL_STRING,
                PERSISTENCE_VERIFICATION_TOKEN, DRIVER.name(), TEST_PASSWORD, PERSISTENCE_SESSION_TOKEN);

        assertThat(persistenceUser.getUuid(), is(VALID_UUID.toString()));
        assertThat(persistenceUser.getEmail(), is(EMAIL_STRING));
        assertThat(persistenceUser.getPersistenceVerificationToken(),
                is(PERSISTENCE_VERIFICATION_TOKEN));
        assertThat(persistenceUser.getUserType(), is(DRIVER.name()));
        assertThat(persistenceUser.getPassword(), is(TEST_PASSWORD));
        assertThat(persistenceUser.getPersistenceSessionToken(), is(PERSISTENCE_SESSION_TOKEN));
    }

}
