package pl.adroads.system.infrastructure.adapters.driving.driver.persistance.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.domain.driver.exception.DriverAlreadyExistsException;
import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.assemblers.PersistenceDriverAssembler;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceDriver;
import pl.adroads.system.infrastructure.adapters.shared.DriverStorage;

import java.util.Optional;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.adroads.system.CommonTestFixtures.EMAIL;
import static pl.adroads.system.CommonTestFixtures.EMAIL_STRING;

@RunWith(MockitoJUnitRunner.class)
public class DriverRepositoryImplUTest {

  @Mock
  private PersistenceDriver mockPersistenceDriver;
  @Mock
  private Driver mockDriver;
  @Mock
  private DriverStorage driverStorage;
  @Mock
  private PersistenceDriverAssembler driverAssembler;
  @InjectMocks
  private DriverRepositoryImpl driverRepository;

  @Test
  public void test_findDriverByEmail_returns_empty_when_email_is_not_found() {

    when(driverStorage.findByEmail(EMAIL_STRING)).thenReturn(Optional.empty());

    Optional<Driver> driver = driverRepository.findDriverByEmail(EMAIL_STRING);

    assertThat(driver, equalTo(Optional.<Driver>empty()));
  }

  @Test
  public void test_findDriverByEmail_returns_driver_when_email_is_found_in_db() {

    when(driverStorage.findByEmail(EMAIL_STRING))
        .thenReturn(Optional.<PersistenceDriver>of(mockPersistenceDriver));
    when(driverAssembler.disassemble(mockPersistenceDriver)).thenReturn(mockDriver);

    Optional<Driver> driver = driverRepository.findDriverByEmail(EMAIL_STRING);

    assertThat(driver, equalTo(Optional.<Driver>of(mockDriver)));

  }

  @Test
  public void test_saveNewDriver_delegates_to_store_when_email_doesnt_exists_in_db() {
    when(mockDriver.getEmail()).thenReturn(EMAIL);
    when(driverStorage.findByEmail(EMAIL_STRING)).thenReturn(Optional.empty());

    when(driverAssembler.assemble(mockDriver)).thenReturn(mockPersistenceDriver);

    driverRepository.storeDriver(mockDriver);

    verify(driverAssembler).assemble(mockDriver);
    verify(driverStorage).saveNewDriver(mockPersistenceDriver);
  }

  @Test
  public void test_saveNewDriver_delegates_to_store_when_email_found_in_db() {
    when(mockDriver.getEmail()).thenReturn(EMAIL);
    when(driverStorage.findByEmail(EMAIL_STRING))
        .thenReturn(Optional.<PersistenceDriver>of(mockPersistenceDriver));
    when(driverAssembler.disassemble(mockPersistenceDriver)).thenReturn(mockDriver);

    when(driverAssembler.assemble(mockDriver)).thenReturn(mockPersistenceDriver);

    catchException(driverRepository).storeDriver(mockDriver);

    assertThat(caughtException().getClass(), equalTo(DriverAlreadyExistsException.class));
  }

}
