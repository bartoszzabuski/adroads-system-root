package pl.adroads.system.common.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.function.Consumer;
import java.util.function.Supplier;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertThat;
import static pl.adroads.system.domain.driver.exception.DriverValidationFields.DRIVER_NAME;


@RunWith(MockitoJUnitRunner.class)
public class ValidateUTest {

  private static final Object NULL_OBJECT = null;

  @Test
  public void test_notNull_assembles_correct_validationExcepiton() {
    Supplier<Object> supplier = new Supplier<Object>() {
      @Override
      public Object get() {
        return Validate.notNull(NULL_OBJECT, DRIVER_NAME);
      }
    };
    catchException(supplier).get();

    assertThat(caughtException(), instanceOf(ValidationException.class));
    assertThat(((ValidationException) caughtException()).getValidationField(), is(DRIVER_NAME));

  }

  @Test
  public void test_isTrue_assembles_correct_validationExcepiton() {
    Consumer<Object> supplier = new Consumer<Object>() {
      @Override
      public void accept(Object o) {
        Validate.isTrue(Boolean.FALSE, DRIVER_NAME);
      }
    };
    catchException(supplier).accept(new Object());

    assertThat(caughtException(), instanceOf(ValidationException.class));
    assertThat(((ValidationException) caughtException()).getValidationField(), is(DRIVER_NAME));

  }

}
