package pl.adroads.system.common;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.common.util.SystemException;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasItemInArray;
import static org.junit.Assert.assertThat;
import static pl.adroads.system.common.util.errors.CommonError.INTERNAL_ERROR;
import static pl.adroads.system.common.util.errors.ValidationError.VALUE_REQUIRED;

@RunWith(MockitoJUnitRunner.class)
public class SystemExceptionUTest {

  private static final Object[] EMPTY_ARRAY = new Object[0];
  private static final Object PARAM_1 = "param1";

  @Test
  public void test_SystemExceptionBuilder_assembles_valid_SystemException() {

    SystemException exception = SystemException.builder(VALUE_REQUIRED)
        .withCause(new NullPointerException()).withParams(PARAM_1).build();

    assertThat(exception.getSystemError(), is(VALUE_REQUIRED));
    assertThat(exception.getParams(), hasItemInArray(PARAM_1));
    assertThat(exception.getCause(), instanceOf(NullPointerException.class));
  }

  @Test
  public void when_no_params_specified_SystemExceptionBuilder_defaults_params_to_empty_list() {

    SystemException exception = SystemException.builder(VALUE_REQUIRED)
        .withCause(new NullPointerException()).build();

    assertThat(exception.getSystemError(), is(VALUE_REQUIRED));
    assertThat(exception.getParams(), is(EMPTY_ARRAY));
  }

  @Test
  public void when_no_SystemError_specified_SystemExceptionBuilder_defaults_to_defaultError() {

    SystemException exception = SystemException.builder(null).withCause(new NullPointerException())
        .build();

    assertThat(exception.getSystemError(), is(INTERNAL_ERROR));
  }


}
