package pl.adroads.system.common.util.errors;

import org.apache.http.HttpStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class CommonErrorUTest {

    @Test
    public void test_getCode_returns_valid_code(){
      assertThat(CommonError.INTERNAL_ERROR.getCode(), is(HttpStatus.SC_INTERNAL_SERVER_ERROR));
    }

    @Test
    public void test_getType_returns_valid_type(){
      assertThat(CommonError.INTERNAL_ERROR.getType(), is(CommonError.class.getSimpleName()));
    }
}
