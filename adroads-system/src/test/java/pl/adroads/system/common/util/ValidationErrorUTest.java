package pl.adroads.system.common.util;

import org.apache.http.HttpStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.common.util.errors.ValidationError;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static pl.adroads.system.common.util.errors.ValidationError.VALUE_EMPTY;

@RunWith(MockitoJUnitRunner.class)
public class ValidationErrorUTest {

  @Test
  public void test_getCode_returns_valid_code() {
    assertThat(VALUE_EMPTY.getCode(), is(HttpStatus.SC_BAD_REQUEST));
  }

  @Test
  public void test_getType_returns_valid_type() {
    assertThat(VALUE_EMPTY.getType(), is(ValidationError.class.getSimpleName()));
  }
}
