package pl.adroads.system.domain.user;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.domain.user.model.VerificationToken;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static pl.adroads.system.CommonTestFixtures.REGISTRATION_CONFIRMATION;
import static pl.adroads.system.CommonTestFixtures.TIMESTAMP;

@RunWith(MockitoJUnitRunner.class)
public class TokenFactoryUTest {

  private static final int EXPIRY_TIME_IN_DAYS = 2;
  private static final String SECRET_KEY = "secretKey";
  private static final String DATA = "data";
  public static final int FIVE = 5;
  public static final ChronoUnit MINUTES = ChronoUnit.MINUTES;
  private TokenFactory tokenFactory = new TokenFactory();

  @Test
  public void test_verificationTokenOf_assembles_valid_token() {
    VerificationToken verificationToken = tokenFactory
        .newUserverificationTokenOf(DATA, SECRET_KEY, EXPIRY_TIME_IN_DAYS, TIMESTAMP);

    assertThat(verificationToken.getToken(), is(notNullValue()));
    assertThat(compareWithinFiveMinutesRange(verificationToken.getExpiryDate(),
                                             LocalDateTime.now().plusDays(EXPIRY_TIME_IN_DAYS)),
               is(TRUE));
    assertThat(verificationToken.getVerificationDate(), is(nullValue()));
    assertThat(verificationToken.getTokenType(), is(REGISTRATION_CONFIRMATION));
    assertThat(verificationToken.isVerified(), is(FALSE));
    assertThat(verificationToken.getTimestamp(), is(TIMESTAMP));
  }

  private boolean compareWithinFiveMinutesRange(final LocalDateTime firstDate,
      final LocalDateTime secondDate) {
    LocalDateTime firstDatePlus5 = LocalDateTime
        .of(firstDate.getYear(), firstDate.getMonth(), firstDate.getDayOfMonth(),
            firstDate.getHour(), firstDate.getMinute()).plus(FIVE, MINUTES);
    LocalDateTime firstDateMinus5 = LocalDateTime
        .of(firstDate.getYear(), firstDate.getMonth(), firstDate.getDayOfMonth(),
            firstDate.getHour(), firstDate.getMinute()).minus(FIVE, MINUTES);

    return secondDate.isAfter(firstDateMinus5) && secondDate.isBefore(firstDatePlus5);
  }

}
