package pl.adroads.system.domain.user.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.common.util.ValidationException;

import java.util.UUID;
import java.util.function.Supplier;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.springframework.test.util.MatcherAssertionErrors.assertThat;
import static pl.adroads.system.CommonTestFixtures.VALID_UUID;
import static pl.adroads.system.domain.user.model.UserId.userIdOf;

@RunWith(MockitoJUnitRunner.class)
public class UserIdUTest {

  private static final UUID INVALID_UUID = null;
  private static final String INVALID_UUID_STRING = null;

  @Test
  public void when_UUID_is_valid_then_userIdOf_constructs_object() {
    UserId userId = userIdOf(VALID_UUID);
    assertThat(userId, is(notNullValue()));
    assertThat(userId.getValue(), is(VALID_UUID));
  }

  @Test
  public void when_string_UUID_is_valid_then_userIdOf_constructs_object() {
    UserId userId = userIdOf(VALID_UUID.toString());
    assertThat(userId, is(notNullValue()));
    assertThat(userId.getValue(), is(VALID_UUID));
  }

  @Test
  public void when_UUID_is_not_valid_then_userIdOf_constructs_object() {
    Supplier<UserId> supplier = new Supplier<UserId>() {
      @Override
      public UserId get() {
        return userIdOf(INVALID_UUID);
      }
    };
    catchException(supplier).get();
    assertThat(caughtException(), instanceOf(ValidationException.class));
  }

  @Test
  public void when_string_UUID_is_not_valid_then_userIdOf_constructs_object() {
    Supplier<UserId> supplier = new Supplier<UserId>() {
      @Override
      public UserId get() {
        return userIdOf(INVALID_UUID_STRING);
      }
    };
    catchException(supplier).get();
    assertThat(caughtException(), instanceOf(ValidationException.class));
  }
}
