package pl.adroads.system.domain.driver.model;

import org.junit.Test;

import pl.adroads.system.common.util.ValidationException;

import java.util.function.Supplier;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static pl.adroads.system.CommonTestFixtures.INVALID_STREET_VALUE;
import static pl.adroads.system.CommonTestFixtures.STREET_VALUE;

public class StreetUTest {

  @Test
  public void test_streetOf_creates_a_valid_street_provided_with_valid_street_string() {
    Street street = Street.streetOf(STREET_VALUE);

    assertThat(street.getValue(), equalTo(STREET_VALUE));
  }

  @Test
  public void test_postcodeOf_throws_exception_provided_with_invalid_email_string() {
    Supplier<Street> supplier = new Supplier<Street>() {
      @Override
      public Street get() {
        return Street.streetOf(INVALID_STREET_VALUE);
      }
    };
    catchException(supplier).get();
    assertThat(caughtException(), instanceOf(ValidationException.class));
  }

}
