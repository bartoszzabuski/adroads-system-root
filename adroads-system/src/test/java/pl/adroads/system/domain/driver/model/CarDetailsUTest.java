package pl.adroads.system.domain.driver.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.function.Supplier;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertThat;
import static pl.adroads.system.CommonTestFixtures.BODY;
import static pl.adroads.system.CommonTestFixtures.CAR_DETAILS;
import static pl.adroads.system.CommonTestFixtures.COLOUR;
import static pl.adroads.system.CommonTestFixtures.INVALID_MILEAGE;
import static pl.adroads.system.CommonTestFixtures.INVALID_YEAR;
import static pl.adroads.system.CommonTestFixtures.MAKE;
import static pl.adroads.system.CommonTestFixtures.MILEAGE_VALUE;
import static pl.adroads.system.CommonTestFixtures.MODEL;
import static pl.adroads.system.CommonTestFixtures.WINDSCREEN_DIMENSIONS;
import static pl.adroads.system.CommonTestFixtures.YEAR;

@RunWith(MockitoJUnitRunner.class)
public class CarDetailsUTest {

  @Test
  public void when_year_is_incorrect_throw_validation_exception() {
    Supplier<CarDetails> supplier = new Supplier<CarDetails>() {
      @Override
      public CarDetails get() {
        return CarDetails
            .of(MAKE, MODEL, INVALID_YEAR, MILEAGE_VALUE, BODY, COLOUR, WINDSCREEN_DIMENSIONS);
      }
    };
    catchException(supplier).get();
    assertThat(caughtException(),
               instanceOf(pl.adroads.system.common.util.ValidationException.class));
  }

  @Test
  public void when_mileage_is_incorrect_throw_validation_exception() {
    Supplier<CarDetails> supplier = new Supplier<CarDetails>() {
      @Override
      public CarDetails get() {
        return CarDetails
            .of(MAKE, MODEL, YEAR, INVALID_MILEAGE, BODY, COLOUR, WINDSCREEN_DIMENSIONS);
      }
    };
    catchException(supplier).get();
    assertThat(caughtException(),
               instanceOf(pl.adroads.system.common.util.ValidationException.class));

  }

  @Test
  public void when_details_are_correct_then_valid_object_is_constructed() {
    CarDetails carDetails = CarDetails
        .of(MAKE, MODEL, YEAR, MILEAGE_VALUE, BODY, COLOUR, WINDSCREEN_DIMENSIONS);

    assertThat(carDetails.getMake(), is(MAKE));
    assertThat(carDetails.getBody(), is(BODY));
    assertThat(carDetails.getModel(), is(MODEL));
    assertThat(carDetails.getYear(), is(YEAR));
    assertThat(carDetails.getMileage().getValue(), is(MILEAGE_VALUE));
    assertThat(carDetails.getColour(), is(COLOUR));
    assertThat(carDetails.getWindscreenDimensions(), is(WINDSCREEN_DIMENSIONS));

  }

  @Test
  public void test_equals_method_works() {
    CarDetails carDetails = CarDetails
        .of(MAKE, MODEL, YEAR, MILEAGE_VALUE, BODY, COLOUR, WINDSCREEN_DIMENSIONS);

    assertThat(carDetails, is(CAR_DETAILS));
  }

}
