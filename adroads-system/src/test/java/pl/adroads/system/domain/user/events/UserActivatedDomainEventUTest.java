package pl.adroads.system.domain.user.events;


import nl.jqno.equalsverifier.EqualsVerifier;

import org.junit.Test;

import static nl.jqno.equalsverifier.Warning.ANNOTATION;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static pl.adroads.system.CommonTestFixtures.USER_ID;

public class UserActivatedDomainEventUTest {

  @Test
  public void equals_contract() {
    EqualsVerifier.forClass(UserActivatedDomainEvent.class).suppress(ANNOTATION.STRICT_INHERITANCE)
	.verify();
  }

  @Test
  public void test_object_creation() {
    assertThat(new UserActivatedDomainEvent(USER_ID).getUserId(), is(USER_ID));
  }

}
