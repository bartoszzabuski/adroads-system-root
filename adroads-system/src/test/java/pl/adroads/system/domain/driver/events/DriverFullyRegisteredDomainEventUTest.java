package pl.adroads.system.domain.driver.events;


import nl.jqno.equalsverifier.EqualsVerifier;

import org.junit.Test;
import org.mockito.Mock;

import pl.adroads.system.domain.driver.model.Driver;

import static nl.jqno.equalsverifier.Warning.ANNOTATION;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class DriverFullyRegisteredDomainEventUTest {

  @Mock
  private Driver mockDriver;

  @Test
  public void equals_contract() {
    EqualsVerifier.forClass(DriverFullyRegisteredDomainEvent.class)
	.suppress(ANNOTATION.STRICT_INHERITANCE).verify();
  }

  @Test
  public void test_object_creation() {
    assertThat(new DriverFullyRegisteredDomainEvent(mockDriver).getDriver(), is(mockDriver));
  }


}
