package pl.adroads.system.domain.driver.model;


import com.google.common.base.Supplier;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.common.util.ValidationException;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class WindscreenDimensionsUTest {

  private static final Integer HEIGHT = 120;
  private static final Integer WIDTH = 220;
  private static final Integer NEGATIVE_HEIGHT = -10;
  private static final Integer NEGATIVE_WIDTH = -10;

  @Test
  public void when_height_is_null_then_of_method_throws_validation_exception() {

    Supplier<WindscreenDimensions> supplier = new Supplier<WindscreenDimensions>() {
      @Override
      public WindscreenDimensions get() {
        return WindscreenDimensions.windscreenDimensionsOf(null, WIDTH);
      }
    };
    catchException(supplier).get();
    assertThat(caughtException(), instanceOf(ValidationException.class));
  }

  @Test
  public void when_width_is_null_then_of_method_throws_validation_exception() {

    Supplier<WindscreenDimensions> supplier = new Supplier<WindscreenDimensions>() {
      @Override
      public WindscreenDimensions get() {
        return WindscreenDimensions.windscreenDimensionsOf(HEIGHT, null);
      }
    };
    catchException(supplier).get();
    assertThat(caughtException(), instanceOf(ValidationException.class));
  }

  @Test
  public void when_height_is_negative_then_of_method_throws_validation_exception() {

    Supplier<WindscreenDimensions> supplier = new Supplier<WindscreenDimensions>() {
      @Override
      public WindscreenDimensions get() {
        return WindscreenDimensions.windscreenDimensionsOf(NEGATIVE_HEIGHT, WIDTH);
      }
    };
    catchException(supplier).get();
    assertThat(caughtException(), instanceOf(ValidationException.class));
  }

  @Test
  public void when_width_is_negative_then_of_method_throws_validation_exception() {

    Supplier<WindscreenDimensions> supplier = new Supplier<WindscreenDimensions>() {
      @Override
      public WindscreenDimensions get() {
        return WindscreenDimensions.windscreenDimensionsOf(HEIGHT, NEGATIVE_WIDTH);
      }
    };
    catchException(supplier).get();
    assertThat(caughtException(), instanceOf(ValidationException.class));
  }

  @Test
  public void test_of_method_constructs_valid_object() {

    WindscreenDimensions windscreenDimensions = WindscreenDimensions
        .windscreenDimensionsOf(HEIGHT, WIDTH);

    assertThat(windscreenDimensions.getHeight().getValue(), is(HEIGHT));
    assertThat(windscreenDimensions.getWidth().getValue(), is(WIDTH));
  }

}
