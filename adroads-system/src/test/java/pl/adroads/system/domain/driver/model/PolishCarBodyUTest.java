package pl.adroads.system.domain.driver.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.common.util.ValidationException;

import java.util.function.Supplier;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static pl.adroads.system.domain.driver.model.PolishCarBody.SEDAN;
import static pl.adroads.system.domain.driver.model.PolishCarBody.fromString;

@RunWith(MockitoJUnitRunner.class)
public class PolishCarBodyUTest {

  @Test
  public void when_invalid_string_provided_then_fromString_return_enum() {
    Supplier<PolishCarBody> supplier = new Supplier<PolishCarBody>() {
      @Override
      public PolishCarBody get() {
        return fromString("kabriolettt111");
      }
    };
    catchException(supplier).get();
    assertThat(caughtException(), instanceOf(ValidationException.class));
  }

  @Test
  public void when_valid_string_provided_then_fromString_returns_found_enum() {
    PolishCarBody polishCarBody = fromString("sedan");
    assertThat(polishCarBody, is(SEDAN));
  }

  @Test
  public void when_valid_string_provided_with_capital_letter_then_fromString_returns_found_enum() {
    PolishCarBody polishCarBody = fromString("Sedan");
    assertThat(polishCarBody, is(SEDAN));
  }

}
