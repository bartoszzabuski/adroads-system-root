package pl.adroads.system.domain.driver.model;

import org.apache.http.HttpStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.domain.driver.exception.DriverDomainError;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class DriverDomainErrorUTest {

  @Test
  public void test_getCode_returns_valid_code() {
    assertThat(DriverDomainError.DRIVER_ALREADY_EXISTS.getCode(), is(HttpStatus.SC_CONFLICT));
  }

  @Test
  public void test_getType_returns_valid_type() {
    assertThat(DriverDomainError.DRIVER_ALREADY_EXISTS.getType(),
               is(DriverDomainError.class.getSimpleName()));
  }

}
