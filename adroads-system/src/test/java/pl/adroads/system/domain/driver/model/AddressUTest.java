package pl.adroads.system.domain.driver.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static pl.adroads.system.CommonTestFixtures.CITY;
import static pl.adroads.system.CommonTestFixtures.POSTCODE;
import static pl.adroads.system.CommonTestFixtures.STREET;

@RunWith(MockitoJUnitRunner.class)
public class AddressUTest {

  @Test
  public void test_of_method_constructs_valid_address() {

    Address address = Address.of(POSTCODE, STREET, CITY, PolishProvince.DOLNOŚLĄSKIE);

    assertThat(address.getProvince(), is(PolishProvince.DOLNOŚLĄSKIE));
    assertThat(address.getCity(), is(CITY));
    assertThat(address.getStreet(), is(STREET));
    assertThat(address.getPostcode(), is(POSTCODE));
  }


}
