package pl.adroads.system.domain.driver.model;

import pl.adroads.system.common.util.ValidationException;

import nl.jqno.equalsverifier.EqualsVerifier;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.function.Supplier;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class DistanceMeasurementUTest {

  private static final Integer VALUE = 1;
  private static final UnitsOfMeasurement UNIT = UnitsOfMeasurement.CM;

  @Test
  public void test_distanceMesurementOf_constructs_valid_object(){
    DistanceMeasurement distanceMeasurement = DistanceMeasurement.distanceMeasurementOf(VALUE, UNIT);

    assertThat(distanceMeasurement.getValue(), is(VALUE));
    assertThat(distanceMeasurement.getUnit(), is(UNIT));
  }

  @Test
  public void when_unit_is_null_then_distanceMesurementOf_throws_exception() {
    Supplier<DistanceMeasurement> supplier = new Supplier<DistanceMeasurement>() {
      @Override
      public DistanceMeasurement get() {
        return DistanceMeasurement.distanceMeasurementOf(VALUE, null);
      }
    };
    catchException(supplier).get();
    assertThat(caughtException(), instanceOf(ValidationException.class));
  }

  @Test
  public void when_value_is_null_then_distanceMesurementOf_throws_exception() {
    Supplier<DistanceMeasurement> supplier = new Supplier<DistanceMeasurement>() {
      @Override
      public DistanceMeasurement get() {
        return DistanceMeasurement.distanceMeasurementOf(null, UNIT);
      }
    };
    catchException(supplier).get();
    assertThat(caughtException(), instanceOf(ValidationException.class));
  }

  @Test
  public void test_equals() {
    EqualsVerifier.forClass(DistanceMeasurement.class).verify();
  }
}
