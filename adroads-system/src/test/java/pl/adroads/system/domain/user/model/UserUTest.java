package pl.adroads.system.domain.user.model;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static com.spencerwi.hamcrestJDK8Time.matchers.IsWithin.within;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.time.LocalDateTime.now;
import static java.time.temporal.ChronoUnit.MINUTES;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static pl.adroads.system.CommonTestFixtures.EMAIL;
import static pl.adroads.system.CommonTestFixtures.EXPIRED_VERIFICATION_TOKEN;
import static pl.adroads.system.CommonTestFixtures.TOKEN_STRING;
import static pl.adroads.system.CommonTestFixtures.USER_ID;
import static pl.adroads.system.CommonTestFixtures.VERIFICATION_TOKEN;
import static pl.adroads.system.domain.user.model.UserType.DRIVER;

import java.util.function.Supplier;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.common.util.ValidationException;
import pl.adroads.system.domain.user.exception.VerificationTokenNotValidException;

@RunWith(MockitoJUnitRunner.class)
public class UserUTest {

    public static final String DIFFERENT_TOKEN = "temp_token";

    @Test
    public void test_constructor_without_token_assembles_valid_object() {

        User user = User.userOf(USER_ID, EMAIL, DRIVER);

        assertThat(user.getUuid(), is(USER_ID));
        assertThat(user.getEmail(), is(EMAIL));
        assertThat(user.getVerificationToken(), is(nullValue()));
        assertThat(user.getUserType(), is(DRIVER));
    }

    @Test
    public void test_constructor_with_token_assembles_valid_object() {

        User user = new User(USER_ID, EMAIL, VERIFICATION_TOKEN, DRIVER);

        assertThat(user.getUuid(), is(USER_ID));
        assertThat(user.getEmail(), is(EMAIL));
        assertThat(user.getVerificationToken(), is(VERIFICATION_TOKEN));
    }

    @Test
    public void test_assignVerificationToken_assigns_valid_token() {
        User user = User.userOf(USER_ID, EMAIL, DRIVER);
        assertThat(user.getVerificationToken(), is(nullValue()));

        user.assignVerificationToken(VERIFICATION_TOKEN);

        assertThat(user.getVerificationToken(), is(VERIFICATION_TOKEN));
    }

    @Test
    public void when_userId_is_null_constructor_throws_exception() {
        Supplier<User> supplier = new Supplier<User>() {
            @Override
            public User get() {
                return User.userOf(null, EMAIL, DRIVER);
            }
        };
        catchException(supplier).get();

        assertThat(caughtException(), instanceOf(ValidationException.class));
    }

    @Test
    public void when_userId_is_null_other_constructor_throws_exception() {
        Supplier<User> supplier = new Supplier<User>() {
            @Override
            public User get() {
                return new User(null, EMAIL, VERIFICATION_TOKEN, DRIVER);
            }
        };
        catchException(supplier).get();

        assertThat(caughtException(), instanceOf(ValidationException.class));
    }

    @Test
    public void when_email_is_null_constructor_throws_exception() {
        Supplier<User> supplier = new Supplier<User>() {
            @Override
            public User get() {
                return User.userOf(USER_ID, null, DRIVER);
            }
        };
        catchException(supplier).get();

        assertThat(caughtException(), instanceOf(ValidationException.class));
    }

    @Test
    public void when_email_is_null_other_constructor_throws_exception() {
        Supplier<User> supplier = new Supplier<User>() {
            @Override
            public User get() {
                return new User(USER_ID, null, VERIFICATION_TOKEN, DRIVER);
            }
        };
        catchException(supplier).get();

        assertThat(caughtException(), instanceOf(ValidationException.class));
    }

    @Test
    public void when_verification_token_is_null_other_constructor_throws_exception() {
        Supplier<User> supplier = new Supplier<User>() {
            @Override
            public User get() {
                return new User(USER_ID, EMAIL, null, DRIVER);
            }
        };
        catchException(supplier).get();

        assertThat(caughtException(), instanceOf(ValidationException.class));
    }

    @Test
    public void when_user_has_no_verification_token_then_verifyAgainst_throws_an_exception() {
        User user = User.userOf(USER_ID, EMAIL, DRIVER);

        catchException(user).verifyAgainst(TOKEN_STRING);

        assertThat(caughtException(), instanceOf(ValidationException.class));
    }

    @Test
    public void when_provided_token_string_is_null_then_verifyAgainst_throws_an_exception() {
        User user = new User(USER_ID, EMAIL, VERIFICATION_TOKEN, DRIVER);

        catchException(user).verifyAgainst(null);

        assertThat(caughtException(), instanceOf(ValidationException.class));
    }

    @Test
    public void when_verification_token_is_expired_then_verifyAgainst_throws_an_exception() {
        User user = new User(USER_ID, EMAIL, EXPIRED_VERIFICATION_TOKEN, DRIVER);

        catchException(user).verifyAgainst(TOKEN_STRING);

        assertThat(caughtException(), instanceOf(VerificationTokenNotValidException.class));
    }

    @Test
    public void when_verification_token_is_not_as_one_saved_in_db_then_verifyAgainst_throws_an_exception() {
        User user = new User(USER_ID, EMAIL, VERIFICATION_TOKEN, DRIVER);

        catchException(user).verifyAgainst(DIFFERENT_TOKEN);

        assertThat(caughtException(), instanceOf(VerificationTokenNotValidException.class));
    }

    @Test
    public void when_token_is_not_expired_and_is_same_as_saved_token_then_verifyAgainst_updates_token_information() {
        User user = new User(USER_ID, EMAIL, VERIFICATION_TOKEN, DRIVER);

        assertThat(user.getVerificationToken().isVerified(), is(FALSE));
        assertThat(user.getVerificationToken().getVerificationDate(), is(nullValue()));

        user.verifyAgainst(TOKEN_STRING);

        assertThat(user.getVerificationToken().isVerified(), is(TRUE));
        assertThat(user.getVerificationToken().getVerificationDate(), within(1, MINUTES).of(now()));
    }

}
