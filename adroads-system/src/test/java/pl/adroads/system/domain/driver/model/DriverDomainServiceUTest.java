package pl.adroads.system.domain.driver.model;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static java.lang.Boolean.TRUE;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.util.MatcherAssertionErrors.assertThat;
import static pl.adroads.system.CommonTestFixtures.*;

import java.util.Optional;
import java.util.concurrent.locks.Lock;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.domain.DomainEventPublisher;
import pl.adroads.system.domain.UUIDGenerator;
import pl.adroads.system.domain.driver.DriverDomainService;
import pl.adroads.system.domain.driver.DriverRepository;
import pl.adroads.system.domain.driver.events.DriverCreatedEvent;
import pl.adroads.system.domain.driver.events.DriverFullyRegisteredDomainEvent;
import pl.adroads.system.domain.driver.exception.DriverNotFoundException;

@RunWith(MockitoJUnitRunner.class)
public class DriverDomainServiceUTest {

    @Mock
    private Driver mockDriver;
    @Mock
    private DriverRepository mockDriverRepository;
    @Mock
    private UUIDGenerator mockIdGenerator;
    // @Mock
    // private CampaignRepository mockCampaignRepository;
    @Mock
    private DomainEventPublisher mockPublisher;

    @InjectMocks
    private DriverDomainService driverService;
    @Mock
    private Driver mockUpdatedDriver;
    @Mock
    private Lock mockLock;
    @Mock
    private CarDetails mockCarDetails;
    @Mock
    private DriverDetails mockDriverDetails;
    @Captor
    private ArgumentCaptor<Driver> driverCaptor;

    @Before
    public void setup() throws InterruptedException {
        when(mockDriverRepository.getLockFor(DRIVER_ID_1.getValue().toString())).thenReturn(mockLock);
        when(mockLock.tryLock(anyLong(), eq(MILLISECONDS))).thenReturn(TRUE);
    }

    @Test
    public void test_registration_of_driver_assigns_ID_and_delegates_to_repository() {
        when(mockIdGenerator.generateId()).thenReturn(DRIVER_UUID_1);

        DriverId driverId = driverService.createNewDriver(mockDriver);

        assertThat(driverId, equalTo(DRIVER_ID_1));
        verify(mockDriver).assignID(DRIVER_ID_1);
        verify(mockDriverRepository).storeDriver(mockDriver);
        verify(mockPublisher).publish(new DriverCreatedEvent(mockDriver));
    }

    @Test
    public void when_driver_doesnt_exist_in_db_then_activate_throws_an_exception() {
        when(mockDriverRepository.findDriverById(DRIVER_ID_1)).thenReturn(Optional.empty());

        catchException(driverService).activateDriver(DRIVER_ID_1);

        assertThat(caughtException(), instanceOf(DriverNotFoundException.class));
        verify(mockDriver, never()).activate();
        verify(mockDriverRepository, never()).updateDriver(mockDriver);
        verify(mockPublisher, never()).publish(new DriverFullyRegisteredDomainEvent(mockDriver));
    }

    @Test
    public void test_activation_of_driver_updates_status_and_delegates_to_repository() {
        when(mockDriverRepository.findDriverById(DRIVER_ID_1)).thenReturn(Optional.of(mockDriver));

        driverService.activateDriver(DRIVER_ID_1);

        verify(mockDriver).activate();
        verify(mockDriverRepository).updateDriver(mockDriver);
        verify(mockPublisher).publish(new DriverFullyRegisteredDomainEvent(mockDriver));
    }

    @Test
    public void when_driver_doesnt_exist_in_db_then_assignToCampaign_throws_an_exception() {
        when(mockDriverRepository.findDriverById(DRIVER_ID_1)).thenReturn(Optional.empty());

        catchException(driverService).assignToCampaign(DRIVER_ID_1);

        assertThat(caughtException(), instanceOf(DriverNotFoundException.class));
        verify(mockDriver, never()).activate();
        verify(mockDriverRepository, never()).updateDriver(mockDriver);
    }

    @Test
    public void verify_assignToCampaign_updates_status_and_delegates_to_repository() {
        when(mockDriverRepository.findDriverById(DRIVER_ID_1)).thenReturn(Optional.of(mockDriver));

        driverService.assignToCampaign(DRIVER_ID_1);

        verify(mockDriver).markAsTaken();
        verify(mockDriverRepository).updateDriver(mockDriver);
    }

    // @Test
    // public void test_approveCampaign_and_updates_status_and_delegates_to_repository() {
    // when(mockDriverRepository.findDriverById(DRIVER_ID_1)).thenReturn(Optional.of(mockDriver));
    // driverService.approveCampaign(DRIVER_ID_1);
    // verify(mockDriver).approveCampaign(DRIVER_ID_1);
    // verify(mockDriverRepository).updateDriver(mockDriver);
    // }
    //
    // @Test
    // public void when_driver_doesnt_exist_then_approveCampaign_throws_exception() {
    // when(mockDriverRepository.findDriverById(DRIVER_ID_1)).thenReturn(Optional.empty());
    // catchException(driverService).approveCampaign(DRIVER_ID_1);
    // verify(mockDriverRepository).findDriverById(DRIVER_ID_1);
    // assertThat(caughtException(), instanceOf(DriverNotFoundException.class));
    // verifyNoMoreInteractions(mockDriverRepository);
    // }

    @Test
    public void when_driver_doesnt_exist_then_updateDriver_throws_exception() throws InterruptedException {

        when(mockDriverRepository.findDriverById(DRIVER_ID_1)).thenReturn(Optional.empty());

        catchException(driverService).updateDriver(DRIVER_ID_1, mockUpdatedDriver);

        assertThat(caughtException(), instanceOf(DriverNotFoundException.class));
    }

    @Test
    public void verify_driver_data_is_being_updated_apart_from_id_and_status() {
        when(mockDriverRepository.findDriverById(DRIVER_ID_1)).thenReturn(Optional.of(mockDriver));

        when(mockDriver.getUuid()).thenReturn(DRIVER_ID_1);
        when(mockDriver.getStatus()).thenReturn(Status.REGISTERED);

        when(mockUpdatedDriver.getName()).thenReturn(NAME);
        when(mockUpdatedDriver.getSurname()).thenReturn(SURNAME);
        when(mockUpdatedDriver.getDateOfBirth()).thenReturn(DATE_OF_BIRTH);
        when(mockUpdatedDriver.getEmail()).thenReturn(EMAIL);
        when(mockUpdatedDriver.getAddress()).thenReturn(ADDRESS);
        when(mockUpdatedDriver.getCarDetails()).thenReturn(mockCarDetails);
        when(mockUpdatedDriver.getDriverDetails()).thenReturn(mockDriverDetails);

        driverService.updateDriver(DRIVER_ID_1, mockUpdatedDriver);

        verify(mockDriverRepository).updateDriver(driverCaptor.capture());

        Driver updatedDriver = driverCaptor.getValue();
        assertThat(updatedDriver.getName(), is(NAME));
        assertThat(updatedDriver.getSurname(), is(SURNAME));
        assertThat(updatedDriver.getStatus(), is(Status.REGISTERED));
        assertThat(updatedDriver.getDateOfBirth(), is(DATE_OF_BIRTH));
        assertThat(updatedDriver.getEmail(), is(EMAIL));
        assertThat(updatedDriver.getAddress(), is(ADDRESS));
        assertThat(updatedDriver.getCarDetails(), is(mockCarDetails));
        assertThat(updatedDriver.getDriverDetails(), is(mockDriverDetails));
    }

}
