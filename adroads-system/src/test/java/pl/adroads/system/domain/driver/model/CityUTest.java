package pl.adroads.system.domain.driver.model;

import org.junit.Test;

import pl.adroads.system.CommonTestFixtures;
import pl.adroads.system.common.util.ValidationException;

import java.util.function.Supplier;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static pl.adroads.system.CommonTestFixtures.CITY_VALUE;

public class CityUTest {

  @Test
  public void test_cityOf_creates_a_valid_city_provided_with_valid_city_string() {
    City city = City.cityOf(CommonTestFixtures.CITY_VALUE);

    assertThat(city.getValue(), equalTo(CITY_VALUE));
  }

  @Test
  public void test_postcodeOf_throws_exception_provided_with_invalid_email_string() {
    Supplier<City> supplier = new Supplier<City>() {
      @Override
      public City get() {
        return City.cityOf(CommonTestFixtures.INVALID_CITY_VALUE);
      }
    };
    catchException(supplier).get();
    assertThat(caughtException(), instanceOf(ValidationException.class));
  }

}
