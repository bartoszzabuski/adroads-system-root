package pl.adroads.system.domain.campaign.model;

import org.junit.Test;

import static jersey.repackaged.com.google.common.collect.Sets.newHashSet;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertThat;
import static pl.adroads.system.CommonTestFixtures.CAMPAIGN_DESCRIPTION;
import static pl.adroads.system.CommonTestFixtures.CAMPAIGN_NAME;
import static pl.adroads.system.CommonTestFixtures.END_TIME;
import static pl.adroads.system.CommonTestFixtures.START_TIME;
import static pl.adroads.system.CommonTestFixtures.VALID_UUID;
import static pl.adroads.system.domain.campaign.model.CampaignId.campaignIdOf;
import static pl.adroads.system.domain.campaign.model.CampaignStatus.CREATED;

public class CampaignUTest {


  @Test
  public void test_createNewCampaign_correctly_creates_campaing_object() {
    Campaign campaign = Campaign
	.campaignOf(CAMPAIGN_NAME, CAMPAIGN_DESCRIPTION, START_TIME, END_TIME);
    assertThat(campaign.getUuid(), nullValue());
    assertThat(campaign.getName(), equalTo(CAMPAIGN_NAME));
    assertThat(campaign.getDescription(), equalTo(CAMPAIGN_DESCRIPTION));
    assertThat(campaign.getStartDateTime(), equalTo(START_TIME));
    assertThat(campaign.getEndDateTime(), equalTo(END_TIME));
    assertThat(campaign.getCampaignStatus(), equalTo(CREATED));
    assertThat(campaign.getDrivers(), is(empty()));
  }

  @Test
  public void test_assignId_sets_id() {
    Campaign campaign = new Campaign(CAMPAIGN_NAME, CAMPAIGN_DESCRIPTION, START_TIME, END_TIME,
                                     CREATED, newHashSet());
    assertThat(campaign.getUuid(), is(nullValue()));
    campaign.assignID(campaignIdOf(VALID_UUID));
    assertThat(campaign.getUuid().getValue(), is(VALID_UUID));

  }


}
