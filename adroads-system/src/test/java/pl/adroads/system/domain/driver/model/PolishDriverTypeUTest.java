package pl.adroads.system.domain.driver.model;


import org.junit.Test;

import pl.adroads.system.common.util.ValidationException;

import java.util.function.Supplier;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static pl.adroads.system.domain.driver.model.PolishDriverType.DOJAZD_DO_PRACY;
import static pl.adroads.system.domain.driver.model.PolishDriverType.INNY;
import static pl.adroads.system.domain.driver.model.PolishDriverType.KURIER;
import static pl.adroads.system.domain.driver.model.PolishDriverType.PRZEDSTAWICIEL;
import static pl.adroads.system.domain.driver.model.PolishDriverType.TAKSOWKARZ;
import static pl.adroads.system.domain.driver.model.PolishDriverType.polishDriverTypeOf;

public class PolishDriverTypeUTest {

  public static final int INVALID_TYPE = 0;

  @Test
  public void when_polishDriverTypeOf_gets_null_argument_then_throws_exception() {
    Supplier<PolishDriverType> supplier = new Supplier<PolishDriverType>() {
      @Override
      public PolishDriverType get() {
        return polishDriverTypeOf(null);
      }
    };
    catchException(supplier).get();
    assertThat(caughtException(), instanceOf(ValidationException.class));
  }

  @Test
  public void when_polishDriverTypeOf_gets_non_existing_argument_then_throws_exception() {
    Supplier<PolishDriverType> supplier = new Supplier<PolishDriverType>() {
      @Override
      public PolishDriverType get() {
        return polishDriverTypeOf(INVALID_TYPE);
      }
    };
    catchException(supplier).get();
    assertThat(caughtException(), instanceOf(ValidationException.class));
  }

  @Test
  public void when_polishDriverTypeOf_gets_valid_argument_5_then_type_is_returned() {
    PolishDriverType type = polishDriverTypeOf(5);
    assertThat(type, is(INNY));
  }

  @Test
  public void when_polishDriverTypeOf_gets_valid_argument_4_then_type_is_returned() {
    PolishDriverType type = polishDriverTypeOf(4);
    assertThat(type, is(DOJAZD_DO_PRACY));
  }

  @Test
  public void when_polishDriverTypeOf_gets_valid_argument_3_then_type_is_returned() {
    PolishDriverType type = polishDriverTypeOf(3);
    assertThat(type, is(TAKSOWKARZ));
  }

  @Test
  public void when_polishDriverTypeOf_gets_valid_argument_2_then_type_is_returned() {
    PolishDriverType type = polishDriverTypeOf(2);
    assertThat(type, is(KURIER));
  }

  @Test
  public void when_polishDriverTypeOf_gets_valid_argument_1_then_type_is_returned() {
    PolishDriverType type = polishDriverTypeOf(1);
    assertThat(type, is(PRZEDSTAWICIEL));
  }
}
