package pl.adroads.system.domain.driver.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.common.util.ValidationException;

import java.util.function.Supplier;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class DriverDetailsUTest {

  private static final PolishDriverType DRIVER_TYPE = PolishDriverType.INNY;
  private static final Integer INVALID_MONTHLY_DISTANCE_INDEX = 0;
  private static final Integer MONTHLY_DISTANCE_INDEX = 3;

  @Test
  public void when_monthlyDistanceIndex_is_not_in_range_then_throw_exception() {

    Supplier<DriverDetails> supplier = new Supplier<DriverDetails>() {
      @Override
      public DriverDetails get() {
        return DriverDetails.of(DRIVER_TYPE, INVALID_MONTHLY_DISTANCE_INDEX);
      }
    };
    catchException(supplier).get();
    assertThat(caughtException(), instanceOf(ValidationException.class));
  }

  @Test
  public void when_driver_type_is_null_then_throw_exception() {

    Supplier<DriverDetails> supplier = new Supplier<DriverDetails>() {
      @Override
      public DriverDetails get() {
        return DriverDetails.of(null, INVALID_MONTHLY_DISTANCE_INDEX);
      }
    };
    catchException(supplier).get();
    assertThat(caughtException(), instanceOf(ValidationException.class));
  }

  @Test
  public void when_data_is_correct_then_of_methods_constructs_valid_object() {
    DriverDetails driverDetails = DriverDetails.of(DRIVER_TYPE, MONTHLY_DISTANCE_INDEX);

    assertThat(driverDetails.getType(), is(DRIVER_TYPE));
    assertThat(driverDetails.getMonthlyDistanceIndex(), is(MONTHLY_DISTANCE_INDEX));
  }

}
