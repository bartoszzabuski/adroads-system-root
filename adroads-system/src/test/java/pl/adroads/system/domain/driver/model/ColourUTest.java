package pl.adroads.system.domain.driver.model;

import pl.adroads.system.common.util.ValidationException;

import nl.jqno.equalsverifier.EqualsVerifier;

import org.junit.Test;

import java.util.function.Supplier;

import static pl.adroads.system.CommonTestFixtures.COLOUR_VALUE;
import static pl.adroads.system.CommonTestFixtures.INVALID_COLOUR_VALUE;
import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

public class ColourUTest {

  @Test
  public void test_colourOf_creates_a_valid_colour_provided_with_valid_colour_string() {
    Colour colour = Colour.colourOf(COLOUR_VALUE);

    assertThat(colour.getValue(), equalTo(COLOUR_VALUE));
  }

  @Test
  public void test_colourOf_throws_exception_provided_with_invalid_colour_string() {
    Supplier<Colour> supplier = new Supplier<Colour>() {
      @Override
      public Colour get() {
        return Colour.colourOf(INVALID_COLOUR_VALUE);
      }
    };
    catchException(supplier).get();
    assertThat(caughtException(), instanceOf(ValidationException.class));
  }

  @Test
  public void equalsContract() {
    EqualsVerifier.forClass(Colour.class).verify();
  }

}
