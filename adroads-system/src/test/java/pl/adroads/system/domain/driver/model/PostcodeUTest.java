package pl.adroads.system.domain.driver.model;

import org.junit.Test;

import pl.adroads.system.common.util.ValidationException;

import java.util.function.Supplier;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static pl.adroads.system.CommonTestFixtures.INVALID_POSTCODE;
import static pl.adroads.system.CommonTestFixtures.POSTCODE_STRING;

public class PostcodeUTest {

  @Test
  public void test_postcodeOf_creates_a_valid_Postcode_provided_with_valid_postcode_string() {
    Postcode postcode = Postcode.postcodeOf(POSTCODE_STRING);

    assertThat(postcode.getValue(), equalTo(POSTCODE_STRING));
  }

  @Test
  public void test_postcodeOf_throws_exception_provided_with_invalid_email_string() {
    Supplier<Postcode> supplier = new Supplier<Postcode>() {
      @Override
      public Postcode get() {
        return Postcode.postcodeOf(INVALID_POSTCODE);
      }
    };
    catchException(supplier).get();
    assertThat(caughtException(), instanceOf(ValidationException.class));
  }

}
