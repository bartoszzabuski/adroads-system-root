package pl.adroads.system.domain.driver.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.common.util.ValidationException;

import java.util.function.Supplier;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static pl.adroads.system.CommonTestFixtures.EMAIL_STRING;
import static pl.adroads.system.CommonTestFixtures.INVALID_EMAIL;
import static pl.adroads.system.CommonTestFixtures.POLISH_EMAIL_STRING;
import static pl.adroads.system.domain.driver.model.Email.emailOf;

@RunWith(MockitoJUnitRunner.class)
public class EmailUTest {

  private static final String EMAIL_1 = "Bartek_zabuski123@gmail323.com";
  private static final String EMAIL_2 = "Bartek_zabuski123@gmail.com";
  private static final String EMAIL_3 = "Bartek_zabuski@gmail.com";
  private static final String EMAIL_4 = "bartek_zabuski@gmail.com";
  private static final String EMAIL_5 = "bartek-zabuski@gmail.com";
  private static final String EMAIL_6 = "bartek.zabuski@gmail.com";

  @Test
  public void test_emailOf_creates_a_valid_Email_provided_with_valid_email_string() {
    assertThat(emailOf(EMAIL_STRING).getValue(), equalTo(EMAIL_STRING));
    assertThat(emailOf(EMAIL_1).getValue(), equalTo(EMAIL_1));
    assertThat(emailOf(EMAIL_2).getValue(), equalTo(EMAIL_2));
    assertThat(emailOf(EMAIL_3).getValue(), equalTo(EMAIL_3));
    assertThat(emailOf(EMAIL_4).getValue(), equalTo(EMAIL_4));
    assertThat(emailOf(EMAIL_5).getValue(), equalTo(EMAIL_5));
    assertThat(emailOf(EMAIL_6).getValue(), equalTo(EMAIL_6));
  }

  @Test
  public void test_emailOf_throws_exception_provided_with_valid_email_string() {
    catchException(supplyEmail(INVALID_EMAIL)).get();
    assertThat(caughtException(), instanceOf(ValidationException.class));
  }

  @Test
  public void test_emailOf_accepts_polish_characters() {
    catchException(supplyEmail(POLISH_EMAIL_STRING)).get();
    assertThat(caughtException(), is(nullValue()));
  }

  private Supplier<Email> supplyEmail(final String emailValue) {
    return new Supplier<Email>() {
      @Override
      public Email get() {
	return emailOf(emailValue);
      }
    };
  }

}
