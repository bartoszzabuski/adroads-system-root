package pl.adroads.system.domain.driver.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static pl.adroads.system.CommonTestFixtures.DRIVER_ID_1;
import static pl.adroads.system.domain.driver.model.Status.CREATED;
import static pl.adroads.system.domain.driver.model.Status.REGISTERED;
import static pl.adroads.system.domain.driver.model.Status.TAKEN;

@RunWith(MockitoJUnitRunner.class)
public class StatusUTest {

  @Test
  public void test_CREATED_can_transition_to_REGISTERED() {
    assertThat(CREATED.markAsRegistered(DRIVER_ID_1), equalTo(REGISTERED));
  }
// TODO fix it
//  @Test
//  public void test_CREATED_cannot_transition_to_TAKEN(){
//    catchException(CREATED).markAsTaken(DRIVER_ID_1);
//    assertThat(caughtException(), instanceOf(DriverNotFullyRegisteredException.class));
//  }

  @Test
  public void test_REGISTERED_can_transition_to_REGISTERED() {
    assertThat(REGISTERED.markAsRegistered(DRIVER_ID_1), equalTo(REGISTERED));
  }

  @Test
  public void test_REGISTERED_can_transition_to_TAKEN() {
    assertThat(REGISTERED.markAsTaken(DRIVER_ID_1), equalTo(TAKEN));
  }


  @Test
  public void test_TAKEN_can_transition_to_REGISTERED() {
    assertThat(TAKEN.markAsRegistered(DRIVER_ID_1), equalTo(REGISTERED));
  }

  @Test
  public void test_TAKEN_can_transition_to_TAKEN() {
    assertThat(TAKEN.markAsTaken(DRIVER_ID_1), equalTo(TAKEN));
  }
}
