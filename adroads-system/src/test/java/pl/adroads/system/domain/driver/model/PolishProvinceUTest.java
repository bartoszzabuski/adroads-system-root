package pl.adroads.system.domain.driver.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.common.util.ValidationException;

import java.util.function.Supplier;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class PolishProvinceUTest {

  @Test
  public void test_fromString_parses_province_correctly() {
    PolishProvince province = PolishProvince.fromString("dolnośląskie");
    assertThat(province, is(PolishProvince.DOLNOŚLĄSKIE));
  }

  @Test
  public void test_fromString_parses_province_correctly_regardles_of_letter_case() {
    PolishProvince province = PolishProvince.fromString("Dolnośląskie");
    assertThat(province, is(PolishProvince.DOLNOŚLĄSKIE));
  }

  @Test
  public void when_invalid_province_provided_then_fromString_throws_exception() {

    Supplier<PolishProvince> supplier = new Supplier<PolishProvince>() {

      @Override
      public PolishProvince get() {
        return PolishProvince.fromString("Dolnośsssląskie");
      }
    };
    catchException(supplier).get();
    assertThat(caughtException(), instanceOf(ValidationException.class));
  }

}
