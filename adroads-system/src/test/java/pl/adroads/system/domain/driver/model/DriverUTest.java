package pl.adroads.system.domain.driver.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static pl.adroads.system.CommonTestFixtures.ADDRESS;
import static pl.adroads.system.CommonTestFixtures.CAR_DETAILS;
import static pl.adroads.system.CommonTestFixtures.DATE_OF_BIRTH;
import static pl.adroads.system.CommonTestFixtures.DRIVER_DETAILS;
import static pl.adroads.system.CommonTestFixtures.DRIVER_ID_1;
import static pl.adroads.system.CommonTestFixtures.EMAIL;
import static pl.adroads.system.CommonTestFixtures.NAME;
import static pl.adroads.system.CommonTestFixtures.SURNAME;
import static pl.adroads.system.domain.driver.model.Status.CREATED;
import static pl.adroads.system.domain.driver.model.Status.REGISTERED;

@RunWith(MockitoJUnitRunner.class)
public class DriverUTest {

  private Driver newlyCreatedDriver;

  @Before
  public void setup() {
    newlyCreatedDriver = new Driver.DriverBuilder().withName(NAME).withSurname(SURNAME)
	.withDateOfBirth(DATE_OF_BIRTH).withEmail(EMAIL).withAddress(ADDRESS).withStatus(CREATED)
	.withCarDetails(CAR_DETAILS).withDriverDetails(DRIVER_DETAILS).build();
  }

  @Test
  public void test_builder_assembles_valid_driver() {
    assertThat(newlyCreatedDriver.getUuid(), is(nullValue()));
    assertThat(newlyCreatedDriver.getName(), is(NAME));
    assertThat(newlyCreatedDriver.getSurname(), is(SURNAME));
    assertThat(newlyCreatedDriver.getDateOfBirth(), is(DATE_OF_BIRTH));
    assertThat(newlyCreatedDriver.getEmail(), is(EMAIL));
    assertThat(newlyCreatedDriver.getStatus(), is(CREATED));
    assertThat(newlyCreatedDriver.getAddress(), is(ADDRESS));
    assertThat(newlyCreatedDriver.getDriverDetails(), is(DRIVER_DETAILS));
    assertThat(newlyCreatedDriver.getCarDetails(), is(CAR_DETAILS));
  }

  @Test
  public void test_assignId_adds_ID_to_domain_object() {
    assertThat(newlyCreatedDriver.getUuid(), nullValue());

    newlyCreatedDriver.assignID(DRIVER_ID_1);

    assertThat(newlyCreatedDriver.getUuid(), equalTo(DRIVER_ID_1));
  }

  @Test
  public void when_user_has_status_CREATED_then_activate_changes_status_to_REGISTERED() {
    assertThat(newlyCreatedDriver.getStatus(), is(CREATED));

    newlyCreatedDriver.activate();

    assertThat(newlyCreatedDriver.getStatus(), is(REGISTERED));
  }

// FIXME
//  @Test
//  public void test_approveCampaign_updates_status_to_TAKEN() {
//    testDriver = new Driver.DriverBuilder(EMAIL, POSTCODE, REGISTERED).build();
//
//    testDriver.approveCampaign(DRIVER_ID_1);
//
//    assertThat(testDriver.getStatus(), equalTo(TAKEN));
//  }

}
