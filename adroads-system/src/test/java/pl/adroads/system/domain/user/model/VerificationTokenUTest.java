package pl.adroads.system.domain.user.model;

import com.spencerwi.hamcrestJDK8Time.matchers.IsWithin;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.CommonTestFixtures;
import pl.adroads.system.common.util.ValidationException;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.time.LocalDateTime.now;
import static java.time.temporal.ChronoUnit.MINUTES;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.springframework.test.util.MatcherAssertionErrors.assertThat;
import static pl.adroads.system.CommonTestFixtures.*;
import static pl.adroads.system.CommonTestFixtures.DIFFERENT_TOKEN_STRING;
import static pl.adroads.system.CommonTestFixtures.EXPIRED_VERIFICATION_TOKEN;
import static pl.adroads.system.CommonTestFixtures.EXPIRY_DATE;
import static pl.adroads.system.CommonTestFixtures.REGISTRATION_CONFIRMATION;
import static pl.adroads.system.CommonTestFixtures.TIMESTAMP;
import static pl.adroads.system.CommonTestFixtures.TOKEN_STRING;

@RunWith(MockitoJUnitRunner.class)
public class VerificationTokenUTest {

  private VerificationToken verificationToken;

  @Before
  public void setup() {
    verificationToken = new VerificationToken(TOKEN_STRING, TIMESTAMP, EXPIRY_DATE,
					      REGISTRATION_CONFIRMATION);
  }

  @Test
  public void test_constructor_assembles_valid_object() {

    assertThat(verificationToken.getToken(), is(TOKEN_STRING));
    assertThat(verificationToken.getExpiryDate(), is(EXPIRY_DATE));
    assertThat(verificationToken.getVerificationDate(), is(nullValue()));
    assertThat(verificationToken.getTokenType(), is(REGISTRATION_CONFIRMATION));
    assertThat(verificationToken.isVerified(), is(FALSE));
    assertThat(verificationToken.getTimestamp(), is(TIMESTAMP));
  }

  @Test
  public void when_token_is_null_verifyAgainst_throws_an_exception() {
    catchException(verificationToken).verifyAgainst(null);
    assertThat(caughtException(), instanceOf(ValidationException.class));
  }

  @Test
  public void when_verification_token_has_already_been_verified_then_verify_against_returns_false() {
    assertThat(VERIFIED_TOKEN.verifyAgainst(TOKEN_STRING), is(false));
  }

  @Test
  public void when_verification_token_expired_then_verify_against_returns_false() {
    assertThat(EXPIRED_VERIFICATION_TOKEN.verifyAgainst(TOKEN_STRING), is(false));
  }

  @Test
  public void when_verification_token_not_equals_provided_token_then_verify_against_returns_false() {
    assertThat(verificationToken.isVerified(), is(FALSE));
    assertThat(verificationToken.getVerificationDate(), is(nullValue()));

    assertThat(verificationToken.verifyAgainst(DIFFERENT_TOKEN_STRING), is(false));

    assertThat(verificationToken.isVerified(), is(FALSE));
    assertThat(verificationToken.getVerificationDate(), is(nullValue()));
  }

  @Test
  public void when_token_equals_provided_token_and_it_not_expired_then_verify_against_returns_true() {
    assertThat(verificationToken.isVerified(), is(FALSE));
    assertThat(verificationToken.getVerificationDate(), is(nullValue()));

    assertThat(verificationToken.verifyAgainst(TOKEN_STRING), is(TRUE));

    assertThat(verificationToken.isVerified(), is(TRUE));
    assertThat(verificationToken.getVerificationDate(), IsWithin.within(1, MINUTES).of(now()));
  }

}
