package pl.adroads.system.filters;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;
import static jersey.repackaged.com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static pl.adroads.system.CommonTestFixtures.EMAIL;
import static pl.adroads.system.CommonTestFixtures.EMAIL_STRING;
import static pl.adroads.system.CommonTestFixtures.TOKEN_STRING;
import static pl.adroads.system.filters.AuthenticationFilter.AUTHENTICATED_USER_REQUEST_PROPERTY;
import static pl.adroads.system.filters.AuthenticationFilter.BEARER_HEADER_PREFIX;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.SecurityContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.application.user.UserManagementService;
import pl.adroads.system.domain.user.model.User;
import pl.adroads.system.filters.exceptions.InvalidAuthorizationHeaderException;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationFilterUTest {

    @Mock
    private UserManagementService mockUserManagementService;
    @Mock
    private ContainerRequestContext mockContainerRequestContext;
    @Mock
    private User mockUser;

    private AuthenticationFilter authenticationFilter;

    @Captor
    private ArgumentCaptor<SecurityContext> securityContextCaptor;

    @Before
    public void setup() {
        authenticationFilter = new AuthenticationFilter(mockUserManagementService);
    }

    @Test
    public void when_authorization_header_comes_empty_filter_throws_exception() throws IOException {
        when(mockContainerRequestContext.getHeaderString(AUTHORIZATION)).thenReturn(null);

        catchException(authenticationFilter).filter(mockContainerRequestContext);

        assertThat(caughtException(), instanceOf(InvalidAuthorizationHeaderException.class));
        verifyZeroInteractions(mockUserManagementService);
    }

    @Test
    public void when_authorization_header_is_invalid_filter_throws_exception() throws IOException {

        List<String> invalidAuthorizationHeaders = newArrayList("Bear sfsfsdfsdfs", "sfsxc2323",
                BEARER_HEADER_PREFIX, "");

        for (String invalidAuthorizationHeader : invalidAuthorizationHeaders) {
            when(mockContainerRequestContext.getHeaderString(AUTHORIZATION)).thenReturn(invalidAuthorizationHeader);
            catchException(authenticationFilter).filter(mockContainerRequestContext);
            assertThat(caughtException(), instanceOf(InvalidAuthorizationHeaderException.class));
            verifyZeroInteractions(mockUserManagementService);
        }
    }

    @Test
    public void when_valid_authorization_header_comes_in_filter_delegates_to_userManagementService()
            throws IOException {

        when(mockContainerRequestContext.getHeaderString(AUTHORIZATION))
                .thenReturn(BEARER_HEADER_PREFIX + TOKEN_STRING);

        authenticationFilter.filter(mockContainerRequestContext);

        verify(mockUserManagementService).verifySessionToken(TOKEN_STRING);
    }

    @Test
    public void when_valid_authorization_header_comes_in_filter_saves_user_in_for_use_in_authorization_filter()
            throws IOException {
        when(mockContainerRequestContext.getHeaderString(AUTHORIZATION))
                .thenReturn(BEARER_HEADER_PREFIX + TOKEN_STRING);
        when(mockUserManagementService.verifySessionToken(TOKEN_STRING)).thenReturn(mockUser);

        authenticationFilter.filter(mockContainerRequestContext);

        verify(mockContainerRequestContext).setProperty(AUTHENTICATED_USER_REQUEST_PROPERTY, mockUser);
    }

    @Test
    public void when_valid_authorization_header_comes_in_filter_saves_user_principal_for_resource_classes_to_access()
            throws IOException {
        when(mockContainerRequestContext.getHeaderString(AUTHORIZATION))
                .thenReturn(BEARER_HEADER_PREFIX + TOKEN_STRING);
        when(mockUserManagementService.verifySessionToken(TOKEN_STRING)).thenReturn(mockUser);
        when(mockUser.getEmail()).thenReturn(EMAIL);

        authenticationFilter.filter(mockContainerRequestContext);

        verify(mockContainerRequestContext).setSecurityContext(securityContextCaptor.capture());

        assertThat(securityContextCaptor.getValue().getUserPrincipal().getName(), is(EMAIL_STRING));
    }

}
