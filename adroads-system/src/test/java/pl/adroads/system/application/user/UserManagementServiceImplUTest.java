package pl.adroads.system.application.user;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static com.spencerwi.hamcrestJDK8Time.matchers.IsWithin.within;
import static java.lang.Boolean.TRUE;
import static java.time.LocalDateTime.now;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.adroads.system.CommonTestFixtures.EMAIL;
import static pl.adroads.system.CommonTestFixtures.SESSION_TOKEN_VALUE;
import static pl.adroads.system.CommonTestFixtures.TOKEN_STRING;
import static pl.adroads.system.CommonTestFixtures.USER_ID;
import static pl.adroads.system.domain.user.model.UserType.DRIVER;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.Optional;

import javax.ws.rs.core.UriBuilder;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import pl.adroads.system.application.user.email.EmailService;
import pl.adroads.system.application.user.exception.InvalidSessionTokenException;
import pl.adroads.system.application.user.exception.SessionTokenNotFoundException;
import pl.adroads.system.application.user.exception.UserNotFoundException;
import pl.adroads.system.domain.DomainEventPublisher;
import pl.adroads.system.domain.user.TokenFactory;
import pl.adroads.system.domain.user.UserRepository;
import pl.adroads.system.domain.user.events.UserActivatedDomainEvent;
import pl.adroads.system.domain.user.model.SessionToken;
import pl.adroads.system.domain.user.model.User;
import pl.adroads.system.domain.user.model.VerificationToken;

@RunWith(MockitoJUnitRunner.class)
public class UserManagementServiceImplUTest {

    private static final String STRING_1 = "string1";
    private static final String STRING_2 = "string2";
    private static final String STRING_3 = "string3";
    private static final String[] STRING_ARRAY = new String[3];
    public static final String VERIFICATION_WEBSITE_URL = "verificationWebsiteURL";
    private static final String URL_PARAM_NAME = "url";

    static {
        STRING_ARRAY[0] = STRING_1;
        STRING_ARRAY[1] = STRING_2;
        STRING_ARRAY[2] = STRING_3;
    }

    private static final int EXPECTED_EXPIRY_DAYS = 2;
    private static final int EXPIRY_DAYS = 2;
    private static final String EXPIRY_DAYS_FIELD = "verificationTokenExpiryInDays";
    private static final String VERIFICATION_ENDPOINT_URL_FIELD = "verificationEndpointUrl";
    private static final String VERIFICATION_WEBSITE_URL_FIELD = "verificationWebsiteUrl";

    @Mock
    private TokenFactory mockTokenFactory;
    @Mock
    private UserRepository mockUserStore;
    @Mock
    private EmailService mockEmailService;
    @Mock
    private DomainEventPublisher mockPublisher;
    @InjectMocks
    private UserManagementServiceImpl userService;
    @Mock
    private User mockUser;
    @Mock
    private SessionToken mockSessionToken;
    @Mock
    private VerificationToken mockVerificationToken;
    @Captor
    private ArgumentCaptor<String> stringCaptor;
    @Captor
    private ArgumentCaptor<LocalDateTime> dateCaptor;
    @Captor
    private ArgumentCaptor<URI> uriCaptor;
    @Captor
    private ArgumentCaptor<User> userCaptor;

    private static final String VERIFICATION_ENDPOINT_URL = "http://localhost:80";
    private static final String TOKEN_PARAM_NAME = "token";
    private static final String EMAIL_PARAM_NAME = "email";

    @Before
    public void setup() {
        ReflectionTestUtils.setField(userService, EXPIRY_DAYS_FIELD, EXPIRY_DAYS);
        ReflectionTestUtils.setField(userService, VERIFICATION_ENDPOINT_URL_FIELD, VERIFICATION_ENDPOINT_URL);
        ReflectionTestUtils.setField(userService, VERIFICATION_WEBSITE_URL_FIELD, VERIFICATION_WEBSITE_URL);

        when(mockUser.getUuid()).thenReturn(USER_ID);
        when(mockUser.getEmail()).thenReturn(EMAIL);
        when(mockVerificationToken.getToken()).thenReturn(TOKEN_STRING);
        when(mockTokenFactory
                .newUserverificationTokenOf(stringCaptor.capture(), anyString(), eq(EXPECTED_EXPIRY_DAYS),
                        dateCaptor.capture())).thenReturn(mockVerificationToken)
                                .thenReturn(mockVerificationToken);
    }

    @Test
    public void verifyRegisterMethodAssignsTokenToUserAndStoresIt() {

        userService.register(mockUser);

        verify(mockUser).assignVerificationToken(mockVerificationToken);
        verify(mockUserStore).store(mockUser);
        assertThat(dateCaptor.getValue(), within(EXPIRY_DAYS, DAYS).of(now()));
        assertThat(stringCaptor.getValue(), containsString(USER_ID.getValue().toString()));
        assertThat(stringCaptor.getValue(), containsString(dateCaptor.getValue().toString()));
    }

    @Test
    public void verifyRegisterMethodAssemblesCorrectVerificationUrlAndDelegatesToEmailService() {

        userService.register(mockUser);

        verify(mockEmailService).sendEmailConfirmationTo(eq(mockUser), uriCaptor.capture());
        assertThat(uriCaptor.getValue(), is(getVerificationURL()));
    }

    @Test
    public void when_user_cannot_be_found_then_verifyToken_throws_an_exception() {
        when(mockUserStore.findByEmail(EMAIL)).thenReturn(Optional.<User> empty());

        catchException(userService).verifyToken(EMAIL, TOKEN_STRING);

        assertThat(caughtException(), instanceOf(UserNotFoundException.class));
    }

    @Test
    public void when_user_exists_then_verifyToken_verifies_token_and_updates_user_through_repository() {
        User user = new User(USER_ID, EMAIL, mockVerificationToken, DRIVER);
        when(mockUserStore.findByEmail(EMAIL)).thenReturn(of(user));
        when(mockVerificationToken.verifyAgainst(TOKEN_STRING)).thenReturn(TRUE);

        userService.verifyToken(EMAIL, TOKEN_STRING);

        verify(mockUserStore).update(user);
        verify(mockPublisher).publish(new UserActivatedDomainEvent(USER_ID));

    }

    private URI getVerificationURL() {
        return UriBuilder.fromPath(VERIFICATION_WEBSITE_URL).queryParam(TOKEN_PARAM_NAME, TOKEN_STRING)
                .queryParam(EMAIL_PARAM_NAME, EMAIL.getEmailValue())
                .queryParam(URL_PARAM_NAME, VERIFICATION_ENDPOINT_URL)
                .build();
    }

    @Test
    public void when_token_not_found_in_database_then_verifySessionToken_throws_exception() {
        when(mockUserStore.findBySessionToken(SESSION_TOKEN_VALUE)).thenReturn(empty());

        catchException(userService).verifySessionToken(SESSION_TOKEN_VALUE);

        assertThat(caughtException(), instanceOf(SessionTokenNotFoundException.class));
    }

    @Test
    public void when_token_is_invalid_then_verifySessionToken_throws_exception() {
        when(mockUserStore.findBySessionToken(SESSION_TOKEN_VALUE)).thenReturn(Optional.of(mockUser));
        when(mockUser.getSessionToken()).thenReturn(Optional.of(mockSessionToken));
        when(mockSessionToken.verifyAgainst(SESSION_TOKEN_VALUE)).thenReturn(false);

        catchException(userService).verifySessionToken(SESSION_TOKEN_VALUE);

        assertThat(caughtException(), instanceOf(InvalidSessionTokenException.class));
    }

    @Test
    public void when_token_valid_then_verifySessionToken_returns_found_user() {
        when(mockUserStore.findBySessionToken(SESSION_TOKEN_VALUE)).thenReturn(Optional.of(mockUser));
        when(mockUser.getSessionToken()).thenReturn(Optional.of(mockSessionToken));
        when(mockSessionToken.verifyAgainst(SESSION_TOKEN_VALUE)).thenReturn(true);

        User user = userService.verifySessionToken(SESSION_TOKEN_VALUE);

        assertThat(user, is(mockUser));
    }
}
