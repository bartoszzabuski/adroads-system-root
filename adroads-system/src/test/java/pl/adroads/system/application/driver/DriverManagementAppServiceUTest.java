package pl.adroads.system.application.driver;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.domain.driver.DriverService;
import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.domain.driver.model.DriverId;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static pl.adroads.system.CommonTestFixtures.DRIVER_ID_1;

@RunWith(MockitoJUnitRunner.class)
public class DriverManagementAppServiceUTest {

  @Mock
  private Driver mockDriver;

  @Mock
  private DriverService driverService;

  @InjectMocks
  private DriverManagementAppService driverManagementAppService;

  @Test
  public void test_registerNewDriver_delegates_to_domainService() {

    when(driverService.createNewDriver(mockDriver)).thenReturn(DRIVER_ID_1);

    DriverId driverId = driverManagementAppService.registerNewDriver(mockDriver);

    assertThat(driverId, equalTo(DRIVER_ID_1));
  }

  @Test
  public void test_activateDriver_delegates_to_domainService() {
    driverManagementAppService.activateDriver(DRIVER_ID_1);
    verify(driverService).activateDriver(DRIVER_ID_1);
  }

//  @Test
//  public void test_driverApprovedCampaign_delegates_to_domainService() {
//
//    driverManagementAppService.driverApprovedCampaign(DRIVER_ID_1, CAMPAIGN_ID);
//
//    verify(driverService).approveCampaign(DRIVER_ID_1);
//  }

//  TODO test transactionality !?!?!?

}
