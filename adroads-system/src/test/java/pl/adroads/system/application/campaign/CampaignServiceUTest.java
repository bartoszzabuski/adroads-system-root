package pl.adroads.system.application.campaign;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static java.lang.Boolean.TRUE;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;
import static pl.adroads.system.CommonTestFixtures.*;
import static pl.adroads.system.domain.driver.model.Status.*;
import static pl.adroads.system.domain.user.model.VerificationToken.VerificationTokenType.DRIVER_ACCEPTS_REJECTS_CAMPAIGN;

import java.util.Optional;
import java.util.concurrent.locks.Lock;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import pl.adroads.system.application.user.email.EmailService;
import pl.adroads.system.domain.DomainEventPublisher;
import pl.adroads.system.domain.UUIDGenerator;
import pl.adroads.system.domain.campaign.CampaignRepository;
import pl.adroads.system.domain.campaign.events.DriverAddedToCampaignDomainEvent;
import pl.adroads.system.domain.campaign.exception.CampaignNotFoundException;
import pl.adroads.system.domain.campaign.model.Campaign;
import pl.adroads.system.domain.campaign.model.CampaignId;
import pl.adroads.system.domain.driver.DriverRepository;
import pl.adroads.system.domain.driver.exception.DriverAlreadyParticipatesInCampaignException;
import pl.adroads.system.domain.driver.exception.DriverNotFoundException;
import pl.adroads.system.domain.driver.exception.DriverNotFullyRegisteredException;
import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.domain.user.TokenFactory;
import pl.adroads.system.domain.user.model.VerificationToken;

@RunWith(MockitoJUnitRunner.class)
public class CampaignServiceUTest {

    public static final Campaign CAMPAIGN = Campaign
            .campaignOf(CAMPAIGN_NAME, CAMPAIGN_DESCRIPTION, START_TIME, END_TIME);
    public static final int TOKEN_EXPIRY_IN_DAYS = 2;

    @Mock
    private Lock mockLock;
    @Mock
    private UUIDGenerator idGenerator;
    @Mock
    private CampaignRepository campaignRepository;
    @Mock
    private DriverRepository driverRepository;

    @InjectMocks
    private CampaignService campaignService;
    @Mock
    private Campaign mockCampaign;
    @Mock
    private Driver mockDriver;

    @Mock
    private DomainEventPublisher domainEventPublisher;
    @Mock
    private TokenFactory tokenFactory;
    @Mock
    private EmailService emailService;
    @Captor
    private ArgumentCaptor<VerificationToken> tokenCaptor;
    @Mock
    private VerificationToken mockToken;

    @Before
    public void setup() throws InterruptedException {
        when(mockLock.tryLock(anyLong(), eq(MILLISECONDS))).thenReturn(TRUE);
        when(mockDriver.getUuid()).thenReturn(DRIVER_ID_1);
        when(mockCampaign.getUuid()).thenReturn(CAMPAIGN_ID);
        ReflectionTestUtils.setField(campaignService, "emailExpiryInDays", TOKEN_EXPIRY_IN_DAYS);

    }

    // TODO test campaign validation tests
    @Test
    public void test_createNewCampaign_assigns_id_and_delegates_to_store() {
        when(idGenerator.generateId()).thenReturn(VALID_UUID);

        CampaignId campaignId = campaignService.createNewCampaign(mockCampaign);

        assertThat(campaignId.getValue(), is(VALID_UUID));
        verify(campaignRepository).store(mockCampaign);
        verify(mockCampaign).assignID(CAMPAIGN_ID);
    }

    @Test
    public void when_driver_doesnt_exists_then_addPotentialDriver_throws_exception() {
        when(driverRepository.findDriverById(DRIVER_ID_1)).thenReturn(Optional.empty());

        catchException(campaignService).addPotentialDriver(CAMPAIGN_ID, DRIVER_ID_1);

        assertThat(caughtException(), instanceOf(DriverNotFoundException.class));
        verifyZeroInteractions(emailService, domainEventPublisher);
        verify(campaignRepository, never()).update(any());
    }

    @Test
    public void when_driver_already_taken_exists_then_addPotentialDriver_throws_exception() {
        when(driverRepository.findDriverById(DRIVER_ID_1)).thenReturn(Optional.of(mockDriver));
        when(mockDriver.getStatus()).thenReturn(TAKEN);

        catchException(campaignService).addPotentialDriver(CAMPAIGN_ID, DRIVER_ID_1);

        assertThat(caughtException(), instanceOf(DriverAlreadyParticipatesInCampaignException.class));
        verifyZeroInteractions(emailService, domainEventPublisher);
        verify(campaignRepository, never()).update(any());
    }

    @Test
    public void when_driver_not_fully_registered_then_addPotentialDriver_throws_exception() {
        when(driverRepository.findDriverById(DRIVER_ID_1)).thenReturn(Optional.of(mockDriver));
        when(mockDriver.getStatus()).thenReturn(CREATED);

        catchException(campaignService).addPotentialDriver(CAMPAIGN_ID, DRIVER_ID_1);

        assertThat(caughtException(), instanceOf(DriverNotFullyRegisteredException.class));
        verifyZeroInteractions(emailService, domainEventPublisher);
        verify(campaignRepository, never()).update(any());
    }

    @Test
    public void when_campaign_doesnt_exist_then_addPotentialDriver_throws_exception() {
        when(driverRepository.findDriverById(DRIVER_ID_1)).thenReturn(Optional.of(mockDriver));
        when(mockDriver.getStatus()).thenReturn(REGISTERED);
        when(campaignRepository.getLockFor(CAMPAIGN_ID.getValue().toString())).thenReturn(mockLock);
        when(campaignRepository.get(CAMPAIGN_ID)).thenReturn(Optional.empty());

        catchException(campaignService).addPotentialDriver(CAMPAIGN_ID, DRIVER_ID_1);

        assertThat(caughtException(), instanceOf(CampaignNotFoundException.class));
        verifyZeroInteractions(emailService, domainEventPublisher);
        verify(campaignRepository, never()).update(any());
    }

    @Test
    public void verify_addPotentialDriver_updates_campaign_and_saves_it_to_db() {
        when(driverRepository.findDriverById(DRIVER_ID_1)).thenReturn(Optional.of(mockDriver));
        when(mockDriver.getStatus()).thenReturn(REGISTERED);
        when(campaignRepository.getLockFor(CAMPAIGN_ID.getValue().toString())).thenReturn(mockLock);
        when(campaignRepository.get(CAMPAIGN_ID)).thenReturn(Optional.of(mockCampaign));
        when(tokenFactory.driverApprovesRejectsCampaignTokenOf(any(), any(), eq(TOKEN_EXPIRY_IN_DAYS), any())).thenReturn(mockToken);

        campaignService.addPotentialDriver(CAMPAIGN_ID, DRIVER_ID_1);

        verify(campaignRepository, times(1)).update(mockCampaign);
        verify(mockCampaign).addPotentialDriver(mockDriver, mockToken);
    }

    @Test
    public void verify_addPotentialDriver_sends_out_email_and_publishes_domain_event() {
        when(driverRepository.findDriverById(DRIVER_ID_1)).thenReturn(Optional.of(mockDriver));
        when(mockDriver.getStatus()).thenReturn(REGISTERED);
        when(campaignRepository.getLockFor(CAMPAIGN_ID.getValue().toString())).thenReturn(mockLock);
        when(campaignRepository.get(CAMPAIGN_ID)).thenReturn(Optional.of(mockCampaign));

        campaignService.addPotentialDriver(CAMPAIGN_ID, DRIVER_ID_1);

        verify(emailService, times(1)).sendEmailToAssignedDriver(mockDriver, mockCampaign);
        verify(domainEventPublisher, times(1)).publish(new DriverAddedToCampaignDomainEvent(DRIVER_ID_1));
    }

}
