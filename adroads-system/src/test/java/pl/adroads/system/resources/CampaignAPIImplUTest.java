package pl.adroads.system.resources;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.api.model.campaign.CreateCampaignRequest;
import pl.adroads.system.application.campaign.CampaignService;
import pl.adroads.system.domain.campaign.model.Campaign;
import pl.adroads.system.infrastructure.adapters.driven.campaign.integration.rest.assemblers.CampaignAssembler;

import java.net.URI;

import javax.ws.rs.core.Response;

import static org.apache.http.HttpStatus.SC_CREATED;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static pl.adroads.api.CampaignAPI.PATH;
import static pl.adroads.system.CommonTestFixtures.CAMPAIGN_ID;

@RunWith(MockitoJUnitRunner.class)
public class CampaignAPIImplUTest {

  @Mock
  private CampaignService mockCampaignService;
  @Mock
  private CampaignAssembler mockCampaignAssembler;

  @InjectMocks
  private CampaignAPIImpl campaignAPI;
  @Mock
  private CreateCampaignRequest mockCreateCampaignRequest;
  @Mock
  private Campaign mockCampaign;

  @Test
  public void test_createNewCampaign_assembles_domain_object_and_delegates_to_driverManagementService() {
    when(mockCampaignService.createNewCampaign(mockCampaign)).thenReturn(CAMPAIGN_ID);
    when(mockCampaignAssembler.assemble(mockCreateCampaignRequest)).thenReturn(mockCampaign);

    Response response = campaignAPI.createCampaign(mockCreateCampaignRequest);

    assertThat(response.getStatus(), equalTo(SC_CREATED));
    assertThat(response.getLocation(), equalTo(URI.create(PATH + CAMPAIGN_ID.getValue())));
  }
}