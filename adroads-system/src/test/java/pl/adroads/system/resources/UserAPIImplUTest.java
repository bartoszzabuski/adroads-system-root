package pl.adroads.system.resources;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import pl.adroads.system.application.user.UserManagementService;

import javax.ws.rs.core.Response;

import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.verify;
import static pl.adroads.system.CommonTestFixtures.EMAIL;
import static pl.adroads.system.CommonTestFixtures.EMAIL_STRING;
import static pl.adroads.system.CommonTestFixtures.TOKEN_STRING;

@RunWith(MockitoJUnitRunner.class)
public class UserAPIImplUTest {

  @Mock
  private UserManagementService mockUserManagementService;

  @InjectMocks
  private UserAPIImpl userAPIImpl;

  @Test
  public void test_verifyNewlyRegisteredUser_delegates_to_service() {
    userAPIImpl.verifyNewlyRegisteredUser(EMAIL_STRING, TOKEN_STRING);

    verify(mockUserManagementService).verifyToken(EMAIL, TOKEN_STRING);
  }

  @Test
  public void test_verifyNewlyRegisteredUser_returns_Ok_response() {
    Response response = userAPIImpl.verifyNewlyRegisteredUser(EMAIL_STRING, TOKEN_STRING);

    assertThat(response.getStatus(), is(SC_OK));
  }


}
