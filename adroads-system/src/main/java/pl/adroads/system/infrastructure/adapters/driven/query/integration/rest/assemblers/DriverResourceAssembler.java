package pl.adroads.system.infrastructure.adapters.driven.query.integration.rest.assemblers;

import static java.util.stream.Collectors.toList;

import java.util.List;

import javax.inject.Named;

import pl.adroads.api.model.driver.DriverCampaignDetailsResource;
import pl.adroads.api.model.driver.DriverResource;
import pl.adroads.api.model.shared.AddressResource;
import pl.adroads.api.model.shared.CarDetailsResource;
import pl.adroads.api.model.shared.DriverDetailsResource;
import pl.adroads.api.model.shared.WindscreenDimensionsResource;
import pl.adroads.system.domain.driver.model.Address;
import pl.adroads.system.domain.driver.model.CarDetails;
import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.domain.driver.model.DriverDetails;
import pl.adroads.system.domain.driver.model.WindscreenDimensions;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceAddress;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceCarDetails;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceDriverDetails;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceWindscreenDimensions;
import pl.adroads.system.infrastructure.adapters.driving.query.persistance.model.PersistenceCampaignDetails;
import pl.adroads.system.infrastructure.adapters.driving.query.persistance.model.PersistenceQueryDriver;

@Named
public class DriverResourceAssembler {

    public DriverResource assemble(final Driver driver) {
        return new DriverResource(driver.getUuid().getValue().toString(), driver.getName(),
                driver.getSurname(), driver.getDateOfBirth().toString(),
                driver.getEmail().getValue(), driver.getStatus().name(),
                assembleAddressResource(driver.getAddress()),
                assembleCarDetailsResource(driver.getCarDetails()),
                assembleDriverDetailsResource(driver.getDriverDetails()), null);
    }

    public DriverResource assemble(final PersistenceQueryDriver driver) {
        return new DriverResource(driver.getUuid(), driver.getName(),
                driver.getSurname(), driver.getDateOfBirth(),
                driver.getEmail(), driver.getStatus(),
                assembleAddressResource(driver.getAddress()),
                assembleCarDetailsResource(driver.getCarDetails()),
                assembleDriverDetailsResource(driver.getDriverDetails()),
                assembleDriverCampaignDetailsResource(driver.getCampaignDetails()));
    }

    private DriverCampaignDetailsResource assembleDriverCampaignDetailsResource(
            PersistenceCampaignDetails campaignDetails) {
        if (campaignDetails != null) {
            return new DriverCampaignDetailsResource(campaignDetails.getCampaignId(),
                    campaignDetails.getCampaignName());
        } else {
            return null;
        }
    }

    public List<DriverResource> assemble(final List<PersistenceQueryDriver> drivers) {
        return drivers.stream().map(this::assemble).collect(toList());
    }

    private AddressResource assembleAddressResource(final PersistenceAddress address) {
        return new AddressResource(address.getPostcode(), address.getCity(), address.getStreet(),
                address.getProvince());
    }

    private DriverDetailsResource assembleDriverDetailsResource(final PersistenceDriverDetails driverDetails) {
        return new DriverDetailsResource(driverDetails.getType(),
                driverDetails.getMonthlyDistanceIndex());
    }

    private CarDetailsResource assembleCarDetailsResource(final PersistenceCarDetails carDetails) {
        return new CarDetailsResource(carDetails.getMake(), carDetails.getModel(),
                carDetails.getYear(),
                carDetails.getMileage(),
                carDetails.getBody(),
                carDetails.getColour(),
                assembleWindscreenDimensionsResource(carDetails.getWindscreenDimensions()));
    }

    private WindscreenDimensionsResource assembleWindscreenDimensionsResource(
            final PersistenceWindscreenDimensions windscreenDimensions) {
        return new WindscreenDimensionsResource(windscreenDimensions.getHeight(),
                windscreenDimensions.getWidth());
    }

    private AddressResource assembleAddressResource(final Address address) {
        return new AddressResource(address.getPostcode().getValue(), address.getCity().getValue(),
                address.getStreet().getValue(), address.getProvince().toString());
    }

    private DriverDetailsResource assembleDriverDetailsResource(final DriverDetails driverDetails) {
        return new DriverDetailsResource(driverDetails.getType().getValue(),
                driverDetails.getMonthlyDistanceIndex());
    }

    private CarDetailsResource assembleCarDetailsResource(final CarDetails carDetails) {
        return new CarDetailsResource(carDetails.getMake(), carDetails.getModel(),
                carDetails.getYear().toString(),
                carDetails.getMileage().getValue(),
                carDetails.getBody().toString(),
                carDetails.getColour().getValue(),
                assembleWindscreenDimensionsResource(
                        carDetails.getWindscreenDimensions()));
    }

    private WindscreenDimensionsResource assembleWindscreenDimensionsResource(
            final WindscreenDimensions windscreenDimensions) {
        return new WindscreenDimensionsResource(windscreenDimensions.getHeight().getValue(),
                windscreenDimensions.getWidth().getValue());
    }

}
