package pl.adroads.system.infrastructure.adapters.shared.postgre;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model.PersistenceCampaign;
import pl.adroads.system.infrastructure.adapters.shared.postgre.util.PersistenceCampaignMapper;

@UseStringTemplate3StatementLocator("/sql/templates/CampaignsQueryDao.sql.stg")
public interface CampaignsQueryDao {

    @SqlQuery
    @RegisterMapper(PersistenceCampaignMapper.class)
    List<PersistenceCampaign> getCampaigns(@Define("tableName") String tableName,
            @Define("condition") String condition);

    @SqlQuery
    @RegisterMapper(PersistenceCampaignMapper.class)
    PersistenceCampaign getCampaign(@Define("tableName") String tableName, @Define("condition") String condition);

    // @SqlQuery
    // @RegisterMapper(PersistenceCampaignMapper.class)
    // List<PersistenceCampaign> getCampaigns(@Define("tableName") String tableName,
    // @BindOptional("condition") Optional<String> condition);

}
