package pl.adroads.system.infrastructure.adapters.shared.postgre.util;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.inject.Named;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model.PersistenceCampaign;
import pl.adroads.system.infrastructure.adapters.shared.ObjectSerializer;

@Named
public class PersistenceCampaignMapper implements ResultSetMapper<PersistenceCampaign> {

    private static final String DATA_COLUMN = "data";

    // TODO investigate why injetion doesnt work here
    private final ObjectSerializer objectSerializer = new ObjectSerializer(false, false);

    @Override
    public PersistenceCampaign map(int i, ResultSet resultSet, StatementContext statementContext)
            throws SQLException {
        String dataJSON = resultSet.getString(DATA_COLUMN);
        return objectSerializer.deserialize(dataJSON, PersistenceCampaign.class);
    }
}
