package pl.adroads.system.infrastructure.adapters.driving.query.persistance.model;

import lombok.Data;
import pl.adroads.system.common.domain.PersistenceAggregate;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceAddress;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceCarDetails;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceDriverDetails;

@Data
public class PersistenceQueryDriver extends PersistenceAggregate<String> {

    private String name;
    private String surname;
    private String dateOfBirth;
    private String email;
    private PersistenceAddress address;
    private String status;
    private PersistenceCarDetails carDetails;
    private PersistenceDriverDetails driverDetails;
    private PersistenceCampaignDetails campaignDetails;

    public PersistenceQueryDriver(final String uuid, final String name, final String surname,
            final String dateOfBirth, final String email, final PersistenceAddress address,
            final String status, final PersistenceCarDetails carDetails,
            final PersistenceDriverDetails driverDetails, final PersistenceCampaignDetails campaignDetails) {
        this.uuid = uuid;
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.email = email;
        this.address = address;
        this.status = status;
        this.carDetails = carDetails;
        this.driverDetails = driverDetails;
        this.campaignDetails = campaignDetails;
    }
}
