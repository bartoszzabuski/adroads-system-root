package pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.repository;

import java.util.Optional;
import java.util.concurrent.locks.Lock;

import javax.inject.Inject;
import javax.inject.Named;

import pl.adroads.system.domain.campaign.CampaignRepository;
import pl.adroads.system.domain.campaign.model.Campaign;
import pl.adroads.system.domain.campaign.model.CampaignId;
import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.assemblers.PersistenceCampaignAssembler;
import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model.PersistenceCampaign;
import pl.adroads.system.infrastructure.adapters.shared.CampaignStorage;
import pl.adroads.system.infrastructure.locking.HazelcastLockStore;

@Named
public class CampaignRepositoryImpl implements CampaignRepository {

    @Inject
    private CampaignStorage campaignStore;

    @Inject
    private PersistenceCampaignAssembler persistenceCampaignAssembler;

    @Inject
    private HazelcastLockStore campaignRepositoryLockStore;

    @Override
    public void store(final Campaign campaign) {
        PersistenceCampaign persistenceCampaign = persistenceCampaignAssembler.assemble(campaign);
        campaignStore.save(persistenceCampaign);
    }

    @Override
    public void update(Campaign campaign) {
        PersistenceCampaign persistenceCampaign = persistenceCampaignAssembler.assemble(campaign);
        campaignStore.update(persistenceCampaign);
    }

    @Override
    public Optional<Campaign> get(final CampaignId campaignId) {
        Optional<PersistenceCampaign> persistenceCampaign = campaignStore
                .get(campaignId.getValue().toString());
        if (!persistenceCampaign.isPresent()) {
            return Optional.empty();
        }
        return Optional.of(persistenceCampaignAssembler.dissemble(persistenceCampaign.get()));
    }

    @Override
    public Lock getLockFor(String key) {
        return campaignRepositoryLockStore.getLockFor(key);
    }
}
