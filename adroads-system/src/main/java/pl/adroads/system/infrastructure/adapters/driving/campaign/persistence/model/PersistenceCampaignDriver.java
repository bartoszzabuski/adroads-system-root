package pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import pl.adroads.system.infrastructure.adapters.driving.user.persistence.model.PersistenceVerificationToken;

@AllArgsConstructor
@Data
@EqualsAndHashCode(of = "uuid")
public class PersistenceCampaignDriver {

    private final String uuid;
    private String email;
    private String status;
    private final PersistenceVerificationToken persistenceVerificationToken;

}
