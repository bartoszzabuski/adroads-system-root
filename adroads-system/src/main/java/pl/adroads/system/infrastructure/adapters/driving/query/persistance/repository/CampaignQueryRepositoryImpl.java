package pl.adroads.system.infrastructure.adapters.driving.query.persistance.repository;

import static java.lang.String.format;

import java.time.LocalDate;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import pl.adroads.system.domain.campaign.model.CampaignId;
import pl.adroads.system.domain.query.CampaignQueryRepository;
import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model.PersistenceCampaign;
import pl.adroads.system.infrastructure.adapters.shared.postgre.CampaignsQueryDao;

@Named
public class CampaignQueryRepositoryImpl implements CampaignQueryRepository {

    public static final String TBL_CAMPAIGNS_TABLE_NAME = "tbl_campaigns";
    private static final String WHERE_START_DATE_GREATER_THAN = " to_date(data->> 'startDate', 'YYYY-MM-DD') >= '%s' ";
    private static final String WHERE_UUID_EQUALS_TO = " id = '%s' ";

    @Inject
    private CampaignsQueryDao campaignsQueryDao;

    // TODO fix it move query to stg file
    @Override
    public List<PersistenceCampaign> getCampaigns() {
        String condition = format(WHERE_START_DATE_GREATER_THAN, LocalDate.now().toString());
        return campaignsQueryDao.getCampaigns(TBL_CAMPAIGNS_TABLE_NAME, condition);
    }

    @Override
    public PersistenceCampaign getCampaign(final CampaignId campaignId) {
        String condition = format(WHERE_UUID_EQUALS_TO, campaignId.getValue());
        return campaignsQueryDao.getCampaign(TBL_CAMPAIGNS_TABLE_NAME, condition);
    }
}
