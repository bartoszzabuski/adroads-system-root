package pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class PersistenceAddress {

  private String postcode;
  private String street;
  private String city;
  private String province;

}
