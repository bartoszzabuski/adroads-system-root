package pl.adroads.system.infrastructure.locking;

public interface VoidLockedOperation<K> {

    void perform(K k);

}
