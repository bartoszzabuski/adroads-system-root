package pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model;

import pl.adroads.system.common.domain.PersistenceAggregate;
import pl.adroads.system.domain.campaign.model.CampaignDriver;

import java.util.Set;

import lombok.Data;

@Data
public class PersistenceCampaign extends PersistenceAggregate<String> {

  private final String name;
  private final String description;
  private final String startDate;
  private final String endDate;
  private final String campaignStatus;
  private final Set<PersistenceCampaignDriver> drivers;

  public PersistenceCampaign(String uuid, String name, String description, String startDate,
      String endDate, String campaignStatus, Set<PersistenceCampaignDriver> campaignDrivers) {
    this.uuid = uuid;
    this.name = name;
    this.description = description;
    this.startDate = startDate;
    this.endDate = endDate;
    this.campaignStatus = campaignStatus;
    this.drivers = campaignDrivers;
  }
}
