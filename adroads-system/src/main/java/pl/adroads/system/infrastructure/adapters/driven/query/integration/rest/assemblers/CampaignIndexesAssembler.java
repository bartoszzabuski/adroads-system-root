package pl.adroads.system.infrastructure.adapters.driven.query.integration.rest.assemblers;

import static java.util.stream.Collectors.toList;

import java.util.List;

import javax.inject.Named;

import pl.adroads.api.model.campaign.CampaignIndexResource;
import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model.PersistenceCampaign;

@Named
public class CampaignIndexesAssembler {

    public List<CampaignIndexResource> assemble(final List<PersistenceCampaign> persistenceCampaigns) {
        return persistenceCampaigns.stream()
                .map(campaign -> new CampaignIndexResource(campaign.getUuid(), campaign.getName()))
                .collect(toList());
    }
}
