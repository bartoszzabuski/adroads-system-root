package pl.adroads.system.infrastructure.adapters.driven.query.integration.rest.assemblers;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Set;

import javax.inject.Named;

import pl.adroads.api.model.campaign.CampaignDriverResource;
import pl.adroads.api.model.campaign.CampaignResource;
import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model.PersistenceCampaign;
import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model.PersistenceCampaignDriver;

@Named
public class CampaignResourceAssembler {

    // public List<CampaignResource> assemble(final List<Campaign> campaigns) {
    // return campaigns.stream().map(this::assemble).collect(toList());
    // }

    // private CampaignResource assemble(final Campaign campaign) {
    // return new CampaignResource(campaign.getName(), campaign.getDescription(),
    // campaign.getStartDateTime().toString(),
    // campaign.getEndDateTime().toString(),
    // assembleDriverResources(campaign.getDrivers()));
    // }

    // private List<CampaignDriverResource> assembleDriverResources(final Set<CampaignDriver> drivers) {
    // return drivers.stream().map(this::assembleDriverResource).collect(toList());
    // }
    //
    // private CampaignDriverResource assembleDriverResource(final CampaignDriver driver) {
    // return new CampaignDriverResource(driver.getDriverId().getValue().toString());
    // }

    public List<CampaignResource> assemble(final List<PersistenceCampaign> campaigns) {
        return campaigns.stream().map(this::assemble).collect(toList());
    }

    public CampaignResource assemble(final PersistenceCampaign campaign) {
        return new CampaignResource(campaign.getUuid(), campaign.getName(), campaign.getDescription(),
                campaign.getStartDate(),
                campaign.getEndDate(), assembleDriverResources(campaign.getDrivers()));
    }

    private List<CampaignDriverResource> assembleDriverResources(
            final Set<PersistenceCampaignDriver> persistenceCampaignDrivers) {
        return persistenceCampaignDrivers.stream().map(this::assembleDriverResource).collect(toList());
    }

    private CampaignDriverResource assembleDriverResource(final PersistenceCampaignDriver persistenceCampaignDriver) {
        return new CampaignDriverResource(persistenceCampaignDriver.getUuid(), persistenceCampaignDriver.getEmail(),
                persistenceCampaignDriver.getStatus());
    }
}
