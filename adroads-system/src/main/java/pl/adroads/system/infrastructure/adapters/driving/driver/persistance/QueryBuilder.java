package pl.adroads.system.infrastructure.adapters.driving.driver.persistance;

import pl.adroads.system.domain.driver.model.PolishCarBody;
import pl.adroads.system.domain.driver.model.PolishProvince;
import pl.adroads.system.domain.driver.model.Status;

import java.util.List;
import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.join;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.CAR_BODY_KEY_AS_TEXT;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.CAR_COLOUR_AS_TEXT;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.CAR_MAKE_KEY_AS_TEXT;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.CAR_MILEAGE_KEY_AS_TEXT;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.CAR_MODEL_KEY_AS_TEXT;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.CAR_YEAR_KEY_AS_TEXT;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.CITY_KEY_AS_TEXT;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.EMAIL_KEY;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.NAME_KEY;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.POSTCODE_KEY_AS_TEXT;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.PROVINCE_KEY_AS_TEXT;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.STATUS_KEY;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.STREET_KEY_AS_TEXT;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.DriversTableJSONKeys.SURNAME_KEY;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.PostgresSqlConditionsHelper.anyInArray;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.PostgresSqlConditionsHelper.equalTo;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.PostgresSqlConditionsHelper.greaterThanOrEqual;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.PostgresSqlConditionsHelper.iLike;
import static pl.adroads.system.infrastructure.adapters.driving.driver.persistance.PostgresSqlConditionsHelper.lessThanOrEqualTo;

public class QueryBuilder {

  private static final String AND = " AND ";

  private final List<String> andConditions;

  public QueryBuilder() {
    this.andConditions = newArrayList();
  }

  public Optional<String> build() {
    if (!andConditions.isEmpty()) {
      return Optional.of(joinedConditions());
    } else {
      return Optional.empty();
    }
  }

  private String joinedConditions() {
    return join(andConditions, AND);
  }

  public QueryBuilder searchName(final Optional<String> name) {
    if (name.isPresent()) {
      contains(NAME_KEY, name.get());
    }
    return this;
  }

  public QueryBuilder searchEmail(final Optional<String> email) {
    if (email.isPresent()) {
      contains(EMAIL_KEY, email.get());
    }
    return this;
  }

  public QueryBuilder searchSurname(final Optional<String> surname) {
    if (surname.isPresent()) {
      contains(SURNAME_KEY, surname.get());
    }
    return this;
  }

  public QueryBuilder withStatus(final Optional<Status> status) {
    if (status.isPresent()) {
      isEqualTo(STATUS_KEY, status.get().name());
    }
    return this;
  }

  private void contains(final String key, final String value) {
    and(iLike(key, surroundedWithPercentage(value)));
  }

  private boolean and(final String condition) {
    return andConditions.add(condition);
  }

  private String surroundedWithPercentage(final String value) {
    return format("%%%s%%", value);
  }

  private void startsWith(final String key, final String value) {
    and(iLike(key, startsWithPercentage(value)));
  }

  private String startsWithPercentage(final String value) {
    return format("%s%%", value);
  }

  private void isEqualTo(final String key, final String value) {
    and(equalTo(key, value));
  }

  private void anyValue(final String key, final List<String> values) {
    and(anyInArray(key, values));
  }

  private void biggerThan(final String key, final Integer value) {
    and(greaterThanOrEqual(key, value));
  }

  private void smallerThanOrEqual(final String key, final Integer value) {
    and(lessThanOrEqualTo(key, value));
  }

  public QueryBuilder searchStreet(final Optional<String> street) {
    if (street.isPresent()) {
      contains(STREET_KEY_AS_TEXT, street.get());
    }
    return this;
  }

  public QueryBuilder searchPostcode(final Optional<String> postcode) {
    if (postcode.isPresent()) {
      startsWith(POSTCODE_KEY_AS_TEXT, postcode.get());
    }
    return this;
  }

  public QueryBuilder searchCity(final Optional<String> city) {
    if (city.isPresent()) {
      contains(CITY_KEY_AS_TEXT, city.get());
    }
    return this;
  }

  public QueryBuilder withProvince(final Optional<PolishProvince> province) {
    if (province.isPresent()) {
      isEqualTo(PROVINCE_KEY_AS_TEXT, province.get().toString());
    }
    return this;
  }

  public QueryBuilder searchCarMake(final Optional<String> carMake) {
    if (carMake.isPresent()) {
      startsWith(CAR_MAKE_KEY_AS_TEXT, carMake.get());
    }
    return this;
  }


  public QueryBuilder searchCarModel(final Optional<String> carModel) {
    if (carModel.isPresent()) {
      contains(CAR_MODEL_KEY_AS_TEXT, carModel.get());
    }
    return this;
  }

  public QueryBuilder withCarBody(Optional<List<PolishCarBody>> carBodies) {
    if (carBodies.isPresent()) {
      List<String> orConditions = carBodies.get().stream()
	  .map(carBody -> withQuotes(carBody.toString())).collect(toList());
      anyValue(CAR_BODY_KEY_AS_TEXT, orConditions);
    }
    return this;
  }


  private String withQuotes(final String string) {
    return format("'%s'", string);
  }

  public QueryBuilder mileageFrom(final Optional<Integer> mileageFrom) {
    if (mileageFrom.isPresent()) {
      biggerThan(CAR_MILEAGE_KEY_AS_TEXT, mileageFrom.get());
    }
    return this;
  }


  public QueryBuilder mileageTo(final Optional<Integer> mileageTo) {
    if (mileageTo.isPresent()) {
      smallerThanOrEqual(CAR_MILEAGE_KEY_AS_TEXT, mileageTo.get());
    }
    return this;
  }

  public QueryBuilder yearFrom(final Optional<Integer> yearFrom) {
    if (yearFrom.isPresent()) {
      biggerThan(CAR_YEAR_KEY_AS_TEXT, yearFrom.get());
    }
     return this;
  }

  public QueryBuilder yearTo(final Optional<Integer> yearTo) {
    if (yearTo.isPresent()) {
      smallerThanOrEqual(CAR_YEAR_KEY_AS_TEXT, yearTo.get());
    }
    return this;
  }

  public QueryBuilder searchColour(final Optional<String> carColour) {
    if (carColour.isPresent()) {
      startsWith(CAR_COLOUR_AS_TEXT, carColour.get());
    }
    return this;
  }

}