package pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.assemblers;

import static jersey.repackaged.com.google.common.collect.Sets.newHashSet;
import static pl.adroads.system.domain.campaign.model.CampaignId.campaignIdOf;
import static pl.adroads.system.domain.driver.model.Email.emailOf;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Named;

import pl.adroads.system.domain.campaign.campaigndriverstatus.CampaignDriverStatus;
import pl.adroads.system.domain.campaign.model.Campaign;
import pl.adroads.system.domain.campaign.model.CampaignDriver;
import pl.adroads.system.domain.campaign.model.CampaignStatus;
import pl.adroads.system.domain.driver.model.DriverId;
import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model.PersistenceCampaign;
import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model.PersistenceCampaignDriver;
import pl.adroads.system.infrastructure.adapters.shared.assemblers.VerificationTokenAssembler;

@Named
public class PersistenceCampaignAssembler {

    @Inject
    private VerificationTokenAssembler verificationTokenAssembler;

    public PersistenceCampaign assemble(final Campaign campaign) {
        return new PersistenceCampaign(campaign.getUuid().getValue().toString(), campaign.getName(),
                campaign.getDescription(),
                campaign.getStartDateTime().toString(),
                campaign.getEndDateTime().toString(),
                campaign.getCampaignStatus().name(),
                assembleDrivers(campaign.getDrivers()));
    }

    private Set<PersistenceCampaignDriver> assembleDrivers(Set<CampaignDriver> drivers) {
        Set<PersistenceCampaignDriver> persistenceDrivers = newHashSet();
        persistenceDrivers.addAll(drivers.stream().map(
                driver -> new PersistenceCampaignDriver(driver.getDriverId().getValue().toString(),
                        driver.getEmail().getEmailValue(),
                        driver.getCampaignDriverStatus().name(),
                        verificationTokenAssembler.assemble(driver.getApprovalRejectionToken())))
                .collect(Collectors.toList()));
        return persistenceDrivers;
    }

    public Campaign dissemble(final PersistenceCampaign persistanceCampaign) {
        Campaign campaign = new Campaign(persistanceCampaign.getName(),
                persistanceCampaign.getDescription(),
                LocalDateTime.parse(persistanceCampaign.getStartDate()),
                LocalDateTime.parse(persistanceCampaign.getEndDate()),
                CampaignStatus
                        .valueOf(persistanceCampaign.getCampaignStatus()),
                dissembleDrivers(persistanceCampaign.getDrivers()));
        campaign.assignID(campaignIdOf(persistanceCampaign.getUuid()));
        return campaign;
    }

    private Set<CampaignDriver> dissembleDrivers(
            Set<PersistenceCampaignDriver> persistenceCampaignDrivers) {
        return persistenceCampaignDrivers.stream().map(
                persistenceDriver -> new CampaignDriver(DriverId.of(persistenceDriver.getUuid()),
                        emailOf(persistenceDriver.getEmail()),
                        CampaignDriverStatus.valueOf(persistenceDriver.getStatus()),
                        verificationTokenAssembler.disassemble(persistenceDriver.getPersistenceVerificationToken())))
                .collect(Collectors.toSet());
    }

    // public List<Campaign> dissemble(final List<PersistenceCampaign> persistenceCampaigns) {
    // return persistenceCampaigns.stream().map(this::dissemble).collect(Collectors.toList());
    // }
}
