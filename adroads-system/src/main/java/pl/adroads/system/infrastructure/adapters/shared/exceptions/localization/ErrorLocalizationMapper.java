package pl.adroads.system.infrastructure.adapters.shared.exceptions.localization;

import static com.google.common.collect.Maps.newHashMap;
import static pl.adroads.system.application.ApplicationLayerError.INVALID_SESSION_TOKEN;
import static pl.adroads.system.application.ApplicationLayerError.SESSION_TOKEN_NOT_FOUND;
import static pl.adroads.system.application.ApplicationLayerError.USER_NOT_FOUND;
import static pl.adroads.system.domain.driver.exception.CampaignDomainError.CAMPAIGN_NOT_FOUND;
import static pl.adroads.system.domain.driver.exception.DriverDomainError.DRIVER_ALREADY_EXISTS;
import static pl.adroads.system.domain.driver.exception.DriverDomainError.DRIVER_ALREADY_TAKEN;
import static pl.adroads.system.domain.driver.exception.DriverDomainError.DRIVER_NOT_FOUND;
import static pl.adroads.system.domain.driver.exception.DriverDomainError.DRIVER_NOT_FULLY_REGISTERED;
import static pl.adroads.system.domain.user.exception.UserDomainError.INVALID_LOGIN;
import static pl.adroads.system.domain.user.exception.UserDomainError.PASSWORDS_DONT_MATCH;
import static pl.adroads.system.domain.user.exception.UserDomainError.VERIFICATION_TOKEN_NOT_VALID;
import static pl.adroads.system.filters.exceptions.AuthorizationError.INVALID_AUTHORIZATION_HEADER;
import static pl.adroads.system.filters.exceptions.AuthorizationError.USER_NOT_AUTHORIZED;

import java.util.Map;

import javax.inject.Named;

import pl.adroads.system.common.util.errors.SystemError;

// TODO externalize messages to config file
// TODO add UTest!!!
@Named
public class ErrorLocalizationMapper {

    private static final Map<SystemError, String> mappedMessages = newHashMap();

    static {
        mappedMessages.put(DRIVER_NOT_FOUND, "Kierowca %s nie zostal znaleziony");
        mappedMessages.put(DRIVER_ALREADY_EXISTS, "%s istnieje juz w systemie");
        mappedMessages.put(DRIVER_NOT_FULLY_REGISTERED, "%s nie zostal aktywowany");

        mappedMessages.put(USER_NOT_FOUND, "Uzytkownik %s nie zostal znaleziony");
        mappedMessages.put(VERIFICATION_TOKEN_NOT_VALID, "Problem podczas weryfikacji uzytkownika %s");
        mappedMessages.put(PASSWORDS_DONT_MATCH, "Nieprawidłowy login lub hasło");
        mappedMessages.put(INVALID_LOGIN, "Nieprawidłowy login lub hasło");

        mappedMessages.put(CAMPAIGN_NOT_FOUND, "Kampania %s nie zostala znaleziona");
        mappedMessages.put(DRIVER_ALREADY_TAKEN, "Kierowca %s przypisany do innej kampanii.");

        mappedMessages.put(INVALID_AUTHORIZATION_HEADER, "Authorization header nie zostal znaleziony");
        mappedMessages.put(USER_NOT_AUTHORIZED, "Użytkownik %s nie autoryzowany");
        mappedMessages.put(SESSION_TOKEN_NOT_FOUND, "Nieprawidłowy token");
        mappedMessages.put(INVALID_SESSION_TOKEN, "Nieprawidłowy token");
    }

    // TODO consider adding default message
    public String getMessageFor(final SystemError systemError) {
        return mappedMessages.get(systemError);
    }

}
