package pl.adroads.system.infrastructure.adapters.driven.driver.integration.listeners;

import static pl.adroads.system.domain.user.model.User.userOf;
import static pl.adroads.system.domain.user.model.UserId.userIdOf;
import static pl.adroads.system.domain.user.model.UserType.DRIVER;

import javax.inject.Inject;
import javax.inject.Named;

import net.engio.mbassy.listener.Handler;
import pl.adroads.system.application.user.UserManagementService;
import pl.adroads.system.domain.driver.events.DriverCreatedEvent;
import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.domain.user.model.User;

@Named
public class DriversEventsListener {

    @Inject
    private UserManagementService userManagementService;

    @Handler
    public void driverCreated(final DriverCreatedEvent driverCreatedEvent) {
        Driver driver = driverCreatedEvent.getDriver();
        User user = userOf(userIdOf(driver.getUuid().getValue()), driver.getEmail(), DRIVER);
        userManagementService.register(user);
    }

}
