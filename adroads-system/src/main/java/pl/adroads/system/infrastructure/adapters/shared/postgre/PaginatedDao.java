package pl.adroads.system.infrastructure.adapters.shared.postgre;

import java.util.List;

public interface PaginatedDao<T> {

  abstract List<T> loadPage(int lastId, int pageSize);

}