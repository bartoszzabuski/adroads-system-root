package pl.adroads.system.infrastructure.adapters.driven.campaign.integration.listeners;

import javax.inject.Inject;
import javax.inject.Named;

import net.engio.mbassy.listener.Handler;
import pl.adroads.system.application.driver.DriverManagementAppService;
import pl.adroads.system.domain.campaign.events.DriverAddedToCampaignDomainEvent;

@Named
public class CampaignsEventsListener {

    @Inject
    private DriverManagementAppService driverManagementAppService;

    @Handler
    public void driverAddedToCampaign(DriverAddedToCampaignDomainEvent driverAddedToCampaignDomainEvent) {
        driverManagementAppService.assignToCampaign(driverAddedToCampaignDomainEvent.getDriverId());
    }
}
