package pl.adroads.system.infrastructure.adapters.shared;

// Copyright 2012,2013 Vaughn Vernon
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import java.lang.reflect.Type;

import javax.inject.Named;

@Named
public class ObjectSerializer extends AbstractSerializer {

    public ObjectSerializer(boolean isPretty, boolean isCompact) {
        super(isPretty, isCompact);
    }

    public <T extends Object> T deserialize(String aSerialization, final Class<T> aType) {
        return this.gson().fromJson(aSerialization, aType);
    }

    public <T extends Object> T deserialize(String aSerialization, final Type aType) {
        return this.gson().fromJson(aSerialization, aType);
    }

    public String serialize(Object anObject) {
        return this.gson().toJson(anObject);
    }

    private ObjectSerializer() {
        this(false, false);
    }
}
