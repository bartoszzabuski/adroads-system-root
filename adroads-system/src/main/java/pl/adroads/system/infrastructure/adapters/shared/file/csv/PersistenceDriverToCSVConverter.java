package pl.adroads.system.infrastructure.adapters.shared.file.csv;

import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.dozer.CsvDozerBeanWriter;
import org.supercsv.io.dozer.ICsvDozerBeanWriter;

import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceDriver;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;
import java.util.Optional;

import javax.inject.Named;

import lombok.extern.slf4j.Slf4j;

import static java.util.Optional.ofNullable;
import static org.supercsv.prefs.CsvPreference.STANDARD_PREFERENCE;

@Named
@Slf4j
public class PersistenceDriverToCSVConverter {

  //    @formatter:off

  private static final String[] FIELD_MAPPING = new String[]{
    "uuid",
    "name",
    "surname",
    "dateOfBirth",
    "email",
      "address.postcode",
      "address.street",
      "address.city",
      "address.province",
    "status",
      "carDetails.make",
      "carDetails.model",
      "carDetails.year",
      "carDetails.mileage",
      "carDetails.body",
      "carDetails.colour",
        "carDetails.windscreenDimensions.height",
        "carDetails.windscreenDimensions.width",
      "driverDetails.type",
      "driverDetails.monthlyDistanceIndex"
    };

  private static final CellProcessor[] PROCESSORS = new CellProcessor[]{
        new NotNull(),          // uuid
        new NotNull(),          // name
        new NotNull(),          // surname
        new NotNull(),          // dateOfBirth
        new NotNull(),          // email
        new NotNull(),          // address.postcode
        new NotNull(),          // address.street
        new NotNull(),          // address.city
        new NotNull(),          // address.province
        new NotNull(),          // status
        new NotNull(),          // carDetails.make
        new NotNull(),          // carDetails.model
        new NotNull(),          // carDetails.year
        new NotNull(),          // carDetails.mileage
        new NotNull(),          // carDetails.body
        new NotNull(),          // carDetails.colour
        new NotNull(),          // carDetails.windscreenDimensions.height
        new NotNull(),          // carDetails.windscreenDimensions.width
        new NotNull(),          // driverDetails.type
        new NotNull(),          // driverDetails.monthlyDistanceIndex
    };

//    @formatter:on

  public Optional<String> fromPersistenceDriver(final PersistenceDriver persistenceDriver) {
    return ofNullable(persistenceDriverToCSV(persistenceDriver));
  }

  private String persistenceDriverToCSV(PersistenceDriver persistenceDriver) {
    final Writer writer = new StringWriter();
    try {
      convertPersistentDriverToCSV(persistenceDriver, writer);
    } catch (IOException e) {
      log.error("Failed to convert persistenceDriver %s to CSV format", persistenceDriver.getUuid());
    }
    return writer.toString();
  }

  private void convertPersistentDriverToCSV(final PersistenceDriver persistenceDriver,
      final Writer writer) throws IOException {
    ICsvDozerBeanWriter beanWriter = null;
    try {
      beanWriter = new CsvDozerBeanWriter(writer, STANDARD_PREFERENCE);
      beanWriter.configureBeanMapping(PersistenceDriver.class, FIELD_MAPPING);
//      beanWriter.writeHeader(FIELD_MAPPING);
      beanWriter.write(persistenceDriver, PROCESSORS);
    } finally {
      if (beanWriter != null) {
	beanWriter.close();
      }
    }
  }

  public Optional<String> fromPersistenceDriver(final List<PersistenceDriver> persistenceDrivers) {
    StringBuilder stringBuilder = new StringBuilder();
    for (PersistenceDriver persistenceDriver : persistenceDrivers) {
      stringBuilder.append(fromPersistenceDriver(persistenceDriver)
			       .orElse("upsss sprawdz log bo cos jest nie tak.."));
    }
    return Optional.of(stringBuilder.toString());
  }
}
