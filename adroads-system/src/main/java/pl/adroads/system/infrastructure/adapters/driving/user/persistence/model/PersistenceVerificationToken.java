package pl.adroads.system.infrastructure.adapters.driving.user.persistence.model;


import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@AllArgsConstructor
@EqualsAndHashCode
public class PersistenceVerificationToken{

  private final String token;

  private final String expiryDate;

  private final String tokenType;

  private final String timestamp;

  private final boolean verified;

  private final String verificationDate;

}
