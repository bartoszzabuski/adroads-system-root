package pl.adroads.system.infrastructure.adapters.shared.postgre;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import pl.adroads.system.infrastructure.adapters.driving.query.persistance.model.PersistenceCampaignDriverDetails;
import pl.adroads.system.infrastructure.adapters.shared.postgre.util.LogSqlFactory;
import pl.adroads.system.infrastructure.adapters.shared.postgre.util.PersistenceCampaignDriverDetailsMapper;

@LogSqlFactory
@UseStringTemplate3StatementLocator("/sql/templates/CampaignDriversQueryDao.sql.stg")
public interface CampaignDriversQueryDao {

    @SqlQuery
    @RegisterMapper(PersistenceCampaignDriverDetailsMapper.class)
    List<PersistenceCampaignDriverDetails> getCampaignDetailsForDrivers(@Define("tableName") String tableName,
            @Define("driversIdsJsonArrays") String driversidsJsonArrays,
            @Define("driverIdsArray") String driverIdsArray);

}
