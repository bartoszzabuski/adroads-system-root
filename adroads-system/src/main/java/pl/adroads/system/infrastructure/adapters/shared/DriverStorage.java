package pl.adroads.system.infrastructure.adapters.shared;


import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceDriver;

import java.util.Optional;

public interface DriverStorage {

//  TODO rename it to 'store'
  void saveNewDriver(PersistenceDriver persistenceDriver);

  //  TODO rename it to 'update'
  void updateDriver(PersistenceDriver persistenceDriver);

  Optional<PersistenceDriver> findByEmail(String email);

  Optional<PersistenceDriver> findDriverById(String driverId);

}
