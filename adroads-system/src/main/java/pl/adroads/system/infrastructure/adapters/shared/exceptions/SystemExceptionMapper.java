package pl.adroads.system.infrastructure.adapters.shared.exceptions;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import lombok.extern.slf4j.Slf4j;
import pl.adroads.system.common.util.SystemException;
import pl.adroads.system.common.util.errors.SystemError;
import pl.adroads.system.infrastructure.adapters.shared.exceptions.localization.ErrorLocalizationMapper;

@Slf4j
@Provider
public class SystemExceptionMapper extends BaseExceptionMapper
        implements ExceptionMapper<SystemException> {

    @Inject
    private ErrorLocalizationMapper errorLocalizationMapper;

    @Override
    public Response toResponse(SystemException exception) {

        // TODO log all the errors

      SystemError systemError = exception.getSystemError();

        return buildExceptionResponse(systemError.getType(), systemError.getCode(),
                getMappedMessage(exception.getSystemError(),
                        exception.getParams()));
    }

    private String getMappedMessage(final SystemError systemError, final Object[] params) {
        final String localizedMessage = errorLocalizationMapper.getMessageFor(systemError);
        return String.format(localizedMessage, params);
    }

}
