package pl.adroads.system.infrastructure.locking;

import static java.lang.String.format;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.util.concurrent.locks.Lock;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LockingTemplate {

    private static final int MAX_NUMBER_OF_ATTEMPTS = 200;
    private static final long LOCKING_TIMEOUT_IN_MILLIS = 50L;

    public  <K> void performVoidWithLock(Lockable<K> lockable, K key, VoidLockedOperation<K> operation) {
        performWithLock(lockable, key, e -> {
            operation.perform(key);
            return null;
        });
    }

    public  <T, K> T performWithLock(Lockable<K> lockable, K key, LockedOperation<K, T> operation) {
        Lock lock = lockable.getLockFor(key);
        try {
            acquireLockOrQuit(lockable, key, lock);
            log.debug("lock acquired on {} for key {}", lockable, key);
            return operation.perform(key);
        } finally {
            lock.unlock();
            log.debug("lock released on {} for key {}", lockable, key);
        }
    }

    private <K> void acquireLockOrQuit(Lockable lockable, K key, Lock lock) {
        if (!attemptToAcquireLock(lock, key)) {
            throw new RuntimeException(
                    format("unable to acquire lock on %s for key %s after %s", lockable, key, MAX_NUMBER_OF_ATTEMPTS));
        }
    }

    private <K> boolean attemptToAcquireLock(Lock lock, K key) {
        boolean locked = false;
        for (int i = 0; !locked && i < MAX_NUMBER_OF_ATTEMPTS; i++) {
            try {
                locked = lock.tryLock(LOCKING_TIMEOUT_IN_MILLIS, MILLISECONDS);
            } catch (InterruptedException e) {
                log.warn(format("Lock attempt %d on %s was interrupted", i, key), e);
            }
        }
        return locked;
    }

}
