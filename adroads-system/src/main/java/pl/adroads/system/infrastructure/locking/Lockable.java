package pl.adroads.system.infrastructure.locking;

import java.util.concurrent.locks.Lock;

public interface Lockable<T> {

    Lock getLockFor(T t);

}
