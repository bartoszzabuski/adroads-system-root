package pl.adroads.system.infrastructure.adapters.driving.driver.persistance;


import static java.lang.String.format;

public final class DriversTableJSONKeys {

  private DriversTableJSONKeys() {
  }

  public static final String DATA = "data -> ";
  public static final String DATA_AS_TEXT = "data ->> ";

  public static final String CAR_DETAILS_COLOUR = "'carDetails' ->> 'colour'";

  public static final String NAME = "'name'";
  public static final String SURNAME = "'surname'";
  public static final String EMAIL = "'email'";
  public static final String STATUS = "'status'";
  public static final String ADDRESS_STREET = "'address' ->> 'street'";
  public static final String ADDRESS_POSTCODE = "'address' ->> 'postcode'";
  public static final String ADDRESS_CITY = "'address' ->> 'city'";
  public static final String ADDRESS_PROVINCE = "'address' ->> 'province'";
  public static final String CAR_DETAILS_MAKE = "'carDetails' ->> 'make'";
  public static final String CAR_DETAILS_MODEL = "'carDetails' ->> 'model'";
  public static final String CAR_DETAILS_BODY = "'carDetails' -> 'body'";
  public static final String CAR_DETAILS_MILEAGE = "'carDetails' ->> 'mileage'";
  public static final String CAR_DETAILS_YEAR = "'carDetails' ->> 'year'";

  public static final String NAME_KEY = constructExpression(DATA_AS_TEXT, NAME);
  public static final String SURNAME_KEY = constructExpression(DATA_AS_TEXT, SURNAME);
  public static final String EMAIL_KEY = constructExpression(DATA_AS_TEXT, EMAIL);
  public static final String STATUS_KEY = constructExpression(DATA_AS_TEXT, STATUS);
  public static final String STREET_KEY_AS_TEXT = constructExpression(DATA, ADDRESS_STREET);
  public static final String POSTCODE_KEY_AS_TEXT = constructExpression(DATA, ADDRESS_POSTCODE);
  public static final String CITY_KEY_AS_TEXT = constructExpression(DATA, ADDRESS_CITY);
  public static final String PROVINCE_KEY_AS_TEXT = constructExpression(DATA, ADDRESS_PROVINCE);
  public static final String CAR_MAKE_KEY_AS_TEXT = constructExpression(DATA, CAR_DETAILS_MAKE);
  public static final String CAR_MODEL_KEY_AS_TEXT = constructExpression(DATA, CAR_DETAILS_MODEL);
  public static final String CAR_BODY_KEY_AS_TEXT = constructExpression(DATA, CAR_DETAILS_BODY);
  public static final String CAR_MILEAGE_KEY_AS_TEXT = constructExpression(DATA,
									   CAR_DETAILS_MILEAGE);
  public static final String CAR_YEAR_KEY_AS_TEXT = constructExpression(DATA, CAR_DETAILS_YEAR);
  public static final String CAR_COLOUR_AS_TEXT = constructExpression(DATA, CAR_DETAILS_COLOUR);

  private static String constructExpression(final String prefix, final String key) {
    return format("%s %s", prefix, key);
  }

}
