package pl.adroads.system.infrastructure.locking;

import lombok.Getter;

import java.util.concurrent.locks.Lock;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;

@Getter
public class HazelcastLockStore<K, V> implements Lockable<K> {

    private final HazelcastInstance hazelcastInstance;
    private final String mapName;
    private final IMap<K, V> map;

    public HazelcastLockStore(HazelcastInstance hazelcastInstance, String mapName) {
        this.hazelcastInstance = hazelcastInstance;
        this.mapName = mapName;
        this.map = hazelcastInstance.getMap(mapName);
    }

    @Override
    public Lock getLockFor(K k) {
        return new HazelcastMapEntryLock(k, map);
    }
}
