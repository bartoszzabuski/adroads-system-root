package pl.adroads.system.infrastructure.locking;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

import lombok.AllArgsConstructor;

import com.hazelcast.core.IMap;

@AllArgsConstructor
public class HazelcastMapEntryLock<K, V> implements Lock {

    private final K key;
    private final IMap<K, V> map;

    @Override
    public void lock() {
        map.lock(key);
    }

    @Override
    public void lockInterruptibly() throws InterruptedException {
        throw new UnsupportedOperationException("lockInterruptibly not supported!");
    }

    @Override
    public boolean tryLock() {
        return map.tryLock(key);
    }

    @Override
    public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
        return map.tryLock(key, time, unit);
    }

    @Override
    public void unlock() {
        map.unlock(key);
    }

    @Override
    public Condition newCondition() {
        throw new UnsupportedOperationException("newCondition not supported!");
    }
}
