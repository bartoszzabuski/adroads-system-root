package pl.adroads.system.infrastructure.adapters.shared.postgre.util;

import org.postgresql.util.PGobject;

import pl.adroads.system.common.domain.PersistenceAggregate;
import pl.adroads.system.infrastructure.adapters.shared.ObjectSerializer;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import static com.google.common.collect.Lists.newArrayList;
import static pl.adroads.system.common.util.GlobalConstants.JSON_TYPE;

@Named
public class PGObjectAssembler {

  @Inject
  private ObjectSerializer serializer;

  public <E extends PersistenceAggregate<String>> PGobject toPGobject(final E anAggregateRoot)
      throws SQLException {
    return toPGobject(serializer.serialize(anAggregateRoot));
  }

  public PGobject toPGobject(final String json) throws SQLException {
    PGobject dataObject = new PGobject();
    dataObject.setType(JSON_TYPE);
    dataObject.setValue(json);
    return dataObject;
  }

  public <E extends PersistenceAggregate<String>> List<E> fromPGobjectList(final Class<E> aType,
      final List<PGobject> pgObjectList) {
    List<E> persistenceAggregates = newArrayList();
    for (PGobject pGobject : pgObjectList) {
      persistenceAggregates.add(fromPGobject(aType, pGobject));
    }
    return persistenceAggregates;
  }

  public <E extends PersistenceAggregate<String>> E fromPGobject(final Class<E> aType,
      final PGobject pgObject) {
    if (pgObject == null) {
      return null;
    }
    String jsonString = pgObject.getValue();
    return serializer.deserialize(jsonString, aType);
  }

}
