package pl.adroads.system.infrastructure.adapters.driving.user.persistence.assemblers;

import static pl.adroads.system.domain.driver.model.Email.emailOf;
import static pl.adroads.system.domain.user.model.UserId.userIdOf;

import java.time.LocalDateTime;
import java.util.Optional;

import javax.inject.Inject;
import javax.inject.Named;

import pl.adroads.system.domain.user.model.SessionToken;
import pl.adroads.system.domain.user.model.User;
import pl.adroads.system.domain.user.model.UserType;
import pl.adroads.system.infrastructure.adapters.driving.user.persistence.model.PersistenceSessionToken;
import pl.adroads.system.infrastructure.adapters.driving.user.persistence.model.PersistenceUser;
import pl.adroads.system.infrastructure.adapters.shared.assemblers.VerificationTokenAssembler;

@Named
public class PersistenceUserAssembler {

    @Inject
    private VerificationTokenAssembler verificationTokenAssembler;

    public PersistenceUser assemble(final User user) {
        return new PersistenceUser(user.getUuid().getValue().toString(), user.getEmail().getValue(),
                verificationTokenAssembler.assemble(user.getVerificationToken()), user.getUserType().name(),
                user.getPassword().orElse(null),
                assemble(user.getSessionToken()));
    }

    private PersistenceSessionToken assemble(final Optional<SessionToken> sessionTokenOptional) {
        if (!sessionTokenOptional.isPresent()) {
            return null;
        }
        SessionToken sessionToken = sessionTokenOptional.get();
        return new PersistenceSessionToken(sessionToken.getToken(), sessionToken.getTimestamp().toString(),
                sessionToken.getExpiryDate().toString());
    }

    public User disassemble(final PersistenceUser persistenceUser) {
        if (persistenceUser == null)
            return null;
        return new User(userIdOf(persistenceUser.getUuid()), emailOf(persistenceUser.getEmail()),
                verificationTokenAssembler.disassemble(persistenceUser.getPersistenceVerificationToken()),
                UserType.valueOf(persistenceUser.getUserType()), persistenceUser.getPassword(),
                disassemble(persistenceUser.getPersistenceSessionToken()));
    }

    private SessionToken disassemble(final PersistenceSessionToken persistenceSessionToken) {
        if (persistenceSessionToken == null) {
            return null;
        }
        return new SessionToken(persistenceSessionToken.getToken(),
                LocalDateTime.parse(persistenceSessionToken.getTimestamp()),
                LocalDateTime.parse(persistenceSessionToken.getExpiryDate()));
    }

}
