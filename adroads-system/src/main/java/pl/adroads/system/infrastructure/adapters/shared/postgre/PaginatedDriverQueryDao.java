package pl.adroads.system.infrastructure.adapters.shared.postgre;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;

import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceDriver;
import pl.adroads.system.infrastructure.adapters.shared.postgre.util.PersistenceDriverMapper;

import java.util.List;

public interface PaginatedDriverQueryDao extends PaginatedDao<PersistenceDriver> {

  final static String SELECT_ALL = "select data from tbl_drivers OFFSET :start LIMIT :size;";

  @SqlQuery(SELECT_ALL)
  @Mapper(PersistenceDriverMapper.class)
  @Override
  public List<PersistenceDriver> loadPage(@Bind("start") int start, @Bind("size") int pageSize);

  /**
   * close with no args is used to close the connection
   */
  void close();

}