package pl.adroads.system.infrastructure.adapters.driving.query.persistance.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import pl.adroads.system.common.domain.Persistence;

@Data
@AllArgsConstructor
public class PersistenceCampaignDriverDetails implements Persistence {

    private final String campaignId;
    private final String campaignName;
    private final String driverId;

}
