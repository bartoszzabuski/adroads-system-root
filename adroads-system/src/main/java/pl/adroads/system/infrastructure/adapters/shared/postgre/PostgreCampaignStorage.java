package pl.adroads.system.infrastructure.adapters.shared.postgre;

import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model.PersistenceCampaign;
import pl.adroads.system.infrastructure.adapters.shared.CampaignStorage;

import java.util.Optional;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class PostgreCampaignStorage implements CampaignStorage {

  private static final String CAMPAIGN_TABLE_NAME = "tbl_campaigns";

  @Inject
  private PostgreSQLJSONStorageFacade<PersistenceCampaign> storageFacade;

  @Override
  public void save(PersistenceCampaign persistenceCampaign) {
    storageFacade.insertAsJSON(CAMPAIGN_TABLE_NAME, persistenceCampaign);
  }

  @Override
  public void update(PersistenceCampaign persistenceCampaign) {
    storageFacade.updateAsJSON(CAMPAIGN_TABLE_NAME, persistenceCampaign);
  }

  @Override
  public Optional<PersistenceCampaign> get(String campaignId) {
    String filter = "id=CAST(? AS uuid)";
    return findAndAssembleCampaign(campaignId, filter);
  }

  private Optional<PersistenceCampaign> findAndAssembleCampaign(String campaignId, String filter) {
    PersistenceCampaign persistenceCampaign = storageFacade
        .findExact(CAMPAIGN_TABLE_NAME, PersistenceCampaign.class, filter, "", campaignId);
    return Optional.ofNullable(persistenceCampaign);
  }
}
