package pl.adroads.system.infrastructure.adapters.shared.postgre;

import org.postgresql.util.PGobject;
import org.springframework.jdbc.core.JdbcTemplate;

import pl.adroads.system.infrastructure.adapters.shared.postgre.util.PGObjectAssembler;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class PostgreSQLHelper {

  @Inject
  private JdbcTemplate jdbcTemplate;
  @Inject
  private PGObjectAssembler pgObjectAssembler;

  public void insert(final String tableName, final UUID uuid, final PGobject pgObjectData) {
    final String insertIntoDriverSQL = "insert into "
                                       + tableName
                                       + " (id, data) values (?, ?)";

    jdbcTemplate.update(insertIntoDriverSQL, uuid, pgObjectData);
  }

  public List<PGobject> findAllPGObject(final String tableName, final String aFilterExpression,
                                        final String anOrderBy,
                                        final Object... anArguments) throws SQLException {
    String query =
        "select data from "
        + tableName
        + " where "
        + pgObjectAssembler.toPGobject(aFilterExpression)
        + " "
        + anOrderBy;

    List<PGobject>
        pGobjectList =
        jdbcTemplate.queryForList(query, PGobject.class, anArguments);
    return pGobjectList;

  }

  public Integer count(final String tableName, final String aFilterExpression,
                       final Object... anArguments) throws SQLException {
    String query =
        "select count(*) from "
        + tableName
        + " where "
        + pgObjectAssembler.toPGobject(aFilterExpression);

    Integer count =
        jdbcTemplate.queryForObject(query, Integer.class, anArguments);
    return count;

  }

  public int update(final String tableName, final UUID uuid, final PGobject pgObjectData)
      throws SQLException {
    String query =
        "update "
        + tableName
        + " set data = ?"
        + " where id = ?";

    return jdbcTemplate.update(query, pgObjectData, uuid);
  }

}
