package pl.adroads.system.infrastructure.adapters.shared;

import org.springframework.jdbc.core.JdbcTemplate;

import pl.adroads.system.domain.UUIDGenerator;

import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.DataSource;

@Named
public class PosgreUUIDGenerator implements UUIDGenerator {

  private static final String SELECT_UUID_GENERATE_V4 = "SELECT md5(random()::text || clock_timestamp()::text)::uuid";

  @Inject
  private DataSource dataSource;

  @Override
  public UUID generateId() {
    return new JdbcTemplate(dataSource).queryForObject(SELECT_UUID_GENERATE_V4, UUID.class);
  }

}
