package pl.adroads.system.infrastructure.adapters.shared.assemblers;

import static java.time.LocalDateTime.parse;

import javax.inject.Named;

import pl.adroads.system.domain.user.model.VerificationToken;
import pl.adroads.system.infrastructure.adapters.driving.user.persistence.model.PersistenceVerificationToken;

@Named
public class VerificationTokenAssembler {

    public PersistenceVerificationToken assemble(final VerificationToken verificationToken) {
        return new PersistenceVerificationToken(verificationToken.getToken(),
                verificationToken.getExpiryDate().toString(),
                verificationToken.getTokenType().name(),
                verificationToken.getTimestamp().toString(),
                verificationToken.isVerified(),
                (verificationToken.getVerificationDate() == null) ? null
                        : verificationToken
                                .getVerificationDate().toString());
    }

    public VerificationToken disassemble(
            final PersistenceVerificationToken persistenceVerificationToken) {
        return new VerificationToken(persistenceVerificationToken.getToken(),
                parse(persistenceVerificationToken.getTimestamp()),
                parse(persistenceVerificationToken.getExpiryDate()),
                VerificationToken.VerificationTokenType
                        .valueOf(persistenceVerificationToken.getTokenType()),
                persistenceVerificationToken.isVerified(),
                (persistenceVerificationToken.getVerificationDate() == null) ? null
                        : parse(
                                persistenceVerificationToken
                                        .getVerificationDate()));
    }
}
