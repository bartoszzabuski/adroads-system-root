package pl.adroads.system.infrastructure.adapters.driven.driver.integration.rest.assemblers;

import pl.adroads.api.model.shared.AddressResource;
import pl.adroads.system.domain.driver.model.Address;
import pl.adroads.system.domain.driver.model.PolishProvince;

import javax.inject.Named;

import static pl.adroads.system.domain.driver.model.City.cityOf;
import static pl.adroads.system.domain.driver.model.Postcode.postcodeOf;
import static pl.adroads.system.domain.driver.model.Street.streetOf;

@Named
public class AddressAssembler {

  public Address assemble(final AddressResource addressResource) {
    if (addressResource == null) {
      return null;
    } else {
      return Address
	  .of(postcodeOf(addressResource.getPostcode()), streetOf(addressResource.getStreet()),
	      cityOf(addressResource.getCity()),
	      PolishProvince.fromString(addressResource.getProvince()));
    }
  }

}
