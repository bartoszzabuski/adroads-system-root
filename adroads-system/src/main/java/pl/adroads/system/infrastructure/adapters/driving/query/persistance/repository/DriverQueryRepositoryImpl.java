package pl.adroads.system.infrastructure.adapters.driving.query.persistance.repository;

import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.join;
import static pl.adroads.system.infrastructure.adapters.driving.query.persistance.repository.CampaignQueryRepositoryImpl.TBL_CAMPAIGNS_TABLE_NAME;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.inject.Named;

import pl.adroads.system.domain.query.DriverQueryRepository;
import pl.adroads.system.domain.query.model.DriverFilterCriteria;
import pl.adroads.system.domain.query.model.Page;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.QueryBuilder;
import pl.adroads.system.infrastructure.adapters.driving.query.persistance.model.PersistenceCampaignDetails;
import pl.adroads.system.infrastructure.adapters.driving.query.persistance.model.PersistenceCampaignDriverDetails;
import pl.adroads.system.infrastructure.adapters.driving.query.persistance.model.PersistenceQueryDriver;
import pl.adroads.system.infrastructure.adapters.shared.postgre.CampaignDriversQueryDao;
import pl.adroads.system.infrastructure.adapters.shared.postgre.DriversQueryDao;
import pl.adroads.system.infrastructure.adapters.shared.postgre.PageMetadata;

@Named
public class DriverQueryRepositoryImpl implements DriverQueryRepository {

    public static final String TBL_DRIVERS_TABLE_NAME = "tbl_drivers";
    private static final String COMMA = ",";
    private static final String UUID_JSON_ARRAY = "'[{\"uuid\":\"%s\"}]'";
    private static final String WORD_IN_SINGLE_QUOTES_FORMAT = "'%s'";

    @Inject
    private DriversQueryDao driversQueryDao;

    @Inject
    private CampaignDriversQueryDao campaignDriversQueryDao;

    private String convertToDriverIdsArray(final List<String> driverIdsList) {
        return join(driverIdsList.stream().map(id -> format(WORD_IN_SINGLE_QUOTES_FORMAT, id)).collect(toList()),
                COMMA);
    }

    private String convertToDriverIdsJsonArray(final List<String> driverIdsList) {
        return join(driverIdsList.stream()
                .map(id -> format(UUID_JSON_ARRAY, id))
                .collect(toList()), COMMA);
    }

    @Override
    public Page<PersistenceQueryDriver> getFirstPage(final DriverFilterCriteria driverFilterCriteria,
            final Integer pageSize) {
        final List<PageMetadata> pageMetadatas = getPageMetadata(driverFilterCriteria, pageSize);
        final List<PersistenceQueryDriver> foundDrivers = driversQueryDao
                .getFirstPage(TBL_DRIVERS_TABLE_NAME, asConditions(driverFilterCriteria), pageSize);

        return new Page(of(pageMetadatas), appendCampaignInformationTo(foundDrivers));
    }

    private List<PersistenceQueryDriver> appendCampaignInformationTo(List<PersistenceQueryDriver> foundDrivers) {
        List<String> driverIdsList = foundDrivers.stream().map(PersistenceQueryDriver::getUuid)
                .collect(toList());

        String driverIdsJsonArray = convertToDriverIdsJsonArray(driverIdsList);
        String driverIdsArray = convertToDriverIdsArray(driverIdsList);

        List<PersistenceCampaignDriverDetails> campaignDriverDetailsList = campaignDriversQueryDao
                .getCampaignDetailsForDrivers(TBL_CAMPAIGNS_TABLE_NAME, driverIdsJsonArray, driverIdsArray);

        return foundDrivers.stream().map(
                persistenceQueryDriver -> enrichWithCampaignDetails(persistenceQueryDriver, campaignDriverDetailsList))
                .collect(toList());

    }

    private PersistenceQueryDriver enrichWithCampaignDetails(final PersistenceQueryDriver persistenceQueryDriver,
            final List<PersistenceCampaignDriverDetails> campaignDriverDetailsList) {
        Optional<PersistenceCampaignDriverDetails> matchingCampaignDriverDetails = campaignDriverDetailsList.stream()
                .filter(campaignDriverDetails -> campaignDriverDetails.getDriverId()
                        .equals(persistenceQueryDriver.getUuid()))
                .findFirst();
        if (matchingCampaignDriverDetails.isPresent()) {
            PersistenceCampaignDriverDetails persistenceCampaignDriverDetails = matchingCampaignDriverDetails.get();
            persistenceQueryDriver.setCampaignDetails(new PersistenceCampaignDetails(
                    persistenceCampaignDriverDetails.getCampaignId(), persistenceCampaignDriverDetails
                            .getCampaignName()));
        }
        return persistenceQueryDriver;
    }

    @Override
    public Page<PersistenceQueryDriver> getSubsequentPage(final DriverFilterCriteria driverFilterCriteria,
            final String uuid, Integer pageSize) {
        final List<PersistenceQueryDriver> foundDrivers = driversQueryDao
                .getSubsequentPage(TBL_DRIVERS_TABLE_NAME, asConditions(driverFilterCriteria), uuid,
                        pageSize);

        appendCampaignInformationTo(foundDrivers);

        return new Page(empty(), foundDrivers);
    }

    private List<PageMetadata> getPageMetadata(final DriverFilterCriteria driverFilterCriteria,
            final Integer pageSize) {
        Optional<String> conditions = asConditions(driverFilterCriteria);
        System.err.println(conditions.orElse("EMPTY!!!!"));
        if (conditions.isPresent()) {
            return driversQueryDao.getPagesMetadata(TBL_DRIVERS_TABLE_NAME, conditions, pageSize);
        } else {
            // TODO decide what to do if no filter criteria has been provided
            // probs return top 30 results?
            return emptyList();
        }
    }

    private Optional<String> asConditions(DriverFilterCriteria driverFilterCriteria) {
    //    @formatter:off
    return new QueryBuilder()
        .searchEmail(driverFilterCriteria.getEmail())
	.searchName(driverFilterCriteria.getName())
        .searchSurname(driverFilterCriteria.getSurname())
	.withStatus(driverFilterCriteria.getStatus())
        .searchStreet(driverFilterCriteria.getStreet())
        .searchPostcode(driverFilterCriteria.getPostcode())
	.searchCity(driverFilterCriteria.getCity())
        .withProvince(driverFilterCriteria.getProvince())
	.searchCarMake(driverFilterCriteria.getCarMake())
	.searchCarModel(driverFilterCriteria.getCarModel())
        .yearFrom(driverFilterCriteria.getYearFrom())
        .yearTo(driverFilterCriteria.getYearTo())
	.withCarBody(driverFilterCriteria.getCarBodies())
	.searchColour(driverFilterCriteria.getCarColour())
        .mileageFrom(driverFilterCriteria.getMileageFrom())
	.mileageTo(driverFilterCriteria.getMileageTo())
        .build();
    //    @formatter:on
    }

}
