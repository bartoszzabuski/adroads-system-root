package pl.adroads.system.infrastructure.adapters.driving.user.persistence;

import pl.adroads.system.infrastructure.adapters.driving.user.persistence.model.PersistenceUser;

public interface UserStore {

  void store(PersistenceUser persistenceUser);

  PersistenceUser findByEmail(String userEmailValue);

  void update(PersistenceUser persistenceUser);

  PersistenceUser findBySessionToken(String sessionToken);
}
