package pl.adroads.system.infrastructure.adapters.driving.query.persistance.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import pl.adroads.system.common.domain.Persistence;

@Data
@RequiredArgsConstructor
public class PersistenceCampaignDetails implements Persistence {
    private final String campaignId;
    private final String campaignName;
}
