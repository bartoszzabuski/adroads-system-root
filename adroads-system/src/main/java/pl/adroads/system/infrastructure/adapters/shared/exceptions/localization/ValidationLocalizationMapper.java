package pl.adroads.system.infrastructure.adapters.shared.exceptions.localization;

import static com.google.common.collect.Maps.newHashMap;
import static pl.adroads.system.common.util.errors.ValidationError.*;
import static pl.adroads.system.domain.CommonValidationFields.EMAIL;
import static pl.adroads.system.domain.driver.exception.DriverValidationFields.*;
import static pl.adroads.system.domain.driver.exception.DriverValidationFields.DEFAULT;

import java.util.Map;

import javax.inject.Named;

import pl.adroads.system.common.util.ValidationFields;
import pl.adroads.system.common.util.errors.ValidationError;

// TODO externalize messages to config file
// TODO add UTest!!!
@Named
public class ValidationLocalizationMapper {

    private static final Map<ValidationFields, String> invalidValueMessages = newHashMap();

    // TODO consider removing emptyValueMessages
    private static final Map<ValidationFields, String> emptyValueMessages = newHashMap();

    static {
        invalidValueMessages.put(DEFAULT, "Uppsss.. niespodziewany blad..");
        invalidValueMessages.put(DATE_OF_BIRTH, "Nieprawidlowa data urodzenia");
        invalidValueMessages.put(DRIVER_NAME, "Nieprawidlowe imie");
        invalidValueMessages.put(DRIVER_ID, "Nieprawidłowy identyfikator kierowcy");
        invalidValueMessages.put(DRIVER_SURNAME, "Nieprawidlowe nazwisko");
        invalidValueMessages.put(STREET, "Nieprawidlowa nazwa ulicy");
        invalidValueMessages.put(CITY, "Nieprawidlowa nazwa miasta");
        invalidValueMessages.put(PROVINCE, "Nieprawidlowa nazwa wojewodztwa");
        invalidValueMessages.put(EMAIL, "Nieprawidlowy adres email");
        invalidValueMessages.put(ADDRESS, "Nieprawidlowy adres");
        invalidValueMessages.put(POSTCODE, "Nieprawidlowy kod pocztowy");
        // TODO add exceptions regarding carDetails and carBody

        emptyValueMessages.put(DATE_OF_BIRTH, "Data urodzenia musi byc podana");
        emptyValueMessages.put(DRIVER_NAME, "Imie musi byc podane");
        emptyValueMessages.put(DRIVER_SURNAME, "Nazwisko musi byc podane");
        emptyValueMessages.put(STREET, "Ulica musi byc podana");
        emptyValueMessages.put(CITY, "Miasto musi byc podane");
        emptyValueMessages.put(PROVINCE, "Wojewodztwo musi by podane");
        emptyValueMessages.put(EMAIL, "Adres email musi by podany");
        emptyValueMessages.put(ADDRESS, "Adres musi by podany");
        emptyValueMessages.put(POSTCODE, "Kod pocztowy musi by podany");
        // TODO add exceptions regarding carDetails and carBody

    }

    private static final String DEFAULT_MESSAGE = "Unexpected error!";

    public String getMappedMessageFor(final ValidationFields validationFields,
            final ValidationError validationError) {

        String mappedMessage = null;

        if (validationError == VALUE_EMPTY || validationError == VALUE_REQUIRED) {
            mappedMessage = emptyValueMessages.get(validationFields);
        }
        if (validationError == VALUE_NOT_VALID || validationError == ValidationError.DEFAULT) {
            mappedMessage = invalidValueMessages.get(validationFields);
        }

        return (mappedMessage == null) ? invalidValueMessages.get(DEFAULT) : mappedMessage;
    }

}
