package pl.adroads.system.infrastructure.adapters.driving.driver.persistance;


import java.util.List;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.join;

public final class PostgresSqlConditionsHelper {

  private static final String ARRAY_FORMAT = "ARRAY[%s]";
  private static final String COMMA = ", ";

  private PostgresSqlConditionsHelper() {
//    prevent initialization
  }

  public static String iLike(String key, String value) {
    return format("%s ILIKE '%s'", key, value);
  }

  public static String equalTo(String key, String value) {
    return format("%s = '%s'", key, value);
  }

  public static String anyInArray(String key, List<String> elements) {
    return format("%s ??| %s", key, toArray(elements));
  }

  private static String toArray(final List<String> elements) {
    return format(ARRAY_FORMAT, join(elements, COMMA));
  }

  public static String greaterThanOrEqual(String key, Integer value) {
    return format("CAST(%s AS int) >= %s", key, value);
  }

  public static String lessThanOrEqualTo(String key, Integer value) {
    return format("CAST(%s AS int) <= %s", key, value);
  }

}
