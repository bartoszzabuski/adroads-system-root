package pl.adroads.system.infrastructure.adapters.shared.exceptions;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import lombok.extern.slf4j.Slf4j;
import pl.adroads.system.common.util.ValidationException;
import pl.adroads.system.common.util.errors.ValidationError;
import pl.adroads.system.infrastructure.adapters.shared.exceptions.localization.ValidationLocalizationMapper;

@Slf4j
@Provider
public class ValidationExceptionMapper extends BaseExceptionMapper
        implements ExceptionMapper<ValidationException> {

    @Inject
    private ValidationLocalizationMapper validationLocalizationMapper;

    @Override
    public Response toResponse(ValidationException exception) {

        // TODO add logging/reporting

        ValidationError validationError = (ValidationError) exception.getSystemError();

        String mappedMessage = validationLocalizationMapper
                .getMappedMessageFor(exception.getValidationField(), validationError);
        return buildExceptionResponse(validationError.getType(), validationError.getCode(),
                mappedMessage);
    }

}
