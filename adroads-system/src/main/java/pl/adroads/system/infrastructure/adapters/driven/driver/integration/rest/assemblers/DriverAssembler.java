package pl.adroads.system.infrastructure.adapters.driven.driver.integration.rest.assemblers;

import static pl.adroads.system.domain.driver.exception.DriverValidationFields.DRIVER_ID;
import static pl.adroads.system.domain.driver.model.Colour.colourOf;
import static pl.adroads.system.domain.driver.model.Email.emailOf;

import java.time.LocalDate;
import java.time.Year;
import java.time.format.DateTimeFormatter;

import javax.inject.Inject;
import javax.inject.Named;

import pl.adroads.api.model.driver.CreateDriverRequest;
import pl.adroads.api.model.driver.UpdateDriverRequest;
import pl.adroads.api.model.shared.CarDetailsResource;
import pl.adroads.api.model.shared.DriverDetailsResource;
import pl.adroads.api.model.shared.WindscreenDimensionsResource;
import pl.adroads.system.common.util.Validate;
import pl.adroads.system.domain.driver.model.CarDetails;
import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.domain.driver.model.DriverDetails;
import pl.adroads.system.domain.driver.model.DriverId;
import pl.adroads.system.domain.driver.model.PolishCarBody;
import pl.adroads.system.domain.driver.model.PolishDriverType;
import pl.adroads.system.domain.driver.model.Status;
import pl.adroads.system.domain.driver.model.WindscreenDimensions;

@Named
public class DriverAssembler {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    @Inject
    private AddressAssembler addressAssembler;

    public Driver assemble(final CreateDriverRequest createDriverRequest) {
        return new Driver.DriverBuilder().withName(createDriverRequest.getName())
                .withSurname(createDriverRequest.getSurname())
                .withDateOfBirth(LocalDate.parse(createDriverRequest.getDateOfBirth(), formatter))
                .withEmail(emailOf(createDriverRequest.getEmail()))
                .withAddress(addressAssembler.assemble(createDriverRequest.getAddress()))
                .withStatus(Status.CREATED)
                .withCarDetails(assembleCarDetails(createDriverRequest.getCarDetails()))
                .withDriverDetails(assembleDriverDetails(createDriverRequest.getDriverDetails())).build();
    }

    public Driver assemble(final String uuid, final UpdateDriverRequest updateDriverRequest) {
        Validate.isTrue(uuid.equals(updateDriverRequest.getUuid()), DRIVER_ID);

        return new Driver.DriverBuilder().withID(DriverId.of(updateDriverRequest.getUuid()))
                .withName(updateDriverRequest.getName())
                .withSurname(updateDriverRequest.getSurname())
                .withDateOfBirth(LocalDate.parse(updateDriverRequest.getDateOfBirth(), formatter))
                .withEmail(emailOf(updateDriverRequest.getEmail()))
                .withAddress(addressAssembler.assemble(updateDriverRequest.getAddress()))
                .withCarDetails(assembleCarDetails(updateDriverRequest.getCarDetails()))
                .withDriverDetails(assembleDriverDetails(updateDriverRequest.getDriverDetails())).build();
    }

    private DriverDetails assembleDriverDetails(final DriverDetailsResource driverDetails) {
        return DriverDetails.of(PolishDriverType.polishDriverTypeOf(driverDetails.getType()),
                driverDetails.getMonthlyDistanceIndex());
    }

    private CarDetails assembleCarDetails(CarDetailsResource carDetailsResource) {
        return CarDetails.of(carDetailsResource.getMake(), carDetailsResource.getModel(),
                Year.parse(carDetailsResource.getYear()), carDetailsResource.getMileage(),
                PolishCarBody.fromString(carDetailsResource.getBody()),
                colourOf(carDetailsResource.getColour()), assembleWindscreenDimensions(
                        carDetailsResource.getWindscreenDimensions()));
    }

    private WindscreenDimensions assembleWindscreenDimensions(
            final WindscreenDimensionsResource windscreenDimensionsResource) {
        return WindscreenDimensions.windscreenDimensionsOf(windscreenDimensionsResource.getHeight(),
                windscreenDimensionsResource.getWidth());
    }

}
