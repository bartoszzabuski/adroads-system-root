package pl.adroads.system.infrastructure.adapters.shared.postgre;

import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceDriver;
import pl.adroads.system.infrastructure.adapters.shared.DriverStorage;

import java.util.Optional;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class PostgreDriverStorage implements DriverStorage {

  private static final String DRIVER_TABLE_NAME = "tbl_drivers";

  @Inject
  private PostgreSQLJSONStorageFacade<PersistenceDriver> storageFacade;

  @Override
  public void saveNewDriver(final PersistenceDriver persistenceDriver) {
    storageFacade.insertAsJSON(DRIVER_TABLE_NAME, persistenceDriver);
  }

  @Override
  public void updateDriver(PersistenceDriver persistenceDriver) {
    storageFacade.updateAsJSON(DRIVER_TABLE_NAME, persistenceDriver);
  }

  @Override
  public Optional<PersistenceDriver> findByEmail(final String email) {
    String filter = "data->>'email' = ?";
    return findAndAssembleDriver(email, filter);
  }

  @Override
  public Optional<PersistenceDriver> findDriverById(String driverId) {
    String filter = "id=CAST(? AS uuid)";
    return findAndAssembleDriver(driverId, filter);
  }

  private Optional<PersistenceDriver> findAndAssembleDriver(String arg, String filter) {
    PersistenceDriver persistenceDriver = storageFacade
        .findExact(DRIVER_TABLE_NAME, PersistenceDriver.class, filter, "", arg);
    return Optional.ofNullable(persistenceDriver);
  }

}
