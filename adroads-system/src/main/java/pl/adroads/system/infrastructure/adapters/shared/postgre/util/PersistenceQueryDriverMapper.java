package pl.adroads.system.infrastructure.adapters.shared.postgre.util;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.inject.Inject;
import javax.inject.Named;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import pl.adroads.system.infrastructure.adapters.driving.query.persistance.model.PersistenceQueryDriver;
import pl.adroads.system.infrastructure.adapters.shared.ObjectSerializer;

@Named
public class PersistenceQueryDriverMapper implements ResultSetMapper<PersistenceQueryDriver> {

    private static final String DATA_COLUMN = "data";
    @Inject
    private ObjectSerializer serializer = new ObjectSerializer(false, false);

    @Override
    public PersistenceQueryDriver map(int i, ResultSet resultSet, StatementContext statementContext)
            throws SQLException {
        String dataJSON = resultSet.getString(DATA_COLUMN);
        return serializer.deserialize(dataJSON, PersistenceQueryDriver.class);
    }
}
