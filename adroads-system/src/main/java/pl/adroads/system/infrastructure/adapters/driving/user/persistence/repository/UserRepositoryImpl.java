package pl.adroads.system.infrastructure.adapters.driving.user.persistence.repository;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static java.util.Optional.ofNullable;

import java.util.Optional;

import javax.inject.Inject;
import javax.inject.Named;

import pl.adroads.system.domain.driver.model.Email;
import pl.adroads.system.domain.user.UserRepository;
import pl.adroads.system.domain.user.model.User;
import pl.adroads.system.infrastructure.adapters.driving.user.persistence.UserStore;
import pl.adroads.system.infrastructure.adapters.driving.user.persistence.assemblers.PersistenceUserAssembler;
import pl.adroads.system.infrastructure.adapters.driving.user.persistence.model.PersistenceUser;

@Named
public class UserRepositoryImpl implements UserRepository {

    @Inject
    private UserStore userStore;
    @Inject
    private PersistenceUserAssembler persistenceUserAssembler;

    @Override
    public void store(final User user) {
        PersistenceUser persistenceUser = persistenceUserAssembler.assemble(user);
        userStore.store(persistenceUser);
    }

    @Override
    public Optional<User> findByEmail(final Email userEmail) {
        return ofNullable(
                persistenceUserAssembler.disassemble(userStore.findByEmail(userEmail.getValue())));
    }

    @Override
    public void update(final User user) {
        userStore.update(persistenceUserAssembler.assemble(user));
    }

    @Override
    public Optional<User> findBySessionToken(String sessionToken) {
        PersistenceUser persistenceUser = userStore.findBySessionToken(sessionToken);
        if (persistenceUser == null) {
            return empty();
        } else {
            return of(persistenceUserAssembler.disassemble(persistenceUser));
        }
    }

}
