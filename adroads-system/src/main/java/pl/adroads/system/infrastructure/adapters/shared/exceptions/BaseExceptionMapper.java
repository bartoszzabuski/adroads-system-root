package pl.adroads.system.infrastructure.adapters.shared.exceptions;


import pl.adroads.api.model.shared.ExceptionResponse;
import pl.adroads.system.infrastructure.adapters.shared.ObjectSerializer;

import javax.inject.Inject;
import javax.ws.rs.core.Response;

import static pl.adroads.system.common.util.GlobalConstants.CONTENT_TYPE_APPLICATION_JSON;

public abstract class BaseExceptionMapper {

  @Inject
  private ObjectSerializer serializer;

  protected Response buildExceptionResponse(final String type, final int status,
      final String message) {
    return Response.status(status).
        entity(serializer.serialize(new ExceptionResponse(type, message))).
        type(CONTENT_TYPE_APPLICATION_JSON).
        build();
  }

}
