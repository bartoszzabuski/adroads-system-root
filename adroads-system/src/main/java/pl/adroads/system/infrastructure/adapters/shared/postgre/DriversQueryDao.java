package pl.adroads.system.infrastructure.adapters.shared.postgre;

import java.util.List;
import java.util.Optional;

import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceDriver;
import pl.adroads.system.infrastructure.adapters.driving.query.persistance.model.PersistenceQueryDriver;
import pl.adroads.system.infrastructure.adapters.shared.postgre.annotations.BindOptional;
import pl.adroads.system.infrastructure.adapters.shared.postgre.util.PersistenceDriverMapper;
import pl.adroads.system.infrastructure.adapters.shared.postgre.util.PersistenceQueryDriverMapper;

@UseStringTemplate3StatementLocator("/sql/templates/DriversQueryDao.sql.stg")
public interface DriversQueryDao {

    @SqlQuery
    @RegisterMapper(PersistenceDriverMapper.class)
    List<PersistenceDriver> getDrivers(@Define("tableName") String tableName,
            @BindOptional("condition") Optional<String> condition);

    @SqlQuery
    @Mapper(PageMetadataMapper.class)
    List<PageMetadata> getPagesMetadata(@Define("tableName") String tableName,
            @BindOptional("condition") Optional<String> condition, @Define("pageSize") Integer pageSize);

    @SqlQuery
    @Mapper(PersistenceQueryDriverMapper.class)
    List<PersistenceQueryDriver> getFirstPage(@Define("tableName") String tableName,
            @BindOptional("condition") Optional<String> condition, @Define("pageSize") Integer pageSize);

    @SqlQuery
    @Mapper(PersistenceQueryDriverMapper.class)
    List<PersistenceQueryDriver> getSubsequentPage(@Define("tableName") String tableName,
            @BindOptional("condition") Optional<String> condition, @Define("uuid") String uuid,
            @Define("pageSize") Integer pageSize);

}
