package pl.adroads.system.infrastructure.locking;

public interface LockedOperation<K, T> {

    T perform(K k);

}
