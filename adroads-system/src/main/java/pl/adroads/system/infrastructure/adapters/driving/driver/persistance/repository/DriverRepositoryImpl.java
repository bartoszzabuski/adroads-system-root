package pl.adroads.system.infrastructure.adapters.driving.driver.persistance.repository;

import java.util.Optional;
import java.util.concurrent.locks.Lock;

import javax.inject.Inject;
import javax.inject.Named;

import pl.adroads.system.domain.driver.DriverRepository;
import pl.adroads.system.domain.driver.exception.DriverAlreadyExistsException;
import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.domain.driver.model.DriverId;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.assemblers.PersistenceDriverAssembler;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceDriver;
import pl.adroads.system.infrastructure.adapters.shared.DriverStorage;
import pl.adroads.system.infrastructure.locking.HazelcastLockStore;

@Named
public class DriverRepositoryImpl implements DriverRepository {

    @Inject
    private DriverStorage driverStorage;
    @Inject
    private PersistenceDriverAssembler driverAssembler;
    @Inject
    private HazelcastLockStore driversRepositoryLockStore;

    @Override
    public Optional<Driver> findDriverByEmail(final String email) {
        Optional<PersistenceDriver> persistenceDriver = driverStorage.findByEmail(email);
        return persistenceDriver.isPresent() ? Optional
                .ofNullable(driverAssembler.disassemble(persistenceDriver.get()))
                : Optional.<Driver> empty();
    }

    @Override
    public void storeDriver(final Driver driver) {
        Optional<Driver> duplicatedDriver = findDriverByEmail(driver.getEmail().getValue());
        if (duplicatedDriver.isPresent()) {
            throw new DriverAlreadyExistsException(driver.getEmail().getValue());
        }
        PersistenceDriver persistenceDriver = driverAssembler.assemble(driver);
        driverStorage.saveNewDriver(persistenceDriver);
    }

    @Override
    public void updateDriver(Driver driver) {
        PersistenceDriver persistenceDriver = driverAssembler.assemble(driver);
        driverStorage.updateDriver(persistenceDriver);
    }

    @Override
    public Optional<Driver> findDriverById(DriverId driverId) {
        Optional<PersistenceDriver> persistenceDriver = driverStorage
                .findDriverById(driverId.getValue().toString());
        if (!persistenceDriver.isPresent()) {
            return Optional.empty();
        }
        return Optional.ofNullable(driverAssembler.disassemble(persistenceDriver.get()));
    }

    @Override
    public Lock getLockFor(final String key) {
        return driversRepositoryLockStore.getLockFor(key);
    }
}
