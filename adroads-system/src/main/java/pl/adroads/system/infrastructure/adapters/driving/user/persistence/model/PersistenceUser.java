package pl.adroads.system.infrastructure.adapters.driving.user.persistence.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import pl.adroads.system.common.domain.PersistenceAggregate;

@Getter
@EqualsAndHashCode(callSuper = true)
public class PersistenceUser extends PersistenceAggregate<String> {

    private final String email;
    private final PersistenceVerificationToken persistenceVerificationToken;
    private final String userType;
    private final String password;
    private final PersistenceSessionToken persistenceSessionToken;

    public PersistenceUser(final String uuid, final String email,
            final PersistenceVerificationToken persistenceVerificationToken, final String userType,
            final String password, final PersistenceSessionToken persistenceSessionToken) {
        this.uuid = uuid;
        this.email = email;
        this.persistenceVerificationToken = persistenceVerificationToken;
        this.userType = userType;
        this.password = password;
        this.persistenceSessionToken = persistenceSessionToken;
    }
}
