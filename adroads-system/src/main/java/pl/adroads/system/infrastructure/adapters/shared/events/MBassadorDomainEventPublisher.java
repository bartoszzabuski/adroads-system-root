package pl.adroads.system.infrastructure.adapters.shared.events;


import net.engio.mbassy.bus.MBassador;

import pl.adroads.system.common.domain.DomainEvent;
import pl.adroads.system.domain.DomainEventPublisher;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class MBassadorDomainEventPublisher implements DomainEventPublisher {

  @Inject
  private MBassador bus;

  @Override
  public <T extends DomainEvent> void publish(T domainEvent) {
    bus.publishAsync(domainEvent);
  }
}
