package pl.adroads.system.infrastructure.adapters.shared.postgre;

import pl.adroads.system.common.domain.PersistenceAggregate;

import java.util.List;

public interface PostgreSQLJSONStorageFacade<E extends PersistenceAggregate<String>> {

  void insertAsJSON(String tableName, E anAggregateRoot);

  void updateAsJSON(String tableName, E anAggregateRoot);

  E findExact(String tableName, Class<E> aType, String aFilterExpression, String anOrderBy,
      Object... anArguments);

  List<E> findAll(String tableName, Class<E> aType, String aFilterExpression, String anOrderBy,
      Object... anArguments);

  Integer count(String tableName, String aFilterExpression, Object... anArguments);
}
