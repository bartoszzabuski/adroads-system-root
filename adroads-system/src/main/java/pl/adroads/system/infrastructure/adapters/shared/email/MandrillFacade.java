package pl.adroads.system.infrastructure.adapters.shared.email;

public interface MandrillFacade {

    void sendNewUserActivationEmail(String recipentEmail, String activationUrl);

    void sendDriverAssignedToCampaignEmail(String recipientEmail, String approvalUrl, String rejectionUrl,
            String campaignName, String startDate, String endDate);

}
