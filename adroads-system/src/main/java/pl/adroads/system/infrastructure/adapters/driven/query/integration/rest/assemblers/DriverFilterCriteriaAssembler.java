package pl.adroads.system.infrastructure.adapters.driven.query.integration.rest.assemblers;

import org.apache.commons.collections.CollectionUtils;

import pl.adroads.api.model.driver.DriverFilterCriteriaResource;
import pl.adroads.system.domain.driver.model.PolishCarBody;
import pl.adroads.system.domain.driver.model.PolishProvince;
import pl.adroads.system.domain.driver.model.Status;
import pl.adroads.system.domain.query.model.DriverFilterCriteria;
import pl.adroads.system.domain.query.model.SortDirection;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Named;

import static org.apache.commons.lang3.StringUtils.isEmpty;

@Named
public class DriverFilterCriteriaAssembler {

  //  TODO consider SQL INJECTION attacks!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  public DriverFilterCriteria assembleFilterCriteria(
      final DriverFilterCriteriaResource driverFilterCriteriaResource) {

    System.out.println(driverFilterCriteriaResource.toString());

    return new DriverFilterCriteria(
	Optional.ofNullable(assembleSortDirectionIfPresent(driverFilterCriteriaResource.getSortDir())),
	Optional.ofNullable(driverFilterCriteriaResource.getSortedBy()),
	Optional.ofNullable(driverFilterCriteriaResource.getEmail()),
	Optional.ofNullable(driverFilterCriteriaResource.getName()),
	Optional.ofNullable(driverFilterCriteriaResource.getSurname()),
	Optional.ofNullable(assembleStatusIfPresent(driverFilterCriteriaResource.getStatus())),
	Optional.ofNullable(driverFilterCriteriaResource.getStreet()),
	Optional.ofNullable(driverFilterCriteriaResource.getPostcode()),
	Optional.ofNullable(driverFilterCriteriaResource.getCity()), Optional.ofNullable(
	assemblePolishProvinceIfPresent(driverFilterCriteriaResource.getProvince())),
	Optional.ofNullable(driverFilterCriteriaResource.getCarMake()),
	Optional.ofNullable(driverFilterCriteriaResource.getCarModel()),
	Optional.ofNullable(driverFilterCriteriaResource.getYearFrom()),
	Optional.ofNullable(driverFilterCriteriaResource.getYearTo()),
	assemblePolishCarBodyIfPresent(driverFilterCriteriaResource.getCarBody()),
	Optional.ofNullable(driverFilterCriteriaResource.getCarColour()),
	Optional.ofNullable(driverFilterCriteriaResource.getMileageFrom()),
	Optional.ofNullable(driverFilterCriteriaResource.getMileageTo()));
  }

  private SortDirection assembleSortDirectionIfPresent(final String sortDir) {
    return (sortDir == null) ? null : SortDirection.valueOf(sortDir);
  }

  private PolishProvince assemblePolishProvinceIfPresent(final String province) {
    return isEmpty(province) ? null : PolishProvince.fromString(province);
  }

  private Status assembleStatusIfPresent(final String status) {
    return isEmpty(status) ? null : Status.valueOf(status);
  }

  private Optional<List<PolishCarBody>> assemblePolishCarBodyIfPresent(
      final List<String> carBodies) {
    if (CollectionUtils.isEmpty(carBodies)) {
      return Optional.empty();
    } else {
      return Optional.of(transformToPolishCarBody(carBodies));
    }
  }

  private List<PolishCarBody> transformToPolishCarBody(final List<String> carBodies) {
    return carBodies.stream().map(PolishCarBody::fromString)
	.collect(Collectors.<PolishCarBody>toList());
  }

}
