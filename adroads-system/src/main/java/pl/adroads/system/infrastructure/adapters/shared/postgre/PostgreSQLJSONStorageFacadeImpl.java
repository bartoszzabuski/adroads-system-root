package pl.adroads.system.infrastructure.adapters.shared.postgre;

import org.postgresql.util.PGobject;

import pl.adroads.system.common.domain.PersistenceAggregate;
import pl.adroads.system.infrastructure.adapters.shared.postgre.util.PGObjectAssembler;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class PostgreSQLJSONStorageFacadeImpl<E extends PersistenceAggregate<String>>
    implements PostgreSQLJSONStorageFacade<E> {

  @Inject
  private PostgreSQLHelper postgreSQLHelper;
  @Inject
  private PGObjectAssembler pgObjectAssembler;

  //  TODO if not transacted with previous findByEmail may cause race condition
  public void insertAsJSON(final String tableName, final E anAggregateRoot) {
    try {

      final PGobject jsonPG = pgObjectAssembler.toPGobject(anAggregateRoot);
      postgreSQLHelper.insert(tableName, UUID.fromString(anAggregateRoot.getUuid()), jsonPG);
    } catch (Exception e) {
      throw new RuntimeException(
          "Cannot save: " + anAggregateRoot.toString() + " because: " + e.getMessage());
    }
  }

  public void updateAsJSON(final String tableName, final E anAggregateRoot) {
    try {

      final PGobject jsonPG = pgObjectAssembler.toPGobject(anAggregateRoot);
      postgreSQLHelper.update(tableName, UUID.fromString(anAggregateRoot.getUuid()), jsonPG);
    } catch (Exception e) {
      throw new RuntimeException(
          "Cannot save: " + anAggregateRoot.toString() + " because: " + e.getMessage());
    }
  }


  public List<E> findAll(final String tableName, final Class<E> aType,
      final String aFilterExpression, final String anOrderBy, final Object... anArguments) {
    try {
      List<PGobject> allPGObject = postgreSQLHelper
          .findAllPGObject(tableName, aFilterExpression, anOrderBy, anArguments);
      return pgObjectAssembler.fromPGobjectList(aType, allPGObject);
    } catch (Exception e) {
      throw new RuntimeException(
          "Cannot find: " + anArguments + " " + aFilterExpression + " because: " + e.getMessage());
    }
  }

  @Override
  public Integer count(String tableName, String aFilterExpression, Object... anArguments) {
    try {
      return postgreSQLHelper.count(tableName, aFilterExpression, anArguments);
    } catch (Exception e) {
      throw new RuntimeException(
          "Cannot perform count campaigns (" + aFilterExpression + ") because: " + e.getMessage());
    }
  }

  public E findExact(final String tableName, final Class<E> aType, final String aFilterExpression,
      final String anOrderBy, final Object... anArguments) {
    try {
      List<PGobject> pgObjectResults = postgreSQLHelper
          .findAllPGObject(tableName, aFilterExpression, anOrderBy, anArguments);
      PGobject pgObjectResult = (pgObjectResults.size() > 0) ? pgObjectResults.get(0) : null;
      return pgObjectAssembler.fromPGobject(aType, pgObjectResult);
    } catch (Exception e) {
      throw new RuntimeException(
          "Cannot find exact for " + aType + "(" + aFilterExpression + ") because: " + e
              .getMessage());
    }
  }

}
