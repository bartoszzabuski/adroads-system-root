package pl.adroads.system.infrastructure.adapters.driving.user.persistence;

import javax.inject.Inject;
import javax.inject.Named;

import pl.adroads.system.infrastructure.adapters.driving.user.persistence.model.PersistenceUser;
import pl.adroads.system.infrastructure.adapters.shared.postgre.PostgreSQLJSONStorageFacade;

@Named
public class UserStoreImpl implements UserStore {

    private static final String USER_TABLE_NAME = "tbl_users";
    @Inject
    private PostgreSQLJSONStorageFacade<PersistenceUser> storageFacade;

    @Override
    public void store(PersistenceUser persistenceUser) {
        storageFacade.insertAsJSON(USER_TABLE_NAME, persistenceUser);
    }

    @Override
    public PersistenceUser findByEmail(String userEmailValue) {
        String filter = "data->>'email' = ?";
        return storageFacade
                .findExact(USER_TABLE_NAME, PersistenceUser.class, filter, "", userEmailValue);
    }

    @Override
    public PersistenceUser findBySessionToken(String sessionToken) {
        String filter = "data->'persistenceSessionToken' ->> 'token' = ?";
        return storageFacade
                .findExact(USER_TABLE_NAME, PersistenceUser.class, filter, "", sessionToken);
    }

    @Override
    public void update(PersistenceUser persistenceUser) {
        storageFacade.updateAsJSON(USER_TABLE_NAME, persistenceUser);
    }

}
