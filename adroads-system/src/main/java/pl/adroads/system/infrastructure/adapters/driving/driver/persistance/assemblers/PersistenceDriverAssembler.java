package pl.adroads.system.infrastructure.adapters.driving.driver.persistance.assemblers;

import pl.adroads.system.domain.driver.model.Address;
import pl.adroads.system.domain.driver.model.CarDetails;
import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.domain.driver.model.DriverDetails;
import pl.adroads.system.domain.driver.model.DriverId;
import pl.adroads.system.domain.driver.model.PolishCarBody;
import pl.adroads.system.domain.driver.model.PolishDriverType;
import pl.adroads.system.domain.driver.model.PolishProvince;
import pl.adroads.system.domain.driver.model.Status;
import pl.adroads.system.domain.driver.model.WindscreenDimensions;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceAddress;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceCarDetails;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceDriver;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceDriverDetails;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceWindscreenDimensions;

import java.time.LocalDate;
import java.time.Year;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Named;

import static pl.adroads.system.domain.driver.model.City.cityOf;
import static pl.adroads.system.domain.driver.model.Colour.colourOf;
import static pl.adroads.system.domain.driver.model.Email.emailOf;
import static pl.adroads.system.domain.driver.model.Postcode.postcodeOf;
import static pl.adroads.system.domain.driver.model.Street.streetOf;

@Named
public class  PersistenceDriverAssembler {

  public PersistenceDriver assemble(final Driver driver) {
    PersistenceDriver persistenceDriver = new PersistenceDriver(
	driver.getUuid().getValue().toString(), driver.getName(), driver.getSurname(),
	driver.getDateOfBirth().toString(), driver.getEmail().getValue(),
	assemblePersistenceAddress(driver.getAddress()), driver.getStatus().toString(),
	assemblerPersistenceCarDetails(driver.getCarDetails()),
	assemblePersistenceDriverDetails(driver.getDriverDetails()));
    return persistenceDriver;
  }

  private PersistenceAddress assemblePersistenceAddress(final Address address) {
    return new PersistenceAddress(address.getPostcode().getValue(), address.getStreet().getValue(),
				  address.getCity().getValue(), address.getProvince().toString());
  }

  private PersistenceCarDetails assemblerPersistenceCarDetails(final CarDetails carDetails) {
    return new PersistenceCarDetails(carDetails.getMake(), carDetails.getModel(),
				     carDetails.getYear().toString(),
				     carDetails.getMileage().getValue(),
				     carDetails.getBody().toString(),
				     carDetails.getColour().getValue(),
				     assemblePersistenceWindscreenDimensions(
					 carDetails.getWindscreenDimensions()));
  }

  private PersistenceWindscreenDimensions assemblePersistenceWindscreenDimensions(
      final WindscreenDimensions windscreenDimensions) {
    return new PersistenceWindscreenDimensions(windscreenDimensions.getHeight().getValue(),
					       windscreenDimensions.getWidth().getValue());
  }

  private PersistenceDriverDetails assemblePersistenceDriverDetails(
      final DriverDetails driverDetails) {
    return new PersistenceDriverDetails(driverDetails.getType().getValue(),
					driverDetails.getMonthlyDistanceIndex());
  }


  public List<Driver> disassemble(final List<PersistenceDriver> persistenceDrivers) {
    return persistenceDrivers.stream().map(this::disassemble).collect(Collectors.toList());
  }

  public Driver disassemble(final PersistenceDriver persistenceDriver) {
    return new Driver.DriverBuilder().withID(DriverId.of(persistenceDriver.getUuid()))
	.withName(persistenceDriver.getName()).withSurname(persistenceDriver.getSurname())
	.withDateOfBirth(LocalDate.parse(persistenceDriver.getDateOfBirth()))
	.withEmail(emailOf(persistenceDriver.getEmail()))
	.withAddress(disassembleAddress(persistenceDriver.getAddress()))
	.withStatus(Status.valueOf(persistenceDriver.getStatus()))
	.withCarDetails(disassembleCarDetails(persistenceDriver.getCarDetails()))
	.withDriverDetails(disassembleDriverDetails(persistenceDriver.getDriverDetails())).build();
  }

  private DriverDetails disassembleDriverDetails(
      PersistenceDriverDetails persistenceDriverDetails) {
    return DriverDetails.of(PolishDriverType.polishDriverTypeOf(persistenceDriverDetails.getType()),
			    persistenceDriverDetails.getMonthlyDistanceIndex());
  }

  private CarDetails disassembleCarDetails(PersistenceCarDetails persistenceCarDetails) {
    return CarDetails.of(persistenceCarDetails.getMake(), persistenceCarDetails.getModel(),
			 Year.parse(persistenceCarDetails.getYear()),
			 persistenceCarDetails.getMileage(),
			 PolishCarBody.fromString(persistenceCarDetails.getBody()),
			 colourOf(persistenceCarDetails.getColour()),
			 disassembleWindscreenDimensions(
			     persistenceCarDetails.getWindscreenDimensions()));
  }

  private WindscreenDimensions disassembleWindscreenDimensions(
      final PersistenceWindscreenDimensions persistenceWindscreenDimensions) {
    return WindscreenDimensions.windscreenDimensionsOf(persistenceWindscreenDimensions.getHeight(),
						       persistenceWindscreenDimensions.getWidth());
  }

  private Address disassembleAddress(PersistenceAddress persistenceAddress) {
    return Address
	.of(postcodeOf(persistenceAddress.getPostcode()), streetOf(persistenceAddress.getStreet()),
	    cityOf(persistenceAddress.getCity()),
	    PolishProvince.fromString(persistenceAddress.getProvince()));
  }

}
