package pl.adroads.system.infrastructure.adapters.shared.postgre.util;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceDriver;
import pl.adroads.system.infrastructure.adapters.shared.ObjectSerializer;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class PersistenceDriverMapper implements ResultSetMapper<PersistenceDriver> {

  private static final String DATA_COLUMN = "data";
  @Inject
  private ObjectSerializer serializer = new ObjectSerializer(false, false);

  @Override
  public PersistenceDriver map(int i, ResultSet resultSet, StatementContext statementContext)
      throws SQLException {
    String dataJSON = resultSet.getString(DATA_COLUMN);
    return serializer.deserialize(dataJSON, PersistenceDriver.class);
  }
}