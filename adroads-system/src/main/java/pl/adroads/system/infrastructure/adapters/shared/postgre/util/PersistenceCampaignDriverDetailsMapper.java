package pl.adroads.system.infrastructure.adapters.shared.postgre.util;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.inject.Named;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import pl.adroads.system.infrastructure.adapters.driving.query.persistance.model.PersistenceCampaignDriverDetails;

@Named
public class PersistenceCampaignDriverDetailsMapper implements ResultSetMapper<PersistenceCampaignDriverDetails> {

    private static final String CAMPAIGN_ID = "campaignid";
    private static final String CAMPAIGN_NAME = "campaignname";
    private static final String DRIVER_ID = "driverid";

    private static final String DOUBLE_QUOTES = "\"";
    private static final String EMPTY_STRING = "";

    @Override
    public PersistenceCampaignDriverDetails map(int i, ResultSet resultSet, StatementContext statementContext)
            throws SQLException {
        String campaignId = resultSet.getString(CAMPAIGN_ID).replace(DOUBLE_QUOTES, EMPTY_STRING);
        String campaignName = resultSet.getString(CAMPAIGN_NAME).replace(DOUBLE_QUOTES, EMPTY_STRING);
        String driverId = resultSet.getString(DRIVER_ID).replace(DOUBLE_QUOTES, EMPTY_STRING);

        return new PersistenceCampaignDriverDetails(campaignId, campaignName, driverId);
    }
}
