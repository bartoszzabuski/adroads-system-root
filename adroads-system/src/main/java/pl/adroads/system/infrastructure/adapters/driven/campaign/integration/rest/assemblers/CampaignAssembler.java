package pl.adroads.system.infrastructure.adapters.driven.campaign.integration.rest.assemblers;

import pl.adroads.api.model.campaign.CreateCampaignRequest;
import pl.adroads.system.domain.campaign.model.Campaign;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import javax.inject.Named;

import static pl.adroads.system.domain.campaign.model.Campaign.campaignOf;

@Named
public class CampaignAssembler {

  public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ISO_INSTANT
      .withZone(ZoneId.systemDefault());

  public Campaign assemble(final CreateCampaignRequest createCampaignRequest) {
    return campaignOf(createCampaignRequest.getName(), createCampaignRequest.getDescription(),
		      LocalDateTime.parse(createCampaignRequest.getStartDate(), FORMATTER),
		      LocalDateTime.parse(createCampaignRequest.getEndDate(), FORMATTER));
  }

}
