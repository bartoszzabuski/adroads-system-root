package pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model;

import pl.adroads.system.common.domain.Persistence;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PersistenceCarDetails implements Persistence {

  private String make;
  private String model;
  private String year;
  private Integer mileage;
  private String body;
  private String colour;
  private PersistenceWindscreenDimensions windscreenDimensions;

}
