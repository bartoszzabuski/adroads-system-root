package pl.adroads.system.infrastructure.adapters.shared.postgre;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.inject.Named;


@Named
public class PageMetadataMapper implements ResultSetMapper<PageMetadata> {

  @Override
  public PageMetadata map(final int i, final ResultSet resultSet,
      final StatementContext statementContext) throws SQLException {
    final String uuid = resultSet.getString("id");
    final Integer pageNumber = resultSet.getInt("page_number");

    return new PageMetadata(uuid, pageNumber);
  }

}
