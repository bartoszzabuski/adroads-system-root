package pl.adroads.system.infrastructure.adapters.driven.user.integration.listeners;

import net.engio.mbassy.listener.Handler;

import pl.adroads.system.application.driver.DriverManagementAppService;
import pl.adroads.system.domain.driver.model.DriverId;
import pl.adroads.system.domain.user.events.UserActivatedDomainEvent;
import pl.adroads.system.domain.user.model.UserId;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class UsersEventsListener {

  @Inject
  private DriverManagementAppService driverManagementAppService;

  @Handler
  public void userActivated(final UserActivatedDomainEvent userActivatedDomainEvent) {
    UserId userId = userActivatedDomainEvent.getUserId();
    driverManagementAppService.activateDriver(DriverId.of(userId.getValue()));
  }

}