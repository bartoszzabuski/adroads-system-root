package pl.adroads.system.infrastructure.adapters.driving.user.persistence.model;


import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@AllArgsConstructor
@EqualsAndHashCode
public class PersistenceSessionToken {

  private final String token;
  private final String timestamp;
  private final String expiryDate;

}
