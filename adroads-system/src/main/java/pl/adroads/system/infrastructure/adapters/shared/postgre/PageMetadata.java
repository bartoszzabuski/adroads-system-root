package pl.adroads.system.infrastructure.adapters.shared.postgre;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public final class PageMetadata {

  private final String uuid;
  private final Integer pageNumber;
}
