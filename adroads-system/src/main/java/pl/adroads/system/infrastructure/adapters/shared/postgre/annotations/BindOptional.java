package pl.adroads.system.infrastructure.adapters.shared.postgre.annotations;

import org.skife.jdbi.v2.SQLStatement;
import org.skife.jdbi.v2.sqlobject.Binder;
import org.skife.jdbi.v2.sqlobject.BinderFactory;
import org.skife.jdbi.v2.sqlobject.BindingAnnotation;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Optional;

@Retention(RetentionPolicy.RUNTIME)
@BindingAnnotation(BindOptional.BindOptionalFactory.class)
@Target({ElementType.PARAMETER})
public @interface BindOptional {

  String value();

  static class BindOptionalFactory implements BinderFactory {

    @Override
    public Binder build(Annotation annotation) {
      final BindOptional bindOptional = (BindOptional) annotation;
      final String key = bindOptional.value();
      return new OptionalBinder(key);

    }


    static class OptionalBinder implements Binder<BindOptional, Object> {

      public static final String IS_PRESENT_POSTFIX = "_isPresent";
      private final String key;

      public OptionalBinder(final String key) {
	this.key = key;
      }

      @Override
      public void bind(final SQLStatement<?> sqlStatement, final BindOptional bindOptional,
	  final Object arg) {
	Optional<?> optional = getOptionalFrom(arg);
	boolean isPresent = optional.isPresent();
	sqlStatement.define(key + IS_PRESENT_POSTFIX, isPresent);
	if (isPresent) {
	  sqlStatement.define(key, optional.get());
	}
      }

      private Optional<?> getOptionalFrom(final Object arg) {
	if (arg == null) {
	  throw new NullPointerException("Parameter to be bind cannot be null!");
	}
	if (!(arg instanceof Optional)) {
	  throw new RuntimeException("Not an Optional!");
	} else {
	  return Optional.class.cast(arg);
	}
      }
    }

  }
}
