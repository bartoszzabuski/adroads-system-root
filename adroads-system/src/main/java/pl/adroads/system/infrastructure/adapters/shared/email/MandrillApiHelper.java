package pl.adroads.system.infrastructure.adapters.shared.email;


import com.microtripit.mandrillapp.lutung.view.MandrillMessage;

import java.util.Collections;
import java.util.List;

import javax.inject.Named;

import static com.google.common.collect.Lists.newArrayList;
import static com.microtripit.mandrillapp.lutung.view.MandrillMessage.MergeVar;
import static com.microtripit.mandrillapp.lutung.view.MandrillMessage.MergeVarBucket;
import static com.microtripit.mandrillapp.lutung.view.MandrillMessage.Recipient;

@Named
public final class MandrillApiHelper {

  public MandrillMessage getMandrillMessageFor(final List<Recipient> recipients,
      final List<MergeVarBucket> mergeVarsBuckets) {
    final MandrillMessage message = new MandrillMessage();
    message.setTo(recipients);
    message.setPreserveRecipients(true);
    message.setMergeVars(mergeVarsBuckets);
    return message;
  }

  public MergeVar[] getMergeVarsArray(final MergeVar... mergeVars) {
    final int length = mergeVars.length;
    MergeVar[] mergeVarArray = new MergeVar[length];
    System.arraycopy(mergeVars, 0, mergeVarArray, 0, length);
    return mergeVarArray;
  }

  public MergeVar getMergeVarFor(final String name, final String content) {
    return new MergeVar(name, content);
  }

  public MergeVarBucket getMergeVarBucketFor(final Recipient recipient,
      final MergeVar[] mergeVars) {
    MergeVarBucket mergeVarBucket = new MergeVarBucket();
    mergeVarBucket.setRcpt(recipient.getEmail());
    mergeVarBucket.setVars(mergeVars);
    return mergeVarBucket;
  }

  public List<MergeVarBucket> getListOfMergeVarBuckets(final MergeVarBucket... mergeVarBuckets) {
    List<MergeVarBucket> mergeVarBucketsList = newArrayList();
    Collections.addAll(mergeVarBucketsList, mergeVarBuckets);
    return mergeVarBucketsList;
  }

  public List<Recipient> getListOfRecipients(final Recipient... recipients) {
    List<Recipient> recipientsList = newArrayList();
    Collections.addAll(recipientsList, recipients);
    return recipientsList;
  }

  public Recipient getRecipientFor(final String email) {
    Recipient recipient = new Recipient();
    recipient.setEmail(email);
    return recipient;
  }

}