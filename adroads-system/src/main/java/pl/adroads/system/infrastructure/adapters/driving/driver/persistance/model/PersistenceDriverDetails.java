package pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model;


import pl.adroads.system.common.domain.Persistence;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PersistenceDriverDetails implements Persistence {

  private Integer type;
  private Integer monthlyDistanceIndex;

}
