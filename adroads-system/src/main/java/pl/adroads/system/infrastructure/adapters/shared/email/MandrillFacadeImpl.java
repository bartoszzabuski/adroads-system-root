package pl.adroads.system.infrastructure.adapters.shared.email;

import static com.google.common.collect.Maps.newConcurrentMap;
import static com.microtripit.mandrillapp.lutung.view.MandrillMessage.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Value;

import com.microtripit.mandrillapp.lutung.MandrillApi;
import com.microtripit.mandrillapp.lutung.controller.MandrillMessagesApi;
import com.microtripit.mandrillapp.lutung.model.MandrillApiError;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage;

@Slf4j
@Named
public class MandrillFacadeImpl implements MandrillFacade {

    @Value("${adroads.system.user.mandrill.new.user.activation.template.name}")
    private String NEW_USER_ACTIVATION_MANDRILL_TEMPLATE_NAME;

    @Value("${adroads.system.user.mandrill.activation.url.merge.tag}")
    private String ACTIVATION_URL_MERGE_TAG;

    @Value("${adroads.system.user.mandrill.send.mode.async}")
    private final Boolean ASYNC = false;

    @Value("${adroads.system.campaign.mandrill.campaign.name.url.merge.tag}")
    private String campaignNameTag;

    @Value("${adroads.system.campaign.mandrill.approval.url.merge.tag}")
    private String approvalUrlTag;

    @Value("${adroads.system.campaign.mandrill.rejection.url.merge.tag}")
    private String rejectionUrlTag;

    @Value("${adroads.system.campaign.mandrill.start.date.merge.tag}")
    private String startDateTag;

    @Value("${adroads.system.campaign.mandrill.end.date.merge.tag}")
    private String endDateTag;

    @Value("${adroads.system.campaign.mandrill.driver.assigned.template.name}")
    private String driverAssignedToCampaignTemplate;

    @Inject
    private MandrillApiHelper mandrillApiHelper;

    @Inject
    private MandrillApi mandrillApi;


    public void sendNewUserActivationEmail(final String recipientEmail, final String activationUrl) {

        MandrillMessagesApi mandrillMessagesApi = mandrillApi.messages();

        final Map<String, String> templateContent = newConcurrentMap();

        final Recipient recipient = mandrillApiHelper.getRecipientFor(recipientEmail);
        final List<MandrillMessage.Recipient> recipients = mandrillApiHelper
                .getListOfRecipients(recipient);

        final MergeVar mergeVar = mandrillApiHelper
                .getMergeVarFor(ACTIVATION_URL_MERGE_TAG, activationUrl);
        final MergeVar[] mergeVars = mandrillApiHelper.getMergeVarsArray(mergeVar);

        final MergeVarBucket mergeVarBucket = mandrillApiHelper
                .getMergeVarBucketFor(recipient, mergeVars);
        final List<MandrillMessage.MergeVarBucket> mergeVarsBuckets = mandrillApiHelper
                .getListOfMergeVarBuckets(mergeVarBucket);

        final MandrillMessage message = mandrillApiHelper
                .getMandrillMessageFor(recipients, mergeVarsBuckets);

        try {
            mandrillMessagesApi
                    .sendTemplate(NEW_USER_ACTIVATION_MANDRILL_TEMPLATE_NAME, templateContent, message,
                            ASYNC);
            log.info(String.format("sending activation email to %s ", recipientEmail));
        } catch (MandrillApiError | IOException exception) {
            exception.printStackTrace();
            log.error(exception.getMessage());
        }
    }

    public void sendDriverAssignedToCampaignEmail(String recipientEmail, String approvalUrl, String rejectionUrl,
            String campaignName, String startDate, String endDate) {

        MandrillMessagesApi mandrillMessagesApi = mandrillApi.messages();

        final Map<String, String> templateContent = newConcurrentMap();

        final Recipient recipient = mandrillApiHelper.getRecipientFor(recipientEmail);
        final List<MandrillMessage.Recipient> recipients = mandrillApiHelper
                .getListOfRecipients(recipient);

        final MergeVar approvalUrlMergeVar = mandrillApiHelper.getMergeVarFor(approvalUrlTag, approvalUrl);
        final MergeVar rejectionUrlMergeVar = mandrillApiHelper.getMergeVarFor(rejectionUrlTag, rejectionUrl);
        final MergeVar campaignNameMergeVar = mandrillApiHelper.getMergeVarFor(campaignNameTag, campaignName);
        final MergeVar startDateTagMergeVar = mandrillApiHelper.getMergeVarFor(startDateTag, startDate);
        final MergeVar endDateTagMergeVar = mandrillApiHelper.getMergeVarFor(endDateTag, endDate);
        final MergeVar[] mergeVars = mandrillApiHelper.getMergeVarsArray(approvalUrlMergeVar, rejectionUrlMergeVar,
                campaignNameMergeVar, startDateTagMergeVar, endDateTagMergeVar);

        final MergeVarBucket mergeVarBucket = mandrillApiHelper
                .getMergeVarBucketFor(recipient, mergeVars);
        final List<MandrillMessage.MergeVarBucket> mergeVarsBuckets = mandrillApiHelper
                .getListOfMergeVarBuckets(mergeVarBucket);

        final MandrillMessage message = mandrillApiHelper
                .getMandrillMessageFor(recipients, mergeVarsBuckets);

        try {
            mandrillMessagesApi.sendTemplate(driverAssignedToCampaignTemplate, templateContent, message, ASYNC);
            log.info(String.format("sending 'driver assigned to campaign' email to %s ", recipientEmail));
        } catch (MandrillApiError | IOException exception) {
            exception.printStackTrace();
            log.error(exception.getMessage());
        }
    }

}
