package pl.adroads.system.infrastructure.adapters.shared;


import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model.PersistenceCampaign;

import java.util.Optional;

public interface CampaignStorage {

  void save(PersistenceCampaign persistenceCampaign);

  void update(PersistenceCampaign persistenceCampaign);

  Optional<PersistenceCampaign> get(String campaignId);

}
