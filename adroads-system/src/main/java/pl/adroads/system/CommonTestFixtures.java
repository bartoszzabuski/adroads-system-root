package pl.adroads.system;

import static com.google.common.collect.Sets.newHashSet;
import static java.util.UUID.randomUUID;
import static pl.adroads.system.domain.campaign.model.CampaignId.campaignIdOf;
import static pl.adroads.system.domain.driver.model.City.cityOf;
import static pl.adroads.system.domain.driver.model.Colour.colourOf;
import static pl.adroads.system.domain.driver.model.DistanceMeasurement.distanceMeasurementOf;
import static pl.adroads.system.domain.driver.model.Status.CREATED;
import static pl.adroads.system.domain.driver.model.Street.streetOf;
import static pl.adroads.system.domain.driver.model.UnitsOfMeasurement.CM;
import static pl.adroads.system.domain.driver.model.UnitsOfMeasurement.KM;
import static pl.adroads.system.domain.user.model.UserId.userIdOf;
import static pl.adroads.system.domain.user.model.VerificationToken.VerificationTokenType;

import java.net.URI;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.Set;
import java.util.UUID;

import pl.adroads.api.model.driver.DriverCampaignDetailsResource;
import pl.adroads.api.model.driver.DriverResource;
import pl.adroads.api.model.shared.AddressResource;
import pl.adroads.api.model.shared.CarDetailsResource;
import pl.adroads.api.model.shared.DriverDetailsResource;
import pl.adroads.api.model.shared.WindscreenDimensionsResource;
import pl.adroads.system.domain.campaign.model.CampaignId;
import pl.adroads.system.domain.campaign.model.CampaignStatus;
import pl.adroads.system.domain.driver.model.Address;
import pl.adroads.system.domain.driver.model.CarDetails;
import pl.adroads.system.domain.driver.model.City;
import pl.adroads.system.domain.driver.model.Colour;
import pl.adroads.system.domain.driver.model.DistanceMeasurement;
import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.domain.driver.model.DriverDetails;
import pl.adroads.system.domain.driver.model.DriverId;
import pl.adroads.system.domain.driver.model.Email;
import pl.adroads.system.domain.driver.model.PolishCarBody;
import pl.adroads.system.domain.driver.model.PolishDriverType;
import pl.adroads.system.domain.driver.model.PolishProvince;
import pl.adroads.system.domain.driver.model.Postcode;
import pl.adroads.system.domain.driver.model.Status;
import pl.adroads.system.domain.driver.model.Street;
import pl.adroads.system.domain.driver.model.WindscreenDimensions;
import pl.adroads.system.domain.user.model.SessionToken;
import pl.adroads.system.domain.user.model.User;
import pl.adroads.system.domain.user.model.UserId;
import pl.adroads.system.domain.user.model.UserType;
import pl.adroads.system.domain.user.model.VerificationToken;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceAddress;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceCarDetails;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceDriver;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceDriverDetails;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceWindscreenDimensions;
import pl.adroads.system.infrastructure.adapters.driving.query.persistance.model.PersistenceCampaignDetails;
import pl.adroads.system.infrastructure.adapters.driving.query.persistance.model.PersistenceQueryDriver;
import pl.adroads.system.infrastructure.adapters.driving.user.persistence.model.PersistenceSessionToken;
import pl.adroads.system.infrastructure.adapters.driving.user.persistence.model.PersistenceUser;
import pl.adroads.system.infrastructure.adapters.driving.user.persistence.model.PersistenceVerificationToken;

public abstract class CommonTestFixtures {

    public static final String EMAIL_STRING = "mockEmail@mock.com";
    public static final String POLISH_EMAIL_STRING = "zżźćńółęąśŻŹĆĄŚĘŁÓŃ@mock.com";
    public static final Email EMAIL = Email.emailOf(EMAIL_STRING);
    public static final UUID VALID_UUID = randomUUID();
    public static final UUID VALID_UUID_2 = randomUUID();
    public static final UUID VALID_UUID_3 = randomUUID();
    public static final UUID VALID_UUID_4 = randomUUID();
    public static final UUID CAMPAIGN_UUID = UUID.randomUUID();
    // public static final CampaignId CAMPAIGN_ID = CampaignId.of(CAMPAIGN_UUID);
    public static final String POSTCODE_STRING = "54-614";
    public static final Postcode POSTCODE = Postcode.postcodeOf(POSTCODE_STRING);
    public static final String INVALID_EMAIL = "invalidEmail";
    public static final String INVALID_POSTCODE = "invalidPostcode";
    public static final String STATUS_CREATED = CREATED.name();

    public static final String TABLE_NAME = "dbTableName";
    public static final String FILTER = "filter";
    public static final String ORDER = "order";

    public static final String COMPANY_NAME = "CompanyName";
    public static final String REGON = "regon";
    public static final String NIP = "nip";

    public static final String STREET_VALUE = "street";
    public static final String INVALID_STREET_VALUE = "st";
    public static final Street STREET = streetOf(STREET_VALUE);
    public static final String CITY_VALUE = "city";
    public static final City CITY = cityOf(CITY_VALUE);
    public static final String INVALID_CITY_VALUE = "Wr";
    public static final PolishProvince PROVINCE = PolishProvince.DOLNOŚLĄSKIE;
    public static final String COUNTRY = "country";
    public static final String FULL_NAME = "fullname";
    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    public static final String DATE_OF_BIRTH_STRING = "25-04-1989";
    public static final LocalDate DATE_OF_BIRTH = LocalDate.parse(DATE_OF_BIRTH_STRING, formatter);
    public static final String PHONE_NUMBER_STRING_1 = "601370400";
    public static final String PHONE_NUMBER_STRING_2 = "0048601370400";
    public static final String PHONE_NUMBER_STRING_3 = "713615810";
    public static final String PHONE_NUMBER_STRING_4 = "+48601370400";
    public static final String INVALID_PHONE_NUMBER_STRING = "1234234234";
    // public static final PolishPhoneNumber PHONE_NUMBER = PolishPhoneNumber.of(PHONE_NUMBER_STRING_1);

    public static final String COLOUR_VALUE = "zielony";
    public static final String INVALID_COLOUR_VALUE = "zi";
    public static final Colour COLOUR = colourOf(COLOUR_VALUE);

    public static final UUID DRIVER_UUID_1 = randomUUID();
    public static final UUID DRIVER_UUID_2 = randomUUID();
    public static final DriverId DRIVER_ID_1 = DriverId.of(DRIVER_UUID_1);
    public static final DriverId DRIVER_ID_2 = DriverId.of(DRIVER_UUID_2);
    public static final Set<UUID> CAMPAIGN_DRIVERS = newHashSet(DRIVER_UUID_1, DRIVER_UUID_2);
    public static final UUID CLIENT_UUID = randomUUID();
    // public static final CampaignStatus CAMPAIGN_STATUS = CampaignStatus.CREATED;

    public static final Set<UUID> REMAINING_ASSIGNED_CAMPAINGS = newHashSet(VALID_UUID_3,
            VALID_UUID_4);

    public static final String MAKE = "Renault";
    public static final String MODEL = "Megane Classic";
    public static final Year YEAR = Year.of(2010);
    public static final Year INVALID_YEAR = Year.of(1700);
    public static final Integer MILEAGE_VALUE = 10000;
    public static final DistanceMeasurement MILEAGE = distanceMeasurementOf(10000, KM);
    public static final Integer INVALID_MILEAGE = -1;
    public static final PolishCarBody BODY = PolishCarBody.SEDAN;

    public static final Integer WINDSCREEN_HEIGHT_VALUE = 120;
    public static final DistanceMeasurement WINDSCREEN_HEIGHT = distanceMeasurementOf(
            WINDSCREEN_HEIGHT_VALUE, CM);
    public static final Integer WINDSCREEN_WIDTH_VALUE = 150;
    public static final DistanceMeasurement WINDSCREEN_WIDTH = distanceMeasurementOf(
            WINDSCREEN_WIDTH_VALUE, CM);
    public static final WindscreenDimensions WINDSCREEN_DIMENSIONS = WindscreenDimensions
            .windscreenDimensionsOf(WINDSCREEN_HEIGHT_VALUE, WINDSCREEN_WIDTH_VALUE);
    public static final PersistenceWindscreenDimensions PERSISTENCE_WINDSCREEN_DIMENSIONS = new PersistenceWindscreenDimensions(
            WINDSCREEN_HEIGHT_VALUE, WINDSCREEN_WIDTH_VALUE);
    public static final WindscreenDimensionsResource WINDSCREEN_DIMENSIONS_RESOURCE = new WindscreenDimensionsResource(
            WINDSCREEN_HEIGHT_VALUE, WINDSCREEN_WIDTH_VALUE);
    public static final Integer MONTHLY_DISTANCE_INDEX = 1;
    public static final PolishDriverType DRIVER_TYPE = PolishDriverType.INNY;

    public static final Address ADDRESS = Address.of(POSTCODE, STREET, CITY, PROVINCE);
    public static final PersistenceAddress PERSISTENCE_ADDRESS = new PersistenceAddress(
            POSTCODE.getValue(), STREET_VALUE, CITY_VALUE, PROVINCE.toString());
    public static final AddressResource ADDRESS_RESOURCE = new AddressResource(POSTCODE_STRING,
            CITY.getValue(),
            STREET.getValue(),
            PROVINCE.toString());

    public static final CarDetails CAR_DETAILS = CarDetails
            .of(MAKE, MODEL, YEAR, MILEAGE_VALUE, BODY, COLOUR, WINDSCREEN_DIMENSIONS);
    public static final PersistenceCarDetails PERSISTENCE_CAR_DETAILS = new PersistenceCarDetails(
            MAKE, MODEL, YEAR.toString(), MILEAGE_VALUE, BODY.toString(), COLOUR_VALUE,
            PERSISTENCE_WINDSCREEN_DIMENSIONS);
    public static final CarDetailsResource CAR_DETAILS_RESOURCE = new CarDetailsResource(MAKE, MODEL,
            YEAR.toString(),
            MILEAGE_VALUE,
            BODY.toString(),
            COLOUR_VALUE,
            WINDSCREEN_DIMENSIONS_RESOURCE);

    public static final DriverDetails DRIVER_DETAILS = DriverDetails
            .of(DRIVER_TYPE, MONTHLY_DISTANCE_INDEX);
    public static final PersistenceDriverDetails PERSISTENCE_DRIVER_DETAILS = new PersistenceDriverDetails(
            DRIVER_TYPE.getValue(), MONTHLY_DISTANCE_INDEX);
    public static final DriverDetailsResource DRIVER_DETAILS_RESOURCE = new DriverDetailsResource(
            DRIVER_TYPE.getValue(), MONTHLY_DISTANCE_INDEX);
    public static final PersistenceDriver PERSISTENCE_DRIVER = new PersistenceDriver(
            VALID_UUID.toString(), NAME, SURNAME, DATE_OF_BIRTH_STRING, EMAIL_STRING, PERSISTENCE_ADDRESS,
            STATUS_CREATED, PERSISTENCE_CAR_DETAILS, PERSISTENCE_DRIVER_DETAILS);

    public static final URI URL = URI.create("url_string");
    public static final String URL_STRING = URL.toString();
    public static final CampaignId CAMPAIGN_ID = campaignIdOf(VALID_UUID);
    public static final String CAMPAIGN_NAME = "campaignName";
    public static final String CAMPAIGN_DESCRIPTION = "campaignDesc";
    public static final CampaignStatus CAMPAIGN_STATUS = CampaignStatus.CREATED;
    public static final LocalDateTime START_TIME = LocalDateTime.now().plusMinutes(10);
    public static final LocalDateTime END_TIME = LocalDateTime.now().plusMonths(3L);

    public static final String SESSION_TOKEN_VALUE = "sessionToken123";

    public static final Driver DRIVER = new Driver.DriverBuilder().withID(DRIVER_ID_1).withName(NAME)
            .withSurname(SURNAME).withDateOfBirth(DATE_OF_BIRTH).withEmail(EMAIL).withAddress(ADDRESS)
            .withStatus(Status.CREATED).withCarDetails(CAR_DETAILS).withDriverDetails(DRIVER_DETAILS)
            .build();

    public static final DriverResource DRIVER_RESOURCE = new DriverResource(
            DRIVER_ID_1.getValue().toString(), NAME, SURNAME, DATE_OF_BIRTH_STRING, EMAIL_STRING,
            Status.CREATED.name(), ADDRESS_RESOURCE, CAR_DETAILS_RESOURCE, DRIVER_DETAILS_RESOURCE,
            new DriverCampaignDetailsResource(CAMPAIGN_ID.getValue().toString(), CAMPAIGN_NAME));

    public static final DriverResource DRIVER_RESOURCE_WITHOUT_CAMPAIGN = new DriverResource(
            DRIVER_ID_1.getValue().toString(), NAME, SURNAME, DATE_OF_BIRTH.toString(), EMAIL_STRING,
            Status.CREATED.name(), ADDRESS_RESOURCE, CAR_DETAILS_RESOURCE, DRIVER_DETAILS_RESOURCE, null);

    public static final UserId USER_ID = userIdOf(VALID_UUID);

    public static final String TOKEN_STRING = "token";
    public static final LocalDateTime EXPIRY_DATE = LocalDateTime.now().plusDays(2);
    public static final LocalDateTime VERIFICATION_DATE = LocalDateTime.now();
    public static final LocalDateTime TIMESTAMP = LocalDateTime.now();
    public static final VerificationTokenType REGISTRATION_CONFIRMATION = VerificationTokenType.REGISTRATION_CONFIRMATION;
    public static final boolean NOT_VERIFIED = false;
    public static final LocalDateTime NOW = LocalDateTime.now();
    public static final boolean VERIFIED = true;
    public static final PersistenceVerificationToken PERSISTENCE_VERIFICATION_TOKEN = new PersistenceVerificationToken(
            TOKEN_STRING, EXPIRY_DATE.toString(),
            REGISTRATION_CONFIRMATION.name(), TIMESTAMP.toString(),
            NOT_VERIFIED, VERIFICATION_DATE.toString());

    public static final LocalDateTime NOW_PLUS_15_MIN = NOW.plusMinutes(15);
    public static final SessionToken SESSION_TOKEN = new SessionToken(TOKEN_STRING, NOW, NOW_PLUS_15_MIN);
    public static final PersistenceSessionToken PERSISTENCE_SESSION_TOKEN = new PersistenceSessionToken(TOKEN_STRING,
            NOW.toString(), NOW_PLUS_15_MIN.toString());
    public static final String TEST_PASSWORD = "testPassword";

    public static final PersistenceVerificationToken NOT_VERIFIED_PERSISTENCE_VERIFICATION_TOKEN = new PersistenceVerificationToken(
            TOKEN_STRING, EXPIRY_DATE.toString(),
            REGISTRATION_CONFIRMATION.name(), TIMESTAMP.toString(),
            NOT_VERIFIED, null);

    public static final VerificationToken VERIFICATION_TOKEN = new VerificationToken(TOKEN_STRING,
            TIMESTAMP,
            EXPIRY_DATE,
            REGISTRATION_CONFIRMATION);

    public static final VerificationToken VERIFIED_VERIFICATION_TOKEN = new VerificationToken(
            TOKEN_STRING, TIMESTAMP, EXPIRY_DATE, REGISTRATION_CONFIRMATION, NOT_VERIFIED,
            VERIFICATION_DATE);

    public static final LocalDateTime EXPIRY_DATE_IN_PAST = LocalDateTime.now().minusMonths(1);
    public static final VerificationToken EXPIRED_VERIFICATION_TOKEN = new VerificationToken(
            TOKEN_STRING, TIMESTAMP, EXPIRY_DATE_IN_PAST, REGISTRATION_CONFIRMATION, NOT_VERIFIED,
            VERIFICATION_DATE);
    public static final VerificationToken VERIFIED_TOKEN = new VerificationToken(TOKEN_STRING,
            TIMESTAMP,
            EXPIRY_DATE_IN_PAST,
            REGISTRATION_CONFIRMATION,
            VERIFIED,
            VERIFICATION_DATE);
    public static final String DIFFERENT_TOKEN_STRING = "different_token";
    public static final VerificationToken DIFFERENT_VERIFICATION_TOKEN = new VerificationToken(
            DIFFERENT_TOKEN_STRING, TIMESTAMP, EXPIRY_DATE_IN_PAST, REGISTRATION_CONFIRMATION,
            NOT_VERIFIED, VERIFICATION_DATE);
    public static final User USER = new User(USER_ID, EMAIL, VERIFICATION_TOKEN, UserType.DRIVER);
    public static final User USER_WITH_VERIFIED_TOKEN = new User(USER_ID, EMAIL,
            VERIFIED_VERIFICATION_TOKEN, UserType.DRIVER);

    public static final PersistenceUser PERSISTENCE_USER = new PersistenceUser(VALID_UUID.toString(),
            EMAIL_STRING, PERSISTENCE_VERIFICATION_TOKEN, UserType.DRIVER.name(), TEST_PASSWORD,
            PERSISTENCE_SESSION_TOKEN);

    public static final PersistenceUser NOT_VERIFIED_PERSISTENCE_USER = new PersistenceUser(
            VALID_UUID.toString(), EMAIL_STRING, NOT_VERIFIED_PERSISTENCE_VERIFICATION_TOKEN, UserType.DRIVER.name(),
            null, null);

    public static final PersistenceCampaignDetails PERSISTENCE_CAMPAIGN_DETAILS = new PersistenceCampaignDetails(
            CAMPAIGN_ID.getValue().toString(), CAMPAIGN_NAME);
    public static final PersistenceQueryDriver PERSISTENCE_QUERY_DRIVER = new PersistenceQueryDriver(
            DRIVER_ID_1.getValue().toString(), NAME, SURNAME, DATE_OF_BIRTH_STRING, EMAIL_STRING, PERSISTENCE_ADDRESS,
            STATUS_CREATED, PERSISTENCE_CAR_DETAILS, PERSISTENCE_DRIVER_DETAILS, PERSISTENCE_CAMPAIGN_DETAILS);

}
