package pl.adroads.system.config.subconfigs;

import static java.lang.String.format;

import javax.sql.DataSource;

import org.skife.jdbi.v2.DBI;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import pl.adroads.system.infrastructure.adapters.shared.postgre.CampaignDriversQueryDao;
import pl.adroads.system.infrastructure.adapters.shared.postgre.CampaignsQueryDao;
import pl.adroads.system.infrastructure.adapters.shared.postgre.DriversQueryDao;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
public class PostgreSQLConfig {

    @Value("${adroads.system.db.host}")
    private String DB_HOST;
    @Value("${adroads.system.db.port}")
    private String DB_PORT;
    @Value("${adroads.system.db.name}")
    private String DATABASE_NAME;
    @Value("${adroads.system.db.options:}")
    private String postgreOptions;
    @Value("${adroads.system.db.username}")
    private String DB_USER;
    @Value("${adroads.system.db.pass}")
    private String DB_PASS;

    public static final String JDBC_POSTGRESQL_PREFIX = "jdbc:postgresql://";

    @Bean
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(dataSource());
    }

    @Bean
    public DataSource dataSource() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(getPostgresqlHost(DB_HOST, DB_PORT, DATABASE_NAME));
        config.setUsername(DB_USER);
        config.setPassword(DB_PASS);
        // config.addDataSourceProperty("cachePrepStmts", "true");
        // config.addDataSourceProperty("prepStmtCacheSize", "250");
        // config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        // config.addDataSourceProperty("useServerPrepStmts", "true");

        return new HikariDataSource(config);
    }

    private String getPostgresqlHost(final String host, final String port, final String databaseName) {
        final String url = format("%s%s:%s/%s?%s", JDBC_POSTGRESQL_PREFIX, host, port, databaseName, postgreOptions);
        System.out.println("postgres url: " + url);
        return url;
    }

    @Bean
    public DataSourceTransactionManager transactionManager() {
        return new DataSourceTransactionManager(dataSource());
    }

    @Bean
    public DBI dbi() {
        // TODO investigate if calling dataSource() returns new object or singleton?
        // TODO investigate how to configure transactionality on handle
        DBI dbi = new DBI(dataSource());
        // CampaignQueryDAO campaignQueryDAO = dbi.onDemand(CampaignQueryDAO.class);
        // DriverQueryDAO driverQueryDAO = dbi.onDemand(DriverQueryDAO.class);
        return dbi;
    }

    //// TODO is this in use?
    // @Bean
    // public Handle handle() {
    // return dbi().open();
    // }
    //
    // // TODO is this in use?
    // @Bean
    // public PaginatedDriverQueryDao paginatedDriverQueryDaoOld() {
    // return dbi().onDemand(PaginatedDriverQueryDao.class);
    // }

    @Bean
    public DriversQueryDao driversQueryDao() {
        return dbi().onDemand(DriversQueryDao.class);
    }

    @Bean
    public CampaignDriversQueryDao campaignDriversQueryDao() {
        return dbi().onDemand(CampaignDriversQueryDao.class);
    }

    @Bean
    public CampaignsQueryDao campaignsQueryDao() {
        return dbi().onDemand(CampaignsQueryDao.class);
    }

}
