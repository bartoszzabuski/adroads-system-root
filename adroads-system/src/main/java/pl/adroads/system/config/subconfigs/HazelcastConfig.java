package pl.adroads.system.config.subconfigs;

import java.util.List;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

import com.hazelcast.config.Config;
import com.hazelcast.config.GroupConfig;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.MulticastConfig;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.config.SymmetricEncryptionConfig;
import com.hazelcast.config.TcpIpConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

@Configuration
@PropertySource("classpath:hazelcast.properties")
@Import(HazelcastLockStoresConfig.class)
public class HazelcastConfig {

    @Value("${hazelcast.group.name}")
    private String username;

    @Value("${hazelcast.group.password}")
    private String password;

    @Value("${hazelcast.logging.type")
    private String loggingType;

    @Value("${hazelcast.jmx.active}")
    private String jmxActive;

    @Value("${hazelcast.network.port}")
    private int port;

    @Value("${hazelcast.network.join.multicast.active}")
    private boolean networkJoinMulticastActive;

    @Value("${hazelcast.network.join.multicast.group}")
    private String networkJoinMulticastGroup;

    @Value("${hazelcast.network.join.multicast.port}")
    private int networkJoinMulticastPort;

    @Value("${hazelcast.network.join.tcpip.active}")
    private boolean networkJoinTcpIpActive;

    @Value("${hazelcast.network.join.tcpip.members}")
    private List<String> members;

    // @Value("${hazelcast.idle.time.window.size.seconds}")
    // private int idleTimeWindowSizeSeconds;

    // @Autowired
    // private ManagementCenterConfig managementCenterConfig;

    @Bean
    public HazelcastInstance hazelcastInstance() {
        return Hazelcast.newHazelcastInstance(hazelcastInstanceConfig());
    }

    @Bean
    public Config hazelcastInstanceConfig() {
        Config config = new Config();
        config.setInstanceName("casinoStoreHazelcastInstance");
        config.setGroupConfig(groupConfig());
        // config.setManagementCenterConfig(managementCenterConfig);
        config.setProperties(hazelcastProperties());
        config.setNetworkConfig(networkConfig());
        return config;
    }

    @Bean
    public NetworkConfig networkConfig() {
        NetworkConfig networkConfig = new NetworkConfig();
        networkConfig.setPort(port);
        networkConfig.setJoin(joinConfig());
        networkConfig.setSymmetricEncryptionConfig(symmetricEncryptionConfig());
        return networkConfig;
    }

    @Bean
    public GroupConfig groupConfig() {
        return new GroupConfig(username, password);
    }

    @Bean
    public Properties hazelcastProperties() {
        Properties properties = new Properties();
        properties.setProperty("hazelcast.logging.type", loggingType);
        properties.setProperty("hazelcast.jmx", jmxActive);
        return properties;
    }

    @Bean
    public SymmetricEncryptionConfig symmetricEncryptionConfig() {
        return new SymmetricEncryptionConfig();
    }

    @Bean
    public JoinConfig joinConfig() {
        JoinConfig joinConfig = new JoinConfig();
        joinConfig.setMulticastConfig(multicastConfig());
        joinConfig.setTcpIpConfig(tcpIpConfig());
        return joinConfig;
    }

    @Bean
    public TcpIpConfig tcpIpConfig() {
        TcpIpConfig tcpIpConfig = new TcpIpConfig();
        tcpIpConfig.setEnabled(networkJoinTcpIpActive);
        tcpIpConfig.setMembers(members);
        return tcpIpConfig;
    }

    @Bean
    public MulticastConfig multicastConfig() {
        MulticastConfig multicastConfig = new MulticastConfig();
        multicastConfig.setEnabled(networkJoinMulticastActive);
        multicastConfig.setMulticastGroup(networkJoinMulticastGroup);
        multicastConfig.setMulticastPort(networkJoinMulticastPort);
        return multicastConfig;
    }
}
