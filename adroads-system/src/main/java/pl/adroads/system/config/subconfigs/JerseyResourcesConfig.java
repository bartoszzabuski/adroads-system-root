package pl.adroads.system.config.subconfigs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import pl.adroads.system.application.user.UserManagementService;
import pl.adroads.system.filters.AuthenticationFilter;
import pl.adroads.system.filters.AuthorizationFilter;
import pl.adroads.system.resources.JerseyConfig;

@Configuration
@ComponentScan({ "pl.adroads.system.resources" })
public class JerseyResourcesConfig {

    @Autowired
    private UserManagementService userManagementService;

    @Bean
    public AuthenticationFilter authenticationFilter() {
        return new AuthenticationFilter(userManagementService);
    }

    @Bean
    public AuthorizationFilter authorizationFilter() {
        return new AuthorizationFilter();
    }

    @Bean
    public JerseyConfig jerseyConfig() {
        return new JerseyConfig(authenticationFilter(), authorizationFilter());
    }
}
