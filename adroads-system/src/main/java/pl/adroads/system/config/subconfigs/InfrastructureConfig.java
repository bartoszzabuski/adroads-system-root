package pl.adroads.system.config.subconfigs;

import javax.inject.Inject;

import net.engio.mbassy.bus.MBassador;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import pl.adroads.system.common.domain.DomainEvent;
import pl.adroads.system.infrastructure.adapters.driven.campaign.integration.listeners.CampaignsEventsListener;
import pl.adroads.system.infrastructure.adapters.driven.driver.integration.listeners.DriversEventsListener;
import pl.adroads.system.infrastructure.adapters.driven.user.integration.listeners.UsersEventsListener;

import com.microtripit.mandrillapp.lutung.MandrillApi;

@Configuration
@Import({ PostgreSQLConfig.class, HazelcastConfig.class, JerseyResourcesConfig.class })
@ComponentScan({ "pl.adroads.system.infrastructure", "pl.adroads.system.application" })
public class InfrastructureConfig {

    @Value("${adroads.system.user.mandrill.api.key}")
    private String apiKey;

    @Inject
    private DriversEventsListener driversEventsListener;

    @Inject
    private UsersEventsListener usersEventsListener;

    @Inject
    private CampaignsEventsListener campaignsEventsListener;

    @Bean
    public MBassador bus() {
        MBassador bus = new MBassador<DomainEvent>();
        bus.subscribe(driversEventsListener);
        bus.subscribe(usersEventsListener);
        bus.subscribe(campaignsEventsListener);
        return bus;
    }

    @Bean
    public MandrillApi mandrillApi() {
        return new MandrillApi(apiKey);
    }

}
