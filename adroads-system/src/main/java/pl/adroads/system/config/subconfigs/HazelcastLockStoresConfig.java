package pl.adroads.system.config.subconfigs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import pl.adroads.system.infrastructure.locking.HazelcastLockStore;

import com.hazelcast.core.HazelcastInstance;

@Configuration
public class HazelcastLockStoresConfig {

    @Autowired
    private HazelcastInstance hazelcastInstance;

    @Bean
    public HazelcastLockStore campaignRepositoryLockStore() {
        return new HazelcastLockStore<String, String>(hazelcastInstance, "campaignRepoLock");
    }

    @Bean
    public HazelcastLockStore driversRepositoryLockStore() {
        return new HazelcastLockStore<String, String>(hazelcastInstance, "driversRepoLock");
    }
}
