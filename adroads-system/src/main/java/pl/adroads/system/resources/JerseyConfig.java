package pl.adroads.system.resources;

import org.glassfish.jersey.server.ResourceConfig;

import pl.adroads.system.filters.AuthenticationFilter;
import pl.adroads.system.filters.AuthorizationFilter;

public class JerseyConfig extends ResourceConfig {

    // TODO probs should inject simpleCORS filter this way too
    public JerseyConfig(AuthenticationFilter authenticationFilter, AuthorizationFilter authorizationFilter) {

        packages("pl.adroads.system");
        register(DriverAPIImpl.class);
        register(CampaignAPIImpl.class);
        register(UserAPIImpl.class);
        register(QueryAPIImpl.class);

        register(authenticationFilter, 1000);
        register(authorizationFilter, 2000);
        // register(authenticationFilter);

        // register(SimpleCORSFilter.class);
        // register(AuthenticationFilter.class);
    }

}
