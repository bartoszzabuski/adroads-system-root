package pl.adroads.system.resources;

import static pl.adroads.system.domain.user.model.UserType.ADMIN;

import java.net.URI;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;

import pl.adroads.api.DriverAPI;
import pl.adroads.api.model.driver.CreateDriverRequest;
import pl.adroads.api.model.driver.UpdateDriverRequest;
import pl.adroads.system.application.driver.DriverManagementAppService;
import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.domain.driver.model.DriverId;
import pl.adroads.system.filters.Secured;
import pl.adroads.system.infrastructure.adapters.driven.driver.integration.rest.assemblers.DriverAssembler;

@Named
public class DriverAPIImpl implements DriverAPI {

    @Inject
    private DriverAssembler driverAssembler;
    @Inject
    private DriverManagementAppService driverManagementAppService;

    public Response createNewDriver(final CreateDriverRequest createDriverRequest) {
        Driver driver = driverAssembler.assemble(createDriverRequest);
        DriverId driverId = driverManagementAppService.registerNewDriver(driver);
        return Response.created(URI.create(PATH + driverId.getValue())).build();
    }

    @Secured(ADMIN)
    public Response updateDriver(final String uuid, final UpdateDriverRequest updateDriverRequest) {
        Driver driver = driverAssembler.assemble(uuid, updateDriverRequest);
        DriverId driverId = DriverId.of(uuid);
        driverManagementAppService.updateDriver(driverId, driver);
        return Response.ok().build();
    }

}
