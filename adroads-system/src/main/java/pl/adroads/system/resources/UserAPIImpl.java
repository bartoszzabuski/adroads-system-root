package pl.adroads.system.resources;

import static pl.adroads.system.domain.driver.model.Email.emailOf;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;

import pl.adroads.api.UserAPI;
import pl.adroads.api.model.user.TokenResource;
import pl.adroads.system.application.user.UserManagementService;
import pl.adroads.system.domain.driver.model.Email;
import pl.adroads.system.domain.user.model.SessionToken;

@Named
public class UserAPIImpl implements UserAPI {

    // @Value("${adroads.system.user.verification.email.verification.web.page.url}")
    // private String verificationWebPageUrl;

    @Inject
    private UserManagementService userManagementService;

    @Override
    public Response verifyNewlyRegisteredUser(final String email, final String token) {
        Email userEmail = emailOf(email);
        userManagementService.verifyToken(userEmail, token);
        return Response.ok().build();
        // return Response.temporaryRedirect(URI.create(verificationWebPageUrl)).build();
    }

    @Override
    public Response authenticateUser(final String login, final String password) {
        SessionToken sessionToken = userManagementService.authenticate(emailOf(login), password);
        return Response.ok(new TokenResource(sessionToken.getToken())).build();
    }

}
