package pl.adroads.system.resources;

import static java.util.stream.Collectors.toList;
import static pl.adroads.system.domain.campaign.model.CampaignId.campaignIdOf;
import static pl.adroads.system.domain.user.model.UserType.ADMIN;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.core.SecurityContext;

import lombok.extern.slf4j.Slf4j;

import org.springframework.stereotype.Component;

import pl.adroads.api.QueryAPI;
import pl.adroads.api.model.campaign.CampaignIndexResource;
import pl.adroads.api.model.campaign.CampaignResource;
import pl.adroads.api.model.driver.DriverFilterCriteriaResource;
import pl.adroads.api.model.driver.DriverResource;
import pl.adroads.api.model.driver.PagedDriversResource;
import pl.adroads.api.model.shared.PageIndexResource;
import pl.adroads.api.model.shared.PageMetadataResource;
import pl.adroads.system.application.query.QueryService;
import pl.adroads.system.domain.driver.model.DriverId;
import pl.adroads.system.domain.query.model.DriverFilterCriteria;
import pl.adroads.system.domain.query.model.Page;
import pl.adroads.system.filters.Secured;
import pl.adroads.system.infrastructure.adapters.driven.query.integration.rest.assemblers.CampaignIndexesAssembler;
import pl.adroads.system.infrastructure.adapters.driven.query.integration.rest.assemblers.CampaignResourceAssembler;
import pl.adroads.system.infrastructure.adapters.driven.query.integration.rest.assemblers.DriverFilterCriteriaAssembler;
import pl.adroads.system.infrastructure.adapters.driven.query.integration.rest.assemblers.DriverResourceAssembler;
import pl.adroads.system.infrastructure.adapters.driving.query.persistance.model.PersistenceQueryDriver;
import pl.adroads.system.infrastructure.adapters.shared.postgre.PageMetadata;

// TODO transactionality ???
@Slf4j
@Component
public class QueryAPIImpl implements QueryAPI {

    @Inject
    private QueryService queryService;
    @Inject
    private DriverResourceAssembler driverResourceAssembler;
    @Inject
    private CampaignResourceAssembler campaignResourceAssembler;
    @Inject
    private CampaignIndexesAssembler campaignIndexesAssembler;
    @Inject
    private DriverFilterCriteriaAssembler driverCriteriaAssembler;

    @Secured(ADMIN)
    @Override
    public DriverResource getDriver(final String uuid) {
        return driverResourceAssembler.assemble(queryService.getDriver(DriverId.of(uuid)));
    }

    @Secured(ADMIN)
    @Override
    public PagedDriversResource getFirstPage(
            final DriverFilterCriteriaResource driverFilterCriteriaResource, final Integer pageSize) {
        final DriverFilterCriteria driverFilterCriteria = driverCriteriaAssembler
                .assembleFilterCriteria(driverFilterCriteriaResource);
        final Page<PersistenceQueryDriver> page = queryService.fetchFirstPage(driverFilterCriteria, pageSize);

        return pagedDriversResourceFrom(page);
    }

    @Secured(ADMIN)
    @Override
    public PagedDriversResource getSubsequentPage(
            final DriverFilterCriteriaResource driverFilterCriteriaResource, final Integer pageSize,
            final String id) {
        final DriverFilterCriteria driverFilterCriteria = driverCriteriaAssembler
                .assembleFilterCriteria(driverFilterCriteriaResource);
        final Page<PersistenceQueryDriver> page = queryService
                .fetchSubsequentPage(driverFilterCriteria, pageSize, DriverId.of(id));

        return pagedDriversResourceFrom(page);
    }

    @Secured(ADMIN)
    @Override
    public List<CampaignResource> getCampaigns() {
        return campaignResourceAssembler.assemble(queryService.getCampaigns());
    }

    @Secured(ADMIN)
    @Override
    public CampaignResource getCampaign(final String campaignId) {
        return campaignResourceAssembler.assemble(queryService.getCampaign(campaignIdOf(campaignId)));
    }

    @Secured(ADMIN)
    @Override
    public List<CampaignIndexResource> getCampaignNames(SecurityContext securityContext) {
        log.info(this.getClass().getSimpleName() + " - getCampaignNames called by "
                + securityContext.getUserPrincipal().getName());
        return campaignIndexesAssembler.assemble(queryService.getCampaigns());
    }

    // TODO clean this up
    private PagedDriversResource pagedDriversResourceFrom(final Page<PersistenceQueryDriver> page) {
        final List<DriverResource> driverResources = driverResourceAssembler
                .assemble(page.getDrivers());
        if (page.getMetadata().isPresent()) {
            final List<PageIndexResource> pageIndexResourceList = assemble(page.getMetadata().get());
            return new PagedDriversResource(
                    new PageMetadataResource(driverResources.size(), pageIndexResourceList), driverResources);
        } else {
            return new PagedDriversResource(null, driverResources);
        }
    }

    // TODO probs move to assembler
    private List<PageIndexResource> assemble(final List<PageMetadata> pageMetadataList) {
        return pageMetadataList.stream().map(
                pageMetadata -> new PageIndexResource(pageMetadata.getUuid(), pageMetadata.getPageNumber()))
                .collect(toList());
    }

}
