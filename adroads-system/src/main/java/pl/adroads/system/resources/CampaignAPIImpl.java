package pl.adroads.system.resources;

import static pl.adroads.system.domain.campaign.model.CampaignId.campaignIdOf;
import static pl.adroads.system.domain.driver.model.DriverId.of;
import static pl.adroads.system.domain.user.model.UserType.ADMIN;

import java.net.URI;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;

import pl.adroads.api.CampaignAPI;
import pl.adroads.api.model.campaign.CreateCampaignRequest;
import pl.adroads.system.application.campaign.CampaignService;
import pl.adroads.system.domain.campaign.model.Campaign;
import pl.adroads.system.domain.campaign.model.CampaignId;
import pl.adroads.system.domain.driver.model.Email;
import pl.adroads.system.filters.Secured;
import pl.adroads.system.infrastructure.adapters.driven.campaign.integration.rest.assemblers.CampaignAssembler;

@Named
public class CampaignAPIImpl implements CampaignAPI {

    @Inject
    private CampaignService campaignService;

    @Inject
    private CampaignAssembler campaignAssembler;

    @Secured(ADMIN)
    @Override
    public Response createCampaign(final CreateCampaignRequest createCampaignRequest) {
        Campaign campaign = campaignAssembler.assemble(createCampaignRequest);
        CampaignId campaignId = campaignService.createNewCampaign(campaign);
        return Response.created(URI.create(PATH + campaignId.getValue())).build();
    }

    @Secured(ADMIN)
    @Override
    public Response addCampaignDriver(String campaignId, String driverId) {
        campaignService.addPotentialDriver(campaignIdOf(campaignId), of(driverId));
        return Response.ok().build();
    }

    @Secured(ADMIN)
    @Override
    public Response driverApprovesCampaign(final String campaignId, final String email, final String token) {
        campaignService.driverApprovesCampaign(CampaignId.campaignIdOf(campaignId), Email.emailOf(email), token);
        return Response.ok().build();
    }

    @Secured(ADMIN)
    @Override
    public Response driverRejectsCampaign(final String campaignId, final String email, final String token) {
        campaignService.driverRejectsCampaign(CampaignId.campaignIdOf(campaignId), Email.emailOf(email), token);
        return Response.ok().build();
    }

}
