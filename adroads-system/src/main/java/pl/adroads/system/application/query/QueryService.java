package pl.adroads.system.application.query;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.inject.Named;

import pl.adroads.system.domain.campaign.model.CampaignId;
import pl.adroads.system.domain.driver.DriverRepository;
import pl.adroads.system.domain.driver.exception.DriverNotFoundException;
import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.domain.driver.model.DriverId;
import pl.adroads.system.domain.query.CampaignQueryRepository;
import pl.adroads.system.domain.query.DriverQueryRepository;
import pl.adroads.system.domain.query.model.DriverFilterCriteria;
import pl.adroads.system.domain.query.model.Page;
import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model.PersistenceCampaign;
import pl.adroads.system.infrastructure.adapters.driving.query.persistance.model.PersistenceQueryDriver;

// TODO consider removing as it barely delgates
// TODO transactionality ???
// consider removing this service as it only delegates repo...?!
@Named
public class QueryService {

    @Inject
    private DriverQueryRepository driverQueryRepository;

    @Inject
    private DriverRepository driverRepository;

    @Inject
    private CampaignQueryRepository campaignQueryRepository;

    public Page<PersistenceQueryDriver> fetchFirstPage(final DriverFilterCriteria driverFilterCriteria,
            final Integer pageSize) {
        return driverQueryRepository.getFirstPage(driverFilterCriteria, pageSize);
    }

    public Page<PersistenceQueryDriver> fetchSubsequentPage(final DriverFilterCriteria driverFilterCriteria,
            final Integer pageSize, final DriverId driverId) {
        return driverQueryRepository
                .getSubsequentPage(driverFilterCriteria, driverId.toString(), pageSize);
    }

    public List<PersistenceCampaign> getCampaigns() {
        return campaignQueryRepository.getCampaigns();
    }

    public PersistenceCampaign getCampaign(final CampaignId campaignId) {
        return campaignQueryRepository.getCampaign(campaignId);
    }

    public Driver getDriver(final DriverId driverId) {
        Optional<Driver> driver = driverRepository.findDriverById(driverId);
        return driver.orElseThrow(() -> new DriverNotFoundException(driverId));
    }
}
