package pl.adroads.system.application.driver;

import static org.springframework.transaction.annotation.Isolation.REPEATABLE_READ;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.transaction.annotation.Transactional;

import pl.adroads.system.domain.driver.DriverService;
import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.domain.driver.model.DriverId;

@Transactional(isolation = REPEATABLE_READ)
@Named
public class DriverManagementAppService {

    @Inject
    private DriverService driverService;

    public DriverId registerNewDriver(final Driver driver) {
        return driverService.createNewDriver(driver);
    }

    public void activateDriver(final DriverId driverId) {
        driverService.activateDriver(driverId);
    }

    public void assignToCampaign(final DriverId driverId) {
        driverService.assignToCampaign(driverId);
    }

    public void updateDriver(final DriverId driverId, final Driver driver) {
        driverService.updateDriver(driverId, driver);
    }

    // public void driverApprovedCampaign(final DriverId driverId, final CampaignId campaignId) {
    // driverService.approveCampaign(driverId);
    // }

}
