package pl.adroads.system.application.campaign;

import static pl.adroads.system.domain.campaign.model.CampaignId.campaignIdOf;
import static pl.adroads.system.domain.driver.model.Status.REGISTERED;
import static pl.adroads.system.domain.driver.model.Status.TAKEN;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Value;

import pl.adroads.system.application.user.email.EmailService;
import pl.adroads.system.domain.DomainEventPublisher;
import pl.adroads.system.domain.UUIDGenerator;
import pl.adroads.system.domain.campaign.CampaignRepository;
import pl.adroads.system.domain.campaign.events.DriverAddedToCampaignDomainEvent;
import pl.adroads.system.domain.campaign.exception.CampaignNotFoundException;
import pl.adroads.system.domain.campaign.model.Campaign;
import pl.adroads.system.domain.campaign.model.CampaignId;
import pl.adroads.system.domain.driver.DriverRepository;
import pl.adroads.system.domain.driver.exception.DriverAlreadyParticipatesInCampaignException;
import pl.adroads.system.domain.driver.exception.DriverNotFoundException;
import pl.adroads.system.domain.driver.exception.DriverNotFullyRegisteredException;
import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.domain.driver.model.DriverId;
import pl.adroads.system.domain.driver.model.Email;
import pl.adroads.system.domain.user.TokenFactory;
import pl.adroads.system.domain.user.model.VerificationToken;
import pl.adroads.system.infrastructure.locking.LockingTemplate;

@Named
public class CampaignService {

    // TODO its same key as in UserManagemetnServiceImpl. Should I change it?
    private static final String SECRET_FOR_APPROVAL_REJECTION_TOKEN = "7664951E8689862404F2EB92AEF76D725E9ECEC5B515D8CC5A33AF0A4E4C8002";

    @Value("${adroads.system.campaign.approval.or.rejection.email.expiry.in.days}")
    private Integer emailExpiryInDays;

    @Inject
    private UUIDGenerator idGenerator;
    @Inject
    private CampaignRepository campaignRepository;
    @Inject
    private DriverRepository driverRepository;
    // TODO that should be here!!! requires some logic to wrap domain publishers around Spring transactions in a way
    // that
    // it will be possible to publish event from within a domain object and flush publisher at the end of successful
    // transaction!
    @Inject
    private DomainEventPublisher domainEventPublisher;
    @Inject
    private TokenFactory tokenFactory;
    @Inject
    private EmailService emailService;

    private LockingTemplate lockingTemplate = new LockingTemplate();

    public CampaignId createNewCampaign(final Campaign campaign) {
        final UUID uuid = idGenerator.generateId();
        CampaignId campaignId = campaignIdOf(uuid);
        campaign.assignID(campaignId);
        campaignRepository.store(campaign);
        return campaignId;
    }

    public void addPotentialDriver(final CampaignId campaignId, final DriverId driverId) {
        Driver driver = getExistingDriver(driverId);
        driverNotTaken(driver);
        driverFullyRegistered(driver);

        lockingTemplate.performVoidWithLock(campaignRepository, campaignId.getValue().toString(), s -> {
            Campaign campaign = getExistingCampaign(campaignId);

            LocalDateTime now = LocalDateTime.now();
            VerificationToken approvalRejectionToken = tokenFactory.driverApprovesRejectsCampaignTokenOf(
                    concatUuidWithTimestamp(campaign, driver, now),
                    SECRET_FOR_APPROVAL_REJECTION_TOKEN, emailExpiryInDays, now);
            campaign.addPotentialDriver(driver, approvalRejectionToken);

            // TODO email service calls should be triggered asynchronously by events
            emailService.sendEmailToAssignedDriver(driver, campaign);
            campaignRepository.update(campaign);
            domainEventPublisher.publish(new DriverAddedToCampaignDomainEvent(driverId));
        });

    }

    private String concatUuidWithTimestamp(final Campaign campaign, final Driver driver, final LocalDateTime dateTime) {
        return campaign.getUuid().getValue().toString()
                .concat(driver.getUuid().getValue().toString())
                .concat(dateTime.toString());
    }

    private Campaign getExistingCampaign(final CampaignId campaignId) {
        Optional<Campaign> campaignOptional = campaignRepository.get(campaignId);
        return campaignOptional.orElseThrow(() -> new CampaignNotFoundException(campaignId));
    }

    private void driverNotTaken(final Driver driver) {
        if (driver.getStatus().equals(TAKEN)) {
            throw new DriverAlreadyParticipatesInCampaignException(driver.getUuid());
        }
    }

    private void driverFullyRegistered(final Driver driver) {
        if (!driver.getStatus().equals(REGISTERED)) {
            throw new DriverNotFullyRegisteredException(driver.getUuid());
        }
    }

    private Driver getExistingDriver(final DriverId driverId) {
        Optional<Driver> driverOptional = driverRepository.findDriverById(driverId);
        return driverOptional.orElseThrow(() -> new DriverNotFoundException(driverId));
    }

    public void driverApprovesCampaign(final CampaignId campaignId, final Email email, final String token) {
        lockingTemplate.performVoidWithLock(campaignRepository, campaignId.getValue().toString(), s -> {
            Campaign campaign = getExistingCampaign(campaignId);
            campaign.driverApprovesCampaign(email, token);
            campaignRepository.update(campaign);
        });
    }

    public void driverRejectsCampaign(final CampaignId campaignId, final Email email, final String token) {
        lockingTemplate.performVoidWithLock(campaignRepository, campaignId.getValue().toString(), s -> {
            Campaign campaign = getExistingCampaign(campaignId);
            campaign.driverRejectsCampaign(email, token);
            campaignRepository.update(campaign);
        });
    }
}
