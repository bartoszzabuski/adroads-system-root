package pl.adroads.system.application.user.exception;

import static pl.adroads.system.application.ApplicationLayerError.SESSION_TOKEN_NOT_FOUND;

import pl.adroads.system.common.util.SystemException;

public class SessionTokenNotFoundException extends SystemException {

    public SessionTokenNotFoundException() {
        super(SystemException.builder(SESSION_TOKEN_NOT_FOUND));
    }

}
