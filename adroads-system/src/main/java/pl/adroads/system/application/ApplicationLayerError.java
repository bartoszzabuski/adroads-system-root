package pl.adroads.system.application;

import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_UNAUTHORIZED;

import pl.adroads.system.common.util.errors.SystemError;

public enum ApplicationLayerError implements SystemError {

    USER_NOT_FOUND(SC_NOT_FOUND),
    SESSION_TOKEN_NOT_FOUND(SC_UNAUTHORIZED),
    INVALID_SESSION_TOKEN(SC_UNAUTHORIZED);

    private final int code;

    private ApplicationLayerError(final int code) {
        this.code = code;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getType() {
        return ApplicationLayerError.class.getSimpleName();
    }

}
