package pl.adroads.system.application.user.exception;

import static pl.adroads.system.application.ApplicationLayerError.USER_NOT_FOUND;

import pl.adroads.system.common.util.SystemException;
import pl.adroads.system.domain.driver.model.Email;

public class UserNotFoundException extends SystemException {

    public UserNotFoundException(final Email userEmail) {
        super(SystemException.builder(USER_NOT_FOUND).withParams(userEmail.getValue()));
    }

    public UserNotFoundException() {
        super(SystemException.builder(USER_NOT_FOUND));
    }

}
