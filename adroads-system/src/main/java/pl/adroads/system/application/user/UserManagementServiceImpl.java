package pl.adroads.system.application.user;

import static org.springframework.transaction.annotation.Isolation.REPEATABLE_READ;

import java.net.URI;
import java.time.LocalDateTime;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.UriBuilder;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import pl.adroads.system.application.user.email.EmailService;
import pl.adroads.system.application.user.exception.InvalidSessionTokenException;
import pl.adroads.system.application.user.exception.SessionTokenNotFoundException;
import pl.adroads.system.application.user.exception.UserNotFoundException;
import pl.adroads.system.common.util.Validate;
import pl.adroads.system.domain.DomainEventPublisher;
import pl.adroads.system.domain.driver.model.Email;
import pl.adroads.system.domain.user.TokenFactory;
import pl.adroads.system.domain.user.UserRepository;
import pl.adroads.system.domain.user.events.UserActivatedDomainEvent;
import pl.adroads.system.domain.user.exception.InvalidLoginException;
import pl.adroads.system.domain.user.model.SessionToken;
import pl.adroads.system.domain.user.model.User;
import pl.adroads.system.domain.user.model.VerificationToken;

// TODO consider adding locking if more methods than 'register' and 'verifyToken' will be added
@Transactional(isolation = REPEATABLE_READ)
@Named
public class UserManagementServiceImpl implements UserManagementService {

    @Value("${adroads.system.user.verification.email.expiry.in.days}")
    private int verificationTokenExpiryInDays;
    @Value("${adroads.system.user.verification.email.verification.web.page.url}")
    private String verificationWebsiteUrl;
    @Value("${adroads.system.user.verification.email.verification.endpoint.url}")
    private String verificationEndpointUrl;
    @Value("${adroads.system.user.session.timeout.in.minutes}")
    private Integer sessionTokenExpiryInMinutes;

    private static final String SECRET_FOR_REGISTRATION_CONFIRMATION_TOKEN = "7664951E8689862404F2EB92AEF76D725E9ECEC5B515D8CC5A33AF0A4E4C8002";
    private static final String SECRET_FOR_SESSION_TOKEN = "71AE70FA7DC3F6FD1186B0F1091628C06C57DAE2963C9AD36DDB275ABBD4BED5";

    // TODO extract to file
    private static final String TOKEN_PARAM_NAME = "token";
    private static final String URL_PARAM_NAME = "url";
    private static final String EMAIL_PARAM_NAME = "email";
    @Inject
    private TokenFactory tokenFactory;
    @Inject
    private UserRepository userStore;
    @Inject
    private EmailService emailService;

    // TODO that should be here!!! requires some logic to wrap domain publishers around Spring transactions in a way
    // that
    // it will be possible to publish event from within a domain object and flush publisher at the end of successful
    // transaction!
    @Inject
    private DomainEventPublisher domainEventPublisher;

    public void register(final User user) {
        LocalDateTime now = LocalDateTime.now();
        VerificationToken verificationToken = tokenFactory
                .newUserverificationTokenOf(concatUuidWithTimestamp(user, now),
                        SECRET_FOR_REGISTRATION_CONFIRMATION_TOKEN,
                        verificationTokenExpiryInDays, now);
        user.assignVerificationToken(verificationToken);
        userStore.store(user);

        // TODO consider publishing async event ?
        URI verificationUri = UriBuilder.fromPath(verificationWebsiteUrl)
                .queryParam(TOKEN_PARAM_NAME, verificationToken.getToken())
                .queryParam(EMAIL_PARAM_NAME, user.getEmail().getEmailValue())
                .queryParam(URL_PARAM_NAME, verificationEndpointUrl)
                .build();
        emailService.sendEmailConfirmationTo(user, verificationUri);
    }

    @Override
    public void verifyToken(final Email userEmail, final String token) {
        User user = getExistingUser(userEmail);
        user.verifyAgainst(token);
        userStore.update(user);
        domainEventPublisher.publish(new UserActivatedDomainEvent(user.getUuid()));
    }

    @Override
    public User verifySessionToken(final String sessionTokenValue) {
        User user = userStore.findBySessionToken(sessionTokenValue).orElseThrow(SessionTokenNotFoundException::new);
        if (isSessionTokenValid(sessionTokenValue, user)) {
            throw new InvalidSessionTokenException();
        }
        return user;
    }

    private boolean isSessionTokenValid(final String sessionTokenValue, final User user) {
        return !user.getSessionToken().get().verifyAgainst(sessionTokenValue);
    }

    @Override
    public SessionToken authenticate(final Email email, final String password) {
        Validate.notNull(password);

        User user = userStore.findByEmail(email).orElseThrow(() -> new InvalidLoginException(email));
        user.validatePassword(password);

        final LocalDateTime now = LocalDateTime.now();
        SessionToken sessionToken = tokenFactory.sessionTokenOf(concatUuidWithTimestamp(user, now),
                SECRET_FOR_SESSION_TOKEN, sessionTokenExpiryInMinutes, now);
        user.assignSessionToken(sessionToken);
        userStore.update(user);
        return sessionToken;
    }

    private User getExistingUser(final Email userEmail) {
        return userStore.findByEmail(userEmail).orElseThrow(() -> new UserNotFoundException(userEmail));
    }

    private String concatUuidWithTimestamp(final User user, final LocalDateTime now) {
        return user.getUuid().getValue().toString().concat(now.toString());
    }

}
