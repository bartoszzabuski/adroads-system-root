package pl.adroads.system.application.user.email;

import java.net.URI;

import javax.inject.Inject;
import javax.inject.Named;

import lombok.extern.slf4j.Slf4j;

import org.springframework.context.annotation.Profile;

import pl.adroads.system.domain.campaign.model.Campaign;
import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.domain.user.model.User;
import pl.adroads.system.infrastructure.adapters.shared.ObjectSerializer;

@Profile("dev")
@Named
@Slf4j
public class LogEmailService implements EmailService {

    @Inject
    private ObjectSerializer objectSerializer;

    @Override
    public void sendEmailConfirmationTo(final User user, final URI verificationUri) {
        String userString = objectSerializer.serialize(user);
        log.info(
                "============ Email sent to : " + userString + "(" + verificationUri + ") ============ ");
    }

    @Override
    public void sendEmailToAssignedDriver(Driver driver, Campaign campaign) {
        String driverString = objectSerializer.serialize(driver);
        String campaignString = objectSerializer.serialize(campaign);
        log.info("============ Email about assigned driverv sent - " + driverString + " / " + campaignString
                + " ============ ");
    }

}
