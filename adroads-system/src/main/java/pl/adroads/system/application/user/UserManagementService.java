package pl.adroads.system.application.user;

import pl.adroads.system.domain.driver.model.Email;
import pl.adroads.system.domain.user.model.SessionToken;
import pl.adroads.system.domain.user.model.User;

public interface UserManagementService {

    void register(User user);

    // TODO rename to verifyVerificationToken
    void verifyToken(Email userEmail, String token);

    User verifySessionToken(String sessionTokenValue);

    SessionToken authenticate(Email email, String password);
}
