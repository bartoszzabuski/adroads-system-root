package pl.adroads.system.application.user.email;

import java.net.URI;

import pl.adroads.system.domain.campaign.model.Campaign;
import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.domain.user.model.User;

public interface EmailService {

    void sendEmailConfirmationTo(final User user, final URI verificationUri);

    void sendEmailToAssignedDriver(final Driver driver, final Campaign campaign);

}
