package pl.adroads.system.application.user.exception;

import static pl.adroads.system.application.ApplicationLayerError.INVALID_SESSION_TOKEN;

import pl.adroads.system.common.util.SystemException;

public class InvalidSessionTokenException extends SystemException {

    public InvalidSessionTokenException() {
        super(SystemException.builder(INVALID_SESSION_TOKEN));
    }

}
