package pl.adroads.system.application.user.email;

import java.net.URI;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.UriBuilder;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;

import pl.adroads.system.domain.campaign.model.Campaign;
import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.domain.user.model.User;
import pl.adroads.system.infrastructure.adapters.shared.email.MandrillFacade;

@Profile("prod")
@Named
public class MandrillEmailService implements EmailService {

    @Inject
    private MandrillFacade mandrillFacade;
    @Value("${adroads.system.campaign.approval.endpoint.url}")
    private String campaignApprovalEndpointUrl;

    @Value("${adroads.system.campaign.rejection.endpoint.url}")
    private String campaignRejectionEndpointUrl;

    private static final String TOKEN_PARAM_NAME = "token";
    private static final String CAMPAIGN_ID_PARAM_NAME = "campaignId";
    private static final String EMAIL_PARAM_NAME = "email";

    @Override
    public void sendEmailConfirmationTo(final User user, final URI verificationUri) {
        mandrillFacade
                .sendNewUserActivationEmail(user.getEmail().getEmailValue(), verificationUri.toString());
    }

    @Override
    public void sendEmailToAssignedDriver(final Driver driver, final Campaign campaign) {

        // FIXME temporarily hardcoded value to send to me
        // final String email = driver.getEmail().getValue();
        final String email = "bartek.zabuski@gmail.com";

        URI approvalUrl = UriBuilder.fromPath(campaignApprovalEndpointUrl)
                .queryParam(TOKEN_PARAM_NAME,
                        campaign.getDriverFor(driver.getUuid()).get().getApprovalRejectionToken().getToken())
                .queryParam(EMAIL_PARAM_NAME, driver.getEmail().getEmailValue())
                .queryParam(CAMPAIGN_ID_PARAM_NAME, campaign.getUuid().getValue()).build();

        URI rejectionUrl = UriBuilder.fromPath(campaignRejectionEndpointUrl)
                .queryParam(TOKEN_PARAM_NAME,
                        campaign.getDriverFor(driver.getUuid()).get().getApprovalRejectionToken().getToken())
                .queryParam(EMAIL_PARAM_NAME, driver.getEmail().getEmailValue())
                .queryParam(CAMPAIGN_ID_PARAM_NAME, campaign.getUuid().getValue()).build();

        mandrillFacade.sendDriverAssignedToCampaignEmail(email, approvalUrl.toString(),
                rejectionUrl.toString(), driver.getName(), campaign.getStartDateTime().toString(),
                campaign.getEndDateTime().toString());
    }

}
