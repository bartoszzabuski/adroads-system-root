package pl.adroads.system.common.domain;

public abstract class Aggregate<T> {

  protected T uuid;

  public T getUuid() {
    return uuid;
  }

  public void assignID(T uuid) {
    this.uuid = uuid;
  }

}
