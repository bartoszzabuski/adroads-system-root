package pl.adroads.system.common.util;


import pl.adroads.system.common.util.errors.CommonError;
import pl.adroads.system.common.util.errors.SystemError;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
public class SystemException extends RuntimeException {

  private final SystemError systemError;
  private final Object[] params;

  public SystemException(final Builder builder) {
    super(builder.cause);
    this.systemError = builder.systemError;
    this.params = builder.params;
  }

  protected SystemException(final SystemError systemError, final Throwable cause,
      final Object... params) {
    super(cause);
    this.systemError = systemError;
    this.params = params;
  }

  public static Builder builder(final SystemError systemError) {
    return new Builder(systemError);
  }

  @Slf4j
  public static final class Builder {

    private static final SystemError DEFAULT_ERROR = CommonError.INTERNAL_ERROR;
    private static final Object[] EMPTY_ARRAY = new Object[0];

    private final SystemError systemError;
    private Object[] params;
    private Throwable cause;

    private Builder(final SystemError systemError) {
      this.systemError = defaultIfNull(systemError);
      this.params = EMPTY_ARRAY;
    }

    public Builder withParams(final Object... params) {
      if (params == null) {
        log.warn("Undefined message parameters, defaulting to empty");
        this.params = EMPTY_ARRAY;
      } else {
        this.params = params;
      }
      return this;
    }


    public Builder withCause(final Throwable cause) {
      this.cause = cause;
      return this;
    }

    public SystemException build() {
      return new SystemException(this);
    }

    private SystemError defaultIfNull(final SystemError systemError) {
      if (systemError == null) {
        log.warn("System error undefined, defaulting to SystemError {}", DEFAULT_ERROR);
        return DEFAULT_ERROR;
      } else {
        return systemError;
      }
    }
  }

}
