package pl.adroads.system.common.util.errors;


import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR;

public enum ValidationError implements SystemError {

  DEFAULT(SC_INTERNAL_SERVER_ERROR),
  VALUE_REQUIRED(SC_BAD_REQUEST),
  VALUE_EMPTY(SC_BAD_REQUEST),
  VALUE_NOT_VALID(SC_BAD_REQUEST);

  private final int code;

  private ValidationError(final int code) {
    this.code = code;
  }

  @Override
  public Integer getCode() {
    return code;
  }

  @Override
  public String getType() {
    return ValidationError.class.getSimpleName();
  }
}