package pl.adroads.system.common.util;


import static pl.adroads.system.common.util.errors.ValidationError.DEFAULT;
import static pl.adroads.system.common.util.errors.ValidationError.VALUE_NOT_VALID;
import static pl.adroads.system.common.util.errors.ValidationError.VALUE_REQUIRED;

public class Validate {

  public static <T> T notNull(final T object, final ValidationFields validationFields) {
    if (object == null) {
      throw ValidationException.builder(VALUE_REQUIRED).withField(validationFields).build();
    } else {
      return object;
    }
  }

  public static void isTrue(final boolean expression, final ValidationFields validationFields) {
    if (!expression) {
      throw ValidationException.builder(VALUE_NOT_VALID).withField(validationFields).build();
    }
  }

  // TODO should that be depracated?
  @Deprecated
  public static <T> T notNull(final T object) {
    if (object == null) {
      throw ValidationException.builder(DEFAULT).build();
    }
    return object;
  }

  // TODO should that be depracated?
  @Deprecated
  public static <T> T notNull(final T object, final String msg) {
    if (object == null) {
      throw ValidationException.builder(DEFAULT).withMessage(msg).build();
    }
    return object;
  }

}