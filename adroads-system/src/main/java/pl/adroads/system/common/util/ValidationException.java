package pl.adroads.system.common.util;

import pl.adroads.system.common.util.errors.CommonError;
import pl.adroads.system.common.util.errors.SystemError;
import pl.adroads.system.common.util.errors.ValidationError;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
public class ValidationException extends SystemException {

  private final ValidationFields validationField;
  private final String message;

  public ValidationException(final Builder builder) {
    super(builder.validationError, builder.cause, builder.params);
    this.validationField = builder.validationField;
    this.message = builder.message;
  }

  public static Builder builder(final ValidationError validationError) {
    return new Builder(validationError);
  }

  @Slf4j
  public static final class Builder {

    private static final SystemError DEFAULT_ERROR = CommonError.INTERNAL_ERROR;

    private final ValidationError validationError;
    private ValidationFields validationField;
    private Throwable cause;
    private Object[] params;
    private String message;

    private Builder(final ValidationError validationError) {
      this.validationError = defaultIfNull(validationError);
    }

    public Builder withField(final ValidationFields field) {
      this.validationField = field;
      return this;
    }

    public Builder withCause(final Throwable cause) {
      this.cause = cause;
      return this;
    }

    public Builder withParams(final Object... params) {
      this.params = params;
      return this;
    }

    public Builder withMessage(final String message) {
      this.message = message;
      return this;
    }

    public ValidationException build() {
      return new ValidationException(this);
    }

    private ValidationError defaultIfNull(final ValidationError validationError) {
      if (validationError == null) {
        log.warn("System error undefined, defaulting to SystemError {}", DEFAULT_ERROR);
        return ValidationError.DEFAULT;
      } else {
        return validationError;
      }
    }
  }

}
