package pl.adroads.system.common.util.errors;

import static org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR;

public enum CommonError implements SystemError {

  INTERNAL_ERROR(SC_INTERNAL_SERVER_ERROR);

  private int statusCode;

  private CommonError(final int statusCode) {
    this.statusCode = statusCode;
  }

  @Override
  public Integer getCode() {
    return statusCode;
  }

  @Override
  public String getType() {
    return CommonError.class.getSimpleName();
  }
}
