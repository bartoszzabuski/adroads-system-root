package pl.adroads.system.common.util;


public abstract class GlobalConstants {

  private GlobalConstants(){}

  public static final String CONTENT_TYPE_APPLICATION_JSON = "application/json";
  public static final String JSON_TYPE = "jsonb";


}
