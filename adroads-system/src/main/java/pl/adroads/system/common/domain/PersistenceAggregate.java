package pl.adroads.system.common.domain;

public abstract class PersistenceAggregate<T> implements Persistence {

  protected T uuid;

  public T getUuid() {
    return uuid;
  }

  public void setUuid(T uuid) {
    this.uuid = uuid;
  }

}
