package pl.adroads.system.common.domain;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public abstract class Id<T> implements ValueObject {

  private final T value;

  public Id(T value) {
    this.value = value;
  }

  public T getValue() {
    return value;
  }

  public String toString() {
    return value.toString();
  }
}
