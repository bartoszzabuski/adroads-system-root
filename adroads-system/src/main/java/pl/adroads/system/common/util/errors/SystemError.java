package pl.adroads.system.common.util.errors;

public interface SystemError {

    Integer getCode();

    String getType();

}
