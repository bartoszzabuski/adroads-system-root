package pl.adroads.system.domain.driver;

import java.util.Optional;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Named;

import pl.adroads.system.domain.DomainEventPublisher;
import pl.adroads.system.domain.UUIDGenerator;
import pl.adroads.system.domain.driver.events.DriverCreatedEvent;
import pl.adroads.system.domain.driver.events.DriverFullyRegisteredDomainEvent;
import pl.adroads.system.domain.driver.exception.DriverNotFoundException;
import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.domain.driver.model.DriverId;
import pl.adroads.system.infrastructure.locking.LockingTemplate;

@Named
public class DriverDomainService implements DriverService {

    @Inject
    private DriverRepository driverRepository;
    @Inject
    private UUIDGenerator idGenerator;
    @Inject
    private DomainEventPublisher domainEventPublisher;

    private final LockingTemplate lockingTemplate = new LockingTemplate();

    @Override
    public DriverId createNewDriver(final Driver driver) {
        UUID uuid = idGenerator.generateId();
        DriverId driverId = DriverId.of(uuid);
        driver.assignID(driverId);
        driverRepository.storeDriver(driver);
        // publish domain event?!
        domainEventPublisher.publish(new DriverCreatedEvent(driver));
        return driverId;
    }

    // TODO dont think its in use anymore. check and remove!
    @Override
    public void approveCampaign(final DriverId driverId) {
        lockingTemplate.performVoidWithLock(driverRepository, driverId.getValue().toString(), s -> {
            Driver driver = getExistingDriver(driverId);
            driver.approveCampaign();
            driverRepository.updateDriver(driver);
        });
    }

    @Override
    public void activateDriver(final DriverId driverId) {
        lockingTemplate.performVoidWithLock(driverRepository, driverId.getValue().toString(), s -> {

            Driver driver = getExistingDriver(driverId);
            driver.activate();
            driverRepository.updateDriver(driver);

            domainEventPublisher.publish(new DriverFullyRegisteredDomainEvent(driver));
        });
    }

    @Override
    public void assignToCampaign(final DriverId driverId) {
        lockingTemplate.performVoidWithLock(driverRepository, driverId.getValue().toString(), s -> {

            Driver driver = getExistingDriver(driverId);
            driver.markAsTaken();
            driverRepository.updateDriver(driver);
        });
    }

    @Override
    public void updateDriver(final DriverId driverId, final Driver updatedDriver) {
        lockingTemplate.performVoidWithLock(driverRepository, driverId.getValue().toString(), s -> {

            Driver driver = getExistingDriver(driverId);

            Driver finalDriver = new Driver.DriverBuilder().withID(driver.getUuid())
                    .withName(updatedDriver.getName())
                    .withSurname(updatedDriver.getSurname())
                    .withDateOfBirth(updatedDriver.getDateOfBirth())
                    .withEmail(updatedDriver.getEmail())
                    .withAddress(updatedDriver.getAddress())
                    .withStatus(driver.getStatus())
                    .withCarDetails(updatedDriver.getCarDetails())
                    .withDriverDetails(updatedDriver.getDriverDetails()).build();

            driverRepository.updateDriver(finalDriver);

        });
    }

    // TODO duplication driver and report context
    private Driver getExistingDriver(final DriverId driverId) {
        Optional<Driver> driverOptional = driverRepository.findDriverById(driverId);
        return driverOptional.orElseThrow(() -> new DriverNotFoundException(driverId));
    }

}
