package pl.adroads.system.domain.driver.model;


import pl.adroads.system.common.util.ValidationException;

import java.util.Arrays;
import java.util.Optional;

import static pl.adroads.system.common.util.errors.ValidationError.VALUE_NOT_VALID;
import static pl.adroads.system.domain.driver.exception.DriverValidationFields.CAR_BODY;

public enum PolishCarBody {
  HATCHBACK,
  KABRIOLET,
  KOMBI,
  MPV,
  PICKUP,
  SEDAN,
  LIMUZYNA,
  COUPE,
  SUV,
  TERENOWY,
  VAN;

  @Override
  public String toString() {
    return this.name().toLowerCase();
  }

  public static PolishCarBody fromString(final String body) {
    Optional<PolishCarBody> polishCarBody = Arrays.stream(PolishCarBody.values())
	.filter(bodyEnum -> body.toUpperCase().equals(bodyEnum.name())).findFirst();
    return polishCarBody.orElseThrow(
	() -> ValidationException.builder(VALUE_NOT_VALID).withField(CAR_BODY).build());
  }

}
