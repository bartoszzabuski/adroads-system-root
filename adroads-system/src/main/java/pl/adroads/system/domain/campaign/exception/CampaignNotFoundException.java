package pl.adroads.system.domain.campaign.exception;


import pl.adroads.system.common.util.SystemException;
import pl.adroads.system.domain.campaign.model.CampaignId;
import pl.adroads.system.domain.driver.exception.CampaignDomainError;
import pl.adroads.system.domain.driver.model.DriverId;

import static pl.adroads.system.domain.driver.exception.CampaignDomainError.*;

public class CampaignNotFoundException extends SystemException {

  public CampaignNotFoundException(final CampaignId campaignId) {
    super(SystemException.builder(CAMPAIGN_NOT_FOUND).withParams(campaignId.getValue()));
  }

}
