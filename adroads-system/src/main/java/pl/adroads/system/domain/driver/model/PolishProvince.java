package pl.adroads.system.domain.driver.model;


import pl.adroads.system.common.util.ValidationException;

import static pl.adroads.system.domain.driver.exception.DriverValidationFields.PROVINCE;
import static pl.adroads.system.common.util.Validate.notNull;
import static pl.adroads.system.common.util.errors.ValidationError.VALUE_NOT_VALID;

public enum PolishProvince {

//  TODO change to capital letters and refactor registration, web-database
  DOLNOŚLĄSKIE("dolnośląskie"),
  KUJAWSKO_POMORSKIE("kujawsko-pomorskie"),
  LUBELSKIE("lubelskie"),
  LUBUSKIE("lubuskie"),
  ŁÓDZKIE("łódzkie"),
  MAŁOPOLSKIE("małopolskie"),
  MAZOWIECKIE("mazowieckie"),
  OPOLSKIE("opolskie"),
  PODKARPACKIE("podkarpackie"),
  PODLASKIE("podlaskie"),
  POMORSKIE("pomorskie"),
  ŚLĄSKIE("śląskie"),
  ŚWIĘTOKRZYSKIE("świętokrzyskie"),
  WARMIŃSKO_MAZURSKIE("warmińsko-mazurskie"),
  WIELKOPOLSKIE("wielkopolskie"),
  ZACHODNIOPOMORSKIE("zachodniopomorskie");

  private String value;

  private PolishProvince(String str) {
    this.value = str.toLowerCase();
  }

  @Override
  public String toString() {
    return value.toLowerCase();
  }

  public static PolishProvince fromString(final String provinceStr) {
    notNull(provinceStr, PROVINCE);
    for (PolishProvince province : PolishProvince.values()) {
      if (provinceStr.equalsIgnoreCase(province.value)) {
        return province;
      }
    }
    throw ValidationException.builder(VALUE_NOT_VALID).withField(PROVINCE).build();
  }

}
