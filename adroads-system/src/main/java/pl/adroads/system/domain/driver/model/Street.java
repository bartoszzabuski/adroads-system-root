package pl.adroads.system.domain.driver.model;


import pl.adroads.system.common.domain.ValueObject;

import java.util.regex.Pattern;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import static java.util.regex.Pattern.compile;
import static pl.adroads.system.domain.driver.exception.DriverValidationFields.STREET;
import static pl.adroads.system.common.util.Validate.isTrue;

@ToString
@Getter
@EqualsAndHashCode(of = "value")
public final class Street implements ValueObject {

  private static final String STREET_REGEX = "^[a-zA-Z0-9ĄĘŚĆŻŹŁÓĆŃąęśćżźłóćń/ .-]{3,}$";
  private static final Pattern PATTERN = compile(STREET_REGEX);

  private final String value;

  private Street(final String street) {
    isTrue(isValid(street), STREET);

    this.value = street;
  }

  public static Street streetOf(final String street) {
    return new Street(street);
  }

  private boolean isValid(final String street) {
    return (street != null) && PATTERN.matcher(street).matches();
  }

  public String getValue() {
    return value;
  }

}
