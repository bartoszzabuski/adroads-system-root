package pl.adroads.system.domain.driver.exception;

import pl.adroads.system.common.util.SystemException;
import pl.adroads.system.domain.driver.model.DriverId;

import static pl.adroads.system.domain.driver.exception.DriverDomainError.DRIVER_ALREADY_TAKEN;

public class DriverAlreadyParticipatesInCampaignException extends SystemException {

  public DriverAlreadyParticipatesInCampaignException(final DriverId driverId) {
    super(SystemException.builder(DRIVER_ALREADY_TAKEN).withParams(driverId.getValue()));
  }

}
