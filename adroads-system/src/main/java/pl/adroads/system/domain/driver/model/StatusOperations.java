package pl.adroads.system.domain.driver.model;

public interface StatusOperations {

  Status markAsRegistered(DriverId driverId);

  Status markAsTaken(DriverId driverId);

}
