package pl.adroads.system.domain.query.model;

import java.util.List;
import java.util.Optional;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import pl.adroads.system.infrastructure.adapters.shared.postgre.PageMetadata;

@Getter
@RequiredArgsConstructor
public class Page<T> {

    private final Optional<List<PageMetadata>> metadata;
    private final List<T> drivers;

}
