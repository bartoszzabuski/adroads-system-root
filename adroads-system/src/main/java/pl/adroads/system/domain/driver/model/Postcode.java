package pl.adroads.system.domain.driver.model;

import pl.adroads.system.common.domain.ValueObject;

import java.util.regex.Pattern;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import static java.util.regex.Pattern.compile;
import static pl.adroads.system.domain.driver.exception.DriverValidationFields.POSTCODE;
import static pl.adroads.system.common.util.Validate.isTrue;

@ToString
@Getter
@EqualsAndHashCode
public final class Postcode implements ValueObject {

  private static final String POSTCODE_REGEX = "^[0-9]{2}-[0-9]{3}$";
  private static final Pattern PATTERN = compile(POSTCODE_REGEX);

  private String value;

  private Postcode(final String postcode) {
    isTrue(isValid(postcode), POSTCODE);

    this.value = postcode;
  }

  public static Postcode postcodeOf(final String postcode) {
    return new Postcode(postcode);
  }

  private boolean isValid(final String postcode) {
    return (postcode != null) && PATTERN.matcher(postcode).matches();
  }
}
