package pl.adroads.system.domain.driver.model;

import pl.adroads.system.common.domain.ValueObject;

import java.util.regex.Pattern;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import static java.util.regex.Pattern.compile;
import static pl.adroads.system.common.util.Validate.isTrue;
import static pl.adroads.system.domain.CommonValidationFields.EMAIL;
import static pl.adroads.system.domain.DomainConstants.EMAIL_REGEX;

@Getter
@EqualsAndHashCode
public final class Email implements ValueObject {


  private static final Pattern PATTERN = compile(EMAIL_REGEX);

  private final String emailValue;

  private Email(final String email) {
    isTrue(isValid(email), EMAIL);

    this.emailValue = email;
  }

  public static Email emailOf(final String email) {
    return new Email(email);
  }

  private boolean isValid(final String email) {
    return (email != null) && PATTERN.matcher(email).matches();
  }

  public String getValue() {
    return emailValue;
  }

}
