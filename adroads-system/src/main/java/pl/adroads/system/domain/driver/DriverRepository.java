package pl.adroads.system.domain.driver;

import java.util.Optional;

import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.domain.driver.model.DriverId;
import pl.adroads.system.infrastructure.locking.Lockable;

public interface DriverRepository extends Lockable<String> {

    Optional<Driver> findDriverByEmail(String email);

    void storeDriver(Driver driver);

    void updateDriver(Driver driver);

    Optional<Driver> findDriverById(DriverId driverId);

}
