package pl.adroads.system.domain.campaign.campaigndriverstatus;

import pl.adroads.system.domain.campaign.model.CampaignId;
import pl.adroads.system.domain.driver.model.DriverId;

import static pl.adroads.system.domain.campaign.campaigndriverstatus.CampaignDriverStatus.DRIVER_ACCEPTED_CAMPAIGN;
import static pl.adroads.system.domain.campaign.campaigndriverstatus.CampaignDriverStatus.DRIVER_APPROVED_BY_ADMIN;
import static pl.adroads.system.domain.campaign.campaigndriverstatus.CampaignDriverStatus.DRIVER_REJECTED_BY_ADMIN;
import static pl.adroads.system.domain.campaign.campaigndriverstatus.CampaignDriverStatus.DRIVER_REJECTED_CAMPAIGN;

public class DriverApprovalPendingStatusOperations implements CampaignDriverStatusOperations {

  @Override
  public CampaignDriverStatus driverAccepts(CampaignId campaignId, DriverId driverId) {
    return DRIVER_ACCEPTED_CAMPAIGN;
  }

  @Override
  public CampaignDriverStatus driverRejects(CampaignId campaignId, DriverId driverId) {
    return DRIVER_REJECTED_CAMPAIGN;
  }

  @Override
  public CampaignDriverStatus adminApproves(CampaignId campaignId, DriverId driverId) {
    return DRIVER_APPROVED_BY_ADMIN;
  }

  @Override
  public CampaignDriverStatus adminRejects(CampaignId campaignId, DriverId driverId) {
    return DRIVER_REJECTED_BY_ADMIN;
  }

}
