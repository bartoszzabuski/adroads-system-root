package pl.adroads.system.domain.user.model;

import pl.adroads.system.common.domain.Id;
import pl.adroads.system.common.util.Validate;

import java.util.UUID;

public final class UserId extends Id<UUID> {

  private UserId(UUID value) {
    super(value);
  }

  public static UserId userIdOf(final String uuid) {
    Validate.notNull(uuid, "UserId cannot be null");
    return new UserId(UUID.fromString(uuid));
  }

  public static UserId userIdOf(final UUID uuid) {
    Validate.notNull(uuid, "UserId cannot be null");
    return new UserId(uuid);
  }

}
