package pl.adroads.system.domain.user.exception;

import pl.adroads.system.common.util.SystemException;
import pl.adroads.system.domain.driver.model.Email;


public class VerificationTokenNotValidException extends SystemException {

  public VerificationTokenNotValidException(Email userEmail) {
    super(SystemException.builder(UserDomainError.VERIFICATION_TOKEN_NOT_VALID)
	      .withParams(userEmail.getValue()));
  }
}
