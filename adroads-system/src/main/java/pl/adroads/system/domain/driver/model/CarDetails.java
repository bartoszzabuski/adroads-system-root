package pl.adroads.system.domain.driver.model;


import pl.adroads.system.common.domain.ValueObject;

import java.time.Year;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import static pl.adroads.system.domain.driver.exception.DriverValidationFields.CAR_BODY;
import static pl.adroads.system.domain.driver.exception.DriverValidationFields.CAR_YEAR;
import static pl.adroads.system.domain.driver.exception.DriverValidationFields.COLOUR;
import static pl.adroads.system.domain.driver.exception.DriverValidationFields.MAKE;
import static pl.adroads.system.domain.driver.exception.DriverValidationFields.MILEAGE;
import static pl.adroads.system.domain.driver.exception.DriverValidationFields.MODEL;
import static pl.adroads.system.domain.driver.exception.DriverValidationFields.WINDSCREEN_DIMENSIONS;
import static pl.adroads.system.common.util.Validate.isTrue;
import static pl.adroads.system.common.util.Validate.notNull;
import static pl.adroads.system.domain.driver.model.DistanceMeasurement.distanceMeasurementOf;


@EqualsAndHashCode
@Getter
public class CarDetails implements ValueObject {

  private static final Year YEAR_OF_THE_INVENTION_OF_CAR = Year.of(1886);

  //  TODO should I create Model and Make objects???
  private String make;
  private String model;
  private Year year;
  private DistanceMeasurement mileage;
  private PolishCarBody body;
  private Colour colour;
  private WindscreenDimensions windscreenDimensions;

  private CarDetails(final String make, final String model, final Year year, final Integer mileage,
      final PolishCarBody body, final Colour colour,
      final WindscreenDimensions windscreenDimensions) {

    notNull(make, MAKE);
    notNull(model, MODEL);
    notNull(mileage, MILEAGE);
    notNull(year, CAR_YEAR);
    notNull(body, CAR_BODY);
    notNull(colour, COLOUR);
    notNull(windscreenDimensions, WINDSCREEN_DIMENSIONS);
    isTrue(year.isAfter(YEAR_OF_THE_INVENTION_OF_CAR), CAR_YEAR);
    isTrue(mileage > 0, MILEAGE);

    this.make = make;
    this.model = model;
    this.year = year;
    this.mileage = distanceMeasurementOf(mileage, UnitsOfMeasurement.KM);
    this.body = body;
    this.colour = colour;
    this.windscreenDimensions = windscreenDimensions;
  }

  public static CarDetails of(final String make, final String model, final Year year,
      final Integer mileage, final PolishCarBody body, final Colour colour,
      final WindscreenDimensions windscreenDimensions) {
    return new CarDetails(make, model, year, mileage, body, colour, windscreenDimensions);
  }

}

