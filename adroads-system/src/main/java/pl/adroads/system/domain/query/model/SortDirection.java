package pl.adroads.system.domain.query.model;

public enum SortDirection {
  ASC, DESC
}
