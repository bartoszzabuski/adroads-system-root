package pl.adroads.system.domain.user;

import static pl.adroads.system.domain.user.model.VerificationToken.VerificationTokenType.DRIVER_ACCEPTS_REJECTS_CAMPAIGN;
import static pl.adroads.system.domain.user.model.VerificationToken.VerificationTokenType.REGISTRATION_CONFIRMATION;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.inject.Named;

import lombok.extern.slf4j.Slf4j;
import pl.adroads.system.domain.user.model.SessionToken;
import pl.adroads.system.domain.user.model.VerificationToken;

@Named
@Slf4j
public class TokenFactory {

    private static final String HMAC_SHA256 = "HmacSHA256";

    public SessionToken sessionTokenOf(final String data, final String secretKey,
            final int expiryTimeInMinutes, final LocalDateTime timeStamp) {
        String token = hmacDigest(data, secretKey, HMAC_SHA256);
        LocalDateTime expiryDateTime = timeStamp.plusMinutes(expiryTimeInMinutes);
        return new SessionToken(token, timeStamp, expiryDateTime);
    }

    public VerificationToken newUserverificationTokenOf(final String data, final String secretKey,
            final int expiryTimeInDays, final LocalDateTime timeStamp) {
        return getVerificationToken(data, secretKey, expiryTimeInDays, timeStamp, REGISTRATION_CONFIRMATION);
    }

    public VerificationToken driverApprovesRejectsCampaignTokenOf(final String data, final String secretKey,
            final int expiryTimeInDays, final LocalDateTime timeStamp) {
        return getVerificationToken(data, secretKey, expiryTimeInDays, timeStamp, DRIVER_ACCEPTS_REJECTS_CAMPAIGN);
    }

    private VerificationToken getVerificationToken(String data, String secretKey, int expiryTimeInDays,
            LocalDateTime timeStamp,
            VerificationToken.VerificationTokenType driverAcceptsRejectsCampaign) {
        final String token = hmacDigest(data, secretKey, HMAC_SHA256);
        final LocalDateTime expiryDate = calculateExpiryDateForVerificationToken(expiryTimeInDays);
        return new VerificationToken(token, timeStamp, expiryDate, driverAcceptsRejectsCampaign);
    }

    private String hmacDigest(String msg, String keyString, String algo) {
        String digest = null;
        try {
            SecretKeySpec key = new SecretKeySpec((keyString).getBytes("UTF-8"), algo);
            Mac mac = Mac.getInstance(algo);
            mac.init(key);

            byte[] bytes = mac.doFinal(msg.getBytes("ASCII"));

            StringBuffer hash = new StringBuffer();
            for (int i = 0; i < bytes.length; i++) {
                String hex = Integer.toHexString(0xFF & bytes[i]);
                if (hex.length() == 1) {
                    hash.append('0');
                }
                hash.append(hex);
            }
            digest = hash.toString();
        } catch (UnsupportedEncodingException | InvalidKeyException | NoSuchAlgorithmException e) {
            log.error(e.getMessage());
        }
        return digest;
    }

    private LocalDateTime calculateExpiryDateForVerificationToken(final int expiryTimeInDays) {
        LocalDateTime now = LocalDateTime.now();
        return now.plusDays(expiryTimeInDays);
    }

}
