package pl.adroads.system.domain.driver.model;

import static pl.adroads.system.domain.driver.model.Status.REGISTERED;
import static pl.adroads.system.domain.driver.model.Status.TAKEN;

public class TakenStatusOperations implements StatusOperations {

  @Override
  public Status markAsRegistered(DriverId driverId) {
    return REGISTERED;
  }

  @Override
  public Status markAsTaken(DriverId driverId) {
    return TAKEN;
  }
}
