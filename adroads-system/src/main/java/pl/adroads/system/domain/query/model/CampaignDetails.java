package pl.adroads.system.domain.query.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import pl.adroads.system.domain.campaign.model.CampaignId;

@Data
@RequiredArgsConstructor
public class CampaignDetails {
    private final CampaignId campaignId;
    private final String campaignName;
}
