package pl.adroads.system.domain.driver.model;

import pl.adroads.system.domain.driver.exception.DriverNotFullyRegisteredException;

import static pl.adroads.system.domain.driver.model.Status.REGISTERED;

public class CreatedStatusOperations implements StatusOperations {

  @Override
  public Status markAsRegistered(final DriverId driverId) {
    return REGISTERED;
  }

  @Override
  public Status markAsTaken(final DriverId driverId) {
    throw new DriverNotFullyRegisteredException(driverId);
  }

}
