package pl.adroads.system.domain.driver.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import static pl.adroads.system.domain.driver.exception.DriverValidationFields.DRIVER_TYPE;
import static pl.adroads.system.domain.driver.exception.DriverValidationFields.MONTHLY_DISTANCE;
import static pl.adroads.system.common.util.Validate.isTrue;
import static pl.adroads.system.common.util.Validate.notNull;

@EqualsAndHashCode
@Getter
public class DriverDetails {

  //  FIXME maybe enum???
  private PolishDriverType type;
  private Integer monthlyDistanceIndex;

  private DriverDetails(final PolishDriverType type, final Integer monthlyDistanceIndex) {
    notNull(type, DRIVER_TYPE);
    notNull(monthlyDistanceIndex, MONTHLY_DISTANCE);
    isTrue(monthlyDistanceIndex > 0 && monthlyDistanceIndex <= 4, MONTHLY_DISTANCE);

    this.type = type;
    this.monthlyDistanceIndex = monthlyDistanceIndex;
  }

  public static DriverDetails of(final PolishDriverType type, final Integer monthlyDistanceIndex) {
    return new DriverDetails(type, monthlyDistanceIndex);
  }

}
