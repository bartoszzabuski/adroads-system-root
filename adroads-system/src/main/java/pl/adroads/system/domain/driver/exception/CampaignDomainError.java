package pl.adroads.system.domain.driver.exception;

import static org.apache.http.HttpStatus.SC_CONFLICT;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;

import pl.adroads.system.common.util.errors.SystemError;

public enum CampaignDomainError implements SystemError {

    CAMPAIGN_NOT_FOUND(SC_NOT_FOUND),
    DRIVER_NOT_ON_CAMPAIGN(SC_NOT_FOUND),
    DUPLICATED_DRIVER(SC_CONFLICT);

    private final int code;

    private CampaignDomainError(final int code) {
        this.code = code;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getType() {
        return CampaignDomainError.class.getSimpleName();
    }

}
