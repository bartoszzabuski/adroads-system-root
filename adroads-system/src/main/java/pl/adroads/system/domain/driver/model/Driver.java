package pl.adroads.system.domain.driver.model;

import static pl.adroads.system.common.util.Validate.notNull;
import static pl.adroads.system.domain.CommonValidationFields.EMAIL;
import static pl.adroads.system.domain.driver.exception.DriverValidationFields.*;

import java.time.LocalDate;

import lombok.Getter;
import pl.adroads.system.common.domain.Aggregate;

@Getter
public class Driver extends Aggregate<DriverId> {

    // TODO refactor to add Name and Surname objects
    private String name;
    private String surname;
    private LocalDate dateOfBirth;
    private Email email;
    private Address address;
    private Status status;
    private CarDetails carDetails;
    private DriverDetails driverDetails;

    private Driver(final DriverBuilder builder) {
        // notNull(builder.IDid);
        notNull(builder.name, DRIVER_NAME);
        notNull(builder.surname, DRIVER_SURNAME);
        notNull(builder.dateOfBirth, DATE_OF_BIRTH);
        notNull(builder.email, EMAIL);
        notNull(builder.address, ADDRESS);
//        notNull(builder.status, DEFAULT);
        notNull(builder.carDetails);
        notNull(builder.driverDetails);

        this.uuid = builder.uuid;
        this.name = builder.name;
        this.surname = builder.surname;
        this.dateOfBirth = builder.dateOfBirth;
        this.email = builder.email;
        this.address = builder.address;
        this.status = builder.status;
        this.carDetails = builder.carDetails;
        this.driverDetails = builder.driverDetails;
    }

    public void approveCampaign() {
        this.status = status.markAsTaken(getUuid());
    }

    public void activate() {
        this.status = status.markAsRegistered(getUuid());
    }

    public void markAsTaken() {
        this.status = status.markAsTaken(getUuid());
    }

    public static class DriverBuilder {

        private DriverId uuid;
        private String name;
        private String surname;
        private LocalDate dateOfBirth;
        private Email email;
        private Address address;
        private Status status;
        private CarDetails carDetails;
        private DriverDetails driverDetails;

        public DriverBuilder() {
            this.uuid = uuid;
        }

        public DriverBuilder withID(final DriverId uuid) {
            this.uuid = uuid;
            return this;
        }

        public DriverBuilder withName(final String name) {
            this.name = name;
            return this;
        }

        public DriverBuilder withSurname(final String surname) {
            this.surname = surname;
            return this;
        }

        public DriverBuilder withDateOfBirth(final LocalDate dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
            return this;
        }

        public DriverBuilder withEmail(final Email email) {
            this.email = email;
            return this;
        }

        public DriverBuilder withAddress(final Address address) {
            this.address = address;
            return this;
        }

        public DriverBuilder withStatus(final Status status) {
            this.status = status;
            return this;
        }

        public DriverBuilder withCarDetails(final CarDetails carDetails) {
            this.carDetails = carDetails;
            return this;
        }

        public DriverBuilder withDriverDetails(final DriverDetails driverDetails) {
            this.driverDetails = driverDetails;
            return this;
        }

        public Driver build() {
            return new Driver(this);
        }

    }

}
