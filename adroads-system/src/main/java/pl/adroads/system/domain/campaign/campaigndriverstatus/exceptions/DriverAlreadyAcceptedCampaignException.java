package pl.adroads.system.domain.campaign.campaigndriverstatus.exceptions;


import pl.adroads.system.common.domain.DomainException;

import static pl.adroads.system.domain.ExceptionMessages.DRIVER_ALREADY_ACCEPTED_CAMPAING;

public class DriverAlreadyAcceptedCampaignException extends DomainException {

  public DriverAlreadyAcceptedCampaignException() {
    super(DRIVER_ALREADY_ACCEPTED_CAMPAING);
  }

}
