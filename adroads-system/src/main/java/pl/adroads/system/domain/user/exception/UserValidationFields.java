package pl.adroads.system.domain.user.exception;


import pl.adroads.system.common.util.ValidationFields;

public enum UserValidationFields implements ValidationFields {
  TOKEN;
}
