package pl.adroads.system.domain.campaign.campaigndriverstatus;


import pl.adroads.system.domain.campaign.model.CampaignId;
import pl.adroads.system.domain.driver.model.DriverId;

public interface CampaignDriverStatusOperations {

  CampaignDriverStatus driverAccepts(CampaignId campaignId, DriverId driverId);

  CampaignDriverStatus driverRejects(CampaignId campaignId, DriverId driverId);

  CampaignDriverStatus adminApproves(CampaignId campaignId, DriverId driverId);

  CampaignDriverStatus adminRejects(CampaignId campaignId, DriverId driverId);

}
