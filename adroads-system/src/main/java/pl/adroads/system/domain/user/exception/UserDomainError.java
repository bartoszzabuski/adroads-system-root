package pl.adroads.system.domain.user.exception;

import pl.adroads.system.common.util.errors.SystemError;

import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_UNAUTHORIZED;

public enum UserDomainError implements SystemError {

  VERIFICATION_TOKEN_NOT_VALID(SC_BAD_REQUEST),
  PASSWORDS_DONT_MATCH(SC_UNAUTHORIZED),
  INVALID_LOGIN(SC_UNAUTHORIZED);

  private final int code;

  UserDomainError(final int code) {
    this.code = code;
  }

  @Override
  public Integer getCode() {
    return code;
  }

  @Override
  public String getType() {
    return UserDomainError.class.getSimpleName();
  }

}
