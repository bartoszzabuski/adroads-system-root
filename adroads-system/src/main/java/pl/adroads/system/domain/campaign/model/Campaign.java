package pl.adroads.system.domain.campaign.model;

import static jersey.repackaged.com.google.common.collect.Sets.newHashSet;
import static pl.adroads.system.common.util.Validate.isTrue;
import static pl.adroads.system.common.util.Validate.notNull;
import static pl.adroads.system.domain.campaign.campaigndriverstatus.CampaignDriverStatus.*;
import static pl.adroads.system.domain.campaign.exception.CampaignValidationFields.*;
import static pl.adroads.system.domain.campaign.model.CampaignStatus.CREATED;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

import lombok.Getter;
import pl.adroads.system.common.domain.Aggregate;
import pl.adroads.system.domain.campaign.exception.CampaignRejectionApprovalTokenNotValidException;
import pl.adroads.system.domain.campaign.exception.DriverAlreadyAssignedToThisCampaignException;
import pl.adroads.system.domain.campaign.exception.DriverNotAssignedToCampaign;
import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.domain.driver.model.DriverId;
import pl.adroads.system.domain.driver.model.Email;
import pl.adroads.system.domain.user.model.VerificationToken;

@Getter
public class Campaign extends Aggregate<CampaignId> {

    private final String name;
    private final String description;
    private final LocalDateTime startDateTime;
    private final LocalDateTime endDateTime;
    private CampaignStatus campaignStatus;

    private Set<CampaignDriver> drivers;

    public Campaign(final String name, final String description, final LocalDateTime startDateTime,
            final LocalDateTime endDateTime, CampaignStatus status, Set<CampaignDriver> drivers) {
        notNull(name, CAMPAIGN_NAME);
        notNull(description, CAMPAIGN_DESCRIPTION);
        notNull(startDateTime, CAMPAIGN_START_DATE);
        notNull(endDateTime, CAMPAIGN_END_DATE);
        final LocalDateTime now = LocalDateTime.now();
        isTrue(startDateTime.isAfter(now), CAMPAIGN_START_DATE);
        isTrue(endDateTime.isAfter(startDateTime), CAMPAIGN_END_DATE);

        this.name = name;
        this.description = description;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.campaignStatus = status;
        this.drivers = drivers;
    }

    public static Campaign campaignOf(final String name, final String description,
            final LocalDateTime startDateTime, final LocalDateTime endDateTime) {
        return new Campaign(name, description, startDateTime, endDateTime, CREATED, newHashSet());
    }

    public void addPotentialDriver(final Driver driver, final VerificationToken approvalRejectionToken) {
        // TODO consider throwing error, catching it and skipping update to the store in case duplicated id
        validateNoDuplicateDrivers(driver.getUuid());
        drivers.add(new CampaignDriver(driver.getUuid(), driver.getEmail(), DRIVER_APPROVAL_PENDING,
                approvalRejectionToken));
    }

    private void validateNoDuplicateDrivers(final DriverId driverId) {
        if (getCampaignDriverById(driverId).isPresent()) {
            throw new DriverAlreadyAssignedToThisCampaignException(driverId);
        }
    }

    public Optional<CampaignDriver> getCampaignDriverById(final DriverId driverId) {
        return this.drivers.stream().filter((driver) -> driver.getDriverId().equals(driverId))
                .findFirst();
    }

    public Optional<CampaignDriver> getDriverFor(final DriverId uuid) {
        return drivers.stream().filter(campaignDriver -> campaignDriver.getDriverId().equals(uuid)).findFirst();
    }

    public void driverApprovesCampaign(final Email email, final String token) {
        notNull(token);
        CampaignDriver campaignDriver = getExistingCampaignDriver(email);
        validateApprovalRejectionToken(email, token, campaignDriver);
        campaignDriver.setCampaignDriverStatus(DRIVER_ACCEPTED_CAMPAIGN);
    }

    private void validateApprovalRejectionToken(Email email, String token, CampaignDriver campaignDriver) {
        boolean isTokenValid = campaignDriver.getApprovalRejectionToken().verifyAgainst(token);
        if (!isTokenValid) {
            throw new CampaignRejectionApprovalTokenNotValidException(email, this.uuid);
        }
    }

    public void driverRejectsCampaign(final Email email, final String token) {
        notNull(token);
        CampaignDriver campaignDriver = getExistingCampaignDriver(email);
        validateApprovalRejectionToken(email, token, campaignDriver);
        campaignDriver.setCampaignDriverStatus(DRIVER_REJECTED_CAMPAIGN);
    }

    private CampaignDriver getExistingCampaignDriver(final Email email) {
        Optional<CampaignDriver> campaignDriver = findCampaignDriverByEmail(email);
        return campaignDriver.orElseThrow(() -> new DriverNotAssignedToCampaign(email, uuid));
    }

    private Optional<CampaignDriver> findCampaignDriverByEmail(final Email email) {
        return drivers.stream().filter(driver -> driver.getEmail().equals(email)).findAny();
    }
}
