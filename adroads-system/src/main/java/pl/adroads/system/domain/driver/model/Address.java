package pl.adroads.system.domain.driver.model;


import pl.adroads.system.common.domain.ValueObject;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import static pl.adroads.system.domain.driver.exception.DriverValidationFields.CITY;
import static pl.adroads.system.domain.driver.exception.DriverValidationFields.POSTCODE;
import static pl.adroads.system.domain.driver.exception.DriverValidationFields.PROVINCE;
import static pl.adroads.system.domain.driver.exception.DriverValidationFields.STREET;
import static pl.adroads.system.common.util.Validate.notNull;


@ToString
@Getter
@EqualsAndHashCode
public final class Address implements ValueObject {

  private final Postcode postcode;
  private final Street street;
  private final City city;
  private final PolishProvince province;

  private Address(final Street street, final Postcode postcode, final City city,
      final PolishProvince province) {
    notNull(postcode, POSTCODE);
    notNull(street, STREET);
    notNull(city, CITY);
    notNull(province, PROVINCE);

    this.street = street;
    this.postcode = postcode;
    this.city = city;
    this.province = province;
  }

  public static Address of(final Postcode postcode, final Street street, final City city,
      final PolishProvince province) {
    return new Address(street, postcode, city, province);
  }

}
