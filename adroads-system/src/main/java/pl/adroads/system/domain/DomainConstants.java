package pl.adroads.system.domain;


public abstract class DomainConstants {

  //  FIXME ugly!!!!
  private static final String POLISH_ALLOWED_LETTERS = "A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ";
  private static final String POLISH_LETTERS_DIGITS_HYPHEN = "[_" + POLISH_ALLOWED_LETTERS
							     + "0-9-]";

  public static final String EMAIL_REGEX = "^" + POLISH_LETTERS_DIGITS_HYPHEN + "+(\\."
					   + POLISH_LETTERS_DIGITS_HYPHEN + "+)*@"
					   + POLISH_LETTERS_DIGITS_HYPHEN + "+(\\."
					   + POLISH_LETTERS_DIGITS_HYPHEN + "+)*(\\."
					   + POLISH_LETTERS_DIGITS_HYPHEN + "{2,})$";

  private DomainConstants() {

  }

}
