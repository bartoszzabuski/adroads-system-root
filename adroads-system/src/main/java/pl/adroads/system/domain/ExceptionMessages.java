package pl.adroads.system.domain;

public final class ExceptionMessages {


  private ExceptionMessages() {
  }

  public static final String DRIVER_NOT_FULLY_REGISTERED = "Driver not fully registered!";
  public static final String DRIVER_ALREADY_ASSIGNED_EXCEPTION
      = "Driver already assigned to campaign";
  public static final String DRIVER_ALREADY_EXISTS_EXCEPTION = "Driver already exists";
  public static final String DRIVER_NOT_FOUND_EXCEPTION = "Driver not found";


  public static final String CLIENT_ALREADY_EXISTS_EXCEPTION = "Client already exists";


  public static final String DRIVER_NOT_FOOUND_ON_CAMPAIGN_EXCEPTION
      = "Driver not found on campaign";
  public static final String CAMPAIGN_NOT_VALID_EXCEPTION = "Campaign not valid";

  public static final String DRIVER_ALREADY_PARTICIPATES_IN_CAMPAIGN
      = "Driver already participates in other campaign";
  public static final String CAMPAIGN_NOT_FOUND_EXCEPTION = "Campaign not found";
  public static final String POTENTIAL_CAMPAIGN_ALREADY_ASSIGNED_EXCEPTION
      = "Potential campaign already assigned to driver";
  public static final String DRIVER_ALREADY_ASSIGNED_TO_CAMPAIGN_EXCEPTION
      = "Driver already assigned to campaign";
  public static final String NO_DRIVERS_ASSIGNED_TO_CAMPAIGN_EXCEPTION
      = "No drivers has been assigned to campaign";
  public static final String NO_APPROVED_DRIVERS_ON_CAMPAIGN = "No approved drivers on campaign";
  public static final String DRIVER_ALREADY_ASSIGNED_TO_THIS_EXCEPTION
      = "Driver already assigned to t campaign";
  public static final String DRIVER_NOT_ASSIGNED_TO_THIS_CAMPAIGN
      = "Driver not assigned to this campaign";

  public static final String DRIVER_ALREADY_REJECTED_CAMPAIGN = "Driver already rejected campaign";
  public static final String DRIVER_ALREADY_REJECTED_BY_ADMIN = "Driver already rejected by admin";
  public static final String DRIVER_ALREADY_ACCEPTED_CAMPAING = "Driver already accepted campaign";
  public static final String DRIVER_ALREADY_ACCEPTED_BY_ADMIN = "Driver already accepted by admin";

}
