package pl.adroads.system.domain.driver.exception;

import pl.adroads.system.common.util.ValidationFields;

public enum DriverValidationFields implements ValidationFields {

  DRIVER_ID, DRIVER_NAME, DRIVER_SURNAME, DATE_OF_BIRTH, PROVINCE, CITY, STREET, POSTCODE, ADDRESS, DEFAULT, WIDTH, HEIGHT, CAR_BODY, MILEAGE, CAR_YEAR, WINDSCREEN_DIMENSIONS, COLOUR, MODEL, MAKE, MONTHLY_DISTANCE, DRIVER_TYPE;

}
