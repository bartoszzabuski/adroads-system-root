package pl.adroads.system.domain.campaign.exception;

import static pl.adroads.system.domain.driver.exception.CampaignDomainError.DUPLICATED_DRIVER;

import pl.adroads.system.common.util.SystemException;
import pl.adroads.system.domain.driver.model.DriverId;

public class DriverAlreadyAssignedToThisCampaignException extends SystemException {

    public DriverAlreadyAssignedToThisCampaignException(final DriverId driverId) {
        super(SystemException.builder(DUPLICATED_DRIVER).withParams(driverId.getValue()));
    }
}
