package pl.adroads.system.domain.query;

import pl.adroads.system.domain.query.model.DriverFilterCriteria;
import pl.adroads.system.domain.query.model.Page;
import pl.adroads.system.infrastructure.adapters.driving.query.persistance.model.PersistenceQueryDriver;

public interface DriverQueryRepository {

    Page<PersistenceQueryDriver> getFirstPage(DriverFilterCriteria driverFilterCriteria, Integer pageSize);

    Page<PersistenceQueryDriver> getSubsequentPage(DriverFilterCriteria driverFilterCriteria, String uuid,
            Integer pageSize);

}
