package pl.adroads.system.domain.user.model;

import java.util.Optional;

import lombok.Getter;
import pl.adroads.system.common.domain.Aggregate;
import pl.adroads.system.common.util.Validate;
import pl.adroads.system.domain.driver.model.Email;
import pl.adroads.system.domain.user.exception.PasswordsDontMatchException;
import pl.adroads.system.domain.user.exception.UserValidationFields;
import pl.adroads.system.domain.user.exception.VerificationTokenNotValidException;

@Getter
public class User extends Aggregate<UserId> {

    private final Email email;
    private VerificationToken verificationToken;
    private final UserType userType;
    private final Optional<String> password;
    private Optional<SessionToken> sessionToken;

    // TODO cleanup those constructors!!!
    public User(final UserId uuid, final Email email, final VerificationToken verificationToken,
            final UserType userType) {
        Validate.notNull(uuid);
        Validate.notNull(email);
        Validate.notNull(verificationToken);
        Validate.notNull(userType);

        this.uuid = uuid;
        this.email = email;
        this.verificationToken = verificationToken;
        this.userType = userType;
        this.password = Optional.empty();
        this.sessionToken = Optional.empty();
    }

    public User(final UserId uuid, final Email email, final VerificationToken verificationToken,
            final UserType userType, final String password) {
        Validate.notNull(uuid);
        Validate.notNull(email);
        Validate.notNull(verificationToken);
        Validate.notNull(userType);

        this.uuid = uuid;
        this.email = email;
        this.verificationToken = verificationToken;
        this.userType = userType;
        this.password = Optional.of(password);
        this.sessionToken = Optional.empty();
    }

    public User(final UserId uuid, final Email email, final VerificationToken verificationToken,
            final UserType userType, final String password, final SessionToken sessionToken) {
        Validate.notNull(uuid);
        Validate.notNull(email);
        Validate.notNull(verificationToken);
        Validate.notNull(userType);

        this.uuid = uuid;
        this.email = email;
        this.verificationToken = verificationToken;
        this.userType = userType;
        this.password = Optional.ofNullable(password);
        this.sessionToken = Optional.ofNullable(sessionToken);
    }

    private User(final UserId uuid, final Email email, final UserType userType) {
        Validate.notNull(uuid);
        Validate.notNull(email);
        Validate.notNull(userType);

        this.uuid = uuid;
        this.email = email;
        this.userType = userType;
        this.password = Optional.empty();
        this.sessionToken = Optional.empty();
    }

    public void assignVerificationToken(final VerificationToken verificationToken) {
        Validate.notNull(verificationToken);
        this.verificationToken = verificationToken;
    }

    public void verifyAgainst(final String token) {
        Validate.notNull(token, UserValidationFields.TOKEN);
        Validate.notNull(verificationToken);

        boolean isTokenValid = verificationToken.verifyAgainst(token);
        if (!isTokenValid) {
            throw new VerificationTokenNotValidException(email);
        }
    }

    public static User userOf(final UserId uuid, final Email email, final UserType userType) {
        return new User(uuid, email, userType);
    }

    public void validatePassword(final String password) {
        if (!this.password.isPresent() || !this.password.get().equals(password)) {
            throw new PasswordsDontMatchException();
        }
    }

    public void assignSessionToken(final SessionToken sessionToken) {
        this.sessionToken = Optional.of(sessionToken);
    }
}
