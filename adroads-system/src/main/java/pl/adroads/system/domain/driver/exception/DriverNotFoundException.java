package pl.adroads.system.domain.driver.exception;


import pl.adroads.system.common.util.SystemException;
import pl.adroads.system.domain.driver.model.DriverId;

import static pl.adroads.system.domain.driver.exception.DriverDomainError.DRIVER_NOT_FOUND;

public class DriverNotFoundException extends SystemException {

  public DriverNotFoundException(final DriverId driverId) {
    super(SystemException.builder(DRIVER_NOT_FOUND).withParams(driverId.getValue()));
  }

}
