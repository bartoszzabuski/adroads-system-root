package pl.adroads.system.domain.user.model;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import pl.adroads.system.common.util.Validate;
import pl.adroads.system.domain.user.exception.UserValidationFields;

@Getter
@Slf4j
public class VerificationToken extends Token {

    private final VerificationTokenType tokenType;

    private boolean verified = false;

    private LocalDateTime verificationDate;

    public VerificationToken(String token, LocalDateTime timestamp, LocalDateTime expiryDate,
            VerificationTokenType tokenType, boolean isVerified, LocalDateTime verificationDate) {
        super(token, timestamp, expiryDate);

        this.tokenType = tokenType;
        this.verified = isVerified;
        this.verificationDate = verificationDate;
    }

    public VerificationToken(String token, LocalDateTime timestamp, LocalDateTime expiryDate,
            VerificationTokenType tokenType) {
        super(token, timestamp, expiryDate);

        this.tokenType = tokenType;
    }

    public boolean verifyAgainst(final String token) {
        // FIXME change
        Validate.notNull(token, UserValidationFields.TOKEN);

        if (isVerified() || isExpired() || !isTokenValid(token)) {
            return false;
        } else {
            this.verified = true;
            this.verificationDate = LocalDateTime.now();
            return true;
        }
    }

    public enum VerificationTokenType {
        REGISTRATION_CONFIRMATION,
        DRIVER_ACCEPTS_REJECTS_CAMPAIGN;
    }

}
