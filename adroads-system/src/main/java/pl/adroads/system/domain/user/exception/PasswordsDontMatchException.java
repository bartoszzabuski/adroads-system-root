package pl.adroads.system.domain.user.exception;

import static pl.adroads.system.domain.user.exception.UserDomainError.PASSWORDS_DONT_MATCH;

import pl.adroads.system.common.util.SystemException;

public class PasswordsDontMatchException extends SystemException {

    public PasswordsDontMatchException() {
        super(SystemException.builder(PASSWORDS_DONT_MATCH));
    }
}
