package pl.adroads.system.domain;

import pl.adroads.system.common.domain.DomainEvent;

public interface DomainEventPublisher {

  <T extends DomainEvent> void publish(T domainEvent);

}
