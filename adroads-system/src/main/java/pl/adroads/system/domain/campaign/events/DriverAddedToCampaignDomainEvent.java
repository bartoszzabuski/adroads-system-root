package pl.adroads.system.domain.campaign.events;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import pl.adroads.system.common.domain.DomainEvent;
import pl.adroads.system.domain.driver.model.DriverId;

@EqualsAndHashCode
@AllArgsConstructor
@Getter
public class DriverAddedToCampaignDomainEvent extends DomainEvent {

    private final DriverId driverId;

}
