package pl.adroads.system.domain.query.model;

import pl.adroads.system.domain.driver.model.PolishCarBody;
import pl.adroads.system.domain.driver.model.PolishProvince;
import pl.adroads.system.domain.driver.model.Status;

import java.util.List;
import java.util.Optional;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class DriverFilterCriteria {

  private final Optional<SortDirection> sortDir;
  private final Optional<String> sortedBy;
  private final Optional<String> email;
  private final Optional<String> name;
  private final Optional<String> surname;
  private final Optional<Status> status;
  private final Optional<String> street;
  private final Optional<String> postcode;
  private final Optional<String> city;
  private final Optional<PolishProvince> province;
  private final Optional<String> carMake;
  private final Optional<String> carModel;
  private final Optional<Integer> yearFrom;
  private final Optional<Integer> yearTo;
  private final Optional<List<PolishCarBody>> carBodies;
  private final Optional<String> carColour;
  private final Optional<Integer> mileageFrom;
  private final Optional<Integer> mileageTo;

}
