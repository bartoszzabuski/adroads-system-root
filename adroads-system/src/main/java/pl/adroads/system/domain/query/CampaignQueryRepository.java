package pl.adroads.system.domain.query;

import java.util.List;

import pl.adroads.system.domain.campaign.model.CampaignId;
import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model.PersistenceCampaign;

public interface CampaignQueryRepository {

    List<PersistenceCampaign> getCampaigns();

    PersistenceCampaign getCampaign(CampaignId campaignId);
}
