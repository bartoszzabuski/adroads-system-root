package pl.adroads.system.domain.driver.model;

import pl.adroads.system.common.domain.ValueObject;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import static pl.adroads.system.domain.driver.exception.DriverValidationFields.HEIGHT;
import static pl.adroads.system.domain.driver.exception.DriverValidationFields.WIDTH;
import static pl.adroads.system.common.util.Validate.isTrue;
import static pl.adroads.system.common.util.Validate.notNull;
import static pl.adroads.system.domain.driver.model.DistanceMeasurement.distanceMeasurementOf;
import static pl.adroads.system.domain.driver.model.UnitsOfMeasurement.CM;

@Getter
@EqualsAndHashCode
public final class WindscreenDimensions implements ValueObject {

  private final DistanceMeasurement height;
  private final DistanceMeasurement width;

  private WindscreenDimensions(final Integer height, final Integer width) {
    notNull(height, HEIGHT);
    notNull(width, WIDTH);
    isTrue(height > 0, HEIGHT);
    isTrue(width > 0, WIDTH);
    this.height = distanceMeasurementOf(height, CM);
    this.width = distanceMeasurementOf(width, CM);
  }

  public static WindscreenDimensions windscreenDimensionsOf(final Integer height,
      final Integer width) {
    return new WindscreenDimensions(height, width);
  }

}
