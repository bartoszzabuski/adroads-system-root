package pl.adroads.system.domain.driver.exception;


import pl.adroads.system.common.util.SystemException;

import static pl.adroads.system.domain.driver.exception.DriverDomainError.DRIVER_ALREADY_EXISTS;

public class DriverAlreadyExistsException extends SystemException {

  public DriverAlreadyExistsException(final String emailAddress) {
    super(SystemException.builder(DRIVER_ALREADY_EXISTS).withParams(emailAddress));
  }

}
