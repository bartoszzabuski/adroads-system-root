package pl.adroads.system.domain.driver.model;

import pl.adroads.system.common.domain.Id;

import java.util.UUID;

import static org.springframework.util.Assert.notNull;

public final class DriverId extends Id<UUID> {

  private DriverId(UUID value) {
    super(value);
  }

  public static DriverId of(final String uuid) {
    notNull(uuid, "DriverId cannot be null");
    return new DriverId(UUID.fromString(uuid));
  }

  public static DriverId of(final UUID uuid) {
    notNull(uuid, "DriverId cannot be null");
    return new DriverId(uuid);
  }

}
