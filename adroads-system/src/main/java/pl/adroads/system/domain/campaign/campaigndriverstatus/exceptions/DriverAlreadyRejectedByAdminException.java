package pl.adroads.system.domain.campaign.campaigndriverstatus.exceptions;


import pl.adroads.system.common.domain.DomainException;

import static pl.adroads.system.domain.ExceptionMessages.DRIVER_ALREADY_REJECTED_BY_ADMIN;

public class DriverAlreadyRejectedByAdminException extends DomainException {

  public DriverAlreadyRejectedByAdminException() {
    super(DRIVER_ALREADY_REJECTED_BY_ADMIN);
  }

}
