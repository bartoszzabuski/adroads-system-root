package pl.adroads.system.domain.campaign.model;


import pl.adroads.system.common.domain.Id;

import java.util.UUID;

import static org.springframework.util.Assert.notNull;

public class CampaignId extends Id<UUID> {

  private CampaignId(UUID value) {
    super(value);
  }

  public static CampaignId campaignIdOf(final String uuid) {
    notNull(uuid, "CampaignId cannot be null");
    return new CampaignId(UUID.fromString(uuid));
  }

  public static CampaignId campaignIdOf(final UUID uuid) {
    notNull(uuid, "CampaignId cannot be null");
    return new CampaignId(uuid);
  }
}
