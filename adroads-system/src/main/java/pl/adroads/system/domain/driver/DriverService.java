package pl.adroads.system.domain.driver;

import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.domain.driver.model.DriverId;

public interface DriverService {

  DriverId createNewDriver(Driver driver);

  void approveCampaign(DriverId driverId);

  void activateDriver(DriverId driverId);

  void assignToCampaign(DriverId driverId);

  void updateDriver(DriverId driverId, Driver driver);
}
