package pl.adroads.system.domain.campaign;


import pl.adroads.system.domain.campaign.model.Campaign;
import pl.adroads.system.domain.campaign.model.CampaignId;
import pl.adroads.system.infrastructure.locking.Lockable;

import java.util.Optional;

public interface CampaignRepository extends Lockable<String> {

  void store(Campaign campaign);

  void update(Campaign campaign);

  Optional<Campaign> get(CampaignId campaignId);
}
