package pl.adroads.system.domain.campaign.exception;

import static pl.adroads.system.domain.driver.exception.CampaignDomainError.DRIVER_NOT_ON_CAMPAIGN;

import pl.adroads.system.common.util.SystemException;
import pl.adroads.system.domain.campaign.model.CampaignId;
import pl.adroads.system.domain.driver.model.Email;

public class DriverNotAssignedToCampaign extends SystemException {

    public DriverNotAssignedToCampaign(final Email email, final CampaignId campaignId) {
        super(SystemException.builder(DRIVER_NOT_ON_CAMPAIGN).withParams(campaignId.getValue(), email.getEmailValue()));
    }

}
