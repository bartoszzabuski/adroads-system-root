package pl.adroads.system.domain.driver.model;


import pl.adroads.system.common.domain.ValueObject;

import java.util.regex.Pattern;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import static java.util.regex.Pattern.compile;
import static pl.adroads.system.domain.driver.exception.DriverValidationFields.COLOUR;
import static pl.adroads.system.common.util.Validate.isTrue;

@ToString
@Getter
@EqualsAndHashCode(of = "value")
public final class Colour implements ValueObject {

  private static final String COLOUR_REGEX = "^[a-zA-Z0-9ĄĘŚĆŻŹŁÓĆŃąęśćżźłóćń/ .-]{3,}$";
  private static final Pattern PATTERN = compile(COLOUR_REGEX);

  private final String value;

  private Colour(final String colour) {
    isTrue(isValid(colour), COLOUR);

    this.value = colour;
  }

  public static Colour colourOf(final String colour) {
    return new Colour(colour);
  }

  private boolean isValid(final String colour) {
    return (colour != null) && PATTERN.matcher(colour).matches();
  }

  public String getValue() {
    return value;
  }

}
