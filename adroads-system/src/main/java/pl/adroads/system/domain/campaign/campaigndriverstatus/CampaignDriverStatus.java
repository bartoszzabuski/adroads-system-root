package pl.adroads.system.domain.campaign.campaigndriverstatus;


import pl.adroads.system.domain.campaign.model.CampaignId;
import pl.adroads.system.domain.driver.model.DriverId;

public enum CampaignDriverStatus implements CampaignDriverStatusOperations {

  DRIVER_APPROVAL_PENDING(new DriverApprovalPendingStatusOperations()),
  DRIVER_REJECTED_CAMPAIGN(new DriverRejectedCampaignStatusOperations()),
  DRIVER_REJECTED_BY_ADMIN(new RejectedByAdminStatusOperations()),
  DRIVER_ACCEPTED_CAMPAIGN(new DriverAcceptedCampaignStatusOperations()),
  DRIVER_APPROVED_BY_ADMIN(new DriverApprovedByAdminStatusOperations());

  private CampaignDriverStatusOperations campaignDriverStatusOperations;

  CampaignDriverStatus(CampaignDriverStatusOperations campaignDriverStatusOperations) {
    this.campaignDriverStatusOperations = campaignDriverStatusOperations;
  }

  public CampaignDriverStatus driverAccepts(CampaignId campaignId, DriverId driverId) {
    return campaignDriverStatusOperations.driverAccepts(campaignId, driverId);
  }

  public CampaignDriverStatus driverRejects(CampaignId campaignId, DriverId driverId) {
    return campaignDriverStatusOperations.driverRejects(campaignId, driverId);
  }

  public CampaignDriverStatus adminApproves(CampaignId campaignId, DriverId driverId) {
    return campaignDriverStatusOperations.adminApproves(campaignId, driverId);
  }

  public CampaignDriverStatus adminRejects(CampaignId campaignId, DriverId driverId) {
    return campaignDriverStatusOperations.adminRejects(campaignId, driverId);
  }

}
