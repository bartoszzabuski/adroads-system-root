package pl.adroads.system.domain.user.model;

import java.time.LocalDateTime;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import pl.adroads.system.common.util.Validate;
import pl.adroads.system.domain.user.exception.UserValidationFields;

@Getter
@Slf4j
@EqualsAndHashCode
public class SessionToken extends Token {

    public SessionToken(String token, LocalDateTime timestamp, LocalDateTime expiryDate) {
        super(token, timestamp, expiryDate);
    }

    public boolean  verifyAgainst(final String token) {
        // FIXME change
        Validate.notNull(token, UserValidationFields.TOKEN);

        return !(isExpired() || !isTokenValid(token));
    }

}
