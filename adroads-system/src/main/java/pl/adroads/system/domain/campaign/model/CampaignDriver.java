package pl.adroads.system.domain.campaign.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import pl.adroads.system.domain.campaign.campaigndriverstatus.CampaignDriverStatus;
import pl.adroads.system.domain.driver.model.DriverId;
import pl.adroads.system.domain.driver.model.Email;
import pl.adroads.system.domain.user.model.VerificationToken;

@AllArgsConstructor
@Data
@EqualsAndHashCode(of = "driverId")
public class CampaignDriver {

    private DriverId driverId;
    private Email email;
    private CampaignDriverStatus campaignDriverStatus;
    private VerificationToken approvalRejectionToken;

}
