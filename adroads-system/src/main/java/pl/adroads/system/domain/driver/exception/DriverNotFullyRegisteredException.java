package pl.adroads.system.domain.driver.exception;

import pl.adroads.system.common.util.SystemException;
import pl.adroads.system.domain.driver.model.DriverId;

import static pl.adroads.system.domain.driver.exception.DriverDomainError.DRIVER_NOT_FULLY_REGISTERED;

public class DriverNotFullyRegisteredException extends SystemException {

  public DriverNotFullyRegisteredException(final DriverId driverId) {
    super(SystemException.builder(DRIVER_NOT_FULLY_REGISTERED).withParams(driverId.getValue()));
  }

}
