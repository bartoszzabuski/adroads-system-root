package pl.adroads.system.domain.driver.model;

import lombok.EqualsAndHashCode;

import static pl.adroads.system.domain.driver.exception.DriverValidationFields.DEFAULT;
import static pl.adroads.system.common.util.Validate.notNull;


@EqualsAndHashCode
public final class DistanceMeasurement {

  private final Integer value;
  private final UnitsOfMeasurement unit;

  private DistanceMeasurement(final Integer value, final UnitsOfMeasurement unit) {
    notNull(value, DEFAULT);
    notNull(unit, DEFAULT);

    this.value = value;
    this.unit = unit;
  }

  public static DistanceMeasurement distanceMeasurementOf(final Integer value,
      final UnitsOfMeasurement unit) {
    return new DistanceMeasurement(value, unit);
  }

  public Integer getValue() {
    return value;
  }

  public UnitsOfMeasurement getUnit() {
    return unit;
  }
}
