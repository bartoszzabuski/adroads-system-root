package pl.adroads.system.domain.campaign.campaigndriverstatus.exceptions;


import pl.adroads.system.common.domain.DomainException;

import static pl.adroads.system.domain.ExceptionMessages.DRIVER_ALREADY_ACCEPTED_BY_ADMIN;

public class DriverAlreadyAcceptedByAdminException extends DomainException {

  public DriverAlreadyAcceptedByAdminException() {
    super(DRIVER_ALREADY_ACCEPTED_BY_ADMIN);
  }
}
