package pl.adroads.system.domain.driver.model;


import pl.adroads.system.common.util.Validate;
import pl.adroads.system.common.util.ValidationException;

import java.util.Arrays;
import java.util.Optional;

import static pl.adroads.system.common.util.errors.ValidationError.VALUE_NOT_VALID;
import static pl.adroads.system.domain.driver.exception.DriverValidationFields.DRIVER_TYPE;

public enum PolishDriverType {

  PRZEDSTAWICIEL(1),
  KURIER(2),
  TAKSOWKARZ(3),
  DOJAZD_DO_PRACY(4),
  INNY(5);

  private final Integer value;

  PolishDriverType(final Integer type) {
    this.value = type;
  }

  public Integer getValue() {
    return value;
  }

  public static PolishDriverType polishDriverTypeOf(final Integer value) {
    Validate.notNull(value, DRIVER_TYPE);
    Optional<PolishDriverType> polishDriverType = Arrays.stream(PolishDriverType.values())
        .filter(type -> type.getValue().equals(value)).findFirst();
    return polishDriverType.orElseThrow(
        () -> ValidationException.builder(VALUE_NOT_VALID).withField(DRIVER_TYPE).build());
  }

}
