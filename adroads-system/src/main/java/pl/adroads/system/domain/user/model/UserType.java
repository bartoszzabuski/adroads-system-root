package pl.adroads.system.domain.user.model;

public enum UserType {

    DRIVER,
    ADMIN

}
