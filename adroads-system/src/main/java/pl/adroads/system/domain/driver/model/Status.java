package pl.adroads.system.domain.driver.model;


public enum Status implements StatusOperations {

  CREATED(new CreatedStatusOperations()),
  REGISTERED(new RegisteredStatusOperations()),
  TAKEN(new TakenStatusOperations());

  private StatusOperations statusOperations;

  Status(StatusOperations statusOperations) {
    this.statusOperations = statusOperations;
  }

  @Override
  public Status markAsRegistered(DriverId driverId) {
    return statusOperations.markAsRegistered(driverId);
  }

  @Override
  public Status markAsTaken(DriverId driverId) {
    return statusOperations.markAsTaken(driverId);
  }

}
