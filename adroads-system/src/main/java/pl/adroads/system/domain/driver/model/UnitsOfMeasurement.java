package pl.adroads.system.domain.driver.model;


public enum UnitsOfMeasurement {
  CM, KM;
}
