package pl.adroads.system.domain.campaign.exception;


import pl.adroads.system.common.util.ValidationFields;

public enum CampaignValidationFields implements ValidationFields{
  CAMPAIGN_NAME, CAMPAIGN_DESCRIPTION, CAMPAIGN_START_DATE, CAMPAIGN_END_DATE;
}
