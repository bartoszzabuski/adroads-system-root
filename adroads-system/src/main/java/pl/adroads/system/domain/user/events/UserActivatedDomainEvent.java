package pl.adroads.system.domain.user.events;

import pl.adroads.system.common.domain.DomainEvent;
import pl.adroads.system.domain.user.model.UserId;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode
@AllArgsConstructor
@Getter
public class UserActivatedDomainEvent extends DomainEvent {

  private final UserId userId;

}
