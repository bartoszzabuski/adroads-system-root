package pl.adroads.system.domain;

import java.util.UUID;

public interface UUIDGenerator {

  UUID generateId();

}
