package pl.adroads.system.domain.driver.model;


import pl.adroads.system.common.domain.ValueObject;

import java.util.regex.Pattern;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import static java.util.regex.Pattern.compile;
import static pl.adroads.system.domain.driver.exception.DriverValidationFields.CITY;
import static pl.adroads.system.common.util.Validate.isTrue;

@ToString
@Getter
@EqualsAndHashCode(of = "value")
public final class City implements ValueObject {

  private static final String CITY_REGEX = "^[a-zA-Z0-9ĄĘŚĆŻŹŁÓĆŃąęśćżźłóćń/ .-]{3,}$";
  private static final Pattern PATTERN = compile(CITY_REGEX);

  private final String value;

  private City(final String city) {
    isTrue(isValid(city), CITY);

    this.value = city;
  }

  public static City cityOf(final String city) {
    return new City(city);
  }

  private boolean isValid(final String city) {
    return (city != null) && PATTERN.matcher(city).matches();
  }

  public String getValue() {
    return value;
  }

}
