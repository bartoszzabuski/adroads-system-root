package pl.adroads.system.domain.user.model;

import java.time.LocalDateTime;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter(AccessLevel.PROTECTED)
@Slf4j
public abstract class Token {

    private final String token;

    private final LocalDateTime expiryDate;

    private final LocalDateTime timestamp;

    public Token(String token, LocalDateTime timestamp, LocalDateTime expiryDate) {
        this.token = token;
        this.expiryDate = expiryDate;
        this.timestamp = timestamp;
    }

    public abstract boolean verifyAgainst(final String token);

    protected boolean isTokenValid(final String token) {
        return getToken().equals(token);
    }

    protected boolean isExpired() {
        LocalDateTime now = LocalDateTime.now();
        if (now.isAfter(expiryDate)) {
            return true;
        }
        return false;
    }

}
