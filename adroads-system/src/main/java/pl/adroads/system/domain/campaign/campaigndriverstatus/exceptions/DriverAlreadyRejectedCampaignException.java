package pl.adroads.system.domain.campaign.campaigndriverstatus.exceptions;


import pl.adroads.system.common.domain.DomainException;

import static pl.adroads.system.domain.ExceptionMessages.DRIVER_ALREADY_REJECTED_CAMPAIGN;

public class DriverAlreadyRejectedCampaignException extends DomainException {

  public DriverAlreadyRejectedCampaignException() {
    super(DRIVER_ALREADY_REJECTED_CAMPAIGN);
  }

}
