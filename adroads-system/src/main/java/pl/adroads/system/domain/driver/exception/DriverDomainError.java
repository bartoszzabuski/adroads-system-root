package pl.adroads.system.domain.driver.exception;

import pl.adroads.system.common.util.errors.SystemError;

import static org.apache.http.HttpStatus.SC_CONFLICT;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;

public enum DriverDomainError implements SystemError {

  DRIVER_ALREADY_EXISTS(SC_CONFLICT),
  DRIVER_NOT_FOUND(SC_NOT_FOUND),
  DRIVER_NOT_FULLY_REGISTERED(SC_CONFLICT),
  DRIVER_ALREADY_TAKEN(SC_CONFLICT);

  private final int code;

  private DriverDomainError(final int code) {
    this.code = code;
  }

  @Override
  public Integer getCode() {
    return code;
  }

  @Override
  public String getType() {
    return DriverDomainError.class.getSimpleName();
  }

}