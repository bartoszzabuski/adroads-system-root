package pl.adroads.system.domain.user.exception;

import static pl.adroads.system.domain.user.exception.UserDomainError.INVALID_LOGIN;

import pl.adroads.system.common.util.SystemException;
import pl.adroads.system.domain.driver.model.Email;

public class InvalidLoginException extends SystemException {

    public InvalidLoginException(final Email email) {
        super(SystemException.builder(INVALID_LOGIN).withParams(email.getEmailValue()));
    }
}
