package pl.adroads.system.domain.driver.events;

import pl.adroads.system.common.domain.DomainEvent;
import pl.adroads.system.domain.driver.model.Driver;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@Getter
public class DriverFullyRegisteredDomainEvent extends DomainEvent {

  private final Driver driver;

}
