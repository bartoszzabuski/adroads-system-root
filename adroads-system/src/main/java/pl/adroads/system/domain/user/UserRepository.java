package pl.adroads.system.domain.user;

import pl.adroads.system.domain.driver.model.Email;
import pl.adroads.system.domain.user.model.SessionToken;
import pl.adroads.system.domain.user.model.User;

import java.util.Optional;

public interface UserRepository {

  void store(User user);

  Optional<User> findByEmail(Email userEmail);

  void update(User user);

  Optional<User> findBySessionToken(String sessionToken);
}
