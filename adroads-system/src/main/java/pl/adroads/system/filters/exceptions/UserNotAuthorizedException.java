package pl.adroads.system.filters.exceptions;

import static pl.adroads.system.filters.exceptions.AuthorizationError.USER_NOT_AUTHORIZED;

import java.lang.reflect.Method;

import pl.adroads.system.common.util.SystemException;
import pl.adroads.system.domain.user.model.User;

public class UserNotAuthorizedException extends SystemException {

    public UserNotAuthorizedException(User user, Method resourceMethod) {
        super(SystemException.builder(USER_NOT_AUTHORIZED).withParams(user.getEmail().getEmailValue(),
                resourceMethod.getName()));
    }
}
