package pl.adroads.system.filters.exceptions;

import static org.apache.http.HttpStatus.SC_FORBIDDEN;
import static org.apache.http.HttpStatus.SC_UNAUTHORIZED;

import pl.adroads.system.common.util.errors.SystemError;

public enum AuthorizationError implements SystemError {

    INVALID_AUTHORIZATION_HEADER(SC_UNAUTHORIZED),
    USER_NOT_AUTHORIZED(SC_FORBIDDEN);

    private final int code;

    private AuthorizationError(final int code) {
        this.code = code;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getType() {
        return AuthorizationError.class.getSimpleName();
    }
}
