package pl.adroads.system.filters.exceptions;

import static pl.adroads.system.filters.exceptions.AuthorizationError.INVALID_AUTHORIZATION_HEADER;

import pl.adroads.system.common.util.SystemException;

public class InvalidAuthorizationHeaderException extends SystemException {

    public InvalidAuthorizationHeaderException() {
        super(SystemException.builder(INVALID_AUTHORIZATION_HEADER));
    }
}
