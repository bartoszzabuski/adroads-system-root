package pl.adroads.system.filters;

import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;

import java.io.IOException;
import java.security.Principal;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;

import pl.adroads.system.application.user.UserManagementService;
import pl.adroads.system.domain.user.model.User;
import pl.adroads.system.filters.exceptions.InvalidAuthorizationHeaderException;

@Provider
@Secured
public class AuthenticationFilter implements ContainerRequestFilter {

    public static final String BEARER_HEADER_PREFIX = "Bearer ";
    public static final String AUTHENTICATED_USER_REQUEST_PROPERTY = "authenticatedUser";

    private final UserManagementService userManagementService;

    public AuthenticationFilter(UserManagementService userManagementService) {
        this.userManagementService = userManagementService;
    }

    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        System.out
                .println("!!!!!! ________ AuthenticationFilter ________ !!!!!!!! " + userManagementService.toString());
        String authorizationHeader = containerRequestContext.getHeaderString(AUTHORIZATION);
        validateHeader(authorizationHeader);
        final String token = extractTokenValue(authorizationHeader);
        // try {
        final User user = validateToken(token);
        // } catch (Exception e) {
        // containerRequestContext.abortWith(status(UNAUTHORIZED).build());
        // }

        containerRequestContext.setSecurityContext(new SecurityContext() {

            @Override
            public Principal getUserPrincipal() {
                return () -> user.getEmail().getEmailValue();
            }

            @Override
            public boolean isUserInRole(String s) {
                return true;
            }

            @Override
            public boolean isSecure() {
                return false;
            }

            @Override
            public String getAuthenticationScheme() {
                return null;
            }

        });
        containerRequestContext.setProperty(AUTHENTICATED_USER_REQUEST_PROPERTY, user);
    }

    private String extractTokenValue(String authorizationHeader) {
        return authorizationHeader.substring(BEARER_HEADER_PREFIX.length()).trim();
    }

    private User validateToken(final String token) {
        System.out.println("received token: " + token);
        return userManagementService.verifySessionToken(token);
    }

    private void validateHeader(String authorizationHeader) {
        if (authorizationHeader == null || validateHeaderFormat(authorizationHeader)) {
            throw new InvalidAuthorizationHeaderException();
        }
    }

    private boolean validateHeaderFormat(String authorizationHeader) {
        return authorizationHeader.length() <= BEARER_HEADER_PREFIX.length()
                || !authorizationHeader.startsWith(BEARER_HEADER_PREFIX);
    }

}
