package pl.adroads.system.filters;

import static java.util.Arrays.asList;
import static pl.adroads.system.filters.AuthenticationFilter.AUTHENTICATED_USER_REQUEST_PROPERTY;

import java.io.IOException;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;

import pl.adroads.system.domain.user.model.User;
import pl.adroads.system.domain.user.model.UserType;
import pl.adroads.system.filters.exceptions.UserNotAuthorizedException;

@Provider
@Secured
public class AuthorizationFilter implements ContainerRequestFilter {

    @Context
    private ResourceInfo resourceInfo;

    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        User user = (User) containerRequestContext.getProperty(AUTHENTICATED_USER_REQUEST_PROPERTY);
        System.out.println("!!!!!! ________ AuthorizationFilter ________ !!!!!!!! " + user.getEmail().getEmailValue());

        Class<?> resourceClass = resourceInfo.getResourceClass();
        List<UserType> classRoles = extractRoles(resourceClass);

        Method resourceMethod = resourceInfo.getResourceMethod();
        List<UserType> methodRoles = extractRoles(resourceMethod);

        if (!methodRoles.isEmpty()) {
            checkUserHasPermissions(user, methodRoles, resourceMethod);
        } else {
            checkUserHasPermissions(user, classRoles, resourceMethod);
        }
    }

    private void checkUserHasPermissions(final User user, final List<UserType> roles, final Method resourceMethod) {
        roles.stream().filter((role) -> role.equals(user.getUserType())).findFirst()
                .orElseThrow(() -> new UserNotAuthorizedException(user, resourceMethod));
    }

    private List<UserType> extractRoles(final AnnotatedElement annotatedElement) {
        if (annotatedElement == null) {
            return new ArrayList<>();
        } else {
            Secured secured = annotatedElement.getAnnotation(Secured.class);
            if (secured == null) {
                return new ArrayList<>();
            } else {
                UserType[] allowedRoles = secured.value();
                return asList(allowedRoles);
            }
        }
    }

}
