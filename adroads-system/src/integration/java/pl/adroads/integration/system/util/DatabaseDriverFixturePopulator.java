package pl.adroads.integration.system.util;

import static com.google.common.collect.Lists.newArrayList;
import static java.lang.String.format;
import static java.time.Month.JANUARY;
import static java.time.ZoneId.systemDefault;
import static java.util.Arrays.asList;
import static java.util.UUID.fromString;
import static java.util.UUID.randomUUID;
import static java.util.stream.Collectors.toList;
import static pl.adroads.integration.system.util.CommonITFixtures.generateStringUUIDs;
import static pl.adroads.system.domain.driver.model.City.cityOf;
import static pl.adroads.system.domain.driver.model.Email.emailOf;
import static pl.adroads.system.domain.driver.model.Postcode.postcodeOf;
import static pl.adroads.system.domain.driver.model.Status.*;
import static pl.adroads.system.domain.driver.model.Street.streetOf;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

import javax.inject.Inject;
import javax.inject.Named;

import pl.adroads.system.domain.driver.DriverRepository;
import pl.adroads.system.domain.driver.model.Address;
import pl.adroads.system.domain.driver.model.CarDetails;
import pl.adroads.system.domain.driver.model.City;
import pl.adroads.system.domain.driver.model.Colour;
import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.domain.driver.model.DriverDetails;
import pl.adroads.system.domain.driver.model.DriverId;
import pl.adroads.system.domain.driver.model.PolishCarBody;
import pl.adroads.system.domain.driver.model.PolishDriverType;
import pl.adroads.system.domain.driver.model.PolishProvince;
import pl.adroads.system.domain.driver.model.Postcode;
import pl.adroads.system.domain.driver.model.Status;
import pl.adroads.system.domain.driver.model.Street;
import pl.adroads.system.domain.driver.model.WindscreenDimensions;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceAddress;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceCarDetails;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceDriver;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceDriverDetails;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceWindscreenDimensions;
import pl.adroads.system.infrastructure.adapters.shared.DriverStorage;

@Named
public class DatabaseDriverFixturePopulator {

    private static final ZoneId TIMEZONE = systemDefault();
    private static final List<String> STREETS = newArrayList("Senatorska", "Trawowa",
            "powstańców śląskich");
    private static final List<String> CITIES = newArrayList("Wrocław", "Wałbrzych", "Zadupie Małe");
    private static final List<String> MAKES = newArrayList("Renault", "Opel", "Polonez");
    private static final List<String> COLOURS = newArrayList("zielony", "żółty", "czerwony");
    public static final String BARTEK = "bartek";
    public static final String KOWALSKI = "kowalski";
    public static final String BARTEK_KOWALSKI_EMAIL = getEmailFor(BARTEK, KOWALSKI);
    public static final String BARTEK_KOWALSKI_MODEL = "Megane Classic";
    public static final String BARTEK_KOWALSKI_MAKE = "Renault";

    public static final String TOMASZ = "tomasz";
    public static final String SMITH = "smith";
    public static final String TOMASZ_SMITH_EMAIL = getEmailFor(TOMASZ, SMITH);
    public static final String TOMASZ_SMITH_POSTCODE_VALUE = "54-614";
    private static final Postcode TOMASZ_SMITH_POSTCODE = postcodeOf(TOMASZ_SMITH_POSTCODE_VALUE);
    public static final String TOMASZ_SMITH_STREET_VALUE = "Ul. Trawowa";
    private static final Street TOMASZ_SMITH_STREET = streetOf(TOMASZ_SMITH_STREET_VALUE);
    public static final String TOMASZ_SMITH_CITY_VALUE = "Poznań";
    private static final City TOMASZ_SMITH_CITY = cityOf(TOMASZ_SMITH_CITY_VALUE);
    public static final PolishProvince TOMASZ_SMITH_PROVINCE = PolishProvince.DOLNOŚLĄSKIE;
    public static final Address TOMASZ_SMITH_ADDRESS = Address
            .of(TOMASZ_SMITH_POSTCODE, TOMASZ_SMITH_STREET, TOMASZ_SMITH_CITY, TOMASZ_SMITH_PROVINCE);

    private AtomicInteger atomicInt = new AtomicInteger(0);

    private static String getEmailFor(final String name, final String surname) {
        return format("%s.%s@test.com", name, surname);
    }

    private final long START_YEAR = LocalDate.of(1940, JANUARY, 1).atStartOfDay(systemDefault())
            .toEpochSecond();

    private final long END_YEAR = LocalDate.of(1995, JANUARY, 1).atStartOfDay(TIMEZONE)
            .toEpochSecond();

    private static final List<String> NAMES = newArrayList("Slawek", "Bartek", "Grzegorz");
    private static final List<String> SURNAMES = newArrayList("Kowalski", "Brzęczyszczykiewicz");

    @Inject
    private DriverStorage driverStore;

    @Inject
    private DriverRepository driverRepository;

    public List<Driver> populateDBWithPagingData(String uuidPrefix, final String name,
            final String surname, final Integer numberOfDrivers) {
        final List<Driver> populatedDrivers = new ArrayList<>(numberOfDrivers);
        for (int i = 0; i < numberOfDrivers; i++) {

            final String finalSurname = surname + i;
            final String finalName = name + i;
            final String email = format("%s.%s@test.com", finalName, finalSurname);
            //
            Driver driver = new Driver.DriverBuilder().withName(finalName).withSurname(finalSurname)
                    .withDateOfBirth(getRandomDate()).withEmail(emailOf(email))
                    .withAddress(getRandomAddress(i)).withStatus(getRandomStatus())
                    // TODO change random car details to fixture cos it makes querying tests nondeterministicly failing
                    .withCarDetails(getRandomCarDetails(i)).withDriverDetails(randomDriversDetails()).build();

            driver.assignID(DriverId.of(fromString(uuidPrefix + i)));
            driverRepository.storeDriver(driver);
            populatedDrivers.add(driver);
            // print(driver);

        }
        return populatedDrivers;
    }

    public List<PersistenceDriver> populateDbWithDrivers(Integer numberOfDrivers, Status status) {
//    @formatter:off
    List<String> uuidStrings = generateStringUUIDs(numberOfDrivers);
    return uuidStrings.stream()
        .map(uuid -> {
               String surname = getRandomSurname();
               String name = getRandomName();

              return new PersistenceDriver(uuid, name, surname, getRandomDate().toString(),
                                           combineEmail(name, surname), getRandomPersistenceAddress(),
                                           status.toString(), getRandomPersistenceCarDetails(),
                                           randomPersistenceDriversDetails());
             }

        ).map(driver -> {
          driverStore.saveNewDriver(driver);
          return driver;
        }).collect(toList());
    //    @formatter:on
    }

    private String combineEmail(String name, String surname) {
        return format("%s.%s@testmail.com", name, surname);
    }

    private String getRandomSurname() {
        return "surname" + atomicInt.getAndIncrement();
    }

    private String getRandomName() {
        return "name" + atomicInt.getAndDecrement();
    }

    public List<Driver> populateDBWithQueryingData() {
        final List<Driver> populatedDrivers = new ArrayList<>();

        final Driver bartekKowalski = new Driver.DriverBuilder().withName(BARTEK).withSurname(KOWALSKI)
                .withDateOfBirth(getRandomDate()).withEmail(emailOf(BARTEK_KOWALSKI_EMAIL))
                .withAddress(getRandomAddress(0)).withStatus(CREATED).withCarDetails(getCarDetails(0))
                .withDriverDetails(randomDriversDetails()).build();
        UUID bartekKowalskiUUID = randomUUID();
        bartekKowalski.assignID(DriverId.of(bartekKowalskiUUID));
        driverRepository.storeDriver(bartekKowalski);
        populatedDrivers.add(bartekKowalski);

        final Driver tomaszSmith = new Driver.DriverBuilder().withName(TOMASZ).withSurname(SMITH)
                .withDateOfBirth(getRandomDate()).withEmail(emailOf(TOMASZ_SMITH_EMAIL))
                .withAddress(TOMASZ_SMITH_ADDRESS).withStatus(REGISTERED)
                .withCarDetails(getRandomCarDetails(0)).withDriverDetails(randomDriversDetails()).build();
        UUID tomaszSmithUUID = randomUUID();
        tomaszSmith.assignID(DriverId.of(tomaszSmithUUID));
        driverRepository.storeDriver(tomaszSmith);
        populatedDrivers.add(tomaszSmith);

        return populatedDrivers;
    }

    private Status getRandomStatus() {
        return getRandomFrom(asList(values()));
    }

    private DriverDetails randomDriversDetails() {
        return DriverDetails.of(getRandomFrom(asList(PolishDriverType.values())),
                ThreadLocalRandom.current().nextInt(1, 5));
    }

    private PersistenceDriverDetails randomPersistenceDriversDetails() {
        return new PersistenceDriverDetails(getRandomFrom(asList(PolishDriverType.values())).getValue(),
                ThreadLocalRandom.current().nextInt(1, 5));
    }

    private CarDetails getRandomCarDetails(final int i) {
        return CarDetails.of(getRandomMake(), getRandomModel(i), randomYear(), randomMileage(),
                randomPolishCarBody(), randomColour(), randomWindscreen());
    }

    private PersistenceCarDetails getRandomPersistenceCarDetails() {
        return new PersistenceCarDetails(getRandomMake(), getRandomModel(atomicInt.getAndIncrement()),
                randomYear().toString(), randomMileage(),
                randomPolishCarBody().toString(), randomColour().getValue(),
                randomPersistenceWindscreen());
    }

    private CarDetails getCarDetails(final int i) {
        return CarDetails.of(BARTEK_KOWALSKI_MAKE, BARTEK_KOWALSKI_MODEL, randomYear(), randomMileage(),
                randomPolishCarBody(), randomColour(), randomWindscreen());
    }

    private WindscreenDimensions randomWindscreen() {
        return WindscreenDimensions.windscreenDimensionsOf(random3DigitNumber(), random3DigitNumber());
    }

    private PersistenceWindscreenDimensions randomPersistenceWindscreen() {
        return new PersistenceWindscreenDimensions(random3DigitNumber(), random3DigitNumber());
    }

    private Integer random3DigitNumber() {
        return ThreadLocalRandom.current().nextInt(100, 200);
    }

    private Colour randomColour() {
        return Colour.colourOf(getRandomFrom(COLOURS));
    }

    private PolishCarBody randomPolishCarBody() {
        return getRandomFrom(asList(PolishCarBody.values()));
    }

    private Integer randomMileage() {
        return ThreadLocalRandom.current().nextInt(1000, 100000);
    }

    private Year randomYear() {
        return Year.now().minusYears((long) randomDigit());
    }

    private String getRandomModel(final int i) {
        return "car model " + i;
    }

    private String getRandomMake() {
        return getRandomFrom(MAKES);
    }

    private Address getRandomAddress(final int i) {
        return Address
                .of(getRandomPostcode(), getRandomStreet(i), getRandomCity(), getRandomProvince());
    }

    private PersistenceAddress getRandomPersistenceAddress() {
        return new PersistenceAddress(getRandomPostcodeValue(),
                getRandomPersistenceStreet(atomicInt.getAndIncrement()),
                getRandomPersistenceCity(), getRandomProvince().toString());
    }

    private PolishProvince getRandomProvince() {
        return getRandomFrom(asList(PolishProvince.values()));
    }

    private City getRandomCity() {
        return cityOf(getRandomFrom(CITIES).toString());
    }

    private String getRandomPersistenceCity() {
        return getRandomFrom(CITIES).toString();
    }

    private Postcode getRandomPostcode() {
        return postcodeOf(getRandomPostcodeValue());
    }

    private String getRandomPostcodeValue() {
        return format("%d%d-%d%d%d", randomDigit(), randomDigit(), randomDigit(), randomDigit(),
                randomDigit());
    }

    private int randomDigit() {
        return ThreadLocalRandom.current().nextInt(9);
    }

    private Street getRandomStreet(final int i) {
        return streetOf(getRandomFrom(STREETS) + i);
    }

    private String getRandomPersistenceStreet(final int i) {
        return getRandomFrom(STREETS) + i;
    }

    private String getSurnameWith(int i) {
        return getRandomFrom(SURNAMES) + i;
    }

    private String getNameWith(final int i) {
        return getRandomFrom(NAMES) + i;
    }

    private LocalDate getRandomDate() {
        long diff = END_YEAR - START_YEAR + 1;
        Instant instant = Instant.ofEpochSecond(START_YEAR + (long) (Math.random() * diff));
        return LocalDateTime.ofInstant(instant, TIMEZONE).toLocalDate();
    }

    private void print(final Driver driver) {
        System.out.println(driver.getUuid().getValue());
        System.out.println(driver.getName());
        System.out.println(driver.getSurname());
        System.out.println(driver.getDateOfBirth());
        System.out.println(driver.getEmail().getValue());
        System.out.println(driver.getStatus());
        print(driver.getAddress());
        print(driver.getDriverDetails());
        print(driver.getCarDetails());
        System.out.println("------------------------------------");

    }

    private void print(final CarDetails carDetails) {
        System.out.println("CarDetails");
        System.out.println("  " + carDetails.getMake());
        System.out.println("  " + carDetails.getModel());
        System.out.println("  " + carDetails.getMileage().getValue());
        System.out.println("  " + carDetails.getBody());
        System.out.println("  " + carDetails.getYear());
        System.out.println("  " + carDetails.getColour().getValue());
        System.out.println("  " + carDetails.getWindscreenDimensions().getHeight().getValue());
        System.out.println("  " + carDetails.getWindscreenDimensions().getWidth().getValue());
    }

    private void print(final DriverDetails driverDetails) {
        System.out.println("DriverDetails");
        System.out.println("  " + driverDetails.getMonthlyDistanceIndex());
        System.out.println("  " + driverDetails.getType());
    }

    private void print(final Address address) {
        System.out.println("Address");
        System.out.println("  " + address.getCity().getValue());
        System.out.println("  " + address.getStreet().getValue());
        System.out.println("  " + address.getProvince());
        System.out.println("  " + address.getPostcode().getValue());
    }

    private <T> T getRandomFrom(final List<T> list) {
        return list.get(ThreadLocalRandom.current().nextInt(list.size()));
    }

}
