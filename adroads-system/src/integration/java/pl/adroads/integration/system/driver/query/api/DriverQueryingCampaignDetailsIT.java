package pl.adroads.integration.system.driver.query.api;

import static com.jayway.restassured.RestAssured.given;
import static org.apache.http.HttpHeaders.AUTHORIZATION;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;
import static pl.adroads.integration.system.util.CommonITFixtures.CAMPAIGN_DRIVERS_ENDPOINT;
import static pl.adroads.integration.system.util.CommonITFixtures.CONTENT_TYPE_APPLICATION_JSON;
import static pl.adroads.integration.system.util.CommonITFixtures.DRIVERS_QUERY_ENDPOINT;
import static pl.adroads.integration.system.util.CommonITFixtures.FILTER_CRITERIA_RESOURCE_QUERY_PARAM;
import static pl.adroads.integration.system.util.CommonITFixtures.PAGE_SIZE;
import static pl.adroads.integration.system.util.CommonITFixtures.PAGE_SIZE_QUERY_PARAM;
import static pl.adroads.system.domain.driver.model.Status.REGISTERED;
import static pl.adroads.system.domain.driver.model.Status.TAKEN;
import static pl.adroads.system.domain.user.model.UserType.ADMIN;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import pl.adroads.api.model.driver.PagedDriversResource;
import pl.adroads.integration.system.IntegtrationTestCommonSteps;
import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model.PersistenceCampaign;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceDriver;

import com.jayway.restassured.response.Response;

public class DriverQueryingCampaignDetailsIT extends IntegtrationTestCommonSteps {

    private static final String CONTAINING_EMAIL_PATTERN = "{\"sortDir\":\"ASC\",\"email\":\"%s\"}";

    @Before
    public void setup() {
        databaseUtil.truncateDriversTable();
        databaseUtil.truncateUsersTable();
        databaseUtil.truncateCampaignTable();

        token = getLoggedInUser(ADMIN);
    }

    @Test
    public void queried_drivers_contain_campaign_details_for_each_assigned_driver() {

        List<PersistenceCampaign> campaigns = databaseCampaignFixturePopulator.populateDbWithCampaigns(1);
        List<PersistenceDriver> drivers = databaseDriverFixturePopulator.populateDbWithDrivers(1, REGISTERED);
        String campaignIdValue = getFirst(campaigns).getUuid();

        PersistenceDriver driver = getFirst(drivers);
        String driverIdValue = driver.getUuid();
        addPotentialDriverToCampaign(campaignIdValue, driverIdValue);

      //    @formatter:off

       Response response =  given()
            .header(AUTHORIZATION, bearerToken(token))
            .queryParam(FILTER_CRITERIA_RESOURCE_QUERY_PARAM,  filterCriteria(CONTAINING_EMAIL_PATTERN, "testmail.com"))
            .queryParam(PAGE_SIZE_QUERY_PARAM,  PAGE_SIZE)
        .when()
            .get(DRIVERS_QUERY_ENDPOINT)
        .then()
            .statusCode(SC_OK)
               .extract().response();
      
        //    @formatter:on
        PagedDriversResource responseBody = responseAs(response, PagedDriversResource.class);
        assertThat(responseBody.getDrivers(), hasSize(1));

        assertThat(responseBody.getDrivers().get(0).getDriverCampaignDetailsResource(), is(notNullValue()));
        assertThat(responseBody.getDrivers().get(0).getDriverCampaignDetailsResource().getCampaignId(),
                is(campaigns.get(0).getUuid()));
        assertThat(responseBody.getDrivers().get(0).getDriverCampaignDetailsResource().getCampaignName(),
                is(campaigns.get(0).getName()));
        assertThat(responseBody.getDrivers().get(0).getStatus(), is(TAKEN.name()));
    }

    @Test
    public void queried_drivers_contain_no_campaign_details_if_driver_not_assigned() {

        List<PersistenceCampaign> campaigns = databaseCampaignFixturePopulator.populateDbWithCampaigns(1);
        List<PersistenceDriver> drivers = databaseDriverFixturePopulator.populateDbWithDrivers(1, REGISTERED);
        String campaignIdValue = getFirst(campaigns).getUuid();

        PersistenceDriver driver = getFirst(drivers);
        String driverIdValue = driver.getUuid();

    //    @formatter:off

       Response response =  given()
            .header(AUTHORIZATION, bearerToken(token))
            .queryParam(FILTER_CRITERIA_RESOURCE_QUERY_PARAM,  filterCriteria(CONTAINING_EMAIL_PATTERN, "testmail.com"))
            .queryParam(PAGE_SIZE_QUERY_PARAM,  PAGE_SIZE)
        .when()
            .get(DRIVERS_QUERY_ENDPOINT)
        .then()
            .statusCode(SC_OK)
               .extract().response();
      
        //    @formatter:on
        PagedDriversResource responseBody = responseAs(response, PagedDriversResource.class);
        assertThat(responseBody.getDrivers(), hasSize(1));

        assertThat(responseBody.getDrivers().get(0).getDriverCampaignDetailsResource(), is(nullValue()));
    }

    private void addPotentialDriverToCampaign(final String campaignIdValue, final String driverIdValue) {
    //    @formatter:off

          given()
              .header(AUTHORIZATION, bearerToken(token))
              .pathParam("campaignId", campaignIdValue)
              .pathParam("driverId", driverIdValue)
              .header(CONTENT_TYPE_APPLICATION_JSON)
          .when()
              .put(CAMPAIGN_DRIVERS_ENDPOINT)
          .then()
              .statusCode(SC_OK);

        //    @formatter:on
    }

}
