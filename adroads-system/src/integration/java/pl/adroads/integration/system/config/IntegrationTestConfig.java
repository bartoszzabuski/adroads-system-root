package pl.adroads.integration.system.config;

import org.skife.jdbi.v2.DBI;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import pl.adroads.integration.system.util.DatabaseDriverFixturePopulator;
import pl.adroads.integration.system.util.TestingDatabaseUtilDAO;
import pl.adroads.system.config.RootApplicationConfig;

import javax.inject.Inject;

@Configuration
@Import(RootApplicationConfig.class)
public class IntegrationTestConfig {

  @Inject
  private DBI dbi;

  @Bean
  public TestingDatabaseUtilDAO databaseUtil() {
    return dbi.onDemand(TestingDatabaseUtilDAO.class);
  }

}
