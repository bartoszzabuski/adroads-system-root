package pl.adroads.integration.system.campaign.api;

import static com.google.common.collect.Lists.newArrayList;
import static com.jayway.restassured.RestAssured.given;
import static java.lang.Boolean.TRUE;
import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static jersey.repackaged.com.google.common.collect.Iterables.getLast;
import static org.apache.http.HttpHeaders.AUTHORIZATION;
import static org.apache.http.HttpStatus.SC_CONFLICT;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static pl.adroads.integration.system.util.CommonITFixtures.*;
import static pl.adroads.integration.system.util.CommonITFixtures.CAMPAIGN_DRIVERS_ENDPOINT;
import static pl.adroads.integration.system.util.CommonITFixtures.RANDOM_UUID_1;
import static pl.adroads.integration.system.util.CommonITFixtures.RANDOM_UUID_2;
import static pl.adroads.system.domain.campaign.campaigndriverstatus.CampaignDriverStatus.DRIVER_APPROVAL_PENDING;
import static pl.adroads.system.domain.driver.model.Status.CREATED;
import static pl.adroads.system.domain.driver.model.Status.REGISTERED;
import static pl.adroads.system.domain.driver.model.Status.TAKEN;
import static pl.adroads.system.domain.user.model.UserType.ADMIN;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import pl.adroads.integration.system.IntegtrationTestCommonSteps;
import pl.adroads.integration.system.util.CommonITFixtures;
import pl.adroads.integration.system.util.DatabaseCampaignFixturePopulator;
import pl.adroads.system.application.user.email.EmailService;
import pl.adroads.system.domain.campaign.model.Campaign;
import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model.PersistenceCampaign;
import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model.PersistenceCampaignDriver;
import pl.adroads.system.infrastructure.adapters.driving.driver.persistance.model.PersistenceDriver;
import pl.adroads.system.infrastructure.adapters.driving.user.persistence.model.PersistenceVerificationToken;

import com.jayway.restassured.response.Header;

public class AddDriverToCampaignIT extends IntegtrationTestCommonSteps {

    public static final PersistenceVerificationToken NULL_PERSISTENCE_VERIFICATION_TOKEN = null;
    @Inject
    private DatabaseCampaignFixturePopulator databaseCampaignFixturePopulator;

    @Inject
    private EmailService emailService;
    private ArgumentCaptor<Campaign> campaignCaptor = ArgumentCaptor.forClass(Campaign.class);
    private ArgumentCaptor<Driver> driverCaptor = ArgumentCaptor.forClass(Driver.class);

    @Before
    public void setup() {
        databaseUtil.truncateCampaignTable();
        databaseUtil.truncateDriversTable();
        databaseUtil.truncateUsersTable();
        Mockito.reset(emailService);
        token = getLoggedInUser(ADMIN);
    }

    @Test
    public void add_potential_driver_to_campaign_results_in_driver_being_added() {

    //    @formatter:off

    List<PersistenceCampaign> campaigns = databaseCampaignFixturePopulator.populateDbWithCampaigns(1);
    int numberOfDrivers = 2;
    List<PersistenceDriver> drivers = databaseDriverFixturePopulator.populateDbWithDrivers(numberOfDrivers, REGISTERED);
    String campaignIdValue = getFirst(campaigns).getUuid();

    for (PersistenceDriver driver : drivers) {
    String driverIdValue = driver.getUuid();

          given()
              .header(AUTHORIZATION, bearerToken(token))
              .pathParam("campaignId", campaignIdValue)
              .pathParam("driverId", driverIdValue)
              .header(CONTENT_TYPE_APPLICATION_JSON)
          .when()
              .put(CAMPAIGN_DRIVERS_ENDPOINT)
          .then()
              .statusCode(SC_OK);

      }


    Optional<PersistenceCampaign> campaign = campaignStorage.get(campaignIdValue);assertThat(campaign.isPresent(), is(TRUE));
    assertThat(campaign.get().getDrivers(), hasSize(numberOfDrivers));
//        TODO add assertation on element campaign driver's fields because atm equals is used which asserts on IDs
    assertThat(campaign.get().getDrivers(), containsInAnyOrder(getExpectedPersistenceDrivers(drivers).toArray()));
        
        //    @formatter:on
    }

    @Test
    public void add_potential_driver_to_campaign_results_in_email_being_sent_to_driver() {

    //    @formatter:off

    PersistenceCampaign campaign = databaseCampaignFixturePopulator.populateDbWithCampaigns(1).get(0);
    PersistenceDriver driver = databaseDriverFixturePopulator.populateDbWithDrivers(1, REGISTERED).get(0);
    String campaignIdValue = campaign.getUuid();

    String driverIdValue = driver.getUuid();

          given()
              .header(AUTHORIZATION, bearerToken(token))
              .pathParam("campaignId", campaignIdValue)
              .pathParam("driverId", driverIdValue)
              .header(CONTENT_TYPE_APPLICATION_JSON)
          .when()
              .put(CAMPAIGN_DRIVERS_ENDPOINT)
          .then()
              .statusCode(SC_OK);
        //    @formatter:on

        Optional<PersistenceCampaign> foundCampaign = campaignStorage.get(campaignIdValue);
        assertThat(foundCampaign.isPresent(), is(TRUE));
        assertThat(foundCampaign.get().getDrivers(), hasSize(1));
        // TODO add assertation on element campaign driver's fields because atm equals is used which asserts on IDs
        assertThat(foundCampaign.get().getDrivers(),
                containsInAnyOrder(getExpectedPersistenceDrivers(newArrayList(driver)).toArray()));

        verify(emailService).sendEmailToAssignedDriver(driverCaptor.capture(), campaignCaptor.capture());
        assertThat(driverCaptor.getValue().getUuid().getValue().toString(), is(driver.getUuid()));
        assertThat(campaignCaptor.getValue().getUuid().getValue().toString(), is(campaign.getUuid()));
    }

    @Test
    public void add_potential_driver_to_campaign_results_in_driver_context_being_updated() throws InterruptedException {

    //    @formatter:off

    PersistenceCampaign campaign = databaseCampaignFixturePopulator.populateDbWithCampaigns(1).get(0);
    PersistenceDriver driver = databaseDriverFixturePopulator.populateDbWithDrivers(1, REGISTERED).get(0);
    String campaignIdValue = campaign.getUuid();
    String driverIdValue = driver.getUuid();
        
          given()
              .header(AUTHORIZATION, bearerToken(token))
              .pathParam("campaignId", campaignIdValue)
              .pathParam("driverId", driverIdValue)
              .header(CONTENT_TYPE_APPLICATION_JSON)
          .when()
              .put(CAMPAIGN_DRIVERS_ENDPOINT)
          .then()
              .statusCode(SC_OK);
        //    @formatter:on

        Thread.currentThread().sleep(1000);
        Optional<PersistenceDriver> foundDriver = driverStorage.findDriverById(driverIdValue);
        assertThat(foundDriver.isPresent(), is(TRUE));
        assertThat(foundDriver.get().getStatus(), is(TAKEN.name()));
    }

    // unhappy paths
    // - driver not taken???

    @Test
    public void when_driver_taken_to_other_campaign_then_add_potential_driver_fails() {
    //    @formatter:off

    List<PersistenceCampaign> campaigns = databaseCampaignFixturePopulator.populateDbWithCampaigns(1);
    int numberOfDrivers = 1;
    List<PersistenceDriver> drivers = databaseDriverFixturePopulator.populateDbWithDrivers(numberOfDrivers, TAKEN);
    String campaignIdValue = getFirst(campaigns).getUuid();

    String driverIdValue = getFirst(drivers).getUuid();

          given()
              .header(AUTHORIZATION, bearerToken(token))
              .pathParam("campaignId", campaignIdValue)
              .pathParam("driverId", driverIdValue)
              .header(CONTENT_TYPE_APPLICATION_JSON)
          .when()
              .put(CAMPAIGN_DRIVERS_ENDPOINT)
          .then()
              .statusCode(SC_CONFLICT);

    Optional<PersistenceCampaign> campaign = campaignStorage.get(campaignIdValue);
    assertThat(campaign.isPresent(), is(TRUE));
    assertThat(campaign.get().getDrivers(), is(empty()));

                verifyZeroInteractions(emailService);

        //    @formatter:on
    }

    @Test
    public void when_driver_not_fully_registered_then_add_potential_driver_fails() {
    //    @formatter:off

    List<PersistenceCampaign> campaigns = databaseCampaignFixturePopulator.populateDbWithCampaigns(1);
    int numberOfDrivers = 1;
    List<PersistenceDriver> drivers = databaseDriverFixturePopulator.populateDbWithDrivers(numberOfDrivers, CREATED);
    String campaignIdValue = getFirst(campaigns).getUuid();

    String driverIdValue = getFirst(drivers).getUuid();
        assertThat(getFirst(drivers).getStatus(), is(CREATED.toString()));

          given()
              .header(AUTHORIZATION, bearerToken(token))
              .pathParam("campaignId", campaignIdValue)
              .pathParam("driverId", driverIdValue)
              .header(CONTENT_TYPE_APPLICATION_JSON)
          .when()
              .put(CAMPAIGN_DRIVERS_ENDPOINT)
          .then()
              .statusCode(SC_CONFLICT);

    Optional<PersistenceCampaign> campaign = campaignStorage.get(campaignIdValue);
    assertThat(campaign.isPresent(), is(TRUE));
    assertThat(campaign.get().getDrivers(), is(empty()));

        verifyZeroInteractions(emailService);
        //    @formatter:on
    }

    @Test
    public void when_campaign_does_not_exist_then_add_potential_driver_fails() {

        int numberOfDrivers = 1;
        List<PersistenceDriver> drivers = databaseDriverFixturePopulator.populateDbWithDrivers(numberOfDrivers,
                REGISTERED);

        String driverIdValue = getFirst(drivers).getUuid();

        given()
                .header(AUTHORIZATION, bearerToken(token))
                .pathParam("campaignId", RANDOM_UUID_1.toString())
                .pathParam("driverId", driverIdValue)
                .header(CONTENT_TYPE_APPLICATION_JSON)
                .when()
                .put(CAMPAIGN_DRIVERS_ENDPOINT)
                .then()
                .statusCode(SC_NOT_FOUND);

    }

    @Test
    public void when_driver_does_not_exist_then_add_potential_driver_fails() {

    //    @formatter:off

          given()
              .header(AUTHORIZATION, bearerToken(token))
              .pathParam("campaignId", RANDOM_UUID_1.toString())
              .pathParam("driverId", RANDOM_UUID_2.toString())
              .header(CONTENT_TYPE_APPLICATION_JSON)
          .when()
              .put(CAMPAIGN_DRIVERS_ENDPOINT)
          .then()
              .statusCode(SC_NOT_FOUND);

        //    @formatter:on
    }

    private List<PersistenceCampaignDriver> getExpectedPersistenceDrivers(
            final List<PersistenceDriver> drivers) {
        return drivers.stream().map(driver -> new PersistenceCampaignDriver(driver.getUuid().toString(),
                driver.getEmail(), DRIVER_APPROVAL_PENDING.toString(), NULL_PERSISTENCE_VERIFICATION_TOKEN))
                .collect(Collectors.toList());
    }

    private String extractStringId(final String location) {
        return getLast(newArrayList(location.split("/")));
    }
}
