package pl.adroads.integration.system.driver.query.api;

import static com.jayway.restassured.RestAssured.given;
import static java.lang.String.format;
import static org.apache.http.HttpHeaders.AUTHORIZATION;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static pl.adroads.integration.system.util.CommonITFixtures.DRIVERS_QUERY_ENDPOINT;
import static pl.adroads.integration.system.util.CommonITFixtures.FILTER_CRITERIA_RESOURCE_QUERY_PARAM;
import static pl.adroads.integration.system.util.CommonITFixtures.ONE;
import static pl.adroads.integration.system.util.CommonITFixtures.PAGE_SIZE;
import static pl.adroads.integration.system.util.CommonITFixtures.PAGE_SIZE_QUERY_PARAM;
import static pl.adroads.integration.system.util.DatabaseDriverFixturePopulator.BARTEK;
import static pl.adroads.integration.system.util.DatabaseDriverFixturePopulator.BARTEK_KOWALSKI_EMAIL;
import static pl.adroads.integration.system.util.DatabaseDriverFixturePopulator.BARTEK_KOWALSKI_MAKE;
import static pl.adroads.integration.system.util.DatabaseDriverFixturePopulator.BARTEK_KOWALSKI_MODEL;
import static pl.adroads.integration.system.util.DatabaseDriverFixturePopulator.KOWALSKI;
import static pl.adroads.integration.system.util.DatabaseDriverFixturePopulator.TOMASZ_SMITH_CITY_VALUE;
import static pl.adroads.integration.system.util.DatabaseDriverFixturePopulator.TOMASZ_SMITH_EMAIL;
import static pl.adroads.integration.system.util.DatabaseDriverFixturePopulator.TOMASZ_SMITH_POSTCODE_VALUE;
import static pl.adroads.integration.system.util.DatabaseDriverFixturePopulator.TOMASZ_SMITH_PROVINCE;
import static pl.adroads.integration.system.util.DatabaseDriverFixturePopulator.TOMASZ_SMITH_STREET_VALUE;
import static pl.adroads.system.domain.driver.model.Status.CREATED;
import static pl.adroads.system.domain.user.model.UserType.ADMIN;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import pl.adroads.integration.system.IntegtrationTestCommonSteps;
import pl.adroads.system.domain.driver.model.Driver;

import com.jayway.restassured.response.Response;

public class DriverQueryingIT extends IntegtrationTestCommonSteps {

    private static final String CONTAINING_NAME_PATTERN = "{\"sortDir\":\"ASC\",\"name\":\"%s\"}";
    private static final String CONTAINING_SURNAME_PATTERN = "{\"sortDir\":\"ASC\",\"surname\":\"%s\"}";
    private static final String CONTAINING_EMAIL_PATTERN = "{\"sortDir\":\"ASC\",\"email\":\"%s\"}";
    private static final String CONTAINING_STATUS_PATTERN = "{\"sortDir\":\"ASC\",\"status\":\"%s\"}";
    private static final String CONTAINING_STREET_PATTERN = "{\"sortDir\":\"ASC\",\"street\":\"%s\"}";
    private static final String CONTAINING_CITY_PATTERN = "{\"sortDir\":\"ASC\",\"city\":\"%s\"}";
    private static final String CONTAINING_CAR_MODEL_PATTERN = "{\"sortDir\":\"ASC\",\"carModel\":\"%s\"}";
    private static final String CONTAINING_CAR_MAKE_PATTERN = "{\"sortDir\":\"ASC\",\"carMake\":\"%s\"}";
    private static final String CONTAINING_PROVINCE_PATTERN = "{\"sortDir\":\"ASC\",\"province\":\"%s\"}";
    private static final String CONTAINING_POSTCODE_PATTERN = "{\"sortDir\":\"ASC\",\"postcode\":\"%s\"}";
    private static final String DRIVERS_JP = "drivers";
    private static final String FIRST_DRIVER_EMAIL_JP = "drivers[0].email";

    private ExpectedResultHolder expectedResultHolder = new ExpectedResultHolder();

    @Before
    public void setup() {
        databaseUtil.truncateDriversTable();
        databaseUtil.truncateUsersTable();
        List<Driver> populatedDrivers = databaseDriverFixturePopulator.populateDBWithQueryingData();
        expectedResultHolder.setResultList(populatedDrivers);

        token = getLoggedInUser(ADMIN);
    }

    @Test
    public void test_querying_by_name_returns_expected_driver() {

        //    @formatter:off

    given()
        .header(AUTHORIZATION, bearerToken(token))
        .queryParam(FILTER_CRITERIA_RESOURCE_QUERY_PARAM,  filterCriteria(CONTAINING_NAME_PATTERN, BARTEK))
        .queryParam(PAGE_SIZE_QUERY_PARAM,  PAGE_SIZE)
    .when()
        .get(DRIVERS_QUERY_ENDPOINT)
    .then()
        .statusCode(SC_OK)
        .body(FIRST_DRIVER_EMAIL_JP, is(BARTEK_KOWALSKI_EMAIL))
        .body(DRIVERS_JP, hasSize(ONE));

        //    @formatter:on
    }

    @Test
    public void test_querying_by_surname_returns_expected_driver() {

        //    @formatter:off

    given()
         .header(AUTHORIZATION, bearerToken(token))
         .queryParam(FILTER_CRITERIA_RESOURCE_QUERY_PARAM,  filterCriteria(CONTAINING_SURNAME_PATTERN, KOWALSKI))
         .queryParam(PAGE_SIZE_QUERY_PARAM,  PAGE_SIZE)
    .when()
        .get(DRIVERS_QUERY_ENDPOINT)
    .then()
        .statusCode(SC_OK)
        .body(FIRST_DRIVER_EMAIL_JP, is(BARTEK_KOWALSKI_EMAIL))
        .body(DRIVERS_JP, hasSize(ONE));

        //    @formatter:on

    }

    @Test
    public void test_querying_by_email_returns_expected_driver() {

        //    @formatter:off

    given()
         .header(AUTHORIZATION, bearerToken(token))
         .queryParam(FILTER_CRITERIA_RESOURCE_QUERY_PARAM,  filterCriteria(CONTAINING_EMAIL_PATTERN, BARTEK_KOWALSKI_EMAIL.substring(3,6)))
         .queryParam(PAGE_SIZE_QUERY_PARAM,  PAGE_SIZE)
    .when()
        .get(DRIVERS_QUERY_ENDPOINT)
    .then()
        .statusCode(SC_OK)
        .body(FIRST_DRIVER_EMAIL_JP, is(BARTEK_KOWALSKI_EMAIL))
        .body(DRIVERS_JP, hasSize(ONE));

        //    @formatter:on
    }

    @Test
    public void test_querying_by_status_returns_expected_driver() {

        //    @formatter:off

    given()
        .header(AUTHORIZATION, bearerToken(token))
        .queryParam(FILTER_CRITERIA_RESOURCE_QUERY_PARAM,  filterCriteria(CONTAINING_STATUS_PATTERN, CREATED.toString()))
        .queryParam(PAGE_SIZE_QUERY_PARAM,  PAGE_SIZE)
    .when()
        .get(DRIVERS_QUERY_ENDPOINT)
    .then()
        .statusCode(SC_OK)
            .body(FIRST_DRIVER_EMAIL_JP, is(BARTEK_KOWALSKI_EMAIL))
            .body(DRIVERS_JP, hasSize(ONE));

        //    @formatter:on
    }

    @Test
    public void test_querying_by_postcode_returns_expected_driver() {

        //    @formatter:off

    given()
        .header(AUTHORIZATION, bearerToken(token))
        .queryParam(FILTER_CRITERIA_RESOURCE_QUERY_PARAM,  filterCriteria(CONTAINING_POSTCODE_PATTERN, beginningOf(TOMASZ_SMITH_POSTCODE_VALUE)))
        .queryParam(PAGE_SIZE_QUERY_PARAM,  PAGE_SIZE)
    .when()
        .get(DRIVERS_QUERY_ENDPOINT)
    .then()
        .statusCode(SC_OK)
            .body(FIRST_DRIVER_EMAIL_JP, is(TOMASZ_SMITH_EMAIL))
            .body(DRIVERS_JP, hasSize(ONE));

        //    @formatter:on
    }

    @Test
    public void test_querying_by_street_returns_expected_driver() {

        //    @formatter:off

    given()
        .header(AUTHORIZATION, bearerToken(token))
        .queryParam(FILTER_CRITERIA_RESOURCE_QUERY_PARAM,  filterCriteria(CONTAINING_STREET_PATTERN, beginningOf(TOMASZ_SMITH_STREET_VALUE)))
        .queryParam(PAGE_SIZE_QUERY_PARAM,  PAGE_SIZE)
    .when()
        .get(DRIVERS_QUERY_ENDPOINT)
    .then()
        .statusCode(SC_OK)
            .body(FIRST_DRIVER_EMAIL_JP, is(TOMASZ_SMITH_EMAIL))
            .body(DRIVERS_JP, hasSize(ONE));

        //    @formatter:on
    }

    @Test
    public void test_querying_by_city_returns_expected_driver() {

        //    @formatter:off

    given()
        .header(AUTHORIZATION, bearerToken(token))
        .queryParam(FILTER_CRITERIA_RESOURCE_QUERY_PARAM,  filterCriteria(CONTAINING_CITY_PATTERN, beginningOf(TOMASZ_SMITH_CITY_VALUE)))
        .queryParam(PAGE_SIZE_QUERY_PARAM,  PAGE_SIZE)
    .when()
        .get(DRIVERS_QUERY_ENDPOINT)
    .then()
        .statusCode(SC_OK)
            .body(FIRST_DRIVER_EMAIL_JP, is(TOMASZ_SMITH_EMAIL))
            .body(DRIVERS_JP, hasSize(ONE));

        //    @formatter:on
    }

    @Test
    public void test_querying_by_province_returns_expected_driver() {

        //    @formatter:off

    given()
        .header(AUTHORIZATION, bearerToken(token))
        .queryParam(FILTER_CRITERIA_RESOURCE_QUERY_PARAM,  filterCriteria(CONTAINING_PROVINCE_PATTERN, TOMASZ_SMITH_PROVINCE.toString()))
        .queryParam(PAGE_SIZE_QUERY_PARAM,  PAGE_SIZE)
    .when()
        .get(DRIVERS_QUERY_ENDPOINT)
    .then()
        .statusCode(SC_OK)
            .body(FIRST_DRIVER_EMAIL_JP, is(TOMASZ_SMITH_EMAIL))
            .body(DRIVERS_JP, hasSize(ONE));

        //    @formatter:on
    }

    @Test
    public void test_querying_by_car_model_returns_expected_driver() {

        //    @formatter:off

    given()
        .header(AUTHORIZATION, bearerToken(token))
        .queryParam(FILTER_CRITERIA_RESOURCE_QUERY_PARAM,  filterCriteria(CONTAINING_CAR_MODEL_PATTERN, BARTEK_KOWALSKI_MODEL))
        .queryParam(PAGE_SIZE_QUERY_PARAM,  PAGE_SIZE)
    .when()
        .get(DRIVERS_QUERY_ENDPOINT)
    .then()
        .statusCode(SC_OK)
            .body(FIRST_DRIVER_EMAIL_JP, is(BARTEK_KOWALSKI_EMAIL))
            .body(DRIVERS_JP, hasSize(ONE));

        //    @formatter:on
    }

    @Test
    public void test_querying_by_car_make_returns_expected_driver() {

        //    @formatter:off

    given()
        .header(AUTHORIZATION, bearerToken(token))
        .queryParam(FILTER_CRITERIA_RESOURCE_QUERY_PARAM,  filterCriteria(CONTAINING_CAR_MAKE_PATTERN, BARTEK_KOWALSKI_MAKE))
        .queryParam(PAGE_SIZE_QUERY_PARAM,  PAGE_SIZE)
    .when()
        .get(DRIVERS_QUERY_ENDPOINT)
    .then()
        .statusCode(SC_OK)
            .body(FIRST_DRIVER_EMAIL_JP, is(BARTEK_KOWALSKI_EMAIL))
            .body(DRIVERS_JP, hasSize(ONE));

        //    @formatter:on
    }

    // TODO
    // write tests przebieg od do
    // typy samochodow
    // color

    private String beginningOf(final String string) {
        return string.substring(0, 2);
    }

    private void debugPrint(final Response response) {
        System.err.println("body: " + response.getBody().print());
    }

}
