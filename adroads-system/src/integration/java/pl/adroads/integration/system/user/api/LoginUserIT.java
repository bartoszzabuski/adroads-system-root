package pl.adroads.integration.system.user.api;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.http.ContentType.URLENC;
import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_NOT_FOUND;
import static org.apache.http.HttpStatus.SC_OK;
import static org.apache.http.HttpStatus.SC_UNAUTHORIZED;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static pl.adroads.integration.system.util.CommonITFixtures.FORM_PARAM_LOGIN_HEADER;
import static pl.adroads.integration.system.util.CommonITFixtures.FORM_PARAM_PASSWORD_HEADER;
import static pl.adroads.integration.system.util.CommonITFixtures.USERS_LOGIN_ENDPOINT;
import static pl.adroads.integration.system.util.DatabaseUserFixturePopulator.ADMIN_EMAIL;
import static pl.adroads.integration.system.util.DatabaseUserFixturePopulator.ADMIN_PASSWORD;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;

import pl.adroads.api.model.user.TokenResource;
import pl.adroads.integration.system.IntegtrationTestCommonSteps;
import pl.adroads.integration.system.util.DatabaseUserFixturePopulator;
import pl.adroads.system.infrastructure.adapters.driving.user.persistence.model.PersistenceUser;

import com.jayway.restassured.response.Response;

public class LoginUserIT extends IntegtrationTestCommonSteps {

    @Inject
    private DatabaseUserFixturePopulator userFixturePopulator;

    @Before
    public void setup() {
        databaseUtil.truncateUsersTable();
    }

    @Test
    public void when_no_form_params_provided_then_login_throws_bad_request() {

        given()
                .contentType(URLENC)
                .when()
                .post(USERS_LOGIN_ENDPOINT)
                .then()
                .statusCode(SC_BAD_REQUEST);

    }

    @Test
    public void when_email_is_invalid_then_login_throws_bad_request() {

        given()
                .contentType(URLENC)
                .formParam(FORM_PARAM_LOGIN_HEADER, "bartek.sdfsfsfdsf")
                .formParam(FORM_PARAM_PASSWORD_HEADER, "admin")
                .when()
                .post(USERS_LOGIN_ENDPOINT)
                .then()
                .statusCode(SC_BAD_REQUEST);

    }

    @Test
    public void when_user_doesnt_exists_then_login_throws_not_found() {

        given()
                .contentType(URLENC)
                .formParam(FORM_PARAM_LOGIN_HEADER, "bartek.zabuski@gmail.com")
                .formParam(FORM_PARAM_PASSWORD_HEADER, "admin")
                .when()
                .post(USERS_LOGIN_ENDPOINT)
                .then()
                .statusCode(SC_NOT_FOUND);
    }

    @Test
    public void when_password_is_not_valid_then_login_throws_unauthorized() {

        PersistenceUser user = userFixturePopulator.populateDbWithAdmin();
        Response response = given()
                .contentType(URLENC)
                .formParam(FORM_PARAM_LOGIN_HEADER, ADMIN_EMAIL)
                .formParam(FORM_PARAM_PASSWORD_HEADER, ADMIN_PASSWORD + "123123")
                .when()
                .post(USERS_LOGIN_ENDPOINT);

        assertThat(response.getStatusCode(), is(SC_UNAUTHORIZED));

    }

    @Test
    public void when_credentials_are_valid_and_user_exists_then_login_returns_ok() {

        PersistenceUser user = userFixturePopulator.populateDbWithAdmin();
        Response response = given()
                .contentType(URLENC)
                .formParam(FORM_PARAM_LOGIN_HEADER, ADMIN_EMAIL)
                .formParam(FORM_PARAM_PASSWORD_HEADER, ADMIN_PASSWORD)
                .when()
                .post(USERS_LOGIN_ENDPOINT);

        assertThat(response.getStatusCode(), is(SC_OK));
        TokenResource tokenResourceResponse = responseAs(response, TokenResource.class);
        assertThat(tokenResourceResponse.getToken(), is(notNullValue()));

    }
}
