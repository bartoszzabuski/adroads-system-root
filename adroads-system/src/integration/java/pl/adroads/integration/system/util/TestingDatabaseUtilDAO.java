package pl.adroads.integration.system.util;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

@UseStringTemplate3StatementLocator
public interface TestingDatabaseUtilDAO {

    // TODO fix this and use instead of truncateTable()
    @SqlUpdate("truncate :table")
    void truncateTable(@Bind("table") String table);
    //
    // @SqlUpdate("truncate tbl_drivers")
    // void truncateDriversTable();

    @SqlUpdate("truncate tbl_campaigns")
    void truncateCampaignTable();

    @SqlUpdate("truncate tbl_users")
    void truncateUsersTable();

    @SqlUpdate("truncate tbl_drivers")
    void truncateDriversTable();
}
