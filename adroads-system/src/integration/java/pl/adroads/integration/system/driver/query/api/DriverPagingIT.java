package pl.adroads.integration.system.driver.query.api;

import static com.google.common.collect.Iterables.getLast;
import static com.jayway.restassured.RestAssured.given;
import static java.lang.String.format;
import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static pl.adroads.integration.system.util.CommonITFixtures.DRIVERS_QUERY_ENDPOINT;
import static pl.adroads.integration.system.util.CommonITFixtures.FILTER_CRITERIA_RESOURCE_QUERY_PARAM;
import static pl.adroads.integration.system.util.CommonITFixtures.PAGE_SIZE;
import static pl.adroads.integration.system.util.CommonITFixtures.PAGE_SIZE_QUERY_PARAM;
import static pl.adroads.integration.system.util.CommonITFixtures.QUERY_ENDPOINT_PAGE;
import static pl.adroads.integration.system.util.CommonITFixtures.THREE;
import static pl.adroads.integration.system.util.CommonITFixtures.TWO;
import static pl.adroads.integration.system.util.CommonITFixtures.UUID_QUERY_PARAM;
import static pl.adroads.system.domain.user.model.UserType.ADMIN;

import java.util.List;
import java.util.function.Predicate;

import org.junit.Before;
import org.junit.Test;

import pl.adroads.api.model.driver.DriverResource;
import pl.adroads.api.model.driver.PagedDriversResource;
import pl.adroads.api.model.shared.PageIndexResource;
import pl.adroads.integration.system.IntegtrationTestCommonSteps;
import pl.adroads.system.domain.driver.model.Driver;

import com.jayway.restassured.response.Response;

public class DriverPagingIT extends IntegtrationTestCommonSteps {

    private static final int REQUIRED_NUMBER_OF_DRIVERS = 12;
    private static final String NAME = "tomasz";
    private static final String SURNAME = "kowalski";
    private static final String UUID_PREFIX = "0fb579d6-9479-4e6d-a8d3-8faf39cd5da";
    private static final String FILTER_CRITERIA_CONTAINING_NAME_JSON_PATTERN = "{\"sortDir\":\"ASC\",\"email\":\"%s\"}";

    private ExpectedResultHolder expectedResultHolder = new ExpectedResultHolder();

    @Before
    public void setup() {
        databaseUtil.truncateDriversTable();
        databaseUtil.truncateUsersTable();
        List<Driver> populatedDrivers = databaseDriverFixturePopulator
                .populateDBWithPagingData(UUID_PREFIX, NAME, SURNAME, REQUIRED_NUMBER_OF_DRIVERS);
        expectedResultHolder.setResultList(populatedDrivers);

        token = getLoggedInUser(ADMIN);
    }

    @Test
    public void test_getFirstPage_returns_expected_drivers() {

    //    @formatter:off

    final Response response =
        given()
        .header(AUTHORIZATION, bearerToken(token))
        .queryParam(FILTER_CRITERIA_RESOURCE_QUERY_PARAM,  filterCriteriaResourceContainingName(NAME))
        .queryParam(PAGE_SIZE_QUERY_PARAM,  PAGE_SIZE)
    .when()
        .get(DRIVERS_QUERY_ENDPOINT)
    .then()
        .statusCode(SC_OK)
        .extract().response();

        //    @formatter:on

        PagedDriversResource pagedDriversResource = responseAs(response, PagedDriversResource.class);

        assertThat(pagedDriversResource.getMetadata().getItemCount(), is(PAGE_SIZE));
        assertThat(pagedDriversResource.getMetadata().getPageMetadataList(), hasSize(TWO));
        assertThat(getFirst(pagedDriversResource.getMetadata().getPageMetadataList()).getPageNumber(),
                is(TWO));
        assertThat(getFirst(pagedDriversResource.getMetadata().getPageMetadataList()).getUuid(),
                is(UUID_PREFIX + 4));
        assertThat(getLast(pagedDriversResource.getMetadata().getPageMetadataList()).getPageNumber(),
                is(THREE));
        assertThat(getLast(pagedDriversResource.getMetadata().getPageMetadataList()).getUuid(),
                is(UUID_PREFIX + 9));

        assertFetchedDrivers(pagedDriversResource.getDrivers(), PAGE_SIZE, 0);
    }

    private String filterCriteriaResourceContainingName(final String name) {
        return format(FILTER_CRITERIA_CONTAINING_NAME_JSON_PATTERN, name);
    }

    @Test
    public void test_getSubsequentPage_returns_second_page_with_expected_drivers() {

        final int pageNumber = 2;
        final String criteriaResourceContainingName = filterCriteriaResourceContainingName(NAME);
        final List<PageIndexResource> firstPageIndexResources = getMetadataForQuery(
                criteriaResourceContainingName);

    //    @formatter:off

    final Response response =
        given()
        .header(AUTHORIZATION, bearerToken(token))
        .queryParam(FILTER_CRITERIA_RESOURCE_QUERY_PARAM, criteriaResourceContainingName)
        .queryParam(PAGE_SIZE_QUERY_PARAM, PAGE_SIZE)
        .queryParam(UUID_QUERY_PARAM, fetchUuidForPage(pageNumber, firstPageIndexResources))
    .when()
        .get(QUERY_ENDPOINT_PAGE)
    .then()
        .statusCode(SC_OK)
        .extract()
            .response();

        //    @formatter:on

        PagedDriversResource pagedDriversResource = responseAs(response, PagedDriversResource.class);

        assertThat(pagedDriversResource.getMetadata(), is(nullValue()));

        assertFetchedDrivers(pagedDriversResource.getDrivers(), PAGE_SIZE, PAGE_SIZE);
    }

    @Test
    public void test_getSubsequentPage_returns_third_page_with_expected_drivers() {

        final int pageNumber = 3;
        final String criteriaResourceContainingName = filterCriteriaResourceContainingName(NAME);
        final List<PageIndexResource> firstPageIndexResources = getMetadataForQuery(
                criteriaResourceContainingName);

    //    @formatter:off

    final Response response =
        given()
        .header(AUTHORIZATION, bearerToken(token))
        .queryParam(FILTER_CRITERIA_RESOURCE_QUERY_PARAM, criteriaResourceContainingName)
        .queryParam(PAGE_SIZE_QUERY_PARAM, PAGE_SIZE)
        .queryParam(UUID_QUERY_PARAM, fetchUuidForPage(pageNumber, firstPageIndexResources))
    .when()
        .get(QUERY_ENDPOINT_PAGE)
    .then()
        .statusCode(SC_OK)
        .extract()
            .response();

        //    @formatter:on

        PagedDriversResource pagedDriversResource = responseAs(response, PagedDriversResource.class);

        assertThat(pagedDriversResource.getMetadata(), is(nullValue()));

        assertFetchedDrivers(pagedDriversResource.getDrivers(), 2, PAGE_SIZE * 2);
    }

    // TODO find better name for moveByNumber
    private void assertFetchedDrivers(final List<DriverResource> driverResources,
            final Integer numberOfExpectedElements, final Integer moveByNumber) {
        assertThat(driverResources, hasSize(numberOfExpectedElements));
        for (int i = 0; i < numberOfExpectedElements; i++) {
            assertThat(driverResources.get(i).getUuid(),
                    is(((Driver) expectedResultHolder.getResultList().get(i + moveByNumber)).getUuid()
                            .toString()));
            assertThat(driverResources.get(i).getEmail(),
                    is(((Driver) expectedResultHolder.getResultList().get(i + moveByNumber)).getEmail()
                            .getValue()));
        }
    }

    private String fetchUuidForPage(final Integer pageNumber,
            final List<PageIndexResource> pageIndexResources) {
        return pageIndexResources.stream().filter(new Predicate<PageIndexResource>() {
            @Override
            public boolean test(PageIndexResource pageIndexResource) {
                return pageIndexResource.getPageNumber().equals(pageNumber);
            }
        }).findFirst().get().getUuid();
    }

    private List<PageIndexResource> getMetadataForQuery(final String criteriaResourceContainingName) {
        final Response response = given()
                .header(AUTHORIZATION, bearerToken(token))
                .queryParam(FILTER_CRITERIA_RESOURCE_QUERY_PARAM, criteriaResourceContainingName)
                .queryParam(PAGE_SIZE_QUERY_PARAM, PAGE_SIZE)
                .when()
                .get(DRIVERS_QUERY_ENDPOINT);
        PagedDriversResource pagedDriversResource = responseAs(response, PagedDriversResource.class);
        return pagedDriversResource.getMetadata().getPageMetadataList();
    }

}
