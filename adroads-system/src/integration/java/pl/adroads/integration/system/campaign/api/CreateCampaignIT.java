package pl.adroads.integration.system.campaign.api;

import static com.google.common.collect.Lists.newArrayList;
import static com.jayway.restassured.RestAssured.given;
import static java.lang.Boolean.TRUE;
import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;
import static javax.ws.rs.core.HttpHeaders.LOCATION;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static jersey.repackaged.com.google.common.collect.Iterables.getLast;
import static org.apache.http.HttpHeaders.AUTHORIZATION;
import static org.apache.http.HttpStatus.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static pl.adroads.integration.system.util.CommonITFixtures.CAMPAIGN_ENDPOINT;
import static pl.adroads.system.domain.user.model.UserType.ADMIN;

import java.time.ZonedDateTime;
import java.util.Optional;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;

import pl.adroads.api.model.campaign.CreateCampaignRequest;
import pl.adroads.integration.system.IntegtrationTestCommonSteps;
import pl.adroads.system.domain.campaign.CampaignRepository;
import pl.adroads.system.domain.campaign.model.Campaign;
import pl.adroads.system.domain.campaign.model.CampaignId;

import com.jayway.restassured.response.Header;

public class CreateCampaignIT extends IntegtrationTestCommonSteps {

    private static final String CAMPAIGN_NAME = "campaignName";
    private static final String CAMAPIGN_DESC = "camapignDesc";
    private static final ZonedDateTime START_DATE = NOW.plusMonths(1);
    private static final ZonedDateTime END_DATE = NOW.plusMonths(2);
    public static final String NOW_STRING = NOW.format(FORMATTER);
    private static final String END_DATE_STRING = END_DATE.format(FORMATTER);

    private static final String START_DATE_STRING = START_DATE.format(FORMATTER);

    @Inject
    private CampaignRepository campaignRepository;

    @Before
    public void setup() {
        databaseUtil.truncateCampaignTable();
        databaseUtil.truncateUsersTable();

        token = getLoggedInUser(ADMIN);
    }

    @Test
    public void valid_create_campaign_request_results_in_new_campaign_being_created() {

    //    @formatter:off

    CreateCampaignRequest createCampaignRequest = new CreateCampaignRequest( CAMPAIGN_NAME,  CAMAPIGN_DESC,  START_DATE_STRING,  END_DATE_STRING);

    String location = given()
        .header(AUTHORIZATION, bearerToken(token))
        .body(objectSerializer.serialize(createCampaignRequest))
        .header(new Header(CONTENT_TYPE, APPLICATION_JSON))
    .when()
        .post(CAMPAIGN_ENDPOINT)
    .then()
        .statusCode(SC_CREATED)
    .extract()
      .header(LOCATION);

    assertThat(location, is(notNullValue()));
    assertThat(location.matches(CAMPAIGN_ENDPOINT + ".*"), is(TRUE));
    CampaignId campaignId = CampaignId.campaignIdOf(extractStringId(location));

    Optional<Campaign> campaignOptional = campaignRepository.get(campaignId);
    assertThat(campaignOptional.isPresent(), is(TRUE));
    Campaign campaign = campaignOptional.get();
    assertThat(campaign.getName(), is(CAMPAIGN_NAME));
    assertThat(campaign.getDescription(), is(CAMAPIGN_DESC));
    assertThat(campaign.getStartDateTime(), is(START_DATE.toLocalDateTime()));
    assertThat(campaign.getEndDateTime(), is(END_DATE.toLocalDateTime()));

        //    @formatter:on
    }

    @Test
    public void when_invalid_campaign_name_is_provided_then_create_campaign_endpoint_returns_exception() {

    //    @formatter:off

    CreateCampaignRequest createCampaignRequest = new CreateCampaignRequest(null, CAMAPIGN_DESC,  START_DATE_STRING,  END_DATE_STRING);

        given()
        .header(AUTHORIZATION, bearerToken(token))
        .body(objectSerializer.serialize(createCampaignRequest))
        .header(new Header(CONTENT_TYPE, APPLICATION_JSON))
    .when()
        .post(CAMPAIGN_ENDPOINT)
    .then()
        .statusCode(SC_BAD_REQUEST)
        .log();

        //    @formatter:on
    }

    @Test
    public void when_invalid_campaign_description_is_provided_then_create_campaign_endpoint_returns_exception() {

    //    @formatter:off

    CreateCampaignRequest createCampaignRequest = new CreateCampaignRequest(CAMPAIGN_NAME, null,  START_DATE_STRING,  END_DATE_STRING);

        given()
        .header(AUTHORIZATION, bearerToken(token))
        .body(objectSerializer.serialize(createCampaignRequest))
        .header(new Header(CONTENT_TYPE, APPLICATION_JSON))
    .when()
        .post(CAMPAIGN_ENDPOINT)
    .then()
        .statusCode(SC_BAD_REQUEST)
        .log();

        //    @formatter:on
    }

    @Test
    public void when_invalid_campaign_start_date_is_provided_then_create_campaign_endpoint_returns_exception() {

    //    @formatter:off

    CreateCampaignRequest createCampaignRequest = new CreateCampaignRequest(CAMPAIGN_NAME, CAMAPIGN_DESC,  null,  END_DATE_STRING);

        given()
        .header(AUTHORIZATION, bearerToken(token))
        .body(objectSerializer.serialize(createCampaignRequest))
        .header(new Header(CONTENT_TYPE, APPLICATION_JSON))
    .when()
        .post(CAMPAIGN_ENDPOINT)
    .then()
        .statusCode(SC_INTERNAL_SERVER_ERROR)
        .log();

        //    @formatter:on
    }

    // TODO consider error handler for this null pointer exeption in assemblers and other cases here with
    // SC_INTERNAL_SERVER_ERROR
    @Test
    public void when_invalid_campaign_end_date_is_provided_then_create_campaign_endpoint_returns_exception() {

    //    @formatter:off

    CreateCampaignRequest createCampaignRequest = new CreateCampaignRequest(CAMPAIGN_NAME, CAMAPIGN_DESC,  START_DATE_STRING,  null);

        given()
        .header(AUTHORIZATION, bearerToken(token))
        .body(objectSerializer.serialize(createCampaignRequest))
        .header(new Header(CONTENT_TYPE, APPLICATION_JSON))
    .when()
        .post(CAMPAIGN_ENDPOINT)
    .then()
        .statusCode(SC_INTERNAL_SERVER_ERROR)
        .log();

        //    @formatter:on
    }

    @Test
    public void when_campaign_start_date_is_in_the_past_then_create_campaign_endpoint_returns_exception() {

    //    @formatter:off

    CreateCampaignRequest createCampaignRequest = new CreateCampaignRequest(CAMPAIGN_NAME, CAMAPIGN_DESC,   NOW_STRING,  END_DATE_STRING) ;

        given()
        .header(AUTHORIZATION, bearerToken(token))
        .body(objectSerializer.serialize(createCampaignRequest))
        .header(new Header(CONTENT_TYPE, APPLICATION_JSON))
    .when()
        .post(CAMPAIGN_ENDPOINT)
    .then()
        .statusCode(SC_BAD_REQUEST)
        .log();

        //    @formatter:on
    }

    @Test
    public void when_campaign_end_date_is_before_start_date_then_create_campaign_endpoint_returns_exception() {

    //    @formatter:off

    CreateCampaignRequest createCampaignRequest = new CreateCampaignRequest(CAMPAIGN_NAME, CAMAPIGN_DESC,  START_DATE_STRING,  NOW_STRING);

        given()
        .header(AUTHORIZATION, bearerToken(token))
        .body(objectSerializer.serialize(createCampaignRequest))
        .header(new Header(CONTENT_TYPE, APPLICATION_JSON))
    .when()
        .post(CAMPAIGN_ENDPOINT)
    .then()
        .statusCode(SC_BAD_REQUEST)
        .log();

        //    @formatter:on
    }

    private String extractStringId(final String location) {
        return getLast(newArrayList(location.split("/")));
    }
}
