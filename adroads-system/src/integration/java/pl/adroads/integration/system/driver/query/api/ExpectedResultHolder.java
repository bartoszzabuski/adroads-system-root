package pl.adroads.integration.system.driver.query.api;


import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpectedResultHolder<T> {

  private List<T> resultList;

}
