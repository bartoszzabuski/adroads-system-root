package pl.adroads.integration.system.config;

import static org.mockito.Mockito.mock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import pl.adroads.system.application.user.email.EmailService;

@Configuration
@EnableAutoConfiguration
@ComponentScan("pl.adroads")
@Import(IntegrationTestConfig.class)
public class IntegrationTestApplication {

    @Bean
    public EmailService emailService() {
        return mock(EmailService.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(IntegrationTestApplication.class, args);
    }

}
