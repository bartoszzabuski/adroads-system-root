package pl.adroads.integration.system;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.http.ContentType.URLENC;
import static java.lang.String.format;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static pl.adroads.integration.system.util.CommonITFixtures.FORM_PARAM_LOGIN_HEADER;
import static pl.adroads.integration.system.util.CommonITFixtures.FORM_PARAM_PASSWORD_HEADER;
import static pl.adroads.integration.system.util.CommonITFixtures.USERS_LOGIN_ENDPOINT;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.inject.Inject;

import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import pl.adroads.api.model.user.TokenResource;
import pl.adroads.integration.system.config.IntegrationTestApplication;
import pl.adroads.integration.system.util.DatabaseCampaignFixturePopulator;
import pl.adroads.integration.system.util.DatabaseDriverFixturePopulator;
import pl.adroads.integration.system.util.DatabaseUserFixturePopulator;
import pl.adroads.integration.system.util.TestingDatabaseUtilDAO;
import pl.adroads.system.domain.user.model.UserType;
import pl.adroads.system.infrastructure.adapters.driving.user.persistence.model.PersistenceUser;
import pl.adroads.system.infrastructure.adapters.shared.ObjectSerializer;
import pl.adroads.system.infrastructure.adapters.shared.postgre.PostgreCampaignStorage;
import pl.adroads.system.infrastructure.adapters.shared.postgre.PostgreDriverStorage;

import com.jayway.restassured.response.Response;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = IntegrationTestApplication.class)
@WebAppConfiguration
@IntegrationTest("server.port:8090")
@ActiveProfiles("dev")
public class IntegtrationTestCommonSteps {

    protected static final ZonedDateTime NOW = ZonedDateTime.now();

    protected static final DateTimeFormatter FORMATTER = DateTimeFormatter.ISO_INSTANT.withZone(ZoneId.systemDefault());
    public static final String AUTHORIZATION_HEADER_FORMAT = "Bearer %s";


    @Inject
    protected DatabaseUserFixturePopulator userFixturePopulator;

    @Inject
    protected TestingDatabaseUtilDAO databaseUtil;

    @Inject
    protected DatabaseCampaignFixturePopulator databaseCampaignFixturePopulator;

    @Inject
    protected DatabaseDriverFixturePopulator databaseDriverFixturePopulator;

    @Inject
    protected ObjectSerializer objectSerializer;

    @Inject
    protected PostgreCampaignStorage campaignStorage;

    @Inject
    protected PostgreDriverStorage driverStorage;

    protected String token;

    protected <T> T responseAs(final Response response, final Class<T> returnType) {
        return objectSerializer.deserialize(response.getBody().print(), returnType);
    }

    protected <T> T getFirst(final List<T> list) {
        return list.stream().findFirst().get();
    }

    protected String bearerToken(final PersistenceUser user) {
        return format(AUTHORIZATION_HEADER_FORMAT, user.getPersistenceSessionToken().getToken());
    }

    protected String bearerToken(final String token) {
        return format(AUTHORIZATION_HEADER_FORMAT, token);
    }

    protected String getLoggedInUser(final UserType userType) {

        PersistenceUser user;
        if (userType == UserType.ADMIN) {
            user = userFixturePopulator.populateDbWithAdmin();
        } else {
            user = getFirst(userFixturePopulator.populateDbWithVerifiedUsers(1));
        }
        Response response = given()
                .contentType(URLENC)
                .formParam(FORM_PARAM_LOGIN_HEADER, user.getEmail())
                .formParam(FORM_PARAM_PASSWORD_HEADER, user.getPassword())
                .when()
                .post(USERS_LOGIN_ENDPOINT);

        TokenResource token = responseAs(response, TokenResource.class);
        assertThat(token, is(notNullValue()));

        return token.getToken();
    }

    protected String filterCriteria(final String pattern, final String surname) {
        return format(pattern, surname);
    }

}
