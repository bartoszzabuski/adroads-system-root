package pl.adroads.integration.system.campaign.api;

import static com.jayway.restassured.RestAssured.given;
import static java.lang.Boolean.TRUE;
import static java.util.Arrays.asList;
import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static pl.adroads.integration.system.util.CommonITFixtures.CAMPAIGNS_QUERY_ENDPOINT;
import static pl.adroads.system.domain.user.model.UserType.ADMIN;

import java.util.List;

import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;

import pl.adroads.api.model.campaign.CampaignResource;
import pl.adroads.integration.system.IntegtrationTestCommonSteps;
import pl.adroads.system.domain.campaign.CampaignRepository;
import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model.PersistenceCampaign;

import com.jayway.restassured.response.Response;

public class GetCampaignsIT extends IntegtrationTestCommonSteps {

    @Inject
    private CampaignRepository campaignRepository;

    @Before
    public void setup() {
        databaseUtil.truncateCampaignTable();
        databaseUtil.truncateUsersTable();

        token = getLoggedInUser(ADMIN);
    }

    @Test
    public void when_no_campaigns_exist_getCampaigns_endpoint_returns_empty_list() {

    //    @formatter:off

     final Response response = 
             given()
                .header(AUTHORIZATION, bearerToken(token))
             .when()
                .get(CAMPAIGNS_QUERY_ENDPOINT);

        assertThat(response.getStatusCode(), is(SC_OK));
        assertThat(responseAs(response, List.class).size(), is(0));
        //    @formatter:on
    }

    @Test
    public void when_campaigns_exist_getCampaigns_endpoint_returns_valid_list() {

    //    @formatter:off
        
       List<PersistenceCampaign> persistenceCampaigns = databaseCampaignFixturePopulator.populateDbWithCampaigns(2);

     final Response response =
            given()
                .header(AUTHORIZATION, bearerToken(token))
            .when()
                .get(CAMPAIGNS_QUERY_ENDPOINT);

        final List<CampaignResource> queriedCampaigns = asList(responseAs(response, CampaignResource[].class));
        assertThat(response.getStatusCode(), is(SC_OK));
        assertThat(queriedCampaigns.size(), is(persistenceCampaigns.size()));
        assertThat(campaignIdExists(persistenceCampaigns.get(0).getUuid(), queriedCampaigns), is(TRUE));
        assertThat(campaignIdExists(persistenceCampaigns.get(1).getUuid(), queriedCampaigns), is(TRUE));
        //    @formatter:on
    }

    private boolean campaignIdExists(final String uuid, final List<CampaignResource> queriedCampaigns) {
        return queriedCampaigns.stream().anyMatch(queriedCampaign -> queriedCampaign.getUuid().equals(uuid));
    }

}
