package pl.adroads.integration.system.user.api;

import static com.jayway.restassured.RestAssured.given;
import static org.apache.http.HttpHeaders.AUTHORIZATION;
import static org.apache.http.HttpStatus.SC_FORBIDDEN;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static pl.adroads.integration.system.util.CommonITFixtures.CAMPAIGN_NAMES_QUERY_ENDPOINT;
import static pl.adroads.system.domain.user.model.UserType.ADMIN;
import static pl.adroads.system.domain.user.model.UserType.DRIVER;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import pl.adroads.api.model.campaign.CampaignIndexResource;
import pl.adroads.integration.system.IntegtrationTestCommonSteps;
import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model.PersistenceCampaign;

import com.jayway.restassured.response.Response;

public class AuthorizationMechanismIT extends IntegtrationTestCommonSteps {

    @Before
    public void setup() {
        databaseUtil.truncateUsersTable();
        databaseUtil.truncateCampaignTable();
    }

    @Test
    public void when_user_has_no_permission_to_call_endpoint_then_server_throws_permission_denied() {

        String token = getLoggedInUser(DRIVER);

        Response response = given()
                .header(AUTHORIZATION, bearerToken(token))
                .when()
                .get(CAMPAIGN_NAMES_QUERY_ENDPOINT);

        assertThat(response.getStatusCode(), is(SC_FORBIDDEN));
    }

    @Test
    public void when_user_has_permission_to_call_endpoint_then_server_allows_to_perform_operation() {

        String token = getLoggedInUser(ADMIN);
        List<PersistenceCampaign> persistenceCampaigns = databaseCampaignFixturePopulator.populateDbWithCampaigns(2);

        Response response = given()
                .header(AUTHORIZATION, bearerToken(token))
                .when()
                .get(CAMPAIGN_NAMES_QUERY_ENDPOINT);

        assertThat(response.getStatusCode(), is(SC_OK));
        CampaignIndexResource[] campaignIndexResources = responseAs(response, CampaignIndexResource[].class);
        assertThat(campaignIndexResources.length, is(persistenceCampaigns.size()));
    }

}
