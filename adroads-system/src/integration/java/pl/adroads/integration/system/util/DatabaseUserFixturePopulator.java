package pl.adroads.integration.system.util;

import static java.util.stream.Collectors.toList;
import static pl.adroads.integration.system.util.CommonITFixtures.generateStringUUIDs;
import static pl.adroads.system.domain.user.model.VerificationToken.VerificationTokenType.REGISTRATION_CONFIRMATION;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Named;

import pl.adroads.system.domain.user.model.UserType;
import pl.adroads.system.infrastructure.adapters.driving.user.persistence.UserStore;
import pl.adroads.system.infrastructure.adapters.driving.user.persistence.model.PersistenceSessionToken;
import pl.adroads.system.infrastructure.adapters.driving.user.persistence.model.PersistenceUser;
import pl.adroads.system.infrastructure.adapters.driving.user.persistence.model.PersistenceVerificationToken;

@Named
public class DatabaseUserFixturePopulator {

    public static final String ADMIN_EMAIL = "admin_email@email.com";
    public static final String ADMIN_PASSWORD = "password1";
    @Inject
    private UserStore userStore;

    public List<PersistenceUser> populateDbWithVerifiedUsers(final Integer numberOfUsers) {
        List<String> uuidStrings = generateStringUUIDs(numberOfUsers);
        return uuidStrings.stream().map(uuid -> {
            LocalDateTime now = LocalDateTime.now();
            return new PersistenceUser(uuid, "valid_email@email.com",
                    new PersistenceVerificationToken("verificationToken", now.minusDays(1).toString(),
                            REGISTRATION_CONFIRMATION.name(), now.toString(),
                            true, now.minusHours(1).toString()),
                    UserType.DRIVER.name(), "password1",
                    new PersistenceSessionToken("token123", now.toString(), now.plusMinutes(10).toString()));

        }).map((persistenceUser) -> {
            userStore.store(persistenceUser);
            return persistenceUser;
        }).collect(toList());
    }

    public PersistenceUser populateDbWithAdmin() {
        LocalDateTime now = LocalDateTime.now();
        PersistenceUser persistenceUser = new PersistenceUser(UUID.randomUUID().toString(), ADMIN_EMAIL,
                new PersistenceVerificationToken("verificationToken", now.minusDays(1).toString(),
                        REGISTRATION_CONFIRMATION.name(), now.toString(),
                        true, now.minusHours(1).toString()),
                UserType.ADMIN.name(), ADMIN_PASSWORD,
                null);

        userStore.store(persistenceUser);
        return persistenceUser;
    }
}
