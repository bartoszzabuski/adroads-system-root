package pl.adroads.integration.system.util;

import static java.lang.String.format;
import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static pl.adroads.system.domain.driver.model.City.cityOf;
import static pl.adroads.system.domain.driver.model.DistanceMeasurement.distanceMeasurementOf;
import static pl.adroads.system.domain.driver.model.Street.streetOf;
import static pl.adroads.system.domain.driver.model.UnitsOfMeasurement.CM;
import static pl.adroads.system.domain.driver.model.UnitsOfMeasurement.KM;

import java.time.Year;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import pl.adroads.api.model.shared.AddressResource;
import pl.adroads.api.model.shared.CarDetailsResource;
import pl.adroads.api.model.shared.DriverDetailsResource;
import pl.adroads.api.model.shared.WindscreenDimensionsResource;
import pl.adroads.system.domain.driver.model.City;
import pl.adroads.system.domain.driver.model.DistanceMeasurement;
import pl.adroads.system.domain.driver.model.PolishCarBody;
import pl.adroads.system.domain.driver.model.PolishDriverType;
import pl.adroads.system.domain.driver.model.PolishProvince;
import pl.adroads.system.domain.driver.model.Street;
import pl.adroads.system.domain.driver.model.WindscreenDimensions;

import com.jayway.restassured.response.Header;

public abstract class CommonITFixtures {

    private CommonITFixtures() {
    }

    public static final Integer ZERO = 0;
    public static final int ONE = 1;
    public static final int TWO = 2;
    public static final int THREE = 3;

    public static final String FILTER_CRITERIA_RESOURCE_QUERY_PARAM = "filterCriteriaResource";
    public static final String PAGE_SIZE_QUERY_PARAM = "pageSize";
    public static final int PAGE_SIZE = 5;
    public static final String FILTER_CRITERIA_RESOURCE_JSON = "{\"sortDir\":\"ASC\",\"email\":\"bar\"}";
    public static final String HOST = "http://localhost:8090";
    public static final String DRIVERS_ENDPOINT = format("%s/drivers", HOST);
    public static final String DRIVERS_QUERY_ENDPOINT = format("%s/query/drivers", HOST);
    public static final String CAMPAIGN_ENDPOINT = format("%s/campaigns/", HOST);
    public static final String CAMPAIGN_DRIVERS_ENDPOINT = format("%s/campaigns/{campaignId}/drivers/{driverId}", HOST);
    public static final String QUERY_ENDPOINT_PAGE = format("%s/page", DRIVERS_QUERY_ENDPOINT);
    public static final String CAMPAIGNS_QUERY_ENDPOINT = format("%s/query/campaigns", HOST);
    public static final String CAMPAIGN_NAMES_QUERY_ENDPOINT = format("%s/query/campaign-indexes", HOST);
    public static final String USERS_LOGIN_ENDPOINT = format("%s/users/login", HOST);
    public static final String FIRST_DRIVER_JP = "drivers[0]";
    public static final String METADATA_JP = "metadata";
    public static final String FIRST_PAGE_METADATA_JP = "metadata.pageMetadataList[0]";
    public static final String SECOND_PAGE_METADATA_JP = "metadata.pageMetadataList[1]";
    public static final String PAGE_COUNT_JP = "metadata.itemCount";

    public static final String UUID_QUERY_PARAM = "uuid";
    public static final UUID RANDOM_UUID_1 = UUID.randomUUID();
    public static final UUID RANDOM_UUID_2 = UUID.randomUUID();
    public static final UUID RANDOM_UUID_3 = UUID.randomUUID();
    public static final UUID RANDOM_UUID_4 = UUID.randomUUID();
    public static final UUID RANDOM_UUID_5 = UUID.randomUUID();

    public static List<String> generateStringUUIDs(final Integer numberOfCampaigns) {
        List<String> uuids = new ArrayList<>(numberOfCampaigns);
        for (int i = 0; i < numberOfCampaigns; i++) {
            uuids.add(UUID.randomUUID().toString());
        }
        return uuids;
    }

    public static final String MAKE = "Renault";
    public static final String MODEL = "Megane Classic";
    public static final Year YEAR = Year.of(2010);
    public static final Year INVALID_YEAR = Year.of(1700);
    public static final Integer MILEAGE_VALUE = 10000;
    public static final DistanceMeasurement MILEAGE = distanceMeasurementOf(10000, KM);
    public static final Integer INVALID_MILEAGE = -1;
    public static final PolishCarBody BODY = PolishCarBody.SEDAN;
    public static final String COLOUR_VALUE = "zielony";

    public static final Integer WINDSCREEN_HEIGHT_VALUE = 120;
    public static final DistanceMeasurement WINDSCREEN_HEIGHT = distanceMeasurementOf(
            WINDSCREEN_HEIGHT_VALUE, CM);
    public static final Integer WINDSCREEN_WIDTH_VALUE = 150;
    public static final DistanceMeasurement WINDSCREEN_WIDTH = distanceMeasurementOf(
            WINDSCREEN_WIDTH_VALUE, CM);
    public static final WindscreenDimensions WINDSCREEN_DIMENSIONS = WindscreenDimensions
            .windscreenDimensionsOf(WINDSCREEN_HEIGHT_VALUE, WINDSCREEN_WIDTH_VALUE);
    public static final WindscreenDimensionsResource WINDSCREEN_DIMENSIONS_RESOURCE = new WindscreenDimensionsResource(
            WINDSCREEN_HEIGHT_VALUE, WINDSCREEN_WIDTH_VALUE);
    public static final CarDetailsResource CAR_DETAILS_RESOURCE = new CarDetailsResource(MAKE, MODEL,
            YEAR.toString(),
            MILEAGE_VALUE,
            BODY.toString(),
            COLOUR_VALUE,
            WINDSCREEN_DIMENSIONS_RESOURCE);
    public static final Integer MONTHLY_DISTANCE_INDEX = 1;
    public static final PolishDriverType DRIVER_TYPE = PolishDriverType.INNY;
    public static final DriverDetailsResource DRIVER_DETAILS_RESOURCE = new DriverDetailsResource(
            DRIVER_TYPE.getValue(), MONTHLY_DISTANCE_INDEX);
    public static final String POSTCODE_STRING = "54-614";
    public static final String CITY_VALUE = "city";
    public static final City CITY = cityOf(CITY_VALUE);
    public static final String STREET_VALUE = "street";
    public static final Street STREET = streetOf(STREET_VALUE);
    public static final PolishProvince PROVINCE = PolishProvince.DOLNOŚLĄSKIE;

    private static final String INVALID_PROVINCE = "sdfsdfsdfsd";

    public static final AddressResource ADDRESS_RESOURCE = new AddressResource(POSTCODE_STRING,
            CITY_VALUE,
            STREET_VALUE,
            PROVINCE.toString());
    public static final AddressResource INVALID_PROVINCE_ADDRESS_RESOURCE = new AddressResource(POSTCODE_STRING,
            CITY_VALUE,
            STREET_VALUE,
            INVALID_PROVINCE);


    public static final String FORM_PARAM_PASSWORD_HEADER = "password";
    public static final String FORM_PARAM_LOGIN_HEADER = "login";

    public static final Header CONTENT_TYPE_APPLICATION_JSON = new Header(CONTENT_TYPE,
            APPLICATION_JSON);
}
