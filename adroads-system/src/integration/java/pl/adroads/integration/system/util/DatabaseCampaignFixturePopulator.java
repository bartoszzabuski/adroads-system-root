package pl.adroads.integration.system.util;


import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model.PersistenceCampaign;
import pl.adroads.system.infrastructure.adapters.shared.CampaignStorage;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import javax.inject.Inject;
import javax.inject.Named;

import static java.util.stream.Collectors.toList;
import static jersey.repackaged.com.google.common.collect.Sets.newHashSet;
import static pl.adroads.integration.system.util.CommonITFixtures.generateStringUUIDs;
import static pl.adroads.system.domain.campaign.model.CampaignStatus.CREATED;

@Named
public class DatabaseCampaignFixturePopulator {

  @Inject
  private CampaignStorage campaignStore;

  private Random random = new Random();

  public List<PersistenceCampaign> populateDbWithCampaigns(final Integer numberOfCampaigns) {
    List<String> uuidStrings = generateStringUUIDs(numberOfCampaigns);
    return uuidStrings.stream().map(uuid -> {
      Integer randomInt = random.nextInt();
      LocalDateTime startDate = randomStartDate();
      return new PersistenceCampaign(uuid, randomName(randomInt), randomDesc(randomInt),
				     startDate.toString(),
				     randomEndDate(startDate).toString(),
				     CREATED.toString(), newHashSet());
    }).map((persistenceCampaign) -> {
      campaignStore.save(persistenceCampaign);
      return persistenceCampaign;
    }).collect(toList());
  }

  private LocalDateTime randomEndDate(LocalDateTime startDate) {
    return LocalDateTime.parse(startDate.toString()).plusDays(ThreadLocalRandom.current().nextInt(1, 5));
  }

  private LocalDateTime randomStartDate() {
    return LocalDateTime.now().plusDays(ThreadLocalRandom.current().nextInt(1, 5));
  }

  private String randomName(Integer randomInt) {
    return "name_" + randomInt;
  }

  private String randomDesc(Integer randomInt) {
    return "desc_" + randomInt;
  }

}
