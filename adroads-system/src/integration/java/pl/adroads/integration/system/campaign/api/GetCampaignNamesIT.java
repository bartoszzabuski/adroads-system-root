package pl.adroads.integration.system.campaign.api;

import static com.jayway.restassured.RestAssured.given;
import static java.lang.Boolean.TRUE;
import static org.apache.http.HttpHeaders.AUTHORIZATION;
import static org.apache.http.HttpStatus.SC_OK;
import static org.apache.http.HttpStatus.SC_UNAUTHORIZED;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static pl.adroads.integration.system.util.CommonITFixtures.CAMPAIGN_NAMES_QUERY_ENDPOINT;
import static pl.adroads.integration.system.util.CommonITFixtures.ZERO;
import static pl.adroads.system.domain.user.model.UserType.ADMIN;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import pl.adroads.api.model.campaign.CampaignIndexResource;
import pl.adroads.integration.system.IntegtrationTestCommonSteps;
import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model.PersistenceCampaign;

import com.jayway.restassured.response.Response;

public class GetCampaignNamesIT extends IntegtrationTestCommonSteps {

    @Before
    public void setup() {
        databaseUtil.truncateCampaignTable();
        databaseUtil.truncateUsersTable();
    }

    @Test
    public void when_no_session_token_provided_getCampaignNames_returns_401() {

        given()
                .when()
                .get(CAMPAIGN_NAMES_QUERY_ENDPOINT)
                .then()
                .statusCode(SC_UNAUTHORIZED);

    }

    @Test
    public void when_no_campaigns_exist_getCampaignNames_returns_empty_list() {

        String token = getLoggedInUser(ADMIN);

        Response response = given()
                .header(AUTHORIZATION, bearerToken(token))
                .when()
                .get(CAMPAIGN_NAMES_QUERY_ENDPOINT);

        assertThat(response.getStatusCode(), is(SC_OK));
        assertThat(responseAs(response, CampaignIndexResource[].class).length, is(ZERO));
    }

    @Test
    public void when_two_campaigns_exist_getCampaignNames_returns_two_valid_names() {

        String token = getLoggedInUser(ADMIN);
        List<PersistenceCampaign> persistenceCampaigns = databaseCampaignFixturePopulator.populateDbWithCampaigns(2);

        Response response = given()
                .header(AUTHORIZATION, bearerToken(token))
                .when()
                .get(CAMPAIGN_NAMES_QUERY_ENDPOINT);

        assertThat(response.getStatusCode(), is(SC_OK));
        CampaignIndexResource[] campaignIndexResources = responseAs(response, CampaignIndexResource[].class);
        assertThat(campaignIndexResources.length, is(persistenceCampaigns.size()));
        assertThat(containsUuid(persistenceCampaigns.get(0).getUuid(), campaignIndexResources), is(TRUE));
        assertThat(containsUuid(persistenceCampaigns.get(1).getUuid(), campaignIndexResources), is(TRUE));
    }

    private boolean containsUuid(final String uuid, final CampaignIndexResource[] campaignIndexResources) {
        for (CampaignIndexResource campaignIndexResource : campaignIndexResources) {
            if (campaignIndexResource.getId().equals(uuid)) {
                return true;
            }
        }
        return false;
    }

}
