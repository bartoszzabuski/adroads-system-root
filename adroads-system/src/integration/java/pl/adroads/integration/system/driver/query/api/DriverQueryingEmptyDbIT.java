package pl.adroads.integration.system.driver.query.api;

import static com.jayway.restassured.RestAssured.given;
import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static pl.adroads.integration.system.util.CommonITFixtures.DRIVERS_QUERY_ENDPOINT;
import static pl.adroads.integration.system.util.CommonITFixtures.FILTER_CRITERIA_RESOURCE_JSON;
import static pl.adroads.integration.system.util.CommonITFixtures.FILTER_CRITERIA_RESOURCE_QUERY_PARAM;
import static pl.adroads.integration.system.util.CommonITFixtures.FIRST_DRIVER_JP;
import static pl.adroads.integration.system.util.CommonITFixtures.FIRST_PAGE_METADATA_JP;
import static pl.adroads.integration.system.util.CommonITFixtures.METADATA_JP;
import static pl.adroads.integration.system.util.CommonITFixtures.PAGE_COUNT_JP;
import static pl.adroads.integration.system.util.CommonITFixtures.PAGE_SIZE;
import static pl.adroads.integration.system.util.CommonITFixtures.PAGE_SIZE_QUERY_PARAM;
import static pl.adroads.integration.system.util.CommonITFixtures.QUERY_ENDPOINT_PAGE;
import static pl.adroads.integration.system.util.CommonITFixtures.RANDOM_UUID_1;
import static pl.adroads.integration.system.util.CommonITFixtures.UUID_QUERY_PARAM;
import static pl.adroads.integration.system.util.CommonITFixtures.ZERO;
import static pl.adroads.system.domain.user.model.UserType.ADMIN;

import org.junit.Before;
import org.junit.Test;

import pl.adroads.integration.system.IntegtrationTestCommonSteps;

public class DriverQueryingEmptyDbIT extends IntegtrationTestCommonSteps {

    @Before
    public void setup() {
        // databaseUtil.truncateTable("tbl_drivers");
        databaseUtil.truncateDriversTable();
        databaseUtil.truncateUsersTable();

        token = getLoggedInUser(ADMIN);
    }

    @Test
    public void when_no_drivers_found_getFirstPage_returns_empty_response() {

//    @formatter:off

    given()
        .header(AUTHORIZATION, bearerToken(token))
        .queryParam( FILTER_CRITERIA_RESOURCE_QUERY_PARAM,  FILTER_CRITERIA_RESOURCE_JSON)
        .queryParam( PAGE_SIZE_QUERY_PARAM,  PAGE_SIZE)
    .when()
        .get( DRIVERS_QUERY_ENDPOINT)
    .then()
        .statusCode(SC_OK)
        .body( PAGE_COUNT_JP, is(ZERO))
        .body( FIRST_PAGE_METADATA_JP, is(nullValue()))
        .body( FIRST_DRIVER_JP, is(nullValue()));

    //    @formatter:on

    }

    @Test
    public void when_no_drivers_found_getSubsequentPage_returns_empty_response() {

//    @formatter:off

    given()
        .header(AUTHORIZATION, bearerToken(token))
        .queryParam(FILTER_CRITERIA_RESOURCE_QUERY_PARAM,  FILTER_CRITERIA_RESOURCE_JSON)
        .queryParam(PAGE_SIZE_QUERY_PARAM,  PAGE_SIZE)
        .queryParam(UUID_QUERY_PARAM,   RANDOM_UUID_1)
    .when()
        .get( QUERY_ENDPOINT_PAGE)
    .then()
        .statusCode(SC_OK)
        .body( METADATA_JP, is(nullValue()))
        .body( FIRST_DRIVER_JP, is(nullValue()));

    //    @formatter:on

    }

}
