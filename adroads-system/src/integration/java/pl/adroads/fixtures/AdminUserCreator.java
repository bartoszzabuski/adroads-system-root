package pl.adroads.fixtures;

import static pl.adroads.system.domain.driver.model.Email.emailOf;
import static pl.adroads.system.domain.user.model.UserId.userIdOf;
import static pl.adroads.system.domain.user.model.UserType.ADMIN;
import static pl.adroads.system.domain.user.model.UserType.DRIVER;

import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import pl.adroads.integration.system.config.IntegrationTestConfig;
import pl.adroads.system.domain.UUIDGenerator;
import pl.adroads.system.domain.user.UserRepository;
import pl.adroads.system.domain.user.model.User;
import pl.adroads.system.domain.user.model.VerificationToken;

public class AdminUserCreator {

    private final UserRepository userRepository;
    private final UUIDGenerator idGenerator;

    public AdminUserCreator(UserRepository userRepository, UUIDGenerator idGenerator) {
        this.userRepository = userRepository;
        this.idGenerator = idGenerator;
    }

    private void createUser() {
        UUID uuid = idGenerator.generateId();
        VerificationToken verificationToken = new VerificationToken("temp", LocalDateTime.now(),
                LocalDateTime.now().minusYears(100),
                VerificationToken.VerificationTokenType.REGISTRATION_CONFIRMATION, true, null);
        User newUser = new User(userIdOf(uuid), emailOf("driver@gmail.com"),
                verificationToken, DRIVER, "admin");
        userRepository.store(newUser);
    }

    public static void main(String[] args) {
        System.setProperty("spring.profiles.active", "dev");
        ApplicationContext ctx = new AnnotationConfigApplicationContext(IntegrationTestConfig.class);
        UserRepository userRepository = ctx.getBean(UserRepository.class);
        UUIDGenerator idGenerator = ctx.getBean(UUIDGenerator.class);
        new AdminUserCreator(userRepository, idGenerator).createUser();
    }
}
