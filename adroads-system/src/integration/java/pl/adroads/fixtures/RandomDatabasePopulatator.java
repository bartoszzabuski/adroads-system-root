package pl.adroads.fixtures;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import pl.adroads.integration.system.config.IntegrationTestConfig;
import pl.adroads.integration.system.util.TestingDatabaseUtilDAO;
import pl.adroads.system.domain.driver.DriverRepository;
import pl.adroads.system.domain.driver.model.Address;
import pl.adroads.system.domain.driver.model.CarDetails;
import pl.adroads.system.domain.driver.model.City;
import pl.adroads.system.domain.driver.model.Colour;
import pl.adroads.system.domain.driver.model.Driver;
import pl.adroads.system.domain.driver.model.DriverDetails;
import pl.adroads.system.domain.driver.model.DriverId;
import pl.adroads.system.domain.driver.model.PolishCarBody;
import pl.adroads.system.domain.driver.model.PolishDriverType;
import pl.adroads.system.domain.driver.model.PolishProvince;
import pl.adroads.system.domain.driver.model.Postcode;
import pl.adroads.system.domain.driver.model.Status;
import pl.adroads.system.domain.driver.model.Street;
import pl.adroads.system.domain.driver.model.WindscreenDimensions;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;
import java.time.ZoneId;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

import static com.google.common.collect.Lists.newArrayList;
import static java.lang.String.format;
import static java.time.Month.JANUARY;
import static java.time.ZoneId.systemDefault;
import static java.util.Arrays.asList;
import static pl.adroads.system.domain.driver.model.Email.emailOf;

public class RandomDatabasePopulatator {

  private static final ZoneId TIMEZONE = systemDefault();
  private static final List<String> STREETS = newArrayList("Senatorska", "Trawowa",
							   "powstańców śląskich");
  private static final List<String> CITIES = newArrayList("Wrocław", "Wałbrzych", "Zadupie Małe");
  private static final List<String> MAKES = newArrayList("Renault", "Opel", "Polonez");
  private static final List<String> COLOURS = newArrayList("zielony", "żółty", "czerwony");

  private final long START_YEAR = LocalDate.of(1940, JANUARY, 1).atStartOfDay(systemDefault())
      .toEpochSecond();

  private final long END_YEAR = LocalDate.of(1995, JANUARY, 1).atStartOfDay(TIMEZONE)
      .toEpochSecond();

  private static final int START = 0;
  private static final int END = 100;
  private static final List<String> NAMES = newArrayList("Slawek", "Bartek", "Grzegorz");
  private static final List<String> SURNAMES = newArrayList("Kowalski", "Brzęczyszczykiewicz");

  private DriverRepository driverRepository;

  public RandomDatabasePopulatator(DriverRepository driverRepository) {
    this.driverRepository = driverRepository;
  }

  public static void  main(String[] args) {
    System.setProperty("spring.profiles.active", "dev");
    ApplicationContext ctx = new AnnotationConfigApplicationContext(IntegrationTestConfig.class);
    TestingDatabaseUtilDAO databaseUtil = ctx.getBean(TestingDatabaseUtilDAO.class);
    DriverRepository driverRepository = ctx.getBean(DriverRepository.class);
    databaseUtil.truncateDriversTable();
    new RandomDatabasePopulatator(driverRepository).populateDBWithRandomFixture();
  }

  public void populateDBWithRandomFixture() {
    for (int i = START; i < END; i++) {

      final String surname = getSurnameWith(i);
      final String name = getNameWith(i);
      final String email = format("%s.%s@test.com", name, surname);

      Driver driver = new Driver.DriverBuilder().withName(name).withSurname(surname)
	  .withDateOfBirth(getRandomDate()).withEmail(emailOf(email))
	  .withAddress(getRandomAddress(i)).withStatus(getRandomStatus())
	  .withCarDetails(getRandomCarDetails(i)).withDriverDetails(randomDriversDetails()).build();

      driver.assignID(DriverId.of(UUID.randomUUID()));
      driverRepository.storeDriver(driver);
    }
  }

  private Status getRandomStatus() {
    return getRandomFrom(asList(Status.values()));
  }

  private DriverDetails randomDriversDetails() {
    return DriverDetails.of(getRandomFrom(asList(PolishDriverType.values())),
			    ThreadLocalRandom.current().nextInt(1, 5));
  }

  private CarDetails getRandomCarDetails(final int i) {
    return CarDetails.of(getRandomMake(), getRandomModel(i), randomYear(), randomMileage(),
			 randomPolishCarBody(), randomColour(), randomWindscreen());
  }

  private WindscreenDimensions randomWindscreen() {
    return WindscreenDimensions.windscreenDimensionsOf(random3DigitNumber(), random3DigitNumber());
  }

  private Integer random3DigitNumber() {
    return ThreadLocalRandom.current().nextInt(100, 200);
  }

  private Colour randomColour() {
    return Colour.colourOf(getRandomFrom(COLOURS));
  }

  private PolishCarBody randomPolishCarBody() {
    return getRandomFrom(asList(PolishCarBody.values()));
  }

  private Integer randomMileage() {
    return ThreadLocalRandom.current().nextInt(1000, 100000);
  }

  private Year randomYear() {
    return Year.now().minusYears((long) randomDigit());
  }

  private String getRandomModel(final int i) {
    return "car model " + i;
  }

  private String getRandomMake() {
    return getRandomFrom(MAKES);
  }

  private Address getRandomAddress(final int i) {
    return Address
	.of(getRandomPostcode(), getRandomStreet(i), getRandomCity(), getRandomProvince());
  }

  private PolishProvince getRandomProvince() {
    return getRandomFrom(asList(PolishProvince.values()));
  }

  private City getRandomCity() {
    return City.cityOf(getRandomFrom(CITIES).toString());
  }

  private Postcode getRandomPostcode() {
    return Postcode.postcodeOf(getRandomPostcodeValue());
  }

  private String getRandomPostcodeValue() {
    return format("%d%d-%d%d%d", randomDigit(), randomDigit(), randomDigit(), randomDigit(),
		  randomDigit());
  }

  private int randomDigit() {
    return ThreadLocalRandom.current().nextInt(9);
  }

  private Street getRandomStreet(final int i) {
    return Street.streetOf(getRandomFrom(STREETS) + i);
  }

  private String getSurnameWith(int i) {
    return getRandomFrom(SURNAMES) + i;
  }

  private String getNameWith(final int i) {
    return getRandomFrom(NAMES) + i;
  }

  private LocalDate getRandomDate() {
    long diff = END_YEAR - START_YEAR + 1;
    Instant instant = Instant.ofEpochSecond(START_YEAR + (long) (Math.random() * diff));
    return LocalDateTime.ofInstant(instant, TIMEZONE).toLocalDate();
  }

  private void print(final Driver driver) {
    System.out.println(driver.getUuid().getValue());
    System.out.println(driver.getName());
    System.out.println(driver.getSurname());
    System.out.println(driver.getDateOfBirth());
    System.out.println(driver.getEmail().getValue());
    System.out.println(driver.getStatus());
    print(driver.getAddress());
    print(driver.getDriverDetails());
    print(driver.getCarDetails());
    System.out.println("------------------------------------");

  }

  private void print(final CarDetails carDetails) {
    System.out.println("CarDetails");
    System.out.println("  " + carDetails.getMake());
    System.out.println("  " + carDetails.getModel());
    System.out.println("  " + carDetails.getMileage().getValue());
    System.out.println("  " + carDetails.getBody());
    System.out.println("  " + carDetails.getYear());
    System.out.println("  " + carDetails.getColour().getValue());
    System.out.println("  " + carDetails.getWindscreenDimensions().getHeight().getValue());
    System.out.println("  " + carDetails.getWindscreenDimensions().getWidth().getValue());
  }

  private void print(final DriverDetails driverDetails) {
    System.out.println("DriverDetails");
    System.out.println("  " + driverDetails.getMonthlyDistanceIndex());
    System.out.println("  " + driverDetails.getType());
  }

  private void print(final Address address) {
    System.out.println("Address");
    System.out.println("  " + address.getCity().getValue());
    System.out.println("  " + address.getStreet().getValue());
    System.out.println("  " + address.getProvince());
    System.out.println("  " + address.getPostcode().getValue());
  }


  private <T> T getRandomFrom(final List<T> list) {
    return list.get(ThreadLocalRandom.current().nextInt(list.size()));
  }

}
