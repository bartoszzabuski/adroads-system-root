package pl.adroads.fixtures;

import static pl.adroads.system.domain.campaign.model.CampaignStatus.CREATED;

import java.time.LocalDateTime;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import org.mockito.internal.util.collections.Sets;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import pl.adroads.integration.system.config.IntegrationTestConfig;
import pl.adroads.integration.system.util.TestingDatabaseUtilDAO;
import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model.PersistenceCampaign;
import pl.adroads.system.infrastructure.adapters.driving.campaign.persistence.model.PersistenceCampaignDriver;
import pl.adroads.system.infrastructure.adapters.shared.CampaignStorage;

public class RandomCampaignPopulatator {

    private static final int START = 0;
    private static final int END = 100;

    private CampaignStorage campaignStorage;
    private Random random = new Random();

    public RandomCampaignPopulatator(CampaignStorage campaignStorage) {
        this.campaignStorage = campaignStorage;
    }

    public static void main(String[] args) {
        System.setProperty("spring.profiles.active", "dev");
        ApplicationContext ctx = new AnnotationConfigApplicationContext(IntegrationTestConfig.class);
        TestingDatabaseUtilDAO databaseUtil = ctx.getBean(TestingDatabaseUtilDAO.class);
        CampaignStorage campaignStorage = ctx.getBean(CampaignStorage.class);
        databaseUtil.truncateCampaignTable();
        new RandomCampaignPopulatator(campaignStorage).populateDBWithRandomFixture();
    }

    public void populateDBWithRandomFixture() {
        for (int i = START; i < END; i++) {

            String uuid = UUID.randomUUID().toString();
            String name = "name" + i;
            String description = "description" + i;
            LocalDateTime startDate = LocalDateTime.of(2015, 1, 1, 1, 1).plusMonths(random.nextInt(24)).plusDays(31);
            String startDateString = startDate.toString();
            String endDateString = startDate.plusDays(31).toString();
            String campaignStatus = CREATED.toString();
            Set<PersistenceCampaignDriver> drivers = Sets.newSet();

            PersistenceCampaign persistenceCampaign = new PersistenceCampaign(uuid, name, description, startDateString,
                    endDateString, campaignStatus, drivers);
            campaignStorage.save(persistenceCampaign);
        }
    }
}
